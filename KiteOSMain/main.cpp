// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.

#include <cstdint>
#include "cmsis_core/Include/cmsis_gcc.h"
#include "hal/hal.hpp"
#include "utils/debug_utils.h"
#include "core/core.hpp"
#include "lwip/init.h"


void main() {
  // Enable interrupts if disabled (this is the case if the program was started by the bootloader).
  if (__get_PRIMASK()) {
    __enable_irq();
  }

  ASSERT_RESPONSE(hal::System::Init(), true);
  lwip_init();  // Requires HAL to be initialized
  Core::Start();  // Requires lwip to be initialized
}
