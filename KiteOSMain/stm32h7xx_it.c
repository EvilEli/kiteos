/**
  ******************************************************************************
  * @file    stm32h7xx_it.c
  * @author  Ac6
  * @version V1.0
  * @date    02-Feb-2015
  * @brief   Default Interrupt Service Routines.
  ******************************************************************************
*/
/******************************************************************************/
/*            	  	    Processor Exceptions Handlers                         */
/******************************************************************************/
void HardFault_Handler(void) {
  /* Go to infinite loop when Hard Fault exception occurs */
  while (1) {
  }
}
