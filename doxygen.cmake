cmake_minimum_required(VERSION 3.13)

#-------------------------------------------------------------------------------
# Custom commands for docu generation
add_custom_target(doxygen
    COMMENT "Generating Doxygen API documentation and Doxygen coverage"
    COMMAND mkdir -p ${DOC_ROOT}/build/html
    COMMAND doxygen ${DOC_ROOT}/Doxyfile > ${DOC_ROOT}/KiteOS_doxygen.log 2>&1
    COMMAND python3 -m coverxygen --xml-dir ${DOC_ROOT}/build/html/doxy_output/xml --src-dir ../ --output ${DOC_ROOT}/build/html/doxy_output/doc-coverage.info --kind function,enum,typedef,class,struct,define,variable
    COMMAND genhtml -q --no-function-coverage --no-branch-coverage ${DOC_ROOT}/build/html/doxy_output/doc-coverage.info -o ${DOC_ROOT}/build/html/doxy_output/coverage
    COMMAND lcov --summary ${DOC_ROOT}/build/html/doxy_output/doc-coverage.info
    COMMAND bash -c "if [[ `lcov --summary ${DOC_ROOT}/build/html/doxy_output/doc-coverage.info 2>&1 | grep -oP 'lines......:\\s*\\K[0-9]+'` -lt 100 ]]; then echo 'ERROR: Coverage is below 100 %'; exit 1 ; fi"
    WORKING_DIRECTORY ${DOC_ROOT}
    VERBATIM
    )

#-------------------------------------------------------------------------------
# Custom commands for docu generation
add_custom_target(doc
    DEPENDS doxygen
    COMMENT "Generating Documentation"
    COMMAND mkdir -p ${DOC_ROOT}/build
    COMMAND make -C ${DOC_ROOT} html
    WORKING_DIRECTORY ${DOC_ROOT}
    VERBATIM
    )
