cmake_minimum_required(VERSION 3.13)

#-------------------------------------------------------------------------------
# Configure the compiler for the target machine
set (CMAKE_TRY_COMPILE_TARGET_TYPE STATIC_LIBRARY)

# Define the compiler tools (arm eabi for embedded)
set (CMAKE_C_COMPILER gcc)
set (CMAKE_CXX_COMPILER g++)
set (OBJCOPY objcopy)

#-------------------------------------------------------------------------------
# Custom symbols that configure build properties
set (ARCH_FLAGS "-m32")

# Custom symbols that configure build properties
set(CONFIG_FLAGS "-DSTM32H743xx \
                  -DZCM_EMBEDDED \
                  -DETH_DESC_BASE_ADDR=0 \
                  -DETH_RAM_HEAP_ADDR=0 \
                  -DETH_RAM_HEAP_SIZE=0 \
                  -DBOOTLOADER_ADDR=0 \
                  -DBOOTLOADER_SIZE=0 \
                  -DFIRMWARE_ADDR=0 \
                  -DFIRMWARE_SIZE=0 \
                  -DETH_RX_DESC_CNT=0 \
                  -DETH_TX_DESC_CNT=0 \
                  -DETH_BUF_SIZE=0")
