// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#ifndef SRC_DRIVERS_KSZ8563R_HPP_
#define SRC_DRIVERS_KSZ8563R_HPP_

#include "hal/gpio.hpp"
#include "hal/i2c/slave.hpp"
#include "hal/i2c/definitions.hpp"
#include "core/software_timer.hpp"

/*!
 *  \brief Implements a driver to access the KSZ8563R Ethernet switch I2C.
 */
class Ksz8563R {
 public:
  /*!
   * \brief Constructor of the KSZ8563R driver.
   */
  Ksz8563R();

  /*!
   * \brief Destructor of the KSZ8563R driver.
   */
  ~Ksz8563R() = default;

 private:
  //! Possible states of the KSZ8563R driver.
  enum State {
    kStateReset,
    kStateWaitAfterReset,
    kStateReadingChipId0Register,
    kStateReadingChipId4Register,
    kStateReadingGlobalPtpClockControlRegister,
    kStateEnablingPtpClock,
    kStateReadingGlobalPtpMessageConfig1Register,
    kStateEnablingPtpMode,
    kStateIdle
  };

  //! Contains the state machine of the driver and is called by the I2C and timer callbacks.
  void Run();
  //! Writes a register value to a KSZ8563R register.
  void WriteRegister(uint16_t register_address, uint8_t register_value);
  //! Reads a register value from a KSZ8563R register.
  void ReadRegister(uint16_t register_address, uint32_t read_length);

  //! Callback called when an I2C transceive operation finished.
  static void I2CCallback(hal::i2c::Error error, void *instance);
  //! Callback called when the reset timer elapsed.
  static void TimerCallback(Ksz8563R *instance);

  State state_;  //!< State of the KSZ8563R driver.

  hal::Gpio reset_pin_;  //!< Reset pin of the KSZ8563R.

  hal::i2c::Slave i2c_slave_;   //!< I2C slave instance handling the I2C communication.
  uint8_t tx_buffer_[3] = {0};  //!< TX buffer used for I2C transmissions.
  uint8_t rx_buffer_[3] = {0};  //!< RX buffer used for I2C receptions.

  SoftwareTimer reset_timer_;  //!< Timer used to generate reset signal.

  uint32_t resends_ = 0;  //!< Number of resends executed for the current command.
  uint32_t configuration_attempts_ = 0;  //!< Number of attempts executed to configure a register.
  //! Number of bytes read by the current command (0 for write).
  uint32_t current_read_length_ = 0;
};

#endif  // SRC_DRIVERS_KSZ8563R_HPP_
