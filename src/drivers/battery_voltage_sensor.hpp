// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#ifndef SRC_DRIVERS_BATTERY_VOLTAGE_SENSOR_HPP_
#define SRC_DRIVERS_BATTERY_VOLTAGE_SENSOR_HPP_

#include <cstdint>
#include "hal/adc.hpp"
#include "core/software_timer.hpp"

/*!
 *  \brief  Implements the BatteryVoltageSensor driver.
 *  \details  This class measures the inputs of the "MAX337" Mux differentially
 *            by cycling the through the inputs and measuring the output of the BatteryVoltageSensor
 *            which is connected to the output of the mux.
 *            The Mux uses 3 address bytes to select the input.
 *            The voltages are returned in mV all at once after each measurement in an array.
 *            The amount of channels and the readout frequency are configurable.
 */
template<std::size_t N>
class BatteryVoltageSensor {
 public:
  //!< callback that is called if all channels are finished reading.
  typedef void (*DataReadyCallback)(double voltage_channels_[N], void *instance);

  /*!
   * \brief  Constructor of the BatteryVoltageSensor-Component.
   *
   * \param  max_channel  Number of channels to be read starting from 0.
   * \param  cb  callback that is called if all channels are finished reading.
   * \param  cb_arg  instance pointer.
   *
   * \return Returns constructed BatteryVoltageSensor-instance.
   */
  explicit BatteryVoltageSensor(DataReadyCallback cb, void *cb_arg);

   /*!
   * \brief Destructor of the BatteryVoltageSensor-Component.
   */
  ~BatteryVoltageSensor();

 private:
  /*!
  * \brief  Start a new channel read sequence.
  */
  static void StartChannelRead(BatteryVoltageSensor* instance);
  //! Read all channels (up to selected max channel), stores result in data parameter on success.
  static void ReadNextChannel(BatteryVoltageSensor *instance);
  //! Returns if the given val has a 1 at the given position in binary representation.
  bool IsBitSet(uint8_t val, uint8_t pos);
  //! Callback called when the adc finished conversion.
  static void AdcCallback(double current_adc_val, void *cb_arg);

  hal::Adc adc_ina_;       //!< ADC that measures the output of the mux.
  hal::Gpio gpio_max_en_;  //!< GPIO that enables the mux.
  hal::Gpio gpio_max_a0_;  //!< GPIO that sets the 0th address bit of the mux input.
  hal::Gpio gpio_max_a1_;  //!< GPIO that sets the 1th address bit of the mux input.
  hal::Gpio gpio_max_a2_;  //!< GPIO that sets the 2th address bit of the mux input.

  uint8_t current_channel_;  //!< Input buffer used for i2c-communication.
  DataReadyCallback data_ready_callback_;  //!< instance of DataReadyCallback
  void* cb_arg_;  //!< Argument provided to callback.

  double voltage_channels_[N];  //!< Array of voltages in V.
  SoftwareTimer readout_timer_;  //!< Timer that starts reading all channels.
};

#endif  // SRC_DRIVERS_BATTERY_VOLTAGE_SENSOR_HPP_
