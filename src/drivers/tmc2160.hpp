// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#ifndef SRC_DRIVERS_TMC2160_HPP_
#define SRC_DRIVERS_TMC2160_HPP_

#include "hal/spi/slave.hpp"
#include "hal/pwm.hpp"
#include "hal/gpio.hpp"
#include "core/software_timer.hpp"

//! \brief Implements a driver to access the TMC2160 stepper motor driver via SPI.
class Tmc2160 {
 public:
  //! Typedef of function pointer used as data callback.
  typedef void (*MeasurementDataCallbackFunction)(float current_speed, float current_acceleration,
                                                  void *callback_argument);

  /*!
   * \brief Constructor of the TMC2160 component.
   *
   * \param steps_per_rev              Steps per rev of the stepper motor driven by the TMC2160.
   * \param measurement_data_callback  Callback called when measurement data is available.
   * \param callback_argument           Argument passed to the measurement callback.
   */
  Tmc2160(uint32_t steps_per_rev, MeasurementDataCallbackFunction measurement_data_callback,
          void *callback_argument);

  /*!
   * \brief Destructor of the TMC2160 component.
   */
  ~Tmc2160() = default;

  /*!
   * \brief  Sets the stepper motor speed.
   *
   * \param  speed  Target speed of the stepper motor.
   *
   * \return True on success, false if the stepper motor driver is not yet initialized.
   */
  bool SetSpeed(float speed);

  /*!
   * \brief  Sets the stepper motor acceleration.
   *
   * \param  acceleration  Target acceleration of the stepper motor.
   *
   * \return True on success, false if the stepper motor driver is not yet initialized.
   */
  bool SetAcceleration(float acceleration);

 private:
  //! Possible states of the TMC2160 driver.
  enum State {
    kStateConfig,
    kStateIdle,
    kStateSpeedSetpointMode,
    kStateAccelerationSetpointMode,
  };

  void WriteRegister(uint8_t register_address, uint32_t value);  //!< Writes a TMC2160 register.
  void ReadRegister(uint8_t register_address);  //!< Reads a TMC2160 register.

  //! Callback called when an SPI transceive operation finished.
  static void SpiCallback(void *instance);
  //! Callback called when the acceleration timer elapsed.
  static void AccererationTimerCallback(Tmc2160* instance);

  const uint32_t steps_per_rev_;  //!< Steps per rev of the driven stepper motor.
  //! Callback called when measurement data is available.
  const MeasurementDataCallbackFunction measurement_data_callback_;
  void *callback_argument_;  //!< Argument passed to the measurement callback.

  hal::Gpio spi_mode_pin_;      //!< GPIO connected to the SPI_MODE pin for the TMC2160.
  hal::spi::Slave spi_slave_;   //!< SPI slave handling the communication with the TMC2160.
  uint8_t tx_buffer_[5] = {0};  //!< TX buffer used for SPI transmissions.
  uint8_t rx_buffer_[5] = {0};  //!< RX buffer used for SPI receptions.

  hal::Pwm pwm_;                      //!< PWM driving the STEP pin of the TMC2160.
  hal::Gpio direction_pin_;           //!< GPIO connected to the DIR pin for the TMC2160.
  SoftwareTimer acceleration_timer_;  //!< Timer used for accelerating the stepper motor smoothly.
  hal::Gpio driver_enable_pin_;       //!< GPIO connected to the DR_ENN pin for the TMC2160.
  hal::Gpio dcstep_enable_pin_;       //!< GPIO connected to the DCEN pin for the TMC2160.
  hal::Gpio dcin_pin_;                //!< GPIO connected to the DCIN pin for the TMC2160.

  float speed_setpoint_ = 0;         //!< Target angular velocity in rad/s.
  float acceleration_setpoint_ = 0;  //!< Target angular acceleration in rad/s^2.
  float current_speed_ = 0;          //!< Current angular velocity in rad/s.
  float current_acceleration_ = 0;   //!< Current angular acceleration in rad/s^2.

  State state_;  //!< Current state of the TMC2160 driver.
};

#endif  // SRC_DRIVERS_TMC2160_HPP_
