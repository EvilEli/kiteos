// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#include <cstring>
#include "core_temperature.hpp"

/**************************************************************************************************
 *     Public methods                                                                             *
 **************************************************************************************************/
CoreTemperature::CoreTemperature()
  : temperature_adc_(hal::Adc::kChannelTemperatureSensor) {
  update_timer_.SetTimeoutPeriodic(100, TimerElapsedCallback, this);
  }

CoreTemperature::~CoreTemperature() = default;

double CoreTemperature::GetValue() {
  return temperature_;
}
/**************************************************************************************************
 *     Private methods                                                                            *
 **************************************************************************************************/
void CoreTemperature::TimerElapsedCallback(CoreTemperature *inst) {
  inst->temperature_adc_.StartConversion([](double last_val, void* cb_arg) {
    reinterpret_cast<CoreTemperature *>(cb_arg)->temperature_ = last_val;}, inst);
}
