// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#ifndef SRC_DRIVERS_AK09916_HPP_
#define SRC_DRIVERS_AK09916_HPP_

#include "core/software_timer.hpp"
#include "hal/i2c/slave.hpp"

/*!
 *  \brief  Implements the Ak09916 driver.
 *  \details  This class reads Ak09916 magnetometer data
 */
class Ak09916 {
 public:
  //! Available measurement frequencies.
  enum MeasurementFrequency {
    kMeasurementFrequency10Hz = 10,   //!< Continuous Measurement Mode 1.
    kMeasurementFrequency20Hz = 20,   //!< Continuous Measurement Mode 2.
    kMeasurementFrequency50Hz = 50,   //!< Continuous Measurement Mode 3.
    kMeasurementFrequency100Hz = 100   //!< Continuous Measurement Mode 4.
  };
  //! Struct used for communication with the plugin that calls the driver.
  struct MeasurementData {
    double magnetic_flux_density[3] = {0.0};  //!< MagneticFluxDensity of the 3 axis.
    uint64_t timestamp = 0;   //!< Timestamp when the set point data was measured.
  };
  //! Callback that is called if measurements are finished reading.
  typedef void (*DataReadyCallback)(MeasurementData measurement_data, void *ctx);

  /*!
   * \brief  Constructor of the Ak09916-Component.
   *
   * \param  cb  callback that is called if all channels are finished reading.
   * \param  ctx  instance pointer.
   * \return Returns constructed Ak09916-instance.
   */
  Ak09916(MeasurementFrequency frequency,  Ak09916::DataReadyCallback cb, void *ctx);

   /*!
   * \brief Destructor of the Ak09916-Component.
   */
  ~Ak09916() = default;

 private:
  //! Define update rate in ms.
  static const uint8_t measurement_period_[];

  //! send a request to the INA to contiuously measure all channels.
  void ConfigureContinuousConversion(MeasurementFrequency measurement_frequency);

  //! Start a new channel read sequence.
  static void StartSensorReadout(Ak09916* inst);
  //! Invokes the user callback to report new data
  static void InvokeCallbackTask(Ak09916 *inst);
  //! Callback called when an I2C transmission is finished.
  static void I2cCallback(hal::i2c::Error error, void *cb_arg);

  hal::i2c::Slave slave_;  //!< Slave used for I2C transmissions
  DataReadyCallback data_ready_callback_;  //!< instance of DataReadyCallback
  void* cb_arg_;                 //!< Argument provided to callback.
  uint8_t tx_buffer_[2];       //!< Output buffer used for i2c-communication.
  uint8_t rx_buffer_[8];    //!< Input buffer used for i2c-communication.
  //! Structure holding the measurement data which is passed to the data callback.
  MeasurementData measurement_data_;
  SoftwareTimer readout_timer_;  //!< Timer that starts reading all channels.
};

#endif  // SRC_DRIVERS_AK09916_HPP_
