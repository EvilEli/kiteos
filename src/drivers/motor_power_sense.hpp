// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#ifndef SRC_DRIVERS_MOTOR_POWER_SENSE_HPP_
#define SRC_DRIVERS_MOTOR_POWER_SENSE_HPP_

#include <cstdint>
#include "hal/gpio.hpp"

/*!
 *  \brief Implements a driver to easily access the MotorPowerSense.
 *         The driver uses a hardware timer which triggers a GPIO if the Set()
 *         method is not called for a specific period.
 */
class MotorPowerSense {
 public:
  //! \brief Constructor of the MotorPowerSense driver component.
  MotorPowerSense();

  //! \brief Destructor of the MotorPowerSense driver component.
  ~MotorPowerSense();

  //! \brief Returns value for MTRON signal.
  bool GetMtronSenseSignal() { return mtron_sense_.Read();}
  //! \brief Returns value for Button signal.
  bool GetButtonSenseSignal() { return button_sense_.Read();}
 private:
  hal::Gpio mtron_sense_;  //!< Gpio instance connected to MTRON signal.
  hal::Gpio button_sense_;  //!< Gpio instance connected to BUTTON signal.
};

#endif  // SRC_DRIVERS_MOTOR_POWER_SENSE_HPP_
