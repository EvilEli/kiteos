// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#ifndef SRC_DRIVERS_INA226_HPP_
#define SRC_DRIVERS_INA226_HPP_

#include "core/software_timer.hpp"
#include "hal/i2c/slave.hpp"

/*!
 *  \brief  Implements the Ina226 driver.
 *  \details  This class reads Ina226 shunt voltage
    * OVERVIEW: Measures Voltage on an input
    * INPUTS: IN1 - filtered battery voltage, IN2 - STACK right-top, IN3 - STACK left-top
    * WRITE: 8bit(ADDR+W) 8bit(REGISTER) 16bit(DATA)
    * READ:  8bit(ADDR+R) 8bit(REGISTER)
    * BYTEORDER: MSByte first
 */
class Ina226 {
 public:
  //!< callback that is called if all channels are finished reading.
  typedef void (*DataReadyCallback)(double shunt_voltage, void *ctx);

  /*!
   * \brief  Constructor of the Ina226-Component.
   *
   * \param  cb  callback that is called if all channels are finished reading.
   * \param  ctx  instance pointer.
   * \return Returns constructed Ina226-instance.
   */
  Ina226(Ina226::DataReadyCallback cb, void *ctx);

   /*!
   * \brief Destructor of the Ina226-Component.
   */
  ~Ina226() = default;

 private:
  //! send a request to the INA to contiuously measure all channels.
  void StartContinuousConversion();

  //! Start a new channel read sequence.
  static void StartChannelRead(Ina226* inst);
  //! Invokes the user callback to report new data
  static void InvokeCallbackTask(Ina226 *inst);
  //! Callback called when an I2C transmission is finished.
  static void I2cCallback(hal::i2c::Error error, void *cb_arg);

  hal::i2c::Slave slave_;  //!< Slave used for I2C transmissions
  DataReadyCallback data_ready_callback_;  //!< instance of DataReadyCallback
  void* cb_arg_;                 //!< Argument provided to callback.
  uint8_t send_buffer_[3];       //!< Output buffer used for i2c-communication.
  uint8_t receive_buffer_[2];    //!< Input buffer used for i2c-communication.
  double shunt_voltage_;         //!< Latest measurement of shunt voltage.
  SoftwareTimer readout_timer_;  //!< Timer that starts reading all channels.
};

#endif  // SRC_DRIVERS_INA226_HPP_
