// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#include <cstring>
#include "failsafe_switch.hpp"
#include "core/logger.hpp"
#include "core/system_time/system_time.hpp"

#define FAILSAFE_TIMER hal::Timer::kDevice4
#define FAILSAFE_OUTPUT_PIN hal::Gpio::kPin11
#define FAILSAFE_OUTPUT_PORT hal::Gpio::kPortC

#define FAILSAFE_TIMEOUT 50  // Time after which a failsafe condition is detected in ms.

/**************************************************************************************************
 *     Public methods                                                                             *
 **************************************************************************************************/
FailsafeSwitch::FailsafeSwitch()
  : timeout_timer_{FAILSAFE_TIMER},
    output_pin_{FAILSAFE_OUTPUT_PORT,
                FAILSAFE_OUTPUT_PIN,
                hal::Gpio::kModeOutput} {
  // Start failsafe timer
  timeout_timer_.PeriodicCallback(FAILSAFE_TIMEOUT, FailsafeCallback, this);
}

FailsafeSwitch::~FailsafeSwitch() = default;

void FailsafeSwitch::Set(bool value) {
  timeout_timer_.SetValue(0);
  output_pin_.Write(value);
  if (!switch_state_ && value) {
    Logger::Report("FAILSAFE_SWITCH - powering up.", Logger::kInfo);
  } else if (switch_state_ && !value) {
    Logger::Report("FAILSAFE_SWITCH - shutting down.", Logger::kInfo);
  }
  switch_state_ = value;
}
/**************************************************************************************************
 *     Private methods                                                                            *
 **************************************************************************************************/
void FailsafeSwitch::FailsafeCallback(void *context) {
  auto* inst = reinterpret_cast<FailsafeSwitch*>(context);
  inst->output_pin_.Write(false);
  if (inst->publish_timer_.Elapsed()) {
    inst->publish_timer_.SetTimeout(1000);
    Logger::Report("FAILSAFE_SWITCH - timed out.", Logger::kInfo);
  }
}
