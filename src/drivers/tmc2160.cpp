// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#include "tmc2160.hpp"
#include <algorithm>
#include <cmath>
#include "utils//debug_utils.h"
#include "hal/rcc.hpp"

#define TMC2160_STEP_PWM_PIN hal::Pwm::Pin::kPinB1
#define TMC2160_MAX_SPEED 100  // in rad/s
#define TMC2160_MAX_ACCELERATION 50  // in rad/s^2

#define TMC2160_REGISTER_GCONF 0x00

// SDO_CFG0  - PC4  - MISO
// SDI_CFG1  - PC5  - MOSI
// SCK_CFG2  - PA1  - SPI CLK
// CSN_CFG3  - PA7  - Chip select
// DCEN_CFG4 - PD4  - DcStepenable input
// DCIN_CFG5 - PD3  - DcStepgating input for axis synchronization
// DCO_CFG6  - PD5  - DcStepready output
// DIR       - PB6
// STEP      - PB1
// DIAG1     - PD14 - Configurable diagnostic output, see datasheet p.78
// DIAG0     - PC12 - Configurable diagnostic output, see datasheet p.78
// DRN_ENN   - PC8  - Driver enable - Should be low to enable motor
// SPI_MODE  - PC13 - Set high for SPI mode

/**************************************************************************************************
 *     Public methods                                                                             *
 **************************************************************************************************/
Tmc2160::Tmc2160(uint32_t steps_per_rev, MeasurementDataCallbackFunction measurement_data_callback,
                 void *callback_argument):
    steps_per_rev_(steps_per_rev), measurement_data_callback_(measurement_data_callback),
    callback_argument_(callback_argument),
    spi_mode_pin_(hal::Gpio::kPortC, hal::Gpio::kPin13, hal::Gpio::kModeOutput),
    spi_slave_(hal::spi::kPort1, {.phase = hal::spi::kPhaseSecondEdge,
                              .polarity = hal::spi::kPolarityHigh,
                              .baud = hal::spi::kBaud4MHz,
                              .chip_select_port = hal::Gpio::kPortA,
                              .chip_select_pin = hal::Gpio::kPin7},
           tx_buffer_, rx_buffer_, SpiCallback, this),
    pwm_(TMC2160_STEP_PWM_PIN, 1000000, 0.0),
    direction_pin_(hal::Gpio::kPortB, hal::Gpio::kPin6, hal::Gpio::kModeOutput),
    driver_enable_pin_(hal::Gpio::kPortC, hal::Gpio::kPin8, hal::Gpio::kModeOutput),
    dcstep_enable_pin_(hal::Gpio::kPortD, hal::Gpio::kPin4, hal::Gpio::kModeOutput),
    dcin_pin_(hal::Gpio::kPortD, hal::Gpio::kPin3, hal::Gpio::kModeOutput) {
  spi_mode_pin_.Write(true);  // Pin high enables SPI mode
  driver_enable_pin_.Write(true);  // Motor driver is disabled on power up
  dcstep_enable_pin_.Write(true);  // Enable DcStep
  // TODO(jan): Sicher gehen dass DCIN high sein muss.
  //            Auf S.81 im Datenblatt sieht es zumindest so aus.
  dcin_pin_.Write(true);  // Do not stop the step execution
  // General configuration register - set bit2 to enable stealth chop - bit3 ist default 1
  state_ = kStateConfig;
  WriteRegister(TMC2160_REGISTER_GCONF, 0x0C);
  // TODO(jan): muss man in Register 0X6C toff setzen um den treiber zu enablen?
}

bool Tmc2160::SetSpeed(float speed) {
  if (state_ != kStateIdle && state_ != kStateSpeedSetpointMode &&
      state_ != kStateAccelerationSetpointMode) {
    return false;
  }
  speed_setpoint_ = speed;
  state_ = kStateSpeedSetpointMode;
  return true;
}

bool Tmc2160::SetAcceleration(float acceleration) {
  if (state_ != kStateIdle && state_ != kStateSpeedSetpointMode &&
      state_ != kStateAccelerationSetpointMode) {
    return false;
  }
  acceleration_setpoint_ = acceleration;
  state_ = kStateAccelerationSetpointMode;
  return true;
}

/**************************************************************************************************
 *     Private methods                                                                            *
 **************************************************************************************************/
void Tmc2160::WriteRegister(uint8_t register_address, uint32_t value) {
  ASSERT((register_address & 0x80) == 0);
  tx_buffer_[0] = 0x80 | register_address;
  tx_buffer_[1] = value >> 24;
  tx_buffer_[2] = value >> 16;
  tx_buffer_[3] = value >> 8;
  tx_buffer_[4] = value;
  spi_slave_.Transceive(5);
}

void Tmc2160::ReadRegister(uint8_t register_address) {
  ASSERT((register_address & 0x80) == 0);
  tx_buffer_[0] = ~0x80 & register_address;
  spi_slave_.Transceive(5);
}

/**************************************************************************************************
 *     Callbacks                                                                                  *
 **************************************************************************************************/
void Tmc2160::SpiCallback(void *instance) {
  auto inst = reinterpret_cast<Tmc2160*>(instance);

  switch (inst->state_) {
    case kStateConfig:
      inst->driver_enable_pin_.Write(false);  // Enable motor driver after configuration
      inst->state_ = kStateIdle;
      inst->acceleration_timer_.SetTimeoutPeriodic(100, AccererationTimerCallback, inst);
      break;

    default:
      ASSERT(false);
      break;
  }
}

void Tmc2160::AccererationTimerCallback(Tmc2160* instance) {
  float speed_increment;
  switch (instance->state_) {
    case kStateIdle:
      break;

    case kStateSpeedSetpointMode:
      speed_increment = std::max(std::min(instance->speed_setpoint_ - instance->current_speed_,
                                          TMC2160_MAX_ACCELERATION / static_cast<float>(100.0)),
                                 TMC2160_MAX_ACCELERATION / static_cast<float>(-100.0));
      break;

    case kStateAccelerationSetpointMode:
      speed_increment =
          std::max(std::min(instance->acceleration_setpoint_,
                            (TMC2160_MAX_SPEED - instance->current_speed_)),
                   (TMC2160_MAX_SPEED * static_cast<float>(-1.0) - instance->current_speed_))
                   / static_cast<float>(100.0);
      break;

    default:
      ASSERT(false);
      return;
  }

  if (instance->state_ != kStateIdle) {
    instance->current_acceleration_ = static_cast<float>(100.0) * speed_increment;
    instance->current_speed_ += speed_increment;

    // 256 microsteps, no double edge
    float steps_per_second =
        instance->current_speed_ * instance->steps_per_rev_ * 256 / static_cast<float>(M_2_PI);
    instance->pwm_.SetPeriod(std::abs(static_cast<float>(1000000.0) / steps_per_second));
    instance->pwm_.SetDutycycle(0.5);
    instance->direction_pin_.Write(steps_per_second < 0);  // Move backwards when DIR is high.
  }

  instance->measurement_data_callback_(instance->current_speed_, instance->current_acceleration_,
                                       instance->callback_argument_);
}
