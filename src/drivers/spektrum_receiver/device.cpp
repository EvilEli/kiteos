// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#define SPEKTRUM_RECEIVER_UART_PORT hal::Uart::kPort4
#define SPEKTRUM_RECEIVER_UART_BAUD_RATE 115200
#define SPEKTRUM_RECEIVER_UART_RX_PIN hal::Gpio::kPin0
#define SPEKTRUM_RECEIVER_UART_RX_PORT hal::Gpio::kPortD

#define SPEKTRUM_RECEIVER_PROTOCOL_BYTE_DSMX_11 0xB2
#define SPEKTRUM_RECEIVER_PROTOCOL_BYTE_DSM2_11 0x12
#define SPEKTRUM_RECEIVER_PROTOCOL_BYTE_DSMX_22 0xA2
#define SPEKTRUM_RECEIVER_PROTOCOL_BYTE_DSM2_22 0x01


#define SPEKTRUM_ALIGNMENT_TIMEOUT 5  // Define alignment timeout in ms.


#include <cstring>
#include "device.hpp"
#include "core/logger.hpp"
#include "core/system_time/system_time.hpp"

namespace spektrum_receiver {

/**************************************************************************************************
 *     Public methods                                                                             *
 **************************************************************************************************/
Device::Device(MeasurementDataCallbackFunction data_callback,
               void* callback_argument)
  : data_callback_(data_callback),
    callback_argument_(callback_argument),
    uart_(SPEKTRUM_RECEIVER_UART_PORT, SPEKTRUM_RECEIVER_UART_BAUD_RATE) {
  // PutInBindMode();
  ResetReception();
  readout_timer_.SetTimeoutPeriodic(1,
                                    [](void* ctx) {
                                      auto *inst = reinterpret_cast<Device *>(ctx);
                                      inst->ReadUartTask();
                                    },
                                    this);
}

Device::~Device() = default;

/**************************************************************************************************
 *     Private methods                                                                            *
 **************************************************************************************************/
void Device::ResetReception() {
  memset(receive_buffer_, 0, sizeof(receive_buffer_));
  buffer_cursor_ = 0;
  uart_.ClearRxBuffer();
}

void Device::ParseMessage() {
  // Store the current timestamp in the set point data structure
  channel_data_.timestamp = SystemTime::GetTimestamp();

  // Array mapping from channel IDs to RC channel values in the set point data structure
  ChannelValue* channel_data_map[] = {
      &channel_data_.throttle, &channel_data_.aileron, &channel_data_.elevator,
      &channel_data_.rudder, &channel_data_.gear, &channel_data_.aux1, &channel_data_.aux2,
      &channel_data_.aux3, &channel_data_.aux4, &channel_data_.aux5, &channel_data_.aux6,
      &channel_data_.aux7};

  // Invalidate old set point data structure content
  channel_data_.throttle.valid = false;
  channel_data_.aileron.valid = false;
  channel_data_.elevator.valid = false;
  channel_data_.rudder.valid = false;
  channel_data_.gear.valid = false;
  channel_data_.aux1.valid = false;
  channel_data_.aux2.valid = false;
  channel_data_.aux3.valid = false;
  channel_data_.aux4.valid = false;
  channel_data_.aux5.valid = false;
  channel_data_.aux6.valid = false;
  channel_data_.aux7.valid = false;

  // Parse message in receive buffer to set point data structure
  uint16_t servo_field;
  for (uint8_t i = 0; i < 7; ++i) {
    servo_field = (receive_buffer_[(2 * i) + 2] << 8u) |
        (receive_buffer_[(2 * i) + 3] & 0xFFu);
    uint8_t channel_id = (servo_field & 0x7800u) >> 11u;
    if (channel_id < 7) {
      channel_data_map[channel_id]->value =
          static_cast<float>(servo_field & 0x07FFu) / 1024.0f - 1.0f;
      channel_data_map[channel_id]->valid = true;
    }
  }
  data_callback_(channel_data_, callback_argument_);
}

void Device::PutInBindMode() {
  hal::Gpio uart_rx_pin(SPEKTRUM_RECEIVER_UART_RX_PORT, SPEKTRUM_RECEIVER_UART_RX_PIN,
                        hal::Gpio::kModeOutput);
  SoftwareTimer delay_timer;

  uart_rx_pin.Write(true);
  // 5 pulses for DSM2 internal 11 ms
  for (uint8_t i = 0; i < 5; i++) {
    uart_rx_pin.Write(false);

    // Add small delay because otherwise too fast.
    delay_timer.SetTimeout(1);
    while (!delay_timer.Elapsed()) {}

    uart_rx_pin.Write(true);
  }
}

/**************************************************************************************************
 *     Callbacks                                                                                  *
 **************************************************************************************************/
void Device::ReadUartTask() {
  while (uart_.Read(receive_buffer_ + buffer_cursor_, 1)) {
    // The second byte holds protocol information (11ms, 2048 servo field mode)
    // If wrong then retry parsing one byte further.
    if ((buffer_cursor_ == 1) && (receive_buffer_[1] != SPEKTRUM_RECEIVER_PROTOCOL_BYTE_DSMX_11)) {
      Logger::Report("Lost Sync", Logger::LogLevel::kWarning);
      receive_buffer_[0] = receive_buffer_[1];
      buffer_cursor_ = 0;
    }

    // Set the packet alignment timeout timer
    if (buffer_cursor_ == 0) {
      alignment_timer_.SetTimeout(SPEKTRUM_ALIGNMENT_TIMEOUT);
    }

    // A packet always has 16 bytes
    if (buffer_cursor_ == 15) {
      ParseMessage();
      ResetReception();
    } else if (alignment_timer_.Elapsed()) {
      // Detect incomplete packets by timeout
      Logger::Report("Lost Sync", Logger::LogLevel::kWarning);
      ResetReception();
    } else {
      buffer_cursor_++;
    }
  }
}

}  // namespace spektrum_receiver
