// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#ifndef SRC_DRIVERS_SPEKTRUM_RECEIVER_DEFINITIONS_HPP_
#define SRC_DRIVERS_SPEKTRUM_RECEIVER_DEFINITIONS_HPP_

#include<cstdint>

namespace spektrum_receiver {
//! Structure representing the set point of a single channel.
struct ChannelValue {
  double value = 0;     //!< Set point of the channel.
  bool valid = false;  //!< True if the set point is valid, false otherwise.
};

//! Structure representing the set points of a reference from a pilot.
struct ChannelData {
  ChannelValue throttle;  //!< Set point of the throttle channel.
  ChannelValue aileron;   //!< Set point of the aileron channel.
  ChannelValue elevator;  //!< Set point of the elevator channel.
  ChannelValue rudder;    //!< Set point of the rudder channel.
  ChannelValue gear;      //!< Set point of the gear channel.
  ChannelValue aux1;      //!< Set point of the aux1 channel.
  ChannelValue aux2;      //!< Set point of the aux2 channel.
  ChannelValue aux3;      //!< Set point of the aux3 channel.
  ChannelValue aux4;      //!< Set point of the aux4 channel.
  ChannelValue aux5;      //!< Set point of the aux5 channel.
  ChannelValue aux6;      //!< Set point of the aux6 channel.
  ChannelValue aux7;      //!< Set point of the aux7 channel.
  uint64_t timestamp = 0;   //!< Timestamp when the set point data was measured.
};

//! callback that is called if new channels are available.
typedef void (*MeasurementDataCallbackFunction)(ChannelData channel_data, void *ctx);
};  // namespace spektrum_receiver

#endif  // SRC_DRIVERS_SPEKTRUM_RECEIVER_DEFINITIONS_HPP_
