// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#ifndef SRC_DRIVERS_BUZZER_HPP_
#define SRC_DRIVERS_BUZZER_HPP_

#include <cstdint>
#include "core/software_timer.hpp"
#include "hal/adc.hpp"
#include "hal/pwm.hpp"

/*!
 *  \brief Implements a driver to easily access the FailsafeSwitch.
 *         The driver uses a hardware timer which triggers a GPIO if the Set()
 *         method is not called for a specific period.
 */
class Buzzer {
 public:
  //! \brief Constructor of the FailsafeSwitch driver component.
  Buzzer();

  //! \brief Destructor of the FailsafeSwitch driver component.
  ~Buzzer();

  //! \brief Enables the buzzer sound.
  void Enable();
  //! \brief Disables the buzzer sound.
  void Disable();

 private:
  //! Callback called when the buzzer timer elapsed.
  static void TimerElapsedCallback(Buzzer *inst);

  hal::Pwm pwm_;  //!< PWM used for the buzzer.
  uint8_t current_tone;             //!< index of current playing tone in the piezo tone array.
  bool enabled_;  //!< flag if sound is playing.
  //! sound periods
  const uint16_t tones_[15] = {170, 180, 190, 200, 210, 230, 240,
                               250, 260, 270, 280, 290, 300, 310, 320};
  SoftwareTimer update_timer_;  //!< plays the next tone.
};

#endif  // SRC_DRIVERS_BUZZER_HPP_
