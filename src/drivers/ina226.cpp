// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#include "ina226.hpp"
#include "core/logger.hpp"
#include "core/system_time/system_time.hpp"
#include "core/scheduler.hpp"

//! Define update rate in ms.
#define INA226_MEASUREMENT_PERIOD 10

// ADDRESS: ina226 address is defined by voltage level on address pin
#define INA226_SLAVE_ADDR 0b1000010

// REGISTERS:
#define REG_CONF 0x00
#define REG_SHUNT_VOLTAGE 0x01
#define REG_ID   0xFF

// RST: false
// reserved[3]
// AVERAGES[3]: 64
// bus conversion time 1.1ms
// shunt conversion time 1.1 ms
// shunt: continuous mode
#define INA_CONFIG 0b0000011100100101

/**************************************************************************************************
 *     Public methods                                                                             *
 **************************************************************************************************/
Ina226::Ina226(Ina226::DataReadyCallback cb, void *cb_arg) :
  slave_(hal::i2c::kPort3, INA226_SLAVE_ADDR,
         send_buffer_, receive_buffer_, I2cCallback, this),
         data_ready_callback_(cb),
         cb_arg_(cb_arg) {
  StartContinuousConversion();
  readout_timer_.SetTimeoutPeriodic(INA226_MEASUREMENT_PERIOD, StartChannelRead, this);
}

/**************************************************************************************************
 *     Private methods                                                                            *
 **************************************************************************************************/
void Ina226::StartContinuousConversion() {
  send_buffer_[0] = REG_CONF;
  send_buffer_[1] = (INA_CONFIG >> 8u) & 0xFFu;
  send_buffer_[2] = INA_CONFIG & 0xFFu;

  slave_.Transceive(3, 0);
}

/**************************************************************************************************
 *     Callbacks                                                                                  *
 **************************************************************************************************/
void Ina226::StartChannelRead(Ina226 *inst) {
    inst->send_buffer_[0] = REG_SHUNT_VOLTAGE;
    inst->slave_.Transceive(1, 2);
}

void Ina226::InvokeCallbackTask(Ina226 *inst) {
  inst->data_ready_callback_(inst->shunt_voltage_, inst->cb_arg_);
}

void Ina226::I2cCallback(hal::i2c::Error error, void *cb_arg) {
  auto *inst = reinterpret_cast<Ina226 *>(cb_arg);
  if (error == hal::i2c::kErrorNone) {
    // Bit 0 is LSB = 2.5 μV
    double result = static_cast<int16_t>(
                  ((inst->receive_buffer_[0] << 8) |
                    inst->receive_buffer_[1])) * 0.0000025;
    inst->shunt_voltage_ = result;
    Scheduler::EnqueueTask(InvokeCallbackTask, inst);
  } else {
    Logger::Report("I2C Error", Logger::kError);
  }
}
