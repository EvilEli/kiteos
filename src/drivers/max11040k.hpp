// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#ifndef SRC_DRIVERS_MAX11040K_HPP_
#define SRC_DRIVERS_MAX11040K_HPP_


#include <cstdint>
#include "hal/spi/slave.hpp"
#include "core/software_timer.hpp"

/*!
 *  \brief  Implements the Max11040k driver.
 */
class Max11040k {
 public:
  //! Structure containing the data measured by the Max11040k including the timestamp of the
  //! measurement.
  struct MeasurementData {
    double voltage[4] = {0.0};  //!< Voltage measured on each channel.
    uint64_t timestamp = 0;     //!< Timestamp when the measurement data was measured.
  };
  //! callback that is called if all channels are finished reading.
  typedef void (*Max11040kCallback)(MeasurementData measurement_data, void *inst);

  /*! \brief Constructor of the Max11040k class.
   *
   *  \param  callback           Callback which is called after the initialization is finished.
   *  \param  callback_argument  Argument to pass to the callback which is called after the
   *                             initialization is finished.
   */
  Max11040k(Max11040kCallback callback, void *callback_argument);

  /*! \brief Destructor of the Max11040k class.
   */
  ~Max11040k() = default;

 private:
  //! Call the user callback
  static void InvokeUserCallbackTask(Max11040k *inst);
  //! Callback which is called when there is data ready to read from the max11040k.
  static void DataRdyCallback(void *inst);
  //! SPI callback when configuration step1 is finished.
  static void ConfigurationCallback(void *inst);
  //! SPI Callback which is called when measurement data has been pulled from the IC.
  static void DataPullFinishedCallback(void *inst);

  hal::spi::Slave spi_slave_;   //!< hal::SpiSlave class.
  hal::Gpio int_gpio_;          //!< Gpio pin used for the data ready interrupt.
  Max11040kCallback data_ready_callback_;  //!< Called when new measurements arrive.
  void *cb_arg_;              //!< Argument passed back with the data_ready_callback_.
  uint8_t write_buffer_[3];   //!< Write buffer used for SPI transmissions.
  uint8_t read_buffer_[13];   //!< Read buffer used for SPI transmissions.
  MeasurementData measurement_data_;  //!< Data storing max11040k adc values and timestamp.
};

#endif  // SRC_DRIVERS_MAX11040K_HPP_
