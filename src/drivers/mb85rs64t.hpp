// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.

#ifndef SRC_DRIVERS_MB85RS64T_HPP_
#define SRC_DRIVERS_MB85RS64T_HPP_

/**************************************************************************************************
 *                                                                                                *
 *        '|| '||'  '|'    |      '||''|.    '|.   '|'  '||'  '|.   '|'   ..|'''.|                *
 *         '|. '|.  .'    |||      ||   ||    |'|   |    ||    |'|   |   .|'     '                *
 *          ||  ||  |    |  ||     ||''|'     | '|. |    ||    | '|. |   ||    ....               *
 *           ||| |||    .''''|.    ||   |.    |   |||    ||    |   |||   '|.    ||                *
 *            |   |    .|.  .||.  .||.  '|'  .|.   '|   .||.  .|.   '|    ''|...'|                *
 *                                                                                                *
 *        !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!               *
 *        !!          Do not directly use this class to access the FRAM.         !!               *
 *        !!                  Use The FramAccess class instead.                  !!               *
 *        !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!               *
 *                                                                                                *
 **************************************************************************************************/

#include <cstdint>
#include "hal/spi/slave.hpp"


//! Length of the FRAM write and read buffer. Should not be greater than (255 - 3).
static constexpr const uint16_t kMb85Rs64TBufferLength = 128;
static constexpr const uint16_t kMb85Rs64TSize = 8192;  //!< Size of the FRAM in bytes


/*! \brief     Class providing an interface to easily access the MB85RS64T FRAM via SPI.
 *
 *  \author    Jan Lehmann
 *
 *  \date      2020
 */
class Mb85Rs64T {
 public:
  //! States of the FRAM.
  enum Mb85Rs64TState {
    kStateInitializing,   //!< Initialization in progress.
    kStateIdle,           //!< Initialized and ready to execute write or read operations.
    kStateWriting,        //!< Write operation in progress.
    kStateWriteFailed,    //!< Write operation failed.
    kStateReading,        //!< Read operation in progress.
  };

  //! Callback type to be passed to methods of this object.
  typedef void (*Mb85Rs64TCallback)(void *callback_argument);

  /*! \brief Constructor of the MB85RS64T class.
   *
   *  \param  callback           Callback which is called after the initialization is finished.
   *  \param  callback_argument  Argument to pass to the callback which is called after the
   *                             initialization is finished.
   *
   *  \param spi_port  SPI port used to communicate to the FRAM.
   */
  Mb85Rs64T(Mb85Rs64TCallback callback, void *callback_argument);

  /*! \brief Destructor of the MB85RS64T class.
   */
  ~Mb85Rs64T();

  /*! \brief   Writes data to the FRAM.
   *
   *  \details If the write operation reaches the end of the FRAM, it continues writing at address
   *           0. This function does not wait for the write operation to finish but returns after
   *           starting it.
   *
   *  \param   callback           Callback which is called after the write operation is finished.
   *  \param   callback_argument  Argument to pass to the callback which is called after the write
   *                              operation is finished.
   *
   *  \param   address      FRAM address to write to. May be a value from 0 to (kFramSize - 1).
   *  \param   data         Buffer containing the data to write.
   *  \param   data_length  Number of bytes to write. Should not be greater than kBufferLength.
   *
   *  \return  True on success, false if FRAM is busy or parameters are invalid.
   */
  bool Write(uint16_t  address, const uint8_t* data, int data_length,
             Mb85Rs64TCallback callback, void *callback_argument);

  /*! \brief   Reads data from FRAM.
   *
   *  \details If the read operation reaches the end of the FRAM, it continues reading at address 0.
   *           This function does not wait for the read operation to finish but returns after
   *           starting it.
   *
   *  \param  callback           Callback which is called after the read operation is finished.
   *  \param  callback_argument  Argument to pass to the callback which is called after the read
   *                             operation is finished.
   *
   *  \param   address      FRAM address to read from. May be a value from 0 to (kFramSize - 1).
   *  \param   data         Buffer to write the read data to.
   *  \param   data_length  Number of bytes to read. Should not be greater than kBufferLength.
   *
   *  \return  True on success, false if FRAM is busy or parameters are invalid.
   */
  bool Read(uint16_t  address, uint8_t* data, int data_length,
            Mb85Rs64TCallback callback, void *callback_argument);

  /*! \brief  Gets the current state of the FRAM.
   *
   *  \return Current state of the FRAM.
   */
  Mb85Rs64TState GetState();

  /*! \brief      Gets busy state of the EEPROM.
   *
   *  \details    Gets busy state of the EEPROM. The EEPROM is busy when carrying out an init, write
   *              or read operation. While the EEPROM is busy, read and write operations will fail.
   *
   *  \return     True if the EEPROM is busy, false otherwise.
   */
  bool IsBusy();

 private:
  Mb85Rs64TState state_;  //!< State of the FRAM.

  hal::spi::Slave spi_slave_;  //!< hal::SpiSlave class
  uint8_t write_buffer_[kMb85Rs64TBufferLength + 3];  //!< Write buffer used for SPI transmissions.
  uint8_t read_buffer_[kMb85Rs64TBufferLength + 3];  //!< Read buffer used for SPI transmissions.
  uint8_t transfer_len_;  //!< Storage for length of last transmission for verification.
  //! Callback to be called when the current transmission finished.
  Mb85Rs64TCallback finished_callback_;
  //! Argument to pass to the finished callback which is called when the current transmission
  //! finished.
  void *finished_callback_argument_;
  //! Pointer to the buffer where data read from the FRAM should be stored.
  uint8_t *read_data_pointer_;

  //! Callback called when the transmission of a write status register command finished.
  static void WriteStatusRegisterCommandCallback(void *instance);
  //! Callback called when the transmission of a set write enable latch command finished.
  static void SetWriteEnableLatchCommandCallback(void *instance);
  //! Callback called when the transmission of a write command finished.
  static void WriteCommandCallback(void *instance);
  //! Callback called when the transmission of a read command for verification finished.
  static void VerifyCallback(void *instance);
  //! Callback called when the transmission of a read command finished.
  static void ReadCommandCallback(void *instance);
};


// Poison MB85RS64T class to ensure that it is not directly used.
#pragma GCC poison  Mb85Rs64T

#endif  // SRC_DRIVERS_MB85RS64T_HPP_
