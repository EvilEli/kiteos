// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.

// Define macro for poisoned class
#define MB85RS64T Mb85Rs64T

#include "utils/debug_utils.h"
#include "mb85rs64t.hpp"

/**************************************************************************************************
 *     Defines                                                                                    *
 **************************************************************************************************/

#define MB85RS64T_CMD_WREN (0x06u)  // Set write enable latch
#define MB85RS64T_CMD_WRDI (0x04u)  // Reset write enable latch
#define MB85RS64T_CMD_RDSR (0x05u)  // Read status register
#define MB85RS64T_CMD_WRSR (0x01u)  // Write status register
#define MB85RS64T_CMD_READ (0x03u)  // Read memory code
#define MB85RS64T_CMD_WRITE (0x02u)  // Write memory code
#define MB85RS64T_CMD_RDID (0x9Fu)  // Read device ID
#define MB85RS64T_CMD_SLEEP (0xB9u)  // Sleep mode

// port definitions
#define MB85RS64T_SPI_PORT hal::spi::kPort2
#define MB85RS64T_CS_GPIO_PORT hal::Gpio::Port::kPortA
#define MB85RS64T_CS_GPIO_PIN  hal::Gpio::kPin11


/**************************************************************************************************
 *     Public methods                                                                             *
 **************************************************************************************************/

MB85RS64T::MB85RS64T(Mb85Rs64TCallback callback, void *callback_argument) : spi_slave_(
    MB85RS64T_SPI_PORT,
    { hal::spi::kPhaseFirstEdge, hal::spi::kPolarityLow,
      hal::spi::kBaud8MHz, MB85RS64T_CS_GPIO_PORT, MB85RS64T_CS_GPIO_PIN },
    write_buffer_, read_buffer_, nullptr, nullptr) {
  // Initialize state
  state_ = kStateInitializing;

  // Store callback and callback argument in member variables to call it from the SPI callback
  finished_callback_ = callback;
  finished_callback_argument_ = callback_argument;

  // Prepare write buffer and SPI job for transmission
  write_buffer_[0] = MB85RS64T_CMD_WRSR;
  write_buffer_[1] = 0;

  // Send SPI command
  spi_slave_.Transceive(2, MB85RS64T::WriteStatusRegisterCommandCallback, this);
}

MB85RS64T::~MB85RS64T() = default;

bool MB85RS64T::Write(uint16_t address, const uint8_t *data, int data_length,
                      Mb85Rs64TCallback callback, void *callback_argument) {
  // Check if function parameters are valid
  if (address > kMb85Rs64TSize - 1 || data_length > kMb85Rs64TBufferLength) {
    return false;
  }

  // Check if method is called in the correct state
  if (state_ != kStateIdle && state_ != kStateWriteFailed) {
    return false;
  }
  state_ = kStateWriting;

  // Store callback and callback argument in member variables to call it from the SPI callback
  finished_callback_ = callback;
  finished_callback_argument_ = callback_argument;

  // Prepare write buffer and SPI job for transmission
  write_buffer_[0] = MB85RS64T_CMD_WRITE;
  write_buffer_[1] = (uint8_t)((address & 0xFF00u) >> 8u);
  write_buffer_[2] = (uint8_t)(address & 0x00FFu);
  for (int i = 0; i < data_length; i++) {
    write_buffer_[3 + i] = data[i];
  }

  // Send SPI command
  transfer_len_ = 3 + data_length;
  spi_slave_.Transceive(transfer_len_, MB85RS64T::WriteCommandCallback, this);

  return true;
}

bool MB85RS64T::Read(uint16_t address, uint8_t *data, int data_length,
                     Mb85Rs64TCallback callback, void *callback_argument) {
  // Check if function parameters are valid
  if (address > kMb85Rs64TSize - 1 || data_length > kMb85Rs64TBufferLength) {
    return false;
  }

  // Check if method is called in the correct state
  if (state_ != kStateIdle && state_ != kStateWriteFailed) {
    return false;
  }
  state_ = kStateReading;

  // Store callback and callback argument in member variables to call it from the SPI callback, data
  // pointer needs to be stored since the read data should be stored there in the SPI callback.
  finished_callback_ = callback;
  finished_callback_argument_ = callback_argument;
  read_data_pointer_ = data;

  // Prepare write buffer and SPI job for transmission
  write_buffer_[0] = MB85RS64T_CMD_READ;
  write_buffer_[1] = (uint8_t)((address & 0xFF00u) >> 8u);
  write_buffer_[2] = (uint8_t)(address & 0x00FFu);

  // Send SPI command
  transfer_len_ = 3 + data_length;
  spi_slave_.Transceive(transfer_len_, MB85RS64T::ReadCommandCallback, this);

  return true;
}

MB85RS64T::Mb85Rs64TState MB85RS64T::GetState() {
  return state_;
}

bool MB85RS64T::IsBusy() {
  return (state_ == kStateInitializing) || (state_ == kStateWriting) || (state_ == kStateReading);
}


/**************************************************************************************************
 *     Callbacks                                                                                  *
 **************************************************************************************************/

void MB85RS64T::WriteStatusRegisterCommandCallback(void *instance) {
  // Cast instance pointer
  auto *inst = reinterpret_cast<MB85RS64T*>(instance);

  // Check if state is as expected
  ASSERT(inst->state_ == kStateInitializing);

  // Prepare write buffer and SPI job for transmission of a write enable command
  inst->write_buffer_[0] = MB85RS64T_CMD_WREN;

  // Send SPI command
  inst->spi_slave_.Transceive(1, MB85RS64T::SetWriteEnableLatchCommandCallback, inst);
}

void MB85RS64T::SetWriteEnableLatchCommandCallback(void *instance) {
  // Cast instance pointer
  auto *inst = reinterpret_cast<MB85RS64T*>(instance);

  // Check if state is as expected
  ASSERT(inst->state_ == kStateInitializing);

  // Initialization finished, set state and execute finished callback
  inst->state_ = kStateIdle;
  if (inst->finished_callback_ != nullptr) {
    inst->finished_callback_(inst->finished_callback_argument_);
  }
}

void MB85RS64T::WriteCommandCallback(void *instance) {
  // Cast instance pointer
  auto *inst = reinterpret_cast<MB85RS64T*>(instance);

  // Check if state is as expected
  ASSERT(inst->state_ == kStateWriting);

  // Prepare write buffer and SPI job for transmission
  inst->write_buffer_[0] = MB85RS64T_CMD_READ;

  // Send SPI command
  inst->spi_slave_.Transceive(inst->transfer_len_, MB85RS64T::VerifyCallback, inst);
}

void MB85RS64T::VerifyCallback(void *instance) {
  // Cast instance pointer
  auto *inst = reinterpret_cast<MB85RS64T*>(instance);

  // Check if state is as expected
  ASSERT(inst->state_ == kStateWriting);

  // Verify if read data is the same as the data written before
  for (int i = 3; i < inst->transfer_len_; i++) {
    if (inst->read_buffer_[i] != inst->write_buffer_[i]) {
      inst->state_ = kStateWriteFailed;
      break;
    }
  }

  if (inst->state_ != kStateWriteFailed) {
    inst->state_ = kStateIdle;
  }

  // Write operation finished, execute finished callback
  if (inst->finished_callback_ != nullptr) {
    inst->finished_callback_(inst->finished_callback_argument_);
  }
}

void MB85RS64T::ReadCommandCallback(void *instance) {
  // Cast instance pointer
  auto *inst = reinterpret_cast<MB85RS64T*>(instance);

  // Check if state is as expected
  ASSERT(inst->state_ == kStateReading);

  // Copy read data to data buffer
  for (int i = 0; i < inst->transfer_len_ - 3; i++) {
    inst->read_data_pointer_[i] = inst->read_buffer_[3 + i];
  }

  // Read operation finished, set state and execute finished callback
  inst->state_ = kStateIdle;
  if (inst->finished_callback_ != nullptr) {
    inst->finished_callback_(inst->finished_callback_argument_);
  }
}
