// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#include "ak09916.hpp"
#include "core/logger.hpp"
#include "core/system_time/system_time.hpp"
#include "core/scheduler.hpp"

// ADDRESS: ina226 address is defined by voltage level on address pin
#define AK09916_SLAVE_ADDR 0x0C

// Registers:
#define REG_HXL 0x11
#define REG_CNTL2 0x31

// Configuration:
#define AK09916_CNTL2_MODE_SELFTEST 0x10
#define AK09916_CNTL2_MODE_CONT_4 0x08
#define AK09916_CNTL2_MODE_CONT_3 0x06
#define AK09916_CNTL2_MODE_CONT_2 0x04
#define AK09916_CNTL2_MODE_CONT_1 0x02
#define AK09916_CNTL2_MODE_SINGLE 0x01

/**************************************************************************************************
 *     Public methods                                                                             *
 **************************************************************************************************/
Ak09916::Ak09916(MeasurementFrequency frequency, Ak09916::DataReadyCallback cb, void *cb_arg) :
  slave_(hal::i2c::kPort4, AK09916_SLAVE_ADDR,
         tx_buffer_, rx_buffer_, I2cCallback, this),
  data_ready_callback_(cb),
  cb_arg_(cb_arg) {
  ConfigureContinuousConversion(frequency);
  readout_timer_.SetTimeoutPeriodic(1000/frequency, StartSensorReadout, this);
}

/**************************************************************************************************
 *     Private methods                                                                            *
 **************************************************************************************************/
void Ak09916::ConfigureContinuousConversion(MeasurementFrequency measurement_frequency) {
  tx_buffer_[0] = REG_CNTL2;
  switch (measurement_frequency) {
    case kMeasurementFrequency100Hz:
      tx_buffer_[1] = AK09916_CNTL2_MODE_CONT_4;
      break;
    case kMeasurementFrequency50Hz:
      tx_buffer_[1] = AK09916_CNTL2_MODE_CONT_3;
      break;
    case kMeasurementFrequency20Hz:
      tx_buffer_[1] = AK09916_CNTL2_MODE_CONT_2;
      break;
    case kMeasurementFrequency10Hz:
      tx_buffer_[1] = AK09916_CNTL2_MODE_CONT_1;
      break;
  }

  slave_.Transceive(2, 0);
}

/**************************************************************************************************
 *     Callbacks                                                                                  *
 **************************************************************************************************/
void Ak09916::StartSensorReadout(Ak09916 *inst) {
  inst->tx_buffer_[0] = REG_HXL;
  // Read 6 measurement register + dummy + status 2 register
  inst->slave_.Transceive(1, 8);
}

void Ak09916::InvokeCallbackTask(Ak09916 *inst) {
  inst->data_ready_callback_(inst->measurement_data_, inst->cb_arg_);
}

void Ak09916::I2cCallback(hal::i2c::Error error, void *cb_arg) {
  auto *inst = reinterpret_cast<Ak09916 *>(cb_arg);
  if (error == hal::i2c::kErrorNone) {
    inst->measurement_data_.timestamp = SystemTime::GetTimestamp();
    inst->measurement_data_.magnetic_flux_density[0] =
      static_cast<int16_t>((inst->rx_buffer_[1] << 8) | inst->rx_buffer_[0]) * 0.00000015;
    inst->measurement_data_.magnetic_flux_density[1] =
      static_cast<int16_t>((inst->rx_buffer_[3] << 8) | inst->rx_buffer_[2]) * 0.00000015;
    inst->measurement_data_.magnetic_flux_density[2] =
      static_cast<int16_t>((inst->rx_buffer_[5] << 8) | inst->rx_buffer_[4]) * 0.00000015;
    Scheduler::EnqueueTask(InvokeCallbackTask, inst);
  } else {
    Logger::Report("I2C Error", Logger::kError);
  }
}
