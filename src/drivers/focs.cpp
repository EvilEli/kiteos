// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.

#include "focs.hpp"
#include <cmath>
#include "hal/spi/slave.hpp"
#include "core/system_time/system_time.hpp"
#include "core/logger.hpp"


#define FOCS_STATUS_PERIOD       10    // Status update rate in ms
#define FOCS_SPEED_PERIOD        1     // Speed (.1HZ) update rate in ms
#define FOCS_TEMPERATURE_PERIOD  100   // Temperature (C) update rate in ms

#define FOCS_COMMAND_GET         0x00u  // SPI get command
#define FOCS_COMMAND_SET         0x80u  // SPI set command

#define FOCS_STATUS_ADDR         0x00u  // Status register address
#define FOCS_SPEED_ADDR          0x01u  // Speed register address
#define FOCS_TEMPERATURE_ADDR    0x02u  // Temperature register address


Focs::Focs() :  spi_slave_(hal::spi::kPort2,
                          { hal::spi::kPhaseFirstEdge,
                            hal::spi::kPolarityLow,
                            hal::spi::kBaud250kHz,
                            hal::Gpio::Port::kPortA,
                            hal::Gpio::kPin4},
                            send_buffer_,
                            receive_buffer_,
                            SpiCallback, this),
                send_buffer_{0},
                receive_buffer_{0},
                current_setpoint_(0) {
  // Reset(); // TODO
}
Focs::~Focs() = default;


void Focs::Run(MeasurementData* measurement_data) {
  measurement_data->speed_valid = false;
  measurement_data->temperature_valid = false;

  CheckSpiRx(measurement_data);
  CheckTimers();
}

void Focs::Arm() {
  SetSpeed(150);
}

void Focs::Disarm() {
  SetSpeed(0);
}

void Focs::SetSpeed(float setpoint) {
  current_setpoint_ = setpoint;
  uint16_t speed = ((setpoint+0.5f) * 1.59f);  // Convert rad/s to .1 Hz (unit used in foca)
  send_buffer_[0] = FOCS_COMMAND_SET | FOCS_SPEED_ADDR;
  send_buffer_[1] = speed >> 8u;
  send_buffer_[2] = speed & 0xFFu;
  spi_slave_.Transceive(3);
}

void Focs::CheckTimers() {
  if (spi_slave_.IsBusy())
    return;

  if (speed_timer_.Elapsed()) {
    speed_timer_.SetTimeout(FOCS_SPEED_PERIOD);
    send_buffer_[0] = FOCS_COMMAND_GET | FOCS_SPEED_ADDR;
    spi_slave_.Transceive(4);
    last_spi_request_ = kSpeedRequested;
  } else if (temperature_timer_.Elapsed()) {
    temperature_timer_.SetTimeout(FOCS_TEMPERATURE_PERIOD);
    send_buffer_[0] = FOCS_COMMAND_GET | FOCS_TEMPERATURE_ADDR;
    spi_slave_.Transceive(4);
    last_spi_request_ = kTempRequested;
  } else if (status_timer_.Elapsed()) {
    status_timer_.SetTimeout(FOCS_STATUS_PERIOD);
    send_buffer_[0] = FOCS_COMMAND_GET | FOCS_STATUS_ADDR;
    spi_slave_.Transceive(4);
    last_spi_request_ = kStatusRequested;
  }
}

void Focs::CheckSpiRx(MeasurementData* measurement_data) {
  if (spi_slave_.IsBusy())
    return;

  if (last_spi_request_ == kIdle)
    return;

  // if(rx_buffer_[3] == 0xAE) TODO: use this?

  if (last_spi_request_ == kSpeedRequested) {
    int16_t avg_mec_speed = (receive_buffer_[1] << 8u) | (receive_buffer_[2] & 0xFFu);
    measurement_data->speed_valid = true;
    measurement_data->speed = (avg_mec_speed / 1.59f);  // Convert from .1 Herz to rad/s
    measurement_data->timestamp = last_rx_timestamp_;
  } else if (last_spi_request_ == kTempRequested) {
    int16_t avg_temp_c = (receive_buffer_[1] << 8u) | (receive_buffer_[2] & 0xFFu);
    measurement_data->temperature_valid = true;
    measurement_data->temperature = avg_temp_c;
    measurement_data->timestamp = last_rx_timestamp_;
  } else if (last_spi_request_ == kStatusRequested) {
    uint8_t status_foca = receive_buffer_[1];
    uint16_t faults = (receive_buffer_[2] << 8u) | (receive_buffer_[3] & 0xFFu);
    measurement_data->status_valid = true;
    measurement_data->status = status_foca;
    measurement_data->timestamp = last_rx_timestamp_;
    ProcessFaults(faults);
  }
  last_spi_request_ = kIdle;
}

void Focs::ProcessFaults(uint16_t faults) {
  ReportFault("MC_FOC_DURATION", 0x0001u, faults);  // Error: FOC rate to high.
  ReportFault("MC_OVER_VOLT", 0x0002u, faults);     // Error: Software over voltage.
  ReportFault("MC_UNDER_VOLT", 0x0004u, faults);    // Error: Software under voltage.
  ReportFault("MC_OVER_TEMP", 0x0008u, faults);     // Error: Software over temperature.
  ReportFault("MC_START_UP", 0x0010u, faults);      // Error: Startup failed.
  ReportFault("MC_SPEED_FDBK", 0x0020u, faults);    // Error: Speed feedback.
  ReportFault("MC_BREAK_IN", 0x0040u, faults);      // Error: Emergency input (Over current).
  ReportFault("MC_SW_ERROR", 0x0080u, faults);      // Error: Software Error.
}

void Focs::ReportFault(const char *msg, uint16_t err_mask, uint16_t current_faults) {
  if (current_faults & err_mask) {
    Logger::Report(msg, Logger::LogLevel::kError);
  }
}

void Focs::SpiCallback(void *const cb_arg) {
  Focs* inst = reinterpret_cast<Focs*>(cb_arg);
  inst->last_rx_timestamp_ = SystemTime::GetTimestamp();
}
