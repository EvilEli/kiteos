// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#include <cmath>
#include "a4964.hpp"
#include "hal/systick_timer.hpp"
#include "hal/spi/slave.hpp"
#include "core/system_time/system_time.hpp"
#include "core/logger.hpp"
#include "a4964_configuration.hpp"
#include "a4964_registers.hpp"

//! Define speed (rpm) update rate in ms.
#define A4964_SPEED_PERIOD 10
//! Define speed (rpm) update rate in ms.
#define A4964_TEMPERATURE_PERIOD 100
//! Define watchdog timeout minimum delay for WD toggle in ms
//! (disarm happens after maximum timeout: 150 ms).
#define A4964_WATCHDOG_MIN_TIMEOUT 2
//! Define maximum speed offset expected to be corrected by integrator in rad/s.
#define MAX_INTEGRATOR_SPEED_OFFSET 20

#define A4964_RPM_SPEED_FACTOR ((1.6f * 2.0f * M_PI) / A4964_NUM_POLEPAIRS)

#define MAX(x, y) (((x) > (y)) ? (x) : (y))
#define MIN(x, y) (((x) < (y)) ? (x) : (y))

/**************************************************************************************************
 *     Public Methods                                                                             *
 **************************************************************************************************/
A4964::A4964(StateNode* state_node)
: state_node_(state_node),
  spi_slave_(hal::spi::kPort1,
             { hal::spi::kPhaseSecondEdge,
               hal::spi::kPolarityHigh,
               hal::spi::kBaud8MHz,
               hal::Gpio::Port::kPortA,
               hal::Gpio::kPin4 },
             send_buffer_,
             receive_buffer_,
             nullptr, nullptr),
  gpio_wdog_(hal::Gpio::Port::kPortD,
            hal::Gpio::kPin6,
            hal::Gpio::kModeOutput,
            nullptr, nullptr),
  send_buffer_{0},
  receive_buffer_{0} {
  gpio_wdog_.Write(false);
  Reset();
}

A4964::~A4964() = default;

void A4964::Run(MeasurementData* measurement_data) {
  measurement_data->speed_valid = false;
  measurement_data->temperature_valid = false;

  switch (state_) {
    case kA4964StateConfig:
      RunConfig();
      break;
    case kA4964StateProtected:
      if (set_point_ < arm_threshold_) {
        state_node_->SetState("DISARMED");
        state_ = kA4964StateDisarmed;
      }
      break;
    case kA4964StateDisarmed:
      if (set_point_ > arm_threshold_) {
        state_node_->SetState("THRUST_PROTECTION");
        state_ = kA4964StateProtected;
      }
      break;
    case kA4964StateRamping:
      ToggleWdog();
      // Convert the provided setPoint to a range between minimum turning speed
      // (lower limit) and 1.0.
      if (ramp_timer_.Elapsed()) {
        ramp_timer_.SetTimeout(15);
        out_set_point_ =
            static_cast<uint16_t>(MIN((lower_limit_ / A4964_RPM_SPEED_FACTOR) + 0.5, 1023));
        out_set_point_available_ = true;

        // Wait for the motor to ramp up to 90% of the idle speed
        if (motor_speed_ > (lower_limit_ * 0.9)) {
          state_node_->SetState("ARMED");
          state_ = kA4964StateArmed;
        }
      }
      break;
    case kA4964StateArmed:
      ToggleWdog();
      break;
    case kA4964StateRunning:
      // Process new setPoint if available
      if (set_point_available_) {
        set_point_available_ = false;
        // Clip the provided setPoint at the upper limit
        double control = MIN(MAX(set_point_ + k_error_integral_ * error_integral_, lower_limit_),
                            upper_limit_);
        // Convert the provided setPoint to a range between minimum turning speed (lower limit)
        // and 1.0.
        out_set_point_ = MIN((control / A4964_RPM_SPEED_FACTOR) + 0.5, 1023);
        out_set_point_available_ = true;
      }
      break;
    default:
      break;
  }
  switch (comm_state_) {
    case kA4964CommStateIdle:
      if (out_set_point_available_) {
        if (TransmitWord(A4964_REG_WRITE_ONLY | ((out_set_point_ << 1u) & 0x7FFu))) {
          comm_state_ = kA4964CommStateSetpointPending;
          out_set_point_available_ = false;
        }
      } else if (speed_timer_.Elapsed()) {
        if (TransmitWord(A4964_REG_READBACK_SEL | A4964_REGWRITE | A4964_READBACK_RBS_SPEED)) {
          comm_state_ = kA4964CommStateSpeedReq;
          speed_timer_.SetTimeout(A4964_SPEED_PERIOD);
        }
      } else if (temperature_timer_.Elapsed()) {
        if (TransmitWord(A4964_REG_READBACK_SEL | A4964_REGWRITE | A4964_READBACK_RBS_TEMP)) {
          comm_state_ = kA4964CommStateTemperatureReq;
          temperature_timer_.SetTimeout(A4964_TEMPERATURE_PERIOD);
        }
      }
      break;
    case kA4964CommStateSetpointPending:
      if (!spi_slave_.IsBusy())
        comm_state_ = kA4964CommStateIdle;
      break;
    case kA4964CommStateSpeedReq:
      if (!spi_slave_.IsBusy()) {
        TransmitWord(A4964_REG_READ_ONLY);
        comm_state_ = kA4964CommStateSpeedPending;
      }
      break;
    case kA4964CommStateTemperatureReq:
      if (!spi_slave_.IsBusy()) {
        TransmitWord(A4964_REG_READ_ONLY);
        comm_state_ = kA4964CommStateTemperaturePending;
      }
      break;
    case kA4964CommStateDiagnosticsReq:
      if (!spi_slave_.IsBusy()) {
        // Check the status word
        uint16_t tmp_word = (receive_buffer_[0] << 8u) | (receive_buffer_[1] & 0xFFu);
        if (__builtin_parity(tmp_word)) {
          CheckStatus(tmp_word & ~0x1u);
        }

        TransmitWord(A4964_REG_READ_ONLY);
        comm_state_ = kA4964CommStateDiagnosticsPending;
      }
      break;
    case kA4964CommStateSpeedPending:
      if (!spi_slave_.IsBusy()) {
        uint16_t tmp_word = (receive_buffer_[0] << 8u) | (receive_buffer_[1] & 0xFFu);
        if (__builtin_parity(tmp_word)) {
          raw_motor_speed_ = (tmp_word >> 1u) & 0x3FFu;
          motor_speed_timestamp_ = spi_timestamp_;
          // Perform diagnostic status check
          TransmitWord(A4964_REG_READBACK_SEL | A4964_REGWRITE | A4964_READBACK_RBS_DIAG);
          comm_state_ = kA4964CommStateDiagnosticsReq;
        } else {
          comm_state_ = kA4964CommStateIdle;
        }
      }
      break;
    case kA4964CommStateTemperaturePending:
      if (!spi_slave_.IsBusy()) {
        uint16_t tmp_word = (receive_buffer_[0] << 8u) | (receive_buffer_[1] & 0xFFu);
        if (__builtin_parity(tmp_word)) {
          CopyTempDataToMdata((tmp_word >> 1u) & 0x3FFu, measurement_data);
        }
        comm_state_ = kA4964CommStateIdle;
      }
      break;
    case kA4964CommStateDiagnosticsPending:
      if (!spi_slave_.IsBusy()) {
        uint16_t tmp_word = (receive_buffer_[0] << 8u) | (receive_buffer_[1] & 0xFFu);
        if (__builtin_parity(tmp_word)) {
          CheckDiagnostic(tmp_word & ~0x1u);
          CopySpeedDataToMdata(raw_motor_speed_, measurement_data);
        }
        comm_state_ = kA4964CommStateIdle;
      }
      break;
    default:
      break;
  }
}

void A4964::SetSpeed(double setpoint) {
  set_point_ = setpoint;
  set_point_available_ = true;

  if (state_ == kA4964StateArmed) {
    state_node_->SetState("ARMED");
    state_ = kA4964StateRunning;
    //! TODO(elias): Remove software WD workaround
    wd_timer_.SetCallback(SoftwareWdogCallback, this);
    //! TODO(elias): Remove software WD workaround
    wd_timer_.SetTimeout(100);
  }
  if (state_ == kA4964StateRunning) {
    ToggleWdog();
    //! TODO(elias): Remove software WD workaround
    wd_timer_.SetTimeout(100);
  }
}

void A4964::CommandCallback(std::string_view command) {
  if (command.compare("RESET") == 0) {
    Reset();
  } else if (command.compare("DISARM") == 0) {
    Disarm();
  } else if (command.compare("ARM") == 0) {
    Arm();
  }
}

/**************************************************************************************************
 *     Private Methods                                                                            *
 **************************************************************************************************/
void A4964::Reset() {
  comm_state_ = kA4964CommStateIdle;

  config_counter_ = 0;
  set_point_ = 0.0;
  out_set_point_ = 0;

  // TODO(elias): Remove software WD workaround
  TransmitWord(A4964_REG_WRITE_ONLY | ((out_set_point_ << 1u) & 0x7FFu));

  error_integral_ = 0.0;
  k_error_integral_ = 0.1;
  max_error_integral_ = MAX_INTEGRATOR_SPEED_OFFSET / k_error_integral_;
  speed_timer_.SetTimeout(A4964_SPEED_PERIOD);
  temperature_timer_.SetTimeout(A4964_TEMPERATURE_PERIOD);

  upper_limit_ = MAX_SPEED;
  lower_limit_ = 120.0;
  arm_threshold_ = 160.0;

  state_node_->SetState("UNINITIALIZED");
  state_ = kA4964StateConfig;
}

void A4964::Disarm() {
  if ((state_ == kA4964StateRamping) || (state_ == kA4964StateArmed) ||
      (state_ == kA4964StateRunning)) {
    out_set_point_ = 0;
    out_set_point_available_ = true;
    state_node_->SetState("DISARMED");
    state_ = kA4964StateDisarmed;
  }
}

void A4964::Arm() {
  if (state_ == kA4964StateDisarmed) {
    out_set_point_ = 0;
    out_set_point_available_ = true;
    state_node_->SetState("ARMED");
    state_ = kA4964StateRamping;
  } else if (state_ == kA4964StateProtected) {
    Logger::Report("NOT ARMED: REDUCE THRUST FIRST", Logger::kWarning);
  }
}

void A4964::RunConfig() {
  if (spi_slave_.IsBusy() && config_counter_ > 0) {
    return;
  }

  TransmitWord(register_config.array_type[config_counter_++]);

  if (config_counter_ ==
      sizeof(register_config.array_type) / sizeof(*register_config.array_type)) {
    config_counter_ = 0;
#ifdef A4964_CONFIG_2312E
    Logger::Report("Loaded 2312E configuration", Logger::kInfo);
#else
    Logger::Report("Loaded generic configuration", Logger::kInfo);
#endif
    state_node_->SetState("THRUST_PROTECTION");
    state_ = kA4964StateProtected;
  }
}

void A4964::CopySpeedDataToMdata(uint16_t speed, MeasurementData* measurement_data) {
  motor_speed_ = speed * A4964_RPM_SPEED_FACTOR;
  if (state_ == kA4964StateRunning) {
    error_integral_ = MAX(
        MIN(error_integral_ + (set_point_ - motor_speed_) * A4964_SPEED_PERIOD * 1e-3,
            max_error_integral_), -max_error_integral_);
  } else {
    error_integral_ = 0.0;
  }
  measurement_data->speed = motor_speed_;
  measurement_data->speed_valid = true;
  measurement_data->timestamp = motor_speed_timestamp_;
}

void A4964::CopyTempDataToMdata(uint16_t value, MeasurementData* measurement_data) {
  temp_ = 367.7f - value * 0.451f;
  measurement_data->temperature = temp_;
  measurement_data->temperature_valid = true;
  measurement_data->timestamp = spi_timestamp_;
}

void A4964::CheckStatus(uint16_t status_word) {
  if (status_word & A4964_STATUS_FF) {
    // Mask WD fault to prevent spamming
    if (status_word & (~A4964_STATUS_FF & ~A4964_STATUS_WD)) {
      Logger::Report("STATUS: General fault", Logger::kError);
    }
  }
  if (status_word & A4964_STATUS_POR) {
    Logger::Report("STATUS: Power-on-reset", Logger::kError);
  }
  if (status_word & A4964_STATUS_SE) {
    Logger::Report("STATUS: Serial transfer error", Logger::kError);
  }
  if (status_word & A4964_STATUS_VPU) {
    Logger::Report("STATUS: VPP undervoltage", Logger::kError);
  }
  if (status_word & A4964_STATUS_CLI) {
    Logger::Report("STATUS: Current limit", Logger::kError);
  }
  if (wd_flag_ != ((status_word & A4964_STATUS_WD) == A4964_STATUS_WD)) {
    wd_flag_ = ((status_word & A4964_STATUS_WD) == A4964_STATUS_WD);
    if (wd_flag_) {
      Logger::Report("STATUS: Watchdog", Logger::kError);
    }
  }
  if (status_word & A4964_STATUS_LOS) {
    Logger::Report("STATUS: Loss of BEMF synchonization", Logger::kError);
  }
  if (status_word & A4964_STATUS_OT) {
    Logger::Report("STATUS: Overtemperature", Logger::kError);
  }
  if (status_word & A4964_STATUS_TW) {
    Logger::Report("STATUS: Temperature warning", Logger::kError);
  }
  if (status_word & A4964_STATUS_VSU) {
    Logger::Report("STATUS: VBB undervoltage", Logger::kError);
  }
  if (status_word & A4964_STATUS_VRU) {
    Logger::Report("STATUS: VREG undervoltage", Logger::kError);
  }
  if (status_word & A4964_STATUS_VLU) {
    Logger::Report("STATUS: VLR undervoltage", Logger::kError);
  }
  if (status_word & A4964_STATUS_BU) {
    Logger::Report("STATUS: Bootstrap undervoltage", Logger::kError);
  }
  if (status_word & A4964_STATUS_VO) {
    Logger::Report("STATUS: VDS fault", Logger::kError);
  }
}

void A4964::CheckDiagnostic(uint16_t diagnostic_word) {
  if (diagnostic_word & A4964_DIAG_OSR) {
    // Adjust rawMotorSpeed when overspeed range is detected
    raw_motor_speed_ = raw_motor_speed_ | 0x400u;
    Logger::Report("DIAG: Over speed range", Logger::kError);
  }
  if (diagnostic_word & A4964_DIAG_BA) {
    Logger::Report("DIAG: Bootstrap fault on Ph. A high", Logger::kError);
  }
  if (diagnostic_word & A4964_DIAG_BB) {
    Logger::Report("DIAG: Bootstrap fault on Ph. B high", Logger::kError);
  }
  if (diagnostic_word & A4964_DIAG_BC) {
    Logger::Report("DIAG: Bootstrap fault on Ph. C high", Logger::kError);
  }
  if (diagnostic_word & A4964_DIAG_AH) {
    Logger::Report("DIAG: VDS fault on Ph. A high", Logger::kError);
  }
  if (diagnostic_word & A4964_DIAG_AL) {
    Logger::Report("DIAG: VDS fault on Ph. A low", Logger::kError);
  }
  if (diagnostic_word & A4964_DIAG_BH) {
    Logger::Report("DIAG: VDS fault on Ph. B high", Logger::kError);
  }
  if (diagnostic_word & A4964_DIAG_BL) {
    Logger::Report("DIAG: VDS fault on Ph. B low", Logger::kError);
  }
  if (diagnostic_word & A4964_DIAG_CH) {
    Logger::Report("DIAG: VDS fault on Ph. C high", Logger::kError);
  }
  if (diagnostic_word & A4964_DIAG_CL) {
    Logger::Report("DIAG: VDS fault on Ph. C low", Logger::kError);
  }
}

void A4964::SpiCallback(void *ctx) {
  auto *inst = reinterpret_cast<A4964 *>(ctx);
  inst->spi_timestamp_ = SystemTime::GetTimestamp();
}

bool A4964::TransmitWord(uint16_t word) {
  uint8_t odd_parity_bit = (!__builtin_parity(word)) & 0x1u;
  send_buffer_[0] = word >> 8u;
  send_buffer_[1] = word & 0xFFu;
  send_buffer_[1] |= odd_parity_bit;

  return spi_slave_.Transceive(2, &A4964::SpiCallback, this);
}

void A4964::ToggleWdog() {
  if (watchdog_timer_.Elapsed()) {
    watchdog_timer_.SetTimeout(A4964_WATCHDOG_MIN_TIMEOUT);
    gpio_wdog_.Write(!gpio_wdog_.Read());
  }
}

//! TODO(elias): Remove software WD workaround
void A4964::SoftwareWdogCallback(A4964* inst) {
  inst->Disarm();
}
