// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#ifndef SRC_DRIVERS_A4964_REGISTERS_HPP_
#define SRC_DRIVERS_A4964_REGISTERS_HPP_

#define A4964_REG_PWM_CONFIG0 0x0000
#define A4964_REG_PWM_CONFIG1 0x0800
#define A4964_REG_BRIDGE_CONFIG 0x1000
#define A4964_REG_GATE_CONFIG0 0x1800
#define A4964_REG_GATE_CONFIG1 0x2000
#define A4964_REG_GATE_CONFIG2 0x2800
#define A4964_REG_CURRENT_LIM 0x3000
#define A4964_REG_VDS_MON0 0x3800
#define A4964_REG_VDS_MON1 0x4000
#define A4964_REG_WATCHDOG_CONFIG0 0x4800
#define A4964_REG_WATCHDOG_CONFIG1 0x5000
#define A4964_REG_COMMUTATION0 0x5800
#define A4964_REG_COMMUTATION1 0x6000
#define A4964_REG_BEMF_CONFIG0 0x6800
#define A4964_REG_BEMF_CONFIG1 0x7000
#define A4964_REG_STARTUP_CONFIG0 0x7800
#define A4964_REG_STARTUP_CONFIG1 0x8000
#define A4964_REG_STARTUP_CONFIG2 0x8800
#define A4964_REG_STARTUP_CONFIG3 0x9000
#define A4964_REG_STARTUP_CONFIG4 0x9800
#define A4964_REG_STARTUP_CONFIG5 0xA000
#define A4964_REG_SPEED_LOOP0 0xA800
#define A4964_REG_SPEED_LOOP1 0xB000
#define A4964_REG_SPEED_LOOP2 0xB800
#define A4964_REG_NVM_WRITE 0xC000
#define A4964_REG_SYSTEM 0xC800
#define A4964_REG_PHASE_ADV 0xD000
#define A4964_REG_MOTOR_FUN 0xD800
#define A4964_REG_MASK 0xE000
#define A4964_REG_READBACK_SEL 0xE800
#define A4964_REG_WRITE_ONLY 0xF000
#define A4964_REG_READ_ONLY 0xF800

#define A4964_REGWRITE 0x0400

// Shift offsets of register values
#define A4964_PWM0_PW_OFFSET 1
#define A4964_PWM0_PMD_OFFSET 7
#define A4964_PWM0_MOD_OFFSET 9
#define A4964_PWM1_DS_OFFSET 1
#define A4964_PWM1_DD_OFFSET 5
#define A4964_PWM1_DP_OFFSET 7
#define A4964_BRIDGE_DT_OFFSET 1
#define A4964_BRIDGE_SA_OFFSET 7
#define A4964_GATE0_IR2_OFFSET 1
#define A4964_GATE0_IR1_OFFSET 5
#define A4964_GATE1_IF2_OFFSET 1
#define A4964_GATE1_IF1_OFFSET 5
#define A4964_GATE2_TFS_OFFSET 1
#define A4964_GATE2_TRS_OFFSET 5
#define A4964_CURLIM_VIL_OFFSET 1
#define A4964_CURLIM_OBT_OFFSET 5
#define A4964_VDSMON0_VT_OFFSET 1
#define A4964_VDSMON0_MIT_OFFSET 8
#define A4964_VDSMON1_VQT_OFFSET 1
#define A4964_VDSMON1_VDQ_OFFSET 8
#define A4964_WATCHDOG0_WM_OFFSET 1
#define A4964_WATCHDOG1_WW_OFFSET 1
#define A4964_WATCHDOG1_WC_OFFSET 6
#define A4964_COMMUT0_CI_OFFSET 1
#define A4964_COMMUT0_CP_OFFSET 5
#define A4964_COMMUT1_CIT_OFFSET 1
#define A4964_COMMUT1_CPT_OFFSET 5
#define A4964_BEMF0_BW_OFFSET 1
#define A4964_BEMF1_BF_OFFSET 1
#define A4964_BEMF1_BS_OFFSET 5
#define A4964_STARTUP0_HD_OFFSET 1
#define A4964_STARTUP0_HT_OFFSET 6
#define A4964_STARTUP1_HR_OFFSET 1
#define A4964_STARTUP1_KM_OFFSET 3
#define A4964_STARTUP1_RSC_OFFSET 8
#define A4964_STARTUP1_STM_OFFSET 9
#define A4964_STARTUP2_WBD_OFFSET 1
#define A4964_STARTUP2_WMF_OFFSET 5
#define A4964_STARTUP2_WIN_OFFSET 8
#define A4964_STARTUP3_SF1_OFFSET 1
#define A4964_STARTUP3_SF2_OFFSET 5
#define A4964_STARTUP4_SD1_OFFSET 1
#define A4964_STARTUP4_SD2_OFFSET 5
#define A4964_STARTUP5_SFS_OFFSET 1
#define A4964_STARTUP5_STS_OFFSET 5
#define A4964_SPEED0_SG_OFFSET 1
#define A4964_SPEED0_SGL_OFFSET 5
#define A4964_SPEED1_SR_OFFSET 1
#define A4964_SPEED1_DF_OFFSET 6
#define A4964_SPEED1_DV_OFFSET 8
#define A4964_SPEED2_SH_OFFSET 1
#define A4964_SPEED2_SL_OFFSET 5
#define A4964_NVM_SAV_OFFSET 8
#define A4964_SYSTEM_CM_OFFSET 1
#define A4964_SYSTEM_DIL_OFFSET 3
#define A4964_SYSTEM_IPI_OFFSET 4
#define A4964_SYSTEM_LWK_OFFSET 5
#define A4964_SYSTEM_OPM_OFFSET 6
#define A4964_SYSTEM_VRG_OFFSET 7
#define A4964_SYSTEM_VLR_OFFSET 8
#define A4964_SYSTEM_ESF_OFFSET 9
#define A4964_PHASEADV_PA_OFFSET 1
#define A4964_PHASEADV_KIP_OFFSET 7
#define A4964_PHASEADV_PAM_OFFSET 9
#define A4964_MOTFUN_RUN_OFFSET 1
#define A4964_MOTFUN_DIR_OFFSET 2
#define A4964_MOTFUN_BRK_OFFSET 3
#define A4964_MOTFUN_DRM_OFFSET 4
#define A4964_MOTFUN_OVM_OFFSET 5
#define A4964_MOTFUN_GTS_OFFSET 7
#define A4964_MOTFUN_LEN_OFFSET 8
#define A4964_MASK_VO_OFFSET 1
#define A4964_MASK_BU_OFFSET 2
#define A4964_MASK_VLU_OFFSET 3
#define A4964_MASK_VRU_OFFSET 4
#define A4964_MASK_VSU_OFFSET 5
#define A4964_MASK_TW_OFFSET 6
#define A4964_MASK_OT_OFFSET 7
#define A4964_MASK_LOS_OFFSET 8
#define A4964_MASK_WD_OFFSET 9
#define A4964_READBACK_RBS_OFFSET 1
#define A4964_READBACK_CKS_OFFSET 4
#define A4964_READBACK_LBR_OFFSET 5
#define A4964_READBACK_DSR_OFFSET 7
#define A4964_READBACK_DGS_OFFSET 8

// Currently important register values (can be extended)
#define A4964_PWM_CONF0_PW_20_1US 0x0000 << A4964_PWM0_PW_OFFSET
#define A4964_PWM_CONF0_PW_28_1US 0x000A << A4964_PWM0_PW_OFFSET
#define A4964_PWM_CONF0_PW_35_3US 0x0013 << A4964_PWM0_PW_OFFSET
#define A4964_PWM_CONF0_PW_50_5US 0x0026 << A4964_PWM0_PW_OFFSET
#define A4964_PWM_CONF0_PW_62_5US 0x0035 << A4964_PWM0_PW_OFFSET
#define A4964_PWM_CONF1_DP_0_2US 0x0000 << A4964_PWM1_DP_OFFSET
#define A4964_PWM_CONF1_DP_1US 0x0004 << A4964_PWM1_DP_OFFSET
#define A4964_PWM_CONF1_DD_1MS 0x0000 << A4964_PWM1_DD_OFFSET
#define A4964_PWM_CONF1_DD_10MS 0x0003 << A4964_PWM1_DD_OFFSET
#define A4964_PWM_CONF1_DS_OFF 0x0000 << A4964_PWM1_DS_OFFSET
#define A4964_PWM_CONF1_DS_7 0x0007 << A4964_PWM1_DS_OFFSET
#define A4964_PWM_CONF1_DS_15 0x000F << A4964_PWM1_DS_OFFSET
#define A4964_BRIDGE_SA_2_5 0x0000 << A4964_BRIDGE_SA_OFFSET
#define A4964_BRIDGE_DT_100NS 0x0002 << A4964_BRIDGE_DT_OFFSET
#define A4964_BRIDGE_DT_150NS 0x0003 << A4964_BRIDGE_DT_OFFSET
#define A4964_BRIDGE_DT_200NS 0x0004 << A4964_BRIDGE_DT_OFFSET
#define A4964_BRIDGE_DT_250NS 0x0005 << A4964_BRIDGE_DT_OFFSET
#define A4964_BRIDGE_DT_400NS 0x0008 << A4964_BRIDGE_DT_OFFSET
#define A4964_BRIDGE_DT_500NS 0x000A << A4964_BRIDGE_DT_OFFSET
#define A4964_BRIDGE_DT_800NS 0x0010 << A4964_BRIDGE_DT_OFFSET
#define A4964_BRIDGE_DT_1US 0x0014 << A4964_BRIDGE_DT_OFFSET
#define A4964_BRIDGE_DT_1_6US 0x0020 << A4964_BRIDGE_DT_OFFSET
#define A4964_GATE_CONF0_IR1_OFF 0x0000 << A4964_GATE0_IR1_OFFSET
#define A4964_GATE_CONF0_IR1_5MA 0x0001 << A4964_GATE0_IR1_OFFSET
#define A4964_GATE_CONF0_IR1_20MA 0x0004 << A4964_GATE0_IR1_OFFSET
#define A4964_GATE_CONF0_IR1_40MA 0x0008 << A4964_GATE0_IR1_OFFSET
#define A4964_GATE_CONF0_IR1_50MA 0x000A << A4964_GATE0_IR1_OFFSET
#define A4964_GATE_CONF0_IR1_60MA 0x000C << A4964_GATE0_IR1_OFFSET
#define A4964_GATE_CONF0_IR1_75MA 0x000F << A4964_GATE0_IR1_OFFSET
#define A4964_GATE_CONF0_IR2_OFF 0x0000 << A4964_GATE0_IR2_OFFSET
#define A4964_GATE_CONF0_IR2_5MA 0x0001 << A4964_GATE0_IR2_OFFSET
#define A4964_GATE_CONF0_IR2_20MA 0x0004 << A4964_GATE0_IR2_OFFSET
#define A4964_GATE_CONF0_IR2_40MA 0x0008 << A4964_GATE0_IR2_OFFSET
#define A4964_GATE_CONF0_IR2_50MA 0x000A << A4964_GATE0_IR2_OFFSET
#define A4964_GATE_CONF0_IR2_60MA 0x000C << A4964_GATE0_IR2_OFFSET
#define A4964_GATE_CONF0_IR2_75MA 0x000F << A4964_GATE0_IR2_OFFSET
#define A4964_GATE_CONF1_IF1_OFF 0x0000 << A4964_GATE1_IF1_OFFSET
#define A4964_GATE_CONF1_IF1_5MA 0x0001 << A4964_GATE1_IF1_OFFSET
#define A4964_GATE_CONF1_IF1_20MA 0x0004 << A4964_GATE1_IF1_OFFSET
#define A4964_GATE_CONF1_IF1_40MA 0x0008 << A4964_GATE1_IF1_OFFSET
#define A4964_GATE_CONF1_IF1_50MA 0x000A << A4964_GATE1_IF1_OFFSET
#define A4964_GATE_CONF1_IF1_60MA 0x000C << A4964_GATE1_IF1_OFFSET
#define A4964_GATE_CONF1_IF1_75MA 0x000F << A4964_GATE1_IF1_OFFSET
#define A4964_GATE_CONF1_IF2_OFF 0x0000 << A4964_GATE1_IF2_OFFSET
#define A4964_GATE_CONF1_IF2_5MA 0x0001 << A4964_GATE1_IF2_OFFSET
#define A4964_GATE_CONF1_IF2_40MA 0x0008 << A4964_GATE1_IF2_OFFSET
#define A4964_GATE_CONF1_IF2_50MA 0x000A << A4964_GATE1_IF2_OFFSET
#define A4964_GATE_CONF1_IF2_60MA 0x000C << A4964_GATE1_IF2_OFFSET
#define A4964_GATE_CONF1_IF2_75MA 0x000F << A4964_GATE1_IF2_OFFSET
#define A4964_GATE_CONF2_TRS_0NS 0x0000 << A4964_GATE2_TRS_OFFSET
#define A4964_GATE_CONF2_TFS_0NS 0x0000 << A4964_GATE2_TFS_OFFSET
#define A4964_GATE_CONF2_TRS_100NS 0x0002 << A4964_GATE2_TRS_OFFSET
#define A4964_GATE_CONF2_TFS_100NS 0x0002 << A4964_GATE2_TFS_OFFSET
#define A4964_GATE_CONF2_TRS_250NS 0x0005 << A4964_GATE2_TRS_OFFSET
#define A4964_GATE_CONF2_TFS_250NS 0x0005 << A4964_GATE2_TFS_OFFSET
#define A4964_CURRENT_LIM_OBT_1US 0x0000 << A4964_CURLIM_OBT_OFFSET
#define A4964_CURRENT_LIM_OBT_4_8US 0x0016 << A4964_CURLIM_OBT_OFFSET
#define A4964_CURRENT_LIM_VIL_1D16 0x0000 << A4964_CURLIM_VIL_OFFSET
#define A4964_CURRENT_LIM_VIL_5D16 0x0004 << A4964_CURLIM_VIL_OFFSET
#define A4964_CURRENT_LIM_VIL_8D16 0x0007 << A4964_CURLIM_VIL_OFFSET
#define A4964_CURRENT_LIM_VIL_1 0x000F << A4964_CURLIM_VIL_OFFSET
#define A4964_VDS_MON0_MIT_200MV 0x0000 << A4964_VDSMON0_MIT_OFFSET
#define A4964_VDS_MON0_MIT_100MV 0x0001 << A4964_VDSMON0_MIT_OFFSET
#define A4964_VDS_MON0_MIT_50MV 0x0002 << A4964_VDSMON0_MIT_OFFSET
#define A4964_VDS_MON0_MIT_25MV 0x0003 << A4964_VDSMON0_MIT_OFFSET
#define A4964_VDS_MON0_VT_1_25V 0x0019 << A4964_VDSMON0_VT_OFFSET
#define A4964_VDS_MON1_VDQ_DEB 0x0000 << A4964_VDSMON1_VDQ_OFFSET
#define A4964_VDS_MON1_VQT_3_15US 0x003F << A4964_VDSMON1_VQT_OFFSET
#define A4964_WATCHDOG0_WM_1MS 0x0000 << A4964_WATCHDOG0_WM_OFFSET
#define A4964_WATCHDOG1_WC_OFF 0x0000 << A4964_WATCHDOG1_WC_OFFSET
#define A4964_WATCHDOG1_WW_10MS 0x0000 << A4964_WATCHDOG1_WW_OFFSET
#define A4964_WATCHDOG1_WW_150MS 0x000E << A4964_WATCHDOG1_WW_OFFSET
#define A4964_COMMUT0_CP_1_2 0x0006 << A4964_COMMUT0_CP_OFFSET
#define A4964_COMMUT0_CP_1 0x0007 << A4964_COMMUT0_CP_OFFSET
#define A4964_COMMUT0_CP_2 0x0008 << A4964_COMMUT0_CP_OFFSET
#define A4964_COMMUT0_CP_4 0x0009 << A4964_COMMUT0_CP_OFFSET
#define A4964_COMMUT0_CI_1_8 0x0004 << A4964_COMMUT0_CI_OFFSET
#define A4964_COMMUT0_CI_1_2 0x0006 << A4964_COMMUT0_CI_OFFSET
#define A4964_COMMUT0_CI_1 0x0007 << A4964_COMMUT0_CI_OFFSET
#define A4964_COMMUT0_CI_2 0x0008 << A4964_COMMUT0_CI_OFFSET
#define A4964_COMMUT0_CI_4 0x0009 << A4964_COMMUT0_CI_OFFSET
#define A4964_COMMUT1_CPT_1_128 0x0000 << A4964_COMMUT1_CPT_OFFSET
#define A4964_COMMUT1_CPT_1_2 0x0006 << A4964_COMMUT1_CPT_OFFSET
#define A4964_COMMUT1_CPT_1 0x0007 << A4964_COMMUT1_CPT_OFFSET
#define A4964_COMMUT1_CPT_2 0x0008 << A4964_COMMUT1_CPT_OFFSET
#define A4964_COMMUT1_CPT_4 0x0009 << A4964_COMMUT1_CPT_OFFSET
#define A4964_COMMUT1_CPT_8 0x000A << A4964_COMMUT1_CPT_OFFSET
#define A4964_COMMUT1_CPT_16 0x000B << A4964_COMMUT1_CPT_OFFSET
#define A4964_COMMUT1_CPT_32 0x000C << A4964_COMMUT1_CPT_OFFSET
#define A4964_COMMUT1_CIT_1_128 0x0000 << A4964_COMMUT1_CIT_OFFSET
#define A4964_COMMUT1_CIT_1_2 0x0006 << A4964_COMMUT1_CIT_OFFSET
#define A4964_COMMUT1_CIT_1 0x0007 << A4964_COMMUT1_CIT_OFFSET
#define A4964_COMMUT1_CIT_2 0x0008 << A4964_COMMUT1_CIT_OFFSET
#define A4964_COMMUT1_CIT_4 0x0009 << A4964_COMMUT1_CIT_OFFSET
#define A4964_COMMUT1_CIT_8 0x000A << A4964_COMMUT1_CIT_OFFSET
#define A4964_COMMUT1_CIT_32 0x000C << A4964_COMMUT1_CIT_OFFSET
#define A4964_BEMF0_BW_1_4DEG 0x0000 << A4964_BEMF0_BW_OFFSET
#define A4964_BEMF0_BW_2_8DEG 0x0001 << A4964_BEMF0_BW_OFFSET
#define A4964_BEMF0_BW_4_2DEG 0x0002 << A4964_BEMF0_BW_OFFSET
#define A4964_BEMF0_BW_5_6DEG 0x0003 << A4964_BEMF0_BW_OFFSET
#define A4964_BEMF0_BW_7DEG 0x0004 << A4964_BEMF0_BW_OFFSET
#define A4964_BEMF0_BW_8_4DEG 0x0005 << A4964_BEMF0_BW_OFFSET
#define A4964_BEMF0_BW_9_8DEG 0x0006 << A4964_BEMF0_BW_OFFSET
#define A4964_BEMF0_BW_11_2DEG 0x0007 << A4964_BEMF0_BW_OFFSET
#define A4964_BEMF0_BW_12_6DEG 0x0008 << A4964_BEMF0_BW_OFFSET
#define A4964_BEMF0_BW_15_4DEG 0x000A << A4964_BEMF0_BW_OFFSET
#define A4964_BEMF0_BW_18_2DEG 0x000C << A4964_BEMF0_BW_OFFSET
#define A4964_BEMF0_BW_31DEG 0x001F << A4964_BEMF0_BW_OFFSET
#define A4964_BEMF1_BS_1 0x0000 << A4964_BEMF1_BS_OFFSET
#define A4964_BEMF1_BS_2 0x0001 << A4964_BEMF1_BS_OFFSET
#define A4964_BEMF1_BS_3 0x0002 << A4964_BEMF1_BS_OFFSET
#define A4964_BEMF1_BS_6 0x0003 << A4964_BEMF1_BS_OFFSET
#define A4964_BEMF1_BF_200US 0x0001 << A4964_BEMF1_BF_OFFSET
#define A4964_BEMF1_BF_1MS 0x0005 << A4964_BEMF1_BF_OFFSET
#define A4964_BEMF1_BF_10MS 0x000A << A4964_BEMF1_BF_OFFSET
#define A4964_STARTUP0_HT_0MS 0x0000 << A4964_STARTUP0_HT_OFFSET
#define A4964_STARTUP0_HT_200MS 0x0001 << A4964_STARTUP0_HT_OFFSET
#define A4964_STARTUP0_HD_0_03125 0x0000 << A4964_STARTUP0_HD_OFFSET
#define A4964_STARTUP0_HD_0_1875 0x0005 << A4964_STARTUP0_HD_OFFSET
#define A4964_STARTUP1_STM_OFF 0x0000 << A4964_STARTUP1_STM_OFFSET
#define A4964_STARTUP1_STM_ON 0x0001 << A4964_STARTUP1_STM_OFFSET
#define A4964_STARTUP1_RSC_OFF 0x0000 << A4964_STARTUP1_RSC_OFFSET
#define A4964_STARTUP1_KM_0_3 0x0000 << A4964_STARTUP1_KM_OFFSET
#define A4964_STARTUP1_KM_0_65 0x0007 << A4964_STARTUP1_KM_OFFSET
#define A4964_STARTUP1_KM_1_05 0x000F << A4964_STARTUP1_KM_OFFSET
#define A4964_STARTUP1_HR_0 0x0000 << A4964_STARTUP1_HR_OFFSET
#define A4964_STARTUP2_WIN_OFF 0x0000 << A4964_STARTUP2_WIN_OFFSET
#define A4964_STARTUP2_WIN_ON 0x0001 << A4964_STARTUP2_WIN_OFFSET
#define A4964_STARTUP2_WMF_0_4HZ 0x0000 << A4964_STARTUP2_WMF_OFFSET
#define A4964_STARTUP2_WMF_3_6HZ 0x0001 << A4964_STARTUP2_WMF_OFFSET
#define A4964_STARTUP2_WMF_6_8HZ 0x0002 << A4964_STARTUP2_WMF_OFFSET
#define A4964_STARTUP2_WMF_22_8HZ 0x0007 << A4964_STARTUP2_WMF_OFFSET
#define A4964_STARTUP2_WBD_0_0625 0x0000 << A4964_STARTUP2_WBD_OFFSET
#define A4964_STARTUP2_WBD_0_5 0x0007 << A4964_STARTUP2_WBD_OFFSET
#define A4964_STARTUP3_SF2_10HZ 0x0000 << A4964_STARTUP3_SF2_OFFSET
#define A4964_STARTUP3_SF2_27_5HZ 0x0007 << A4964_STARTUP3_SF2_OFFSET
#define A4964_STARTUP3_SF2_47_5HZ 0x000F << A4964_STARTUP3_SF2_OFFSET
#define A4964_STARTUP3_SF1_0_5HZ 0x0000 << A4964_STARTUP3_SF1_OFFSET
#define A4964_STARTUP3_SF1_1HZ 0x0001 << A4964_STARTUP3_SF1_OFFSET
#define A4964_STARTUP3_SF1_4HZ 0x0007 << A4964_STARTUP3_SF1_OFFSET
#define A4964_STARTUP3_SF1_8HZ 0x000F << A4964_STARTUP3_SF1_OFFSET
#define A4964_STARTUP4_SD1_0_0625 0x0000 << A4964_STARTUP4_SD1_OFFSET
#define A4964_STARTUP4_SD1_0_125 0x0001 << A4964_STARTUP4_SD1_OFFSET
#define A4964_STARTUP4_SD1_0_25 0x0003 << A4964_STARTUP4_SD1_OFFSET
#define A4964_STARTUP4_SD1_0_3125 0x0004 << A4964_STARTUP4_SD1_OFFSET
#define A4964_STARTUP4_SD2_0_0625 0x0000 << A4964_STARTUP4_SD2_OFFSET
#define A4964_STARTUP4_SD2_0_125 0x0001 << A4964_STARTUP4_SD2_OFFSET
#define A4964_STARTUP4_SD2_0_1875 0x0002 << A4964_STARTUP4_SD2_OFFSET
#define A4964_STARTUP4_SD2_0_25 0x0003 << A4964_STARTUP4_SD2_OFFSET
#define A4964_STARTUP4_SD2_0_3125 0x0004 << A4964_STARTUP4_SD2_OFFSET
#define A4964_STARTUP4_SD2_0_5 0x0007 << A4964_STARTUP4_SD2_OFFSET
#define A4964_STARTUP4_SD2_0_8125 0x000D << A4964_STARTUP4_SD2_OFFSET
#define A4964_STARTUP4_SD2_1 0x000F << A4964_STARTUP4_SD2_OFFSET
#define A4964_STARTUP5_STS_80MS 0x0004 << A4964_STARTUP5_STS_OFFSET
#define A4964_STARTUP5_STS_20MS 0x0001 << A4964_STARTUP5_STS_OFFSET
#define A4964_STARTUP5_STS_10MS 0x0000 << A4964_STARTUP5_STS_OFFSET
#define A4964_STARTUP5_SFS_0_0125HZ 0x0000 << A4964_STARTUP5_SFS_OFFSET
#define A4964_STARTUP5_SFS_0_1HZ 0x0003 << A4964_STARTUP5_SFS_OFFSET
#define A4964_STARTUP5_SFS_0_2HZ 0x0004 << A4964_STARTUP5_SFS_OFFSET
#define A4964_STARTUP5_SFS_0_4HZ 0x0005 << A4964_STARTUP5_SFS_OFFSET
#define A4964_STARTUP5_SFS_1HZ 0x0007 << A4964_STARTUP5_SFS_OFFSET
#define A4964_STARTUP5_SFS_2HZ 0x0009 << A4964_STARTUP5_SFS_OFFSET
#define A4964_STARTUP5_SFS_5HZ 0x000C << A4964_STARTUP5_SFS_OFFSET
#define A4964_STARTUP5_SFS_10HZ 0x000E << A4964_STARTUP5_SFS_OFFSET
#define A4964_STARTUP5_SFS_15HZ 0x000F << A4964_STARTUP5_SFS_OFFSET
#define A4964_SPEED0_SGL_6_3HZ 0x0000 << A4964_SPEED0_SGL_OFFSET
#define A4964_SPEED0_SGL_12_7HZ 0x0001 << A4964_SPEED0_SGL_OFFSET
#define A4964_SPEED0_SGL_25_5HZ 0x0003 << A4964_SPEED0_SGL_OFFSET
#define A4964_SPEED0_SGL_38_3HZ 0x0005 << A4964_SPEED0_SGL_OFFSET
#define A4964_SPEED0_SGL_51_5HZ 0x0007 << A4964_SPEED0_SGL_OFFSET
#define A4964_SPEED0_SGL_70_3HZ 0x000A << A4964_SPEED0_SGL_OFFSET
#define A4964_SPEED0_SGL_102_3HZ 0x000F << A4964_SPEED0_SGL_OFFSET
#define A4964_SPEED0_SGL_127_9HZ 0x0013 << A4964_SPEED0_SGL_OFFSET
#define A4964_SPEED0_SGL_147_1HZ 0x0016 << A4964_SPEED0_SGL_OFFSET
#define A4964_SPEED0_SGL_204_7HZ 0x001F << A4964_SPEED0_SGL_OFFSET
#define A4964_SPEED0_SG_1 0x0000 << A4964_SPEED0_SG_OFFSET
#define A4964_SPEED0_SG_11 0x0005 << A4964_SPEED0_SG_OFFSET
#define A4964_SPEED0_SG_21 0x000A << A4964_SPEED0_SG_OFFSET
#define A4964_SPEED0_SG_31 0x000F << A4964_SPEED0_SG_OFFSET
#define A4964_SPEED1_DV_OFF 0x0000 << A4964_SPEED1_DV_OFFSET
#define A4964_SPEED1_DV_12V 0x0001 << A4964_SPEED1_DV_OFFSET
#define A4964_SPEED1_DV_24V 0x0002 << A4964_SPEED1_DV_OFFSET
#define A4964_SPEED1_DF_1 0x0000 << A4964_SPEED1_DF_OFFSET
#define A4964_SPEED1_DF_2 0x0001 << A4964_SPEED1_DF_OFFSET
#define A4964_SPEED1_DF_5 0x0002 << A4964_SPEED1_DF_OFFSET
#define A4964_SPEED1_DF_10 0x0003 << A4964_SPEED1_DF_OFFSET
#define A4964_SPEED1_SR_0_1HZ 0x0000 << A4964_SPEED1_SR_OFFSET
#define A4964_SPEED1_SR_0_8HZ 0x0003 << A4964_SPEED1_SR_OFFSET
#define A4964_SPEED1_SR_1_6HZ 0x0004 << A4964_SPEED1_SR_OFFSET
#define A4964_SPEED1_SR_3_2HZ 0x0005 << A4964_SPEED1_SR_OFFSET
#define A4964_SPEED2_SL_12_8HZ \
  0x0001 << A4964_SPEED2_SL_OFFSET  // At 1.6 Hz resolution
#define A4964_SPEED2_SL_25_6HZ \
  0x0002 << A4964_SPEED2_SL_OFFSET  // At 1.6 Hz resolution
#define A4964_SPEED2_SL_38_4HZ \
  0x0003 << A4964_SPEED2_SL_OFFSET  // At 1.6 Hz resolution
#define A4964_SPEED2_SL_51_2HZ \
  0x0004 << A4964_SPEED2_SL_OFFSET  // At 1.6 Hz resolution
#define A4964_SPEED2_SH_1636_8HZ \
  0x0007 << A4964_SPEED2_SH_OFFSET  // At 1.6 Hz resolution
#define A4964_NVM_SAV 0x0002 << A4964_NVM_SAV_OFFSET
#define A4964_SYSTEM_ESF_OFF 0x0000 << A4964_SYSTEM_ESF_OFFSET
#define A4964_SYSTEM_VLR_3_3V 0x0000 << A4964_SYSTEM_VLR_OFFSET
#define A4964_SYSTEM_VRG_11V 0x0001 << A4964_SYSTEM_VRG_OFFSET
#define A4964_SYSTEM_OPM_SPI 0x0000 << A4964_SYSTEM_OPM_OFFSET
#define A4964_SYSTEM_LWK_PWM 0x0000 << A4964_SYSTEM_LWK_OFFSET
#define A4964_SYSTEM_IPI_ON 0x0000 << A4964_SYSTEM_IPI_OFFSET
#define A4964_SYSTEM_DIL_ON 0x0000 << A4964_SYSTEM_DIL_OFFSET
#define A4964_SYSTEM_CM_CL_SPEED 0x0000 << A4964_SYSTEM_CM_OFFSET
#define A4964_SYSTEM_CM_OL_SPEED 0x0003 << A4964_SYSTEM_CM_OFFSET
#define A4964_PHASE_ADV_PAM_AUTO 0x0001 << A4964_PHASEADV_PAM_OFFSET
#define A4964_PHASE_ADV_KIP_1 0x0000 << A4964_PHASEADV_KIP_OFFSET
#define A4964_PHASE_ADV_KIP_4 0x0002 << A4964_PHASEADV_KIP_OFFSET
#define A4964_PHASE_ADV_KIP_8 0x0003 << A4964_PHASEADV_KIP_OFFSET
#define A4964_PHASE_ADV_PA_0DEG 0x0000 << A4964_PHASEADV_PA_OFFSET
#define A4964_MOTOR_LIN_OFF 0x0000 << A4964_MOTFUN_LEN_OFFSET
#define A4964_MOTOR_GTS_NO 0x0000 << A4964_MOTFUN_GTS_OFFSET
#define A4964_MOTOR_OVM_NONE 0x0000 << A4964_MOTFUN_OVM_OFFSET
#define A4964_MOTOR_DRM_SINUS 0x0000 << A4964_MOTFUN_DRM_OFFSET
#define A4964_MOTOR_DRM_TRAPEZ 0x0001 << A4964_MOTFUN_DRM_OFFSET
#define A4964_MOTOR_BRK_OFF 0x0000 << A4964_MOTFUN_BRK_OFFSET
#define A4964_MOTOR_BRK_ON 0x0001 << A4964_MOTFUN_BRK_OFFSET
#define A4964_MOTOR_DIR_FORWARD 0x0000 << A4964_MOTFUN_DIR_OFFSET
#define A4964_MOTOR_RUN_ON 0x0001 << A4964_MOTFUN_RUN_OFFSET
#define A4964_MASK_WD_OFF 0x0001 << A4964_MASK_WD_OFFSET
#define A4964_MASK_WD_ON 0x0000 << A4964_MASK_WD_OFFSET
#define A4964_READBACK_DGS_FAULT 0x0000 << A4964_READBACK_DGS_OFFSET
#define A4964_READBACK_DSR_ON 0x0000 << A4964_READBACK_DSR_OFFSET
#define A4964_READBACK_LBR_10KHZ 0x0000 << A4964_READBACK_LBR_OFFSET
#define A4964_READBACK_CKS_HIZ 0x0000 << A4964_READBACK_CKS_OFFSET
#define A4964_READBACK_RBS_DIAG 0x0000 << A4964_READBACK_RBS_OFFSET
#define A4964_READBACK_RBS_SPEED 0x0001 << A4964_READBACK_RBS_OFFSET
#define A4964_READBACK_RBS_AVGCURR 0x0002 << A4964_READBACK_RBS_OFFSET
#define A4964_READBACK_RBS_VOLTAGE 0x0003 << A4964_READBACK_RBS_OFFSET
#define A4964_READBACK_RBS_TEMP 0x0004 << A4964_READBACK_RBS_OFFSET
#define A4964_READBACK_RBS_IN 0x0005 << A4964_READBACK_RBS_OFFSET
#define A4964_READBACK_RBS_PEAKDUTY 0x0006 << A4964_READBACK_RBS_OFFSET
#define A4964_READBACK_RBS_PHADV 0x0007 << A4964_READBACK_RBS_OFFSET

// Fault status flags
#define A4964_STATUS_FF 0x8000u
#define A4964_STATUS_POR 0x4000u
#define A4964_STATUS_SE 0x2000u
#define A4964_STATUS_VPU 0x1000u
#define A4964_STATUS_CLI 0x0800u
#define A4964_STATUS_WD 0x0200u
#define A4964_STATUS_LOS 0x0100u
#define A4964_STATUS_OT 0x0080u
#define A4964_STATUS_TW 0x0040u
#define A4964_STATUS_VSU 0x0020u
#define A4964_STATUS_VRU 0x0010u
#define A4964_STATUS_VLU 0x0008u
#define A4964_STATUS_BU 0x0004u
#define A4964_STATUS_VO 0x0002u

// Diagnostic flags
#define A4964_DIAG_OSR 0x0400u
#define A4964_DIAG_BA 0x0200u
#define A4964_DIAG_BB 0x0100u
#define A4964_DIAG_BC 0x0080u
#define A4964_DIAG_AH 0x0040u
#define A4964_DIAG_AL 0x0020u
#define A4964_DIAG_BH 0x0010u
#define A4964_DIAG_BL 0x0008u
#define A4964_DIAG_CH 0x0004u
#define A4964_DIAG_CL 0x0002u

#endif  // SRC_DRIVERS_A4964_REGISTERS_HPP_
