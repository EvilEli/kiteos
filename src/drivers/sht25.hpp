// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#ifndef SRC_DRIVERS_SHT25_HPP_
#define SRC_DRIVERS_SHT25_HPP_

#include "core/software_timer.hpp"
#include "hal/i2c/slave.hpp"

/*!
 *  \brief  Implements the Sht25 driver.
 */
class Sht25 {
 public:
  //! Structure containing the data measured by the Max11040k including the timestamp of the
  //! measurement.
  struct MeasurementData {
    double temperature = 0.0;  //!< Measured temperature.
    uint64_t temperature_timestamp = 0;  //!< Timestamp when the temperature was measured.
    double humidity = 0.0;  //!< Measured humidity.
    uint64_t humidity_timestamp = 0;  //!< Timestamp when the humidity was measured.
  };
  //! callback that is called if all channels are finished reading.
  typedef void (*DataReadyCallback)(MeasurementData measurement_data, void *ctx);

  /*!
   * \brief  Constructor of the Sht25-Component.
   *
   * \param  cb   callback that is called if all channels are finished reading.
   * \param  ctx  instance pointer.
   *
   * \return Returns constructed Sht25-instance.
   */
  Sht25(Sht25::DataReadyCallback cb, void *ctx);

   /*!
   * \brief Destructor of the Sht25-Component.
   */
  ~Sht25() = default;

 private:
  //! Method which checks the validity of the CRC field.
  bool CrcCheck();
  //! Function which is called to start the temperature conversion.
  static void StartTempConv(Sht25 *inst);
  //! Function which is called to start the humidity conversion.
  static void StartHumiConv(Sht25 *inst);
  //! Function that collects temperature measurements from Sht25
  static void PullTemperatureData(Sht25* instance);
  //! Function that collects humidity measurements from Sht25
  static void PullHumidityData(Sht25* instance);
  //! Function that invokes the user callback providing the new measurement.
  static void InvokeUserCallbackTask(Sht25* instance);

  //! Callback called when a reset command I2C transmission is finished.
  static void ResetStartedCallback(hal::i2c::Error error, void *cb_arg);
  //! Callback called when a temp conversion start I2C transmission is finished.
  static void TempConvStartedCallback(hal::i2c::Error error, void *cb_arg);
  //! Callback called when a humi conversion start I2C transmission is finished.
  static void HumiConvStartedCallback(hal::i2c::Error error, void *cb_arg);
  //! Callback called when temperature data transmission is finished.
  static void TempDataPulledCallback(hal::i2c::Error error, void *cb_arg);
  //! Callback called when humidity data transmission is finished.
  static void HumiDataPulledCallback(hal::i2c::Error error, void *cb_arg);

  static const uint8_t kCrcTable[256];  //!< Array holding LUT for CRC calculation.

  hal::i2c::Slave slave_;  //!< Slave used for I2C transmissions
  DataReadyCallback data_ready_callback_;  //!< instance of DataReadyCallback
  void* cb_arg_;                      //!< Argument provided to callback.
  uint8_t send_buffer_[3];            //!< Output buffer used for i2c-communication.
  uint8_t receive_buffer_[3];         //!< Input buffer used for i2c-communication.
  MeasurementData measurement_data_;  //!< Data storing sht25 temperature and humidity values.
  SoftwareTimer update_timer_;        //!< Timer that triggers a function on a timeout.
};
#endif  // SRC_DRIVERS_SHT25_HPP_
