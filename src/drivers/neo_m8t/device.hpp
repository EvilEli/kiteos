// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#ifndef SRC_DRIVERS_NEO_M8T_DEVICE_HPP_
#define SRC_DRIVERS_NEO_M8T_DEVICE_HPP_

#include "core/software_timer.hpp"
#include "hal/i2c/slave.hpp"
#include "ubx-utils/messages.hpp"
#include "ubx-utils/dispatcher.hpp"

namespace neo_m8t {

//! Define i2c tx buffer size in bytes.
static constexpr const uint16_t kTxBufferLength = 128;
//! Define i2c rx buffer size in bytes.
static constexpr const uint16_t kRxBufferLength = 255;

/*!
 * \brief  Driver for devices of the ublox-M8 family
 *
 * \author Elias Rosch
 *
 * \date   2020
 */
class Device {
 public:
  //! Dynamic models available to configure (CFG-NAV5).
  enum DynamicPlatformModel {
    kPortable = 0x0,
    kStationary = 0x2,
    kPedestrian = 0x3,
    kAutomotive = 0x4,
    kSea = 0x5,
    kAirborne1g = 0x6,
    kAirborne2g = 0x7,
    kAirborne4g = 0x8,
    kWristWorn = 0x9,
    kBike = 0xA,
  };

  //! Initialization structure defining how to configure the device.
  struct InitStruct {
    uint16_t navigation_rate;  //!< The navigation solution period to be configured.
    DynamicPlatformModel dynamic_model;  //!< The dynamic model to be configured.
    bool gps_enabled;          //!< If true, GPS is enabled.
    bool glonass_enabled;      //!< If true, Glonass is enabled.
    bool galileo_enabled;      //!< If true, Galileo is enabled.
    bool timepulse_0_enabled;  //!< If true, PPS pulses are generated on the timepulse0 pin.
    bool timepulse_1_enabled;  //!< If true, PPS pulses are generated on the timepulse1 pin.
    bool tim_tp_messages_enabled;     //!< If true, the device publishes TIM-TP messages.
    bool tim_tm2_messages_enabled;    //!< If true, the device publishes TIM-TM2 messages.
    bool nav_pvt_messages_enabled;    //!< If true, the device publishes NAV-PVT messages.
    bool rxm_sfrbx_messages_enabled;  //!< If true, the device publishes RXM-SFRBX messages.
    bool rxm_rawx_messages_enabled;   //!< If true, the device publishes RXM-RAWX messages.
  };

  /*!
   * \brief  Constructor of a Device object.
   *
   * \param  init_struct  Initialization structure to be used to configure the device.
   *
   * \return Returns constructed Device-instance.
   */
  explicit Device(InitStruct init_struct);

  /*!
   * \brief  Function used to register callback for different messages with user provided ctx.
   *         The provided ctx pointer will be provided in the invoked callback.
   *
   * \param  class_id  ClassId to subscribe to.
   * \param  cb_func   callback function with matching signature for desired message type.
   *
   * \return True if successfully registered. False otherwise.
   */
  bool Subscribe(ubx::ClassId class_id, ubx::Dispatcher::RawCallbackFunction cb_func) {
    return dispatcher_.Subscribe(class_id, cb_func);
  }

  /*!
   * \brief  Function used to register callback for different messages with user provided ctx.
   *         The provided ctx pointer will be provided in the invoked callback.
   *
   * \param  class_id  ClassId to subscribe to.
   * \param  cb_func   callback function with matching signature for desired message type.
   * \param  ctx       pointer to user context which will be provided in callback.
   *
   * \return True if successfully registered. False otherwise.
   */
  template<typename ctx_type>
  bool Subscribe(ubx::ClassId class_id, ubx::Dispatcher::RawCallbackCtxFunction<ctx_type> cb_func,
                 ctx_type *ctx) {
    return dispatcher_.Subscribe(class_id, cb_func, ctx);
  }

  /*!
   * \brief  Function used to register callback for different messages without ctx.
   *
   * \param  cb_func  callback function with matching signature for desired message type.
   *
   * \return True if successfully registered. False otherwise.
   */
  template<typename message_type>
  bool Subscribe(ubx::Dispatcher::CallbackFunction<message_type> cb_func) {
    return dispatcher_.Subscribe(cb_func);
  }

  /*!
   * \brief  Function used to register callback for different messages with user provided ctx.
   *         The provided ctx pointer will be provided in the invoked callback.
   *
   * \param  cb_func  callback function with matching signature for desired message type.
   * \param  ctx      pointer to user context which will be provided in callback.
   *
   * \return True if successfully registered. False otherwise.
   */
  template<typename message_type, typename ctx_type>
  bool Subscribe(ubx::Dispatcher::CallbackCtxFunction<message_type, ctx_type> cb_func,
                 ctx_type *ctx) {
    return dispatcher_.Subscribe(cb_func, ctx);
  }

  //! \brief Enables raw messages
  void EnableRaw();

  //! \brief Disables raw messages
  void DisableRaw();

 private:
  //! \brief Triggers the reset routine
  void Reset();
  //! Structure for a register state value which can be uninitialized.
  template<typename T>
  struct ConfigurationValue {
    T value;  //!< Value to be configured.
    bool initialized = false;  //!< Flag indicating that value has been initialized.
  };

  //! Structure describing the current configuration state of the device.
  struct Configuration {
    bool uart_protocol_disabled = false;           //!< Uart protocol disabled.
    ConfigurationValue<uint16_t> navigation_rate;  //!< Navigation solution period in ms.
    ConfigurationValue<DynamicPlatformModel> dynamic_model;  //!< Dynamic model for estimator.
    ConfigurationValue<bool> gps_enabled;         //!< Enable gps satellite system.
    ConfigurationValue<bool> glonass_enabled;     //!< Enable glonass satellite system.
    ConfigurationValue<bool> galileo_enabled;     //!< Enable galileo satellite system.
    ConfigurationValue<bool> timepulse_0_enabled;  //!< PPS pulse idx0 enable status.
    ConfigurationValue<bool> timepulse_1_enabled;  //!< PPS pulse idx1 enable status.
    ConfigurationValue<bool> tim_tp_enabled;      //!< TIM-TP message enable status.
    ConfigurationValue<bool> tim_tm2_enabled;     //!< TIM-TM2 message enable status.
    ConfigurationValue<bool> nav_pvt_enabled;     //!< NAV-PVT message enable status.
    ConfigurationValue<bool> rxm_sfrbx_enabled;   //!< RXM-SFRBX message enable status.
    ConfigurationValue<bool> rxm_rawx_enabled;    //!< RXM-RAWX message enable status.
    bool version_polled = false;                  //!< Version information polled.

    //! Constructor of the configuration structure.
    explicit Configuration(InitStruct init_struct);
  };

  //! Internal state of the ublox-M8 device.
  enum State {
    kStatePowerOnReset,
    kStateResetWait,
    kStateConfiguring,
    kStateIdle,
    kStateBytesAvReadPending,
    kStateDataReadPending,
  };

  //! Configures the I2C protocol to UBX.
  bool ConfigureI2cProtocol();
  //! Disables the Uart protocol.
  bool DisableUartProtocol();
  //! Configures the navigation engine.
  bool ConfigureNav5(uint8_t dynamic_model);
  //! Configures the GNSS system to use GPS.
  bool ConfigureGnssGps();
  //! Configures the GNSS system to use Glonass.
  bool ConfigureGnssGlonass();
  //! Configures the GNSS system to use Galileo.
  bool ConfigureGnssGalileo();
  //! Configures the Navigation Rate.
  bool ConfigureRate(uint16_t navigation_rate);
  //! Configures the time pulse.
  bool ConfigureTp5(uint8_t tp_idx);
  //! Enables messages of given class_id.
  bool EnableMessage(ubx::ClassId class_id);
  //! Generates poll request to MON-VER.
  bool ConfigurePollVersion();

  //! Method called when the reset timer elapsed.
  static void ResetTimerCallback(Device *inst);
  //! Method called when the update timer elapsed.
  static void UpdateTimerCallback(Device *inst);

  //! Callback invoked when bytes available request finishes.
  void BytesAvailableCallback();
  //! Callback invoked when data stream readout finishes.
  void DataCallback();
  //! Callback invoked when instance is in configuring state.
  void ConfigureCallback();

  //! Callback invoked when the I2C transmission finishes.
  static void I2cCallback(hal::i2c::Error error, void *cb_arg);
  //! Callback invoked when a NAK message is parsed.
  static void AckNakCallback(ubx::AckNak *ack_nak);
  //! Callback invoked when a MON-VER message is parsed.
  static void MonVerCallback(uint8_t *data, uint16_t size, Device *ctx);

  Configuration config_;   //!< Configuration of the ublox-m8 instance.
  hal::i2c::Slave slave_;  //!< Slave used for I2C transmissions.
  hal::Gpio reset_gpio_;   //!< Gpio used to reset the ublox-M8 device.
  uint8_t tx_buffer_[kTxBufferLength];  //!< Write buffer used for I2C transmissions.
  uint8_t rx_buffer_[kRxBufferLength];  //!< Read buffer used for I2C transmissions.
  State state_;                 //!< Internal state of the ublox-M8 device.
  uint16_t bytes_available_;    //!< Bytes available to read from ublox-M8 device.
  uint8_t transfer_len_;        //!< number of bytes read in last transmission.
  SoftwareTimer reset_timer_;   //!< Timer used to wait for the reset after power up.
  SoftwareTimer update_timer_;  //!< Timer used to periodically read out pending messages.

  ubx::Dispatcher dispatcher_;  //!< Dispatcher of UBX messages.
};

}  // namespace neo_m8t

#endif  // SRC_DRIVERS_NEO_M8T_DEVICE_HPP_
