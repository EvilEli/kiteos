// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#ifndef SRC_DRIVERS_NEO_M8T_UBX_UTILS_COMPOSER_HPP_
#define SRC_DRIVERS_NEO_M8T_UBX_UTILS_COMPOSER_HPP_

/*! \addtogroup ubx UBX
 * @{
 *  \addtogroup ubxComposer UBX Composer
 *
 * @{
 */
#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include "messages.hpp"

namespace ubx {

//! \brief     Composer of UBX frames
//
//  \detail    The ubx::Composer is used to compose byte sequences from structured data. A provided
//             ubx message struct is composed into a byte sequence within provided data buffers.
//
//  \author    Elias Rosch
//
//  \date      2020
class Composer {
 public:
  //! \brief    Compose a poll request for a given message type.
  //
  //  \param    buffer    data buffer where byte sequence is composed
  //  \param    class_id  message type thet should be polled
  //
  //  \return   size of composed byte sequence
  static uint8_t ComposePoll(uint8_t *const buffer, ClassId class_id);

  //! \brief    Compose a poll request for a CfgPrt message type.
  //
  //  \param    buffer   data buffer where byte sequence is composed
  //  \param    port_id  port id of CfgPrt to be polled
  //
  //  \return   size of composed byte sequence
  static uint8_t ComposePollCfgPrt(uint8_t *const buffer, uint8_t port_id);

  //! \brief    Compose a poll request for a CfgMsg message type.
  //
  //  \param    buffer   data buffer where byte sequence is composed
  //  \param    msg_class_id  ClassId of CfgMsg to be polled
  //
  //  \return   size of composed byte sequence
  static uint8_t ComposePollCfgMsg(uint8_t *const buffer, ClassId msg_class_id);

  //! \brief    Compose a message from provided struct.
  //
  //  \param    buffer data buffer where byte sequence is composed
  //  \param    msg  pointer to ubx message struct that should be composed
  //
  //  \return   size of composed byte sequence
  template<typename message_type>
  static uint8_t Compose(uint8_t *const buffer, message_type *msg) {
    uint16_t payload_size = sizeof(message_type);
    uint8_t msg_size = 0;
    msg_size += UbxHeader(buffer, message_type::class_id, payload_size);

    memcpy(buffer + msg_size, msg, sizeof(message_type));
    msg_size += sizeof(message_type);

    msg_size += UbxFooter(buffer, payload_size);
    return msg_size;
  }

  //! \brief Compose a CfgGnss message with provided GnssBlocks.
  //
  //  \param buffer data buffer where byte sequence is composed
  //  \param cfg_gnss_blocks pointer to CfgGnssBlock array that should be composed
  //  \param num number of CfgGnssBlocks in provided array
  //
  //  \return   size of composed byte sequence
  static uint8_t Compose(uint8_t *const buffer, CfgGnssBlock *cfg_gnss_blocks, uint8_t num);

  //! \brief Calculate the CRC value (using ubx specific polynom) iterating within ringbuffer
  //
  //  \param data pointer to buffer containing frame to calculate CRC for
  //  \param size payload size to calculate CRC
  //
  //  \return calculated CRC value
  static uint16_t FrameCrc(const uint8_t *const data, uint32_t size);

 private:
  //! \brief Generate UBX header for a message
  //
  //  \param buffer pointer to buffer to put the header to
  //  \param class_id ClassId of message to build the header for
  //  \param size payload size of message
  //
  //  \return number of bytes written
  static uint8_t UbxHeader(uint8_t *const buffer, ClassId class_id, uint8_t payload_size);

  //! \brief Generate UBX footer (aka CRC) for a message and place it after payload
  //
  //  \param buffer pointer to buffer where footer should be generated for
  //  \param size payload size of message
  //
  //  \return number of bytes written
  static uint8_t UbxFooter(uint8_t *const buffer, uint8_t payload_size);
};

}  // namespace ubx
/*! @} */
/*! @} */
#endif  // SRC_DRIVERS_NEO_M8T_UBX_UTILS_COMPOSER_HPP_
