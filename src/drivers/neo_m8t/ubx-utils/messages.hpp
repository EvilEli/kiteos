// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#ifndef SRC_DRIVERS_NEO_M8T_UBX_UTILS_MESSAGES_HPP_
#define SRC_DRIVERS_NEO_M8T_UBX_UTILS_MESSAGES_HPP_

/*! \addtogroup ubx UBX
 * @{
 *  \addtogroup ubxmessages UBX Messages
 *
 *  \brief      Definitions used within the UBX protocol.
 *
 *  \detail     This file contains definitions of UBX messages. That includes their class/id as well
 *              as their corresponding struct definitions. Dynamicly sized fields are provided as
 *              pointer to a buffer with the respective dynamic field size and the amount of dynamic
 *              fields.
 *  @{
 */

#include <cstdint>

/* ===========================================================================*/
/*                  Defines/Macros                                            */
/*=========================================================================== */
/*!
 * @name Sync Chars
 * This is the header of any ubx message
 * @{
 */
#define UBX_SYNC_CHAR_1 0xB5
#define UBX_SYNC_CHAR_2 0x62
/*! @} */
/*!
 * @name Port IDs
 * Valid definitions for portId
 * @{
 */
#define UBX_PORT_ID_DDC 0x0
#define UBX_PORT_ID_UART 0x1
#define UBX_PORT_ID_USB 0x3
#define UBX_PORT_ID_SPI 0x4
/*! @} */

/*!
 * @name UBX GNSS IDs
 * GNSS IDs for the different satellite systems.
 * @{
 */
#define UBX_GNSS_ID_GPS 0
#define UBX_GNSS_ID_SBAS 1
#define UBX_GNSS_ID_GALILEO 2
#define UBX_GNSS_ID_BEIDOU 3
#define UBX_GNSS_ID_IMES 4
#define UBX_GNSS_ID_QZSS 5
#define UBX_GNSS_ID_GLONASS 6
/*! @} */

/*!
 * @name UBX GNSS configuration flags
 * Flags for the UBX-CFG-GNSS blocks.
 * @{
 */
#define UBX_GNSS_CFG_ENABLE 0x01
#define UBX_GNSS_CFG_GPS_L1CA (0x01 << 16)
#define UBX_GNSS_CFG_GPS_L2C (0x10 << 16)
#define UBX_GNSS_CFG_SBAS_L1CA (0x01 << 16)
#define UBX_GNSS_CFG_GALILEO_E1 (0x01 << 16)
#define UBX_GNSS_CFG_GALILEO_E5B (0x20 << 16)
#define UBX_GNSS_CFG_BEIDOU_B1I (0x01 << 16)
#define UBX_GNSS_CFG_BEIDOU_B2I (0x10 << 16)
#define UBX_GNSS_CFG_IMES_L1 (0x01 << 16)
#define UBX_GNSS_CFG_QZSS_L1CA (0x01 << 16)
#define UBX_GNSS_CFG_QZSS_L1S (0x04 << 16)
#define UBX_GNSS_CFG_QZSS_L2C (0x10 << 16)
#define UBX_GNSS_CFG_GLONASS_L1 (0x01 << 16)
#define UBX_GNSS_CFG_GLONASS_L2 (0x10 << 16)
/*! @} */

namespace ubx {
/* ===========================================================================*/
/*                         Typedefs                                           */
/*=========================================================================== */
//! Class IDs of UBX messages.
enum ClassId {
  kInvalid = 0x0000,
  kMonVer = 0x0A04,
  kMonGnss = 0x0A28,
  kTimTp = 0x0D01,
  kTimTm2 = 0x0D03,
  kNavPosEcef = 0x0101,
  kNavPosLlh = 0x01026,
  kNavTimeGps = 0x0120,
  kNavPvt = 0x0107,
  kRxmSfrbX = 0x0213,
  kRxmRawX = 0x0215,
  kAckAck = 0x0501,
  kAckNak = 0x0500,
  kCfgPrt = 0x0600,
  kCfgMsg = 0x0601,
  kCfgRate = 0x0608,
  kCfgNav5 = 0x0624,
  kCfgTp5 = 0x0631,
  kCfgRxm = 0x0611,
  kCfgGnss = 0x063E,
};


// Ensure that the struct fields are densely packed
#pragma pack(push, 1)

//! Extension field of UBX-MON-VER
struct MonVerExtension {
  char extension[30];  //!< see UBX protocol specification manual
};

//! UBX-MON-VER
struct MonVer {
  char sw_version[30];    //!< see UBX protocol specification manual
  char hw_version[10];    //!< see UBX protocol specification manual
  MonVerExtension extension[];    //!< see UBX protocol specification manual
  static constexpr ClassId class_id = kMonVer;  //!< ClassId of the message
};

//! UBX-MON-GNSS
struct MonGnss {
  uint8_t version;    //!< see UBX protocol specification manual
  uint8_t supported;    //!< see UBX protocol specification manual
  uint8_t defaul_t;    //!< see UBX protocol specification manual
  uint8_t enabled;    //!< see UBX protocol specification manual
  uint8_t simultaneous;    //!< see UBX protocol specification manual
  uint8_t reserved1[3];    //!< see UBX protocol specification manual
  static constexpr ClassId class_id = kMonGnss;  //!< ClassId of the message
};

//! UBX-TIM-TP
struct TimTp {
  uint32_t tow_ms;    //!< see UBX protocol specification manual
  uint32_t tow_sub_ms;    //!< see UBX protocol specification manual
  int32_t q_err;    //!< see UBX protocol specification manual
  uint16_t week;    //!< see UBX protocol specification manual
  uint8_t flags;    //!< see UBX protocol specification manual
  uint8_t ref_info;    //!< see UBX protocol specification manual
  static constexpr ClassId class_id = kTimTp;  //!< ClassId of the message
};

//! Definition of the UBX-TIM-TM2 message
struct TimTm2 {
  uint8_t ch;  //!< Channel (i.e. EXTINT) upon which the pulse was measured.
  //! Bitmask (see table below).
  //! +-----------+------------------+-----------------------------------------------------------+
  //! | Bit 0     | mode             | 0 = single, 1 = running                                   |
  //! +-----------+------------------+-----------------------------------------------------------+
  //! | Bit 1     | run              | 0 = armed, 1 = stopped                                    |
  //! +-----------+------------------+-----------------------------------------------------------+
  //! | Bit 2     | new falling edge | New falling edge detected                                 |
  //! +-----------+------------------+-----------------------------------------------------------+
  //! | Bit 3 + 4 | time base        | 0 = Time base is Receiver time,                           |
  //! |           |                  | 1 = Time base is GNSS time (the system according to the   |
  //! |           |                  |     configuration in UBX-CFG-TP5 for tpIdx=0),            |
  //! |           |                  | 2 = Time base is UTC (the variant according to the        |
  //! |           |                  |     configuration in UBX-CFG-NAV5)                        |
  //! +-----------+------------------+-----------------------------------------------------------+
  //! | Bit 5     | utc              | 0 = UTC not available, 1 = UTC available                  |
  //! +-----------+------------------+-----------------------------------------------------------+
  //! | Bit 6     | time             | 0 = Time is not valid, 1 = Time is valid (Valid GNSS fix) |
  //! +-----------+------------------+-----------------------------------------------------------+
  //! | Bit 7     | new rising edge  | New rising edge detected                                  |
  //! +-----------+------------------+-----------------------------------------------------------+
  uint8_t flags;
  uint16_t count;  //!< Rising edge counter.
  uint16_t wn_r;  //!< Week number of last rising edge.
  uint16_t wn_f;  //!< Week number of last falling edge.
  uint32_t tow_ms_r;  //!< Tow of rising edge in milliseconds.
  uint32_t tow_sub_ms_r;  //!< Millisecond fraction of tow of rising edge in nanoseconds.
  uint32_t tow_ms_f;  //!< Tow of falling edge in milliseconds.
  uint32_t tow_sub_ms_f;  //!< Millisecond fraction of tow of falling edge in nanoseconds.
  uint32_t acc_est;  //!< Accuracy estimate in nanoseconds.
  static constexpr ClassId class_id = kTimTm2;  //!< ClassId of the message
};

//! UBX-NAV-POSECEF
struct NavPosEcef {
  uint32_t i_tow;    //!< see UBX protocol specification manual
  int32_t ecef_x;    //!< see UBX protocol specification manual
  int32_t ecef_y;    //!< see UBX protocol specification manual
  int32_t ecef_z;    //!< see UBX protocol specification manual
  uint32_t p_acc;    //!< see UBX protocol specification manual
  static constexpr ClassId class_id = kNavPosEcef;  //!< ClassId of the message
};

//! UBX-NAV-POSLLH
struct NavPosLlh {
  uint32_t i_tow;    //!< see UBX protocol specification manual
  int32_t lon;    //!< see UBX protocol specification manual
  int32_t lat;    //!< see UBX protocol specification manual
  int32_t height;    //!< see UBX protocol specification manual
  int32_t h_msl;    //!< see UBX protocol specification manual
  uint32_t h_acc;    //!< see UBX protocol specification manual
  uint32_t v_acc;    //!< see UBX protocol specification manual
  static constexpr ClassId class_id = kNavPosLlh;  //!< ClassId of the message
};

//! UBX-NAV-TIMEGPS
struct NavTimeGps {
  uint32_t i_tow;    //!< see UBX protocol specification manual
  int32_t f_tow;    //!< see UBX protocol specification manual
  int16_t week;    //!< see UBX protocol specification manual
  int8_t leap_s;    //!< see UBX protocol specification manual
  uint8_t valid;    //!< see UBX protocol specification manual
  uint32_t t_acc;    //!< see UBX protocol specification manual
  static constexpr ClassId class_id = kNavTimeGps;  //!< ClassId of the message
};

//! UBX-NAV-PVT
struct NavPvt {
  uint32_t i_tow;  //!< see UBX protocol specification manual
  uint16_t year;  //!< see UBX protocol specification manual
  uint8_t month;  //!< see UBX protocol specification manual
  uint8_t day;  //!< see UBX protocol specification manual
  uint8_t hour;  //!< see UBX protocol specification manual
  uint8_t min;  //!< see UBX protocol specification manual
  uint8_t sec;  //!< see UBX protocol specification manual
  uint8_t valid;  //!< see UBX protocol specification manual
  uint32_t t_acc;  //!< see UBX protocol specification manual
  int32_t nano;  //!< see UBX protocol specification manual
  uint8_t fix_type;  //!< see UBX protocol specification manual
  uint8_t flags;  //!< see UBX protocol specification manual
  uint8_t flags_2;  //!< see UBX protocol specification manual
  uint8_t num_sv;  //!< see UBX protocol specification manual
  int32_t lon;  //!< see UBX protocol specification manual
  int32_t lat;  //!< see UBX protocol specification manual
  int32_t height;  //!< see UBX protocol specification manual
  int32_t h_msl;  //!< see UBX protocol specification manual
  uint32_t h_acc;  //!< see UBX protocol specification manual
  uint32_t v_acc;  //!< see UBX protocol specification manual
  int32_t vel_n;  //!< see UBX protocol specification manual
  int32_t vel_e;  //!< see UBX protocol specification manual
  int32_t vel_d;  //!< see UBX protocol specification manual
  int32_t g_speed;  //!< see UBX protocol specification manual
  int32_t head_mot;  //!< see UBX protocol specification manual
  uint32_t s_acc;  //!< see UBX protocol specification manual
  uint32_t head_acc;  //!< see UBX protocol specification manual
  uint16_t p_dop;  //!< see UBX protocol specification manual
  uint8_t reserved1[6];  //!< see UBX protocol specification manual
  int32_t head_veh;  //!< see UBX protocol specification manual
  int16_t mag_dec;  //!< see UBX protocol specification manual
  uint16_t mag_acc;  //!< see UBX protocol specification manual
  static constexpr ClassId class_id = kNavPvt;  //!< ClassId of the message
};

//! measurement block of UBX-RXM-RAWX
struct RxmRawXMeas {
  double prMes;  //!< see UBX protocol specification manual
  double cpMes;  //!< see UBX protocol specification manual
  float doMes;  //!< see UBX protocol specification manual
  uint8_t gnssId;  //!< see UBX protocol specification manual
  uint8_t svId;  //!< see UBX protocol specification manual
  uint8_t reserved2;  //!< see UBX protocol specification manual
  uint8_t freqId;  //!< see UBX protocol specification manual
  uint16_t locktime;  //!< see UBX protocol specification manual
  uint8_t cno;  //!< see UBX protocol specification manual
  uint8_t prStdev;  //!< see UBX protocol specification manual
  uint8_t cpStdev;  //!< see UBX protocol specification manual
  uint8_t doStdev;  //!< see UBX protocol specification manual
  uint8_t trkStat;  //!< see UBX protocol specification manual
  uint8_t reserved3;  //!< see UBX protocol specification manual
};

//! UBX-RXM-RAWX
struct RxmRawX {
  double rcv_tow;  //!< see UBX protocol specification manual
  uint16_t week;  //!< see UBX protocol specification manual
  int8_t leap_s;  //!< see UBX protocol specification manual
  uint8_t num_meas;  //!< see UBX protocol specification manual
  uint8_t rec_stat;  //!< see UBX protocol specification manual
  uint8_t reserved1[3];  //!< see UBX protocol specification manual
  RxmRawXMeas meas[];  //!< see UBX protocol specification manual
  static constexpr ClassId class_id = kRxmRawX;  //!< ClassId of the message
};

//! UBX-ACK-ACK
struct AckAck {
  uint8_t msg_class;  //!< see UBX protocol specification manual
  uint8_t msg_id;  //!< see UBX protocol specification manual
  static constexpr ClassId class_id = kAckAck;  //!< ClassId of the message
};

//! UBX-ACK-NAK
struct AckNak {
  uint8_t msg_class;  //!< see UBX protocol specification manual
  uint8_t msg_id;  //!< see UBX protocol specification manual
  static constexpr ClassId class_id = kAckNak;  //!< ClassId of the message
};

//! UBX-CFG-PRT
struct CfgPrt {
  uint8_t port_id;  //!< see UBX protocol specification manual
  uint8_t reserved1;  //!< see UBX protocol specification manual
  uint16_t tx_ready;  //!< see UBX protocol specification manual
  uint32_t mode;  //!< see UBX protocol specification manual
  uint32_t baud_rate;  //!< see UBX protocol specification manual
  uint16_t in_proto_mask;  //!< see UBX protocol specification manual
  uint16_t out_proto_mask;  //!< see UBX protocol specification manual
  uint16_t flags;  //!< see UBX protocol specification manual
  uint16_t reserved2;  //!< see UBX protocol specification manual
  static constexpr ClassId class_id = kCfgPrt;  //!< ClassId of the message
};

//! UBX-CFG-MSG
struct CfgMsg {
  uint8_t msg_class;  //!< see UBX protocol specification manual
  uint8_t msg_id;  //!< see UBX protocol specification manual
  uint8_t rate[6];  //!< see UBX protocol specification manual
  static constexpr ClassId class_id = kCfgMsg;  //!< ClassId of the message
};

//! UBX-CFG-RATE
struct CfgRate {
  uint16_t meas_rate;  //!< see UBX protocol specification manual
  uint16_t nav_rate;  //!< see UBX protocol specification manual
  uint16_t time_ref;  //!< see UBX protocol specification manual
  static constexpr ClassId class_id = kCfgRate;  //!< ClassId of the message
};

//! UBX-CFG-NAV5
struct CfgNav5 {
  uint16_t mask;  //!< see UBX protocol specification manual
  uint8_t dyn_model;  //!< see UBX protocol specification manual
  uint8_t fix_mode;  //!< see UBX protocol specification manual
  int32_t fixed_alt;  //!< see UBX protocol specification manual
  uint32_t fixed_alt_var;  //!< see UBX protocol specification manual
  int8_t min_elev;  //!< see UBX protocol specification manual
  uint8_t dr_limit;  //!< see UBX protocol specification manual
  uint16_t p_dop;  //!< see UBX protocol specification manual
  uint16_t t_dop;  //!< see UBX protocol specification manual
  uint16_t p_acc;  //!< see UBX protocol specification manual
  uint16_t t_acc;  //!< see UBX protocol specification manual
  uint8_t static_hold_thresh;  //!< see UBX protocol specification manual
  uint8_t dgnss_timeout;  //!< see UBX protocol specification manual
  uint8_t cno_thresh_num_s_vs;  //!< see UBX protocol specification manual
  uint8_t cno_thresh;  //!< see UBX protocol specification manual
  uint8_t reserved1[2];  //!< see UBX protocol specification manual
  uint16_t static_hold_max_dist;  //!< see UBX protocol specification manual
  uint8_t utc_standard;  //!< see UBX protocol specification manual
  uint8_t reserved2[5];  //!< see UBX protocol specification manual
  static constexpr ClassId class_id = kCfgNav5;  //!< ClassId of the message
};

//! UBX-CFG-TP5
struct CfgTp5 {
  uint8_t tp_idx;  //!< see UBX protocol specification manual
  uint8_t version;  //!< see UBX protocol specification manual
  uint8_t reserved1[2];  //!< see UBX protocol specification manual
  int16_t ant_cable_delay;  //!< see UBX protocol specification manual
  int16_t rf_group_delay;  //!< see UBX protocol specification manual
  uint32_t freq_period;  //!< see UBX protocol specification manual
  uint32_t freq_period_lock;  //!< see UBX protocol specification manual
  uint32_t pulse_len_ratio;  //!< see UBX protocol specification manual
  uint32_t pulse_len_ratio_lock;  //!< see UBX protocol specification manual
  int32_t user_config_delay;  //!< see UBX protocol specification manual
  uint32_t flags;  //!< see UBX protocol specification manual
  static constexpr ClassId class_id = kCfgTp5;  //!< ClassId of the message
};

//! extension block of UBX-CFG-GNSS
struct CfgGnssBlock {
  uint8_t gnss_id;  //!< see UBX protocol specification manual     // UBX_GNSS_ID_xxx
  uint8_t res_trk_ch;  //!< see UBX protocol specification manual  // read only
  uint8_t max_trk_ch;  //!< see UBX protocol specification manual  // read only
  uint8_t reserved1;  //!< see UBX protocol specification manual
  uint32_t flags;  //!< see UBX protocol specification manual
};

//! UBX-CFG-GNSS
struct CfgGnss {
  uint8_t msg_ver;  //!< see UBX protocol specification manual         // default = 0
  uint8_t num_trk_ch_hw;  //!< see UBX protocol specification manual   // read only
  uint8_t num_trk_ch_use;  //!< see UBX protocol specification manual  // read only
  uint8_t num_config_blocks;  //!< see UBX protocol specification manual
  CfgGnssBlock config_blocks[];  //!< see UBX protocol specification manual
  static constexpr ClassId class_id = kCfgGnss;  //!< ClassId of the message
};

// Restore previous struct alignment setting
#pragma pack(pop)

}  // namespace ubx
/*! @} */
/*! @} */
#endif  // SRC_DRIVERS_NEO_M8T_UBX_UTILS_MESSAGES_HPP_
