// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#include "multi_access_handler.hpp"

namespace neo_m8t {

/**************************************************************************************************
 *     Private static member variables                                                            *
 **************************************************************************************************/
Device* MultiAccessHandler::device_ = nullptr;

/**************************************************************************************************
 *     Public methods                                                                             *
 **************************************************************************************************/
MultiAccessHandler::MultiAccessHandler() {
  // Lazy-create device instance
  if (device_ == nullptr) {
    device_ = new Device({.navigation_rate = 100, .dynamic_model = neo_m8t::Device::kAirborne2g,
                          .gps_enabled = true, .glonass_enabled = true, .galileo_enabled = true,
                          .timepulse_0_enabled = false, .timepulse_1_enabled = true,
                          .tim_tp_messages_enabled = true, .tim_tm2_messages_enabled = true,
                          .nav_pvt_messages_enabled = true,
                          .rxm_sfrbx_messages_enabled = false, .rxm_rawx_messages_enabled = false});
  }
}

bool MultiAccessHandler::Subscribe(ubx::ClassId class_id,
                                   ubx::Dispatcher::RawCallbackFunction cb_func) {
  return device_->Subscribe(class_id, cb_func);
}

}  // namespace neo_m8t
