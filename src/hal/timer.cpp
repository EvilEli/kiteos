// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.

#include <algorithm>
#include "hal/timer.hpp"
#include "utils/debug_utils.h"

namespace hal {
/**************************************************************************************************
 *     Global variables                                                                           *
 **************************************************************************************************/
const Timer::PeripheralConfig Timer::peripheral_config_[] = {
  { TIM1, TIM1_UP_IRQn, hal::Rcc::kTim1 },  // kDevice1
  { TIM2, TIM2_IRQn, hal::Rcc::kTim2 },  // kDevice2
  { TIM3, TIM3_IRQn, hal::Rcc::kTim3 },  // kDevice3
  { TIM4, TIM4_IRQn, hal::Rcc::kTim4 },  // kDevice4
  { TIM5, TIM5_IRQn, hal::Rcc::kTim5 },  // kDevice5
};

Timer* Timer::timer_registry_[TIMER_CNT] = {nullptr};

uint32_t Timer::in_clk_freq_ = 0;
/**************************************************************************************************
 *     API function implementations                                                               *
 **************************************************************************************************/
Timer::Timer(Device device)
  : device_(device),
    peripheral_(peripheral_config_[device].peripheral)  {
  ASSERT(!(timer_registry_[device]));
  timer_registry_[device] = this;
  in_clk_freq_ = Rcc::GetFrequency(Rcc::kClockHclk) / 1000000;
}

Timer::~Timer() {
  timer_registry_[device_] = nullptr;
}

void Timer::PeriodicCallback(uint16_t period_ms, CallbackFunction cb_func, void *cb_arg) {
  callback_.cb_func = cb_func;
  callback_.cb_arg = cb_arg;

  Rcc::EnablePeripheralClock(peripheral_config_[device_].rcc_clock);
  NVIC_EnableIRQ(peripheral_config_[device_].interrupt_no);

  // Configure timer: no clk-division, edge-aligned, upwards
  CLEAR_BIT(peripheral_->CR1, TIM_CR1_CKD | TIM_CR1_ARPE | TIM_CR1_CMS | TIM_CR1_DIR);

  // Set the Autoreload value
  peripheral_->ARR = (uint32_t) std::min<uint32_t>(65535, period_ms * 30);;
  // Set the Prescaler value 240 000 000 Hz / 8 000 = 30 000 Hz
  peripheral_->PSC = 8000 - 1;

  // TIM2 and TIM5 have a High auto-reload value
  // if (device_ == kDevice2 || device_ == kDevice5) {
  //   peripheral_->ARR = (uint32_t) period_ms * 200000;
  //   peripheral_->PSC = 0;
  // }

  // Clear repetition counter
  peripheral_->RCR = 0;
  peripheral_->EGR = TIM_EGR_UG;

  // Clear pending interrupts
  CLEAR_BIT(peripheral_->SR, TIM_SR_UIF);
  // Enable update interrupts
  SET_BIT(peripheral_->DIER, TIM_DIER_UIE);
  // Enable timer
  SET_BIT(peripheral_->CR1, TIM_CR1_CEN);
}

void Timer::PwmEnable(uint32_t period_us, double dutycycle) {
  ASSERT(dutycycle >= 0.0 && dutycycle <= 1.0);

  callback_.cb_func = nullptr;

  Rcc::EnablePeripheralClock(peripheral_config_[device_].rcc_clock);

  // Configure timer: no clk-division, edge-aligned, upwards
  CLEAR_BIT(peripheral_->CR1, TIM_CR1_CKD | TIM_CR1_ARPE | TIM_CR1_CMS | TIM_CR1_DIR);

  // Configure Output Compare 4 mode as PWM mode 1 ((CNT < CCR) ? HIGH : LOW)
  MODIFY_REG(peripheral_->CCMR2, TIM_CCMR2_OC4M, TIM_CCMR2_OC4M_1 | TIM_CCMR2_OC4M_2);

  // Clear repetition counter
  peripheral_->RCR = 0;

  PwmSet(period_us, dutycycle);

  // Enable capture compare output
  SET_BIT(peripheral_->CCER, TIM_CCER_CC4E);
  // Enable main output
  SET_BIT(peripheral_->BDTR, TIM_BDTR_MOE);
  // Enable timer
  SET_BIT(peripheral_->CR1, TIM_CR1_CEN);
}

void Timer::PwmSet(uint32_t period_us, double dutycycle) {
  ASSERT(dutycycle >= 0.0 && dutycycle <= 1.0);

  // If period is to long to fit into the capture compare register, use prescaler
  peripheral_->PSC = std::min<uint32_t>(65535, period_us * in_clk_freq_ / 65535);

  // Set the autoreload value (period)
  peripheral_->ARR = std::min<uint32_t>(65535, period_us * in_clk_freq_ / (peripheral_->PSC + 1));

  // Update the capture compare value (dutycycle) accordingly
  peripheral_->CCR4 = std::min<uint32_t>(65535, dutycycle * peripheral_->ARR);
}

void Timer::Disable() {
  Rcc::DisablePeripheralClock(peripheral_config_[device_].rcc_clock);
  NVIC_DisableIRQ(peripheral_config_[device_].interrupt_no);

  // Disable update interrupts
  CLEAR_BIT(peripheral_->DIER, TIM_DIER_UIE);
  // Disable timer
  CLEAR_BIT(peripheral_->CR1, TIM_CR1_CEN);
}

uint32_t Timer::GetValue() {
  return peripheral_->CNT;
}

void Timer::SetValue(uint32_t value) {
  peripheral_->CNT = value;
}


/**************************************************************************************************
 *     Private methods                                                                            *
 **************************************************************************************************/
void Timer::IRQHandler(Device device) {
  if (READ_BIT(peripheral_config_[device].peripheral->SR, TIM_SR_UIF)) {
    CLEAR_BIT(peripheral_config_[device].peripheral->SR, TIM_SR_UIF);
    Timer* timer = hal::Timer::timer_registry_[device];
    if (timer && timer->callback_.cb_func) {
        timer->callback_.cb_func(timer->callback_.cb_arg);
    }
  }
}
}  // namespace hal

/**************************************************************************************************
 *     Global Interrupt handler implementations                                                   *
 **************************************************************************************************/
extern "C" {
//! Interrupt handler of timer 1
void TIM1_IRQHandler() {
  hal::Timer::IRQHandler(hal::Timer::kDevice1);
}

//! Interrupt handler of timer 2
void TIM2_IRQHandler() {
  hal::Timer::IRQHandler(hal::Timer::kDevice2);
}

//! Interrupt handler of timer 3
void TIM3_IRQHandler() {
  hal::Timer::IRQHandler(hal::Timer::kDevice3);
}

//! Interrupt handler of timer 4
void TIM4_IRQHandler() {
  hal::Timer::IRQHandler(hal::Timer::kDevice4);
}

//! Interrupt handler of timer 5
void TIM5_IRQHandler() {
  hal::Timer::IRQHandler(hal::Timer::kDevice5);
}
}
// extern "C"
