// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#ifndef SRC_HAL_ETHERNET_HPP_
#define SRC_HAL_ETHERNET_HPP_

#include <cstdint>
#include "hal/gpio.hpp"


/**************************************************************************************************
 *     Defines                                                                                    *
 **************************************************************************************************/

#define HAL_ETHERNET_RX_DESCRIPTOR_COUNT ETH_RX_DESC_CNT  // Size of the Rx descriptor ring
#define HAL_ETHERNET_TX_DESCRIPTOR_COUNT ETH_TX_DESC_CNT  // Size of the Tx descriptor ring

// Size of each data buffer defining the maximum packet size
#define HAL_ETHERNET_BUFFER_SIZE ETH_BUF_SIZE


extern "C" {
void ETH_IRQHandler(void);
}
namespace hal {

//! Implements the hal Ethernet class which is used to access the ethernet peripheral.
class Ethernet {
 public:
  //! Duplex mode data type
  enum DuplexMode {
    kHalfDuplex = 0,
    kFullDuplex = 1
  };

  //! Ethernet speed data type
  enum EthernetSpeed {
    k10Mbps = 0,
    k100Mbps = 1
  };

  //! Structure for Rx buffers with variable length
  struct RxBuffer {
    uint32_t length;        //!< Size of the package available in the "data" buffer.
    uint8_t *data;          //!< Buffer containing the received packet.
    int data_buffer_index;  //!< Index to identify the static data buffer.
  };

  /*!
   * \brief Initializes the ethernet peripheral.
   *
   * @param phy_interface  Interface to use for PHY communication.
   */
  static void Init();

  /*!
   * \brief     Resumes the operation of the ethernet peripheral.
   *
   * \details   Resumes the operation of the ethernet peripheral after suspending it.
   *
   * \attention This is not the initialization. For enabling the ethernet peripheral for the first
   *            time, the function "hal_ethernet_init()" should be used.
   */
  static void Start();

  /*!
   * \brief Suspends the operation of the ethernet peripheral.
   */
  static void Stop();

  /*!
   * \brief  Sends a packet over ethernet.
   *
   * \param  tx_buffer         Data to send.
   * \param  tx_buffer_length  Size of the txBuffer.
   *
   * \return True on success, false if not enough space was available in the Tx queue.
   */
  static bool Send(uint8_t *tx_buffer, uint32_t tx_buffer_length);

  /*!
   * \brief     Gets a packet from the ethernet Rx queue.
   *
   * \attention The returned buffer is only a pointer to the Rx buffer ring. important information
   *            should be parsed fast, since the buffer may be overwritten by the DMA when new
   *            packets are received.
   *
   * \param     rx_buffer  Structure containing the received packet and further information about
   *                       it.
   *
   * \return    True on success, false if no packet was available in the Rx queue.
   */
  static bool Receive(RxBuffer *rx_buffer);

  /*!
   * \brief   Releases a receive buffer to the DMA.
   *
   * \details This function should be called when the data contained in the receive buffer is
   *          processed.
   *
   * \param   data_buffer_index  Index identifying the buffer.
   */
  static void ReleaseReceiveBuffer(int data_buffer_index);

  /*!
   * \brief   Sets the MAC duplex mode to a given value.
   *
   * \details Sets the MAC duplex mode to half-duplex or full-duplex, depending on the given
   *          parameter value.
   *
   * \param   duplex_mode  Duplex mode value to set the MAC to.
   */
  static void SetDuplexMode(DuplexMode duplex_mode);

  /*!
   * \brief   Sets the ethernet speed to a given value.
   *
   * \details Sets the ethernet speed to 10 Mbps or 100 Mbps, depending on the given parameter
   *          value.
   *
   * \param   speed  Speed value to set the ethernet speed to.
   */
  static void SetSpeed(EthernetSpeed speed);

  /*!
   * \brief Enables or disables the reception of multicast messages.
   *
   * \param enabled  If true, the reception of multicast messages is enabled, if false it is
   *                 disabled.
   */
  static void SetMulticastEnabled(bool enabled);

  /*!
   * \brief  Read a PHY register.
   *
   * \param  phy_address     PHY port address, must be a value from 0 to 31.
   * \param  phy_register    PHY register address, must be a value from 0 to 31.
   * \param  register_value  Parameter to hold read value.
   *
   * \return True on success, false otherwise.
   */
  static bool ReadPhyRegister(uint32_t phy_address, uint32_t phy_register,
                              uint32_t *register_value);

  /*!
   * \brief  Writes to a PHY register.
   *
   * \param  phy_address     PHY port address, must be a value from 0 to 31.
   * \param  phy_register    PHY register address, must be a value from 0 to 31.
   * \param  register_value  The value to write.
   *
   * \return True on success, false otherwise.
   */
  static bool WritePhyRegister(uint32_t phy_address, uint32_t phy_register,
                               uint32_t register_value);

  /*!
   * \brief   Registers a callback which is called when a Tx timestamp was captured.
   *
   * \details Registers a callback which is called when a Tx timestamp was captured. Timestamps are
   *          captured if the corresponding parameter is enabled in the send function.
   *
   * \param   tx_timestamp_callback  Callback to be called when a Tx timestamp was captured. The
   *                                 parameters are:
   *                                 (uint32_t timestampSeconds, uint32_t timestampNanoseconds,
   *                                 uint8_t messageType, uint16_t sequenceId).
   */
  static void RegisterTxTimestampCallback(
      void (*tx_timestamp_callback)(uint32_t, uint32_t, uint8_t, uint16_t));

  /*!
   * \brief Registers a callback which is called when an Rx timestamp was captured.
   *
   * \param rx_timestamp_callback  Callback to be called when an Rx timestamp was captured. The
   *                               parameters are:
   *                               (uint32_t timestampSeconds, uint32_t timestampNanoseconds,
   *                               uint8_t messageType, uint16_t sequenceId, uint64_t clockIdentity,
   *                               uint16_t portNumber).
   */
  static void RegisterRxTimestampCallback(
      void (*rx_timestamp_callback)(uint32_t, uint32_t, uint8_t, uint16_t, uint64_t, uint16_t));

  /*!
   * \brief   Sets an event message port that is used for message filtering.
   *
   * \details Sets an event message port that is used for message filtering. Timestamps will be
   *          taken for UDP messages using this port. If no port is defined, the standard PTP event
   *          message port will be used.
   *
   * \param   event_message_port  UDP port that is used for PTP event message filtering.
   */
  static void SetPtpEventMessagePort(uint16_t event_message_port);

  /*!
   * \brief   Gets the current system time.
   *
   * \details Gets the current system time of the ieee1588 timestamping unit and writes it into the
   *          seconds and nanoseconds arguments.
   */
  static void Ieee1588GetSystemTime(uint32_t *seconds, uint32_t *nanoseconds);

  /*!
   * \brief   Updates the current system time.
   *
   * \details Adds or subtracts a time to or from the current system time.
   *
   * \param   seconds      Number of seconds to add to or substract from the system time.
   * \param   nanoseconds  Number of nanoseconds to add to or substract from the system time. The
   *                       value should not exceed 0x3B9AC9FF. Higher values will be clipped.
   * \param   subtract     If true, the values in seconds and nanoseconds are substracted from the
   *                       system time. If false, the values are added.
   *
   * \return  True on success, false otherwise.
   */
  static bool Ieee1588CoarseSystemTimeCorrection(uint32_t seconds, uint32_t nanoseconds,
                                                 bool subtract);

  /*!
   * \brief   Rescales the current addend value.
   *
   * \details Rescales the current addend value with the rescaling factor. The addend value is used
   *          to generate the system time clock. A smaller addend value leads to a faster clock.
   *          This function may be used to correct the system time clock according to a reference
   *          clock.
   *
   * \param   scaling_factor  Factor to rescale the addend value with. Must be greater than zero.
   *
   * \return  True on success, false otherwise.
   */
  static bool Ieee1588RescaleAddend(double scaling_factor);

  //! MAC address for outgoing packets.
  static uint64_t mac_address_;
  static constexpr const uint32_t maximum_payload_size_ = 1500;  //!< Maximum Ethernet payload size

 private:
  static constexpr const uint32_t mdio_bus_timeout_ = 1000;  //!< PHY bus timeout.

  // Definitions for PTP packet parsing
  //! Specifies an IPv4 packet in the Ethernet header.
  static constexpr const uint16_t ethernet_type_ipv4_ = 0x0800;
  //! Specifies an UDP packet in the IP header.
  static constexpr const uint8_t ipv4_protocol_udp_ = 0x11;
  //! The UDP port which is used for PTP event messages according to the PTP specification.
  static constexpr const uint16_t standard_ptp_event_msg_port_ = 319;


  //! Information regarding packet. data fileds:
  //! ------------------------------------------------------------------------------------
  //! | Data valid[31] | not used[30:12] | PTP sequence ID[11:4] | PTP message type[3:0] |
  //! ------------------------------------------------------------------------------------

  // Descriptor packet info definitions
  //! Data valid flag of the packet info of an Ethernet descriptor.
  static constexpr const uint32_t packet_info_data_valid_ = 0x80000000;
  //! Position of the data valid flag in the packet info of an Ethernet descriptor.
  static constexpr const uint8_t packet_info_data_valid_pos_ = 31;
  //! Bits of the Ethernet descriptor packet info describing the PTP sequence ID.
  static constexpr const uint32_t packet_info_ptp_sequence_id_ = 0x000FFFF0;
  //! Position of the PTP sequence ID in the packet info of an Ethernet descriptor.
  static constexpr const uint8_t packet_info_ptp_sequence_id_pos_ = 4;
  //! Bits of the Ethernet descriptor packet info describing the PTP message type.
  static constexpr const uint32_t packet_info_ptp_message_type_ = 0x0000000F;

  // Timestamp control register (ETH_MACTSCR) definitions
  static constexpr const uint32_t ieee1588_tsena_ = 1;  //!< Enable timestamp
  //! Fine or coarse timestamp update
  static constexpr const uint32_t ieee1588_tscfupdt_ = 1u << 1u;
  static constexpr const uint32_t ieee1588_tsinit_ = 1u << 2u;  //!< Initialize timestamp
  static constexpr const uint32_t ieee1588_tsupdt_ = 1u << 3u;  //!< Update timestamp
  static constexpr const uint32_t ieee1588_tsaddreg_ = 1u << 5u;  //!< Update addend register
  //! Enable timestamp for all packets
  static constexpr const uint32_t ieee1588_tsenall_ = 1u << 8u;
  //! Timestamp digital or binary rollover control
  static constexpr const uint32_t ieee1588_tsctrlssr_ = 1u << 9u;
  //! Enable PTP packet processing for version 2 format
  static constexpr const uint32_t ieee1588_tsver2ena_ = 1u << 10u;
  //! Enable processing of PTP over ethernet packets
  static constexpr const uint32_t ieee1588_tsipena_ = 1u << 11u;
  //! Enable processing of PTP packets sent over IPv6-UDP
  static constexpr const uint32_t ieee1588_tsipv6ena_ = 1u << 12u;
  //! Enable processing of PTP packets sent over IPv4-UDP
  static constexpr const uint32_t ieee1588_tsipv4ena_ = 1u << 13u;
  //! Enable timestamp snapshot for event messages
  static constexpr const uint32_t ieee1588_tsevntena_ = 1u << 14u;
  //! Enable snapshot for messages relevant to master
  static constexpr const uint32_t ieee1588_tsmstrena_ = 1u << 15u;
  //! Select PTP packets for taking snapshots bit 0
  static constexpr const uint32_t ieee1588_snaptypsel0_ = 1u << 16u;
  //! Select PTP packets for taking snapshots bit 1
  static constexpr const uint32_t ieee1588_snaptypsel1_ = 1u << 17u;
  //! Enable MAC address for PTP packet filtering
  static constexpr const uint32_t ieee1588_tsenmacaddr_ = 1u << 18u;
  //! Enable checksum correction during OST for PTP over UDP/IPv4 packets
  static constexpr const uint32_t ieee1588_csc_ = 1u << 19u;
  //! Transmit timestamp status mode
  static constexpr const uint32_t ieee1588_txtsstsm_ = 1u << 24u;

  // PPS control register (ETH_MACPPSCR) definitions
  //! Target time register mode for PPS output bit 0
  static constexpr const uint32_t ieee1588_trgtmodsel00_ = 1u << 5u;
  //! Target time register mode for PPS output bit 1
  static constexpr const uint32_t ieee1588_trgtmodsel01_ = 1u << 6u;
  //! Flexible PPS output mode enable
  static constexpr const uint32_t ieee1588_ppsen0_ = 1u << 4u;

  static int dma_rx_descriptor_index_;  //!< Index of current Rx descriptor
  static int dma_tx_descriptor_index_;  //!< Index of current Tx descriptor

  //! Callback called when a ptp message was sent. The arguments are the timestamp seconds and
  //! nanoseconds field, the ptp message ID and the ptp sequence ID.
  static void (*tx_timestamp_callback_)(uint32_t, uint32_t, uint8_t, uint16_t);

  //! Callback called when a ptp message was received. The arguments are the timestamp seconds and
  //! nanoseconds field, the ptp message ID and sequence ID and the source port clock identity and
  //! port number.
  static void (*rx_timestamp_callback_)(uint32_t, uint32_t, uint8_t, uint16_t, uint64_t, uint16_t);

  static uint16_t ptp_event_message_port_;  //!< PTP event message port used for packet filtering

  static uint32_t ieee1588_addend_value_;  //!< Current value of the ieee1588 addednd register

  static Gpio::Config gpio_config_[];  //!< Gpio pins used for ethernet PHY
  static Gpio* gpio_[];  //!< Gpio pins used for ethernet PHY

  static void MacInit();   //!< Initializes the MAC
  static void DmaInit();   //!< Initializes the ethernet DMA

  //! Prepares an Rx descriptor for the DMA and releases it to the DMA
  static void ReleaseRxDescriptor(int descriptor_index);

  //! Returns the number of descriptors available for the application in the Tx descriptor queue
  static int GetAvailableTxDescriptorCount();

  static bool RxPacketAvailable();  //!< Checks if a packet is available in the Rx descriptor queue

  //! Checks if the message in the buffer is a IPv4 PTP event message
  static bool IsPtpEventMessage(uint8_t *message_buffer);

  //! Initializes the system time generation of the ieee1588 timestamping unit.
  static void Ieee1588Init();
  static void Ieee1588PpsInit(uint8_t ssinc);  //!< Initializes PPS pulse generation

  //! Configures the Clock range of ETH MDIO interface.
  static void SetMdioClockRange();

  friend void ::ETH_IRQHandler(void);  //!< Friend EXTI0 ISR for access to class members
};
}  // namespace hal

#endif  // SRC_HAL_ETHERNET_HPP_
