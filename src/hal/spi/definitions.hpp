// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#ifndef SRC_HAL_SPI_DEFINITIONS_HPP_
#define SRC_HAL_SPI_DEFINITIONS_HPP_

#include "hal/gpio.hpp"

namespace hal {
namespace spi {

//! SPI ports available on the microcontroller.
enum Port {
  kPort1,
  kPort2,
};

//! Available SPI clock polarities provided and handled by the SPI module.
enum Polarity {
  kPolarityLow,
  kPolarityHigh
};

//! Available SPI clock phases provided and handled by the SPI module.
enum Phase {
  kPhaseFirstEdge,
  kPhaseSecondEdge
};

//! \brief Available SPI clock speeds provided and handled by the SPI module.
//! \details Enum maps to actual MBR (master baud rate) values used in the SPI_CFG1 register.
enum Baud {
  kBaud250kHz = 0x7U << SPI_CFG1_MBR_Pos,
  kBaud500kHz = 0x6U << SPI_CFG1_MBR_Pos,
  kBaud1MHz = 0x5U << SPI_CFG1_MBR_Pos,
  kBaud2MHz = 0x4U << SPI_CFG1_MBR_Pos,
  kBaud4MHz = 0x3U << SPI_CFG1_MBR_Pos,
  kBaud8MHz = 0x2U << SPI_CFG1_MBR_Pos,
  kBaud16MHz = 0x1U << SPI_CFG1_MBR_Pos,
  kBaud32MHz = 0x0U << SPI_CFG1_MBR_Pos
};

//! Structure defining the peripheral config of a job
struct Config {
  Phase phase;        //!< Defines the clock phase parameter of a job
  Polarity polarity;  //!< Defines the clock polarity parameter of a job
  Baud baud;          //!< Defines the clock baud rate parameter of a job
  Gpio::Port chip_select_port;  //!< Defines the GPIO port used as chip select for a job
  Gpio::Pin chip_select_pin;    //!< Defines the GPIO pin used as chip select for a job
};

//! Typedef of function pointer used as callback by \ref hal_spi_xfer.
typedef void (*CallbackFunction)(void *cb_arg);

}  // namespace spi
}  // namespace hal

#endif  // SRC_HAL_SPI_DEFINITIONS_HPP_
