// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.

#include <stm32h7xx.h>
#include "hal/spi/device.hpp"
#include "utils/debug_utils.h"

namespace hal {
namespace spi {
/**************************************************************************************************
 *     Static member variables                                                                    *
 **************************************************************************************************/
Device* Device::device_registry_[2] = { nullptr };

const Device::PeripheralConfig Device::peripheral_config_[] = {
  // kPort1
  { SPI1,
    SPI1_IRQn,
    hal::Rcc::kSpi1,
    {Gpio::kPortA, Gpio::kPortD},
    {hal::Gpio::kPin5 | hal::Gpio::kPin6, hal::Gpio::kPin7},
    Gpio::Mode::kModeAF5Spi
  },
  // kPort2
  { SPI2,
    SPI2_IRQn,
    hal::Rcc::kSpi2,
    {Gpio::kPortA, Gpio::kPortB},
    {hal::Gpio::kPin12, hal::Gpio::kPin14 | hal::Gpio::kPin15},
    Gpio::Mode::kModeAF5Spi
  }
};

/**************************************************************************************************
 *     Public member functions                                                                    *
 **************************************************************************************************/
Device::Device(Port port) : gpios_{ {peripheral_config_[port].gpio_port[0],
                                     peripheral_config_[port].gpio_pins[0],
                                     peripheral_config_[port].gpio_mode },
                                    {peripheral_config_[port].gpio_port[1],
                                     peripheral_config_[port].gpio_pins[1],
                                     peripheral_config_[port].gpio_mode } } {
      ASSERT(!device_registry_[port]);
      device_registry_[port] = this;

      port_ = port;
      peripheral_ = peripheral_config_[port_].base;
      busy_ = false;

      Rcc::EnablePeripheralClock(peripheral_config_[port_].rcc_clock);
      NVIC_EnableIRQ(peripheral_config_[port_].interrupt_no);
}

Device::~Device() {
  device_registry_[port_] = nullptr;
}

bool Device::Transceive(Job* job) {
  if (job_queue_.IsFull()) {
    return false;
  }
  job->busy = true;
  // Add element to the queue
  bool ret = job_queue_.WriteElement(job);
  // Start the job immediately if port is idle
  StartNextJob();
  return ret;
}

Device* Device::GetDevice(Port port) {
  if (!device_registry_[port]) {
    device_registry_[port] = new Device(port);
  }
  return device_registry_[port];
}

void Device::FreeRegistry() {
  for (auto i : device_registry_) {
    delete i;
  }
}

/**************************************************************************************************
 *     Private member functions                                                                   *
 **************************************************************************************************/
void Device::StartNextJob() {
  if ((!busy_) &&
      (!job_queue_.IsEmpty())) {
    if (job_queue_.ReadElement(&current_job_)) {
      StartTransceive();
    }
  }
}

void Device::StartTransceive() {
  ASSERT(current_job_->tx_buffer);
  ASSERT(current_job_->rx_buffer);
  ASSERT(current_job_->len);

  busy_ = true;
  tx_count_ = 0;
  rx_count_ = 0;

  uint32_t cpol_bit = 0x00000000;
  if (current_job_->config.polarity == kPolarityHigh) { cpol_bit = SPI_CFG2_CPOL; }
  uint32_t cpha_bit = 0x00000000;
  if (current_job_->config.phase == kPhaseSecondEdge) { cpha_bit = SPI_CFG2_CPHA; }

  // Disable the selected SPI peripheral
  CLEAR_BIT(peripheral_->CR1, SPI_CR1_SPE);
  // Set internal SS level manually (because of software NSS mode)
  SET_BIT(peripheral_->CR1, SPI_CR1_SSI);

  // Baud prescaler, no CRC, no DMA, lowest fifo threshold, 8 bit words
  WRITE_REG(peripheral_->CFG1, current_job_->config.baud |
                               0x7U << SPI_CFG1_DSIZE_Pos);

  // Software Slave Management, clock polarity, clock phase, mode, Keep IO State
  WRITE_REG(peripheral_->CFG2, SPI_CFG2_SSM |
                               cpol_bit |
                               cpha_bit |
                               SPI_CFG2_MASTER |
                               SPI_CFG2_AFCNTR);

  // Set the number of data at current transfer
  MODIFY_REG(peripheral_->CR2, SPI_CR2_TSIZE, current_job_->len);

  // Clear interrupts before enabling peripheral
  CLEAR_BIT(peripheral_->IER, SPI_IER_TSERFIE |  // Additional number of transactions
                              SPI_IER_MODFIE  |  // Mode fault flag
                              SPI_IER_TIFREIE |  // Frame error
                              SPI_IER_CRCEIE  |  // Frame error
                              SPI_IER_TXTFIE  |  // Transmission transfer filled
                              SPI_IER_UDRIE);    // Underrun
  // Enable SPI peripheral
  SET_BIT(peripheral_->CR1 , SPI_CR1_SPE);
  SET_BIT(peripheral_->IER, SPI_IER_OVRIE  |  // Overrun
                            SPI_IER_EOTIE  |  // End of transfer
                            SPI_IER_DXPIE  |  // Duplex packet
                            SPI_IER_RXPIE  |  // RX fifo completed
                            SPI_IER_TXPIE);   // TX fifo empty


  current_job_->chip_select.Write(false);
  // Master transfer start
  SET_BIT(peripheral_->CR1, SPI_CR1_CSTART);
}

void Device::FinishTransceive() {
  // Reset chip select
  current_job_->chip_select.Write(true);
  // Disable SPI peripheral
  CLEAR_BIT(peripheral_->CR1 , SPI_CR1_SPE);
  busy_ = false;
  current_job_->busy = false;
  // Call user callback if available
  if (current_job_->cb_func) {
    current_job_->cb_func(current_job_->cb_arg);
  }
  // Start next job in queue
  StartNextJob();
}

void Device::TxIsr8Bit() {
  // Transmit data in 8 Bit mode
  *(volatile uint8_t *)&peripheral_->TXDR = current_job_->tx_buffer[tx_count_];
  tx_count_++;

  // Disable IT if no more data expected
  if (tx_count_ == current_job_->len) {
    // Disable TXP interrupts
    CLEAR_BIT(peripheral_->IER, SPI_IER_TXPIE);
  }
}

void Device::RxIsr8Bit() {
  // Receive data in 8 Bit mode
  current_job_->rx_buffer[rx_count_] = (*(volatile uint8_t *)&peripheral_->RXDR);
  rx_count_++;

  // Disable IT if no more data expected
  if (rx_count_ == current_job_->len) {
    // Disable RXP interrupts
    CLEAR_BIT(peripheral_->IER, SPI_IER_RXPIE);
  }
}

void Device::IRQHandler() {
  uint32_t itsource = peripheral_->IER;
  uint32_t itflag   = peripheral_->SR;
  uint32_t trigger  = itsource & itflag;
  bool handled = false;

  // /// SPI transmitting and receiving
  if (!READ_BIT(trigger, SPI_SR_OVR) &&
      READ_BIT(trigger, SPI_SR_DXP)) {
    TxIsr8Bit();
    RxIsr8Bit();
    handled = true;
  }

  // SPI receiving
  if (!READ_BIT(trigger, SPI_SR_OVR) &&
      READ_BIT(trigger, SPI_SR_RXP) &&
      !READ_BIT(trigger, SPI_SR_DXP)) {
    RxIsr8Bit();
    handled = true;
  }

  // SPI transmitting
  if (!READ_BIT(trigger, SPI_SR_OVR) &&
      READ_BIT(trigger, SPI_SR_TXP) &&
      !READ_BIT(trigger, SPI_SR_DXP)) {
    TxIsr8Bit();
    handled = true;
  }

  if (handled) {
    return;
  }

  // SPI in Error Treatment
  if ((itflag & (SPI_SR_OVR)) != 0UL) {
    // SPI Overrun error interrupt occurred. Clear it.
    SET_BIT(peripheral_->IFCR, SPI_IFCR_OVRC);
    // Restart the job
    Device::StartTransceive();
    return;
  }

  // SPI End Of Transfer
  if (READ_BIT(itflag, SPI_SR_EOT)) {
    /// Clear end-of-transfer flag */
    SET_BIT(peripheral_->IFCR , SPI_IFCR_EOTC);
    // Clear transfer filled complete flag
    SET_BIT(peripheral_->IFCR , SPI_IFCR_TXTFC);

    // Disable EOT interrupt
    CLEAR_BIT(peripheral_->IER, SPI_IER_EOTIE);
    FinishTransceive();
    return;
  }
}
}  // namespace spi
}  // namespace hal


/**************************************************************************************************
 *     Global Interrupt handler implementations                                                   *
 **************************************************************************************************/
extern "C" {

//! Interrupt handler of SPI1
void SPI1_IRQHandler() {
  hal::spi::Device::device_registry_[hal::spi::kPort1]->IRQHandler();
}

//! Interrupt handler of SPI2
void SPI2_IRQHandler() {
  hal::spi::Device::device_registry_[hal::spi::kPort2]->IRQHandler();
}

}  // extern "C"
