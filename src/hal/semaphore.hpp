// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#ifndef SRC_HAL_SEMAPHORE_HPP_
#define SRC_HAL_SEMAPHORE_HPP_

#include <stm32h7xx.h>


extern "C" {
void HSEM1_IRQHandler();  //!< Hardware semaphore global interrupt handler.
}


namespace hal {

/*!
 *  \brief   Implements the HAL semaphore class which is used to access the semaphore peripheral.
 */
class Semaphore {
 public:
  typedef void (*ClearCallback)(void*);  //!< Function type used for clear callbacks.

  /*!
   * \brief Constructor of the semaphore class. Instantiates a semaphore with a clear callback.
   *
   * \param clear_callback     Callback which is called when the Semaphore is cleared.
   * \param callback_argument  Argument to be passed to the clear callback.
   */
  Semaphore(ClearCallback clear_callback, void* callback_argument);

  /*!
   * \brief Constructor of the semaphore class. Instantiates a semaphore without a clear callback.
   */
  Semaphore() : Semaphore(nullptr, nullptr) {}

  /*!
   * \brief Destructor of the semaphore class.
   */
  ~Semaphore();

  /*!
   * \brief  Checks if the semaphore is currently locked.
   *
   * \return True if the semaphore is currently locked, false otherwise.
   */
  bool IsLocked();

  /*!
   * \brief  Tries to lock the semaphore.
   *
   * \param  process_id  ID of the process trying to lock the semaphore.
   *
   * \return True if the semaphore was successfully locked,
   *         false if the semaphore was already locked by another process.
   */
  bool Lock(uint8_t process_id);

  /*!
   * \brief  Tries to clear the semaphore.
   *
   * \param  process_id  ID of the process trying to clear the semaphore.
   *
   * \return True if the semaphore was successfully cleared,
   *         false if the semaphore was locked by another process.
   */
  bool Clear(uint8_t process_id);

 private:
  //! Holds the global registry of semaphore instances for every hardware semaphore.
  static Semaphore* registry_[];

  uint32_t device_index_;  //!< Device index of the semaphore hardware handled by this instance.
  ClearCallback clear_callback_;   //!< Callback called when the semaphore is cleared.
  void *clear_callback_argument_;  //!< Argument passed to the clear callback.

  friend void ::HSEM1_IRQHandler();  //!< Friended ISR to get access to class members.
};

}  // namespace hal

#endif  // SRC_HAL_SEMAPHORE_HPP_
