// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.

#include "utils/debug_utils.h"
#include "hal/uart.hpp"

namespace hal {
/**************************************************************************************************
 *     Static member variables                                                                    *
 **************************************************************************************************/
const Uart::PeripheralConfig Uart::peripheral_config_[] = {
  // kPort1
  { USART1,
    USART1_IRQn,
    hal::Rcc::kUsart1,
    Gpio::kPortA,
    hal::Gpio::kPin9 | hal::Gpio::kPin10,
    Gpio::Mode::kModeAF7Uart,
  },
  // kPort4
  { UART4,
    UART4_IRQn,
    hal::Rcc::kUart4,
    Gpio::kPortD,
    hal::Gpio::kPin0 | hal::Gpio::kPin1,
    Gpio::Mode::kModeAF8Uart,
  },
};

//! Static Uart devices available to use
Uart* Uart::device_registry_[2] = { nullptr };

/**************************************************************************************************
 *     Public member functions                                                                    *
 **************************************************************************************************/
Uart::Uart(Port port, uint32_t baud_rate)
  : port_(port),
    gpio_(peripheral_config_[port_].gpio_port,
          peripheral_config_[port_].gpio_pins,
          peripheral_config_[port_].gpio_mode),
    busy_(false),
    tx_length_(0),
    callback_{0} {
  ASSERT(device_registry_[port] == nullptr);
  device_registry_[port] = this;

  peripheral_ = peripheral_config_[port_].base;

  Rcc::EnablePeripheralClock(peripheral_config_[port_].rcc_clock);
  NVIC_EnableIRQ(peripheral_config_[port_].interrupt_no);

  // Disable peripheral
  CLEAR_BIT(peripheral_->CR1, USART_CR1_UE);

  // Ensure configuration for asynchronous full-duplex operation
  CLEAR_BIT(peripheral_->CR2, (USART_CR2_LINEN | USART_CR2_CLKEN));
  CLEAR_BIT(peripheral_->CR3, (USART_CR3_SCEN | USART_CR3_HDSEL | USART_CR3_IREN));

  // Configure 8 Bit wordlength
  MODIFY_REG(peripheral_->CR1, USART_CR1_M1, 0x0);
  // Configure no parity
  MODIFY_REG(peripheral_->CR1, USART_CR1_PCE, 0x0);
  // Configure TX and RX enabled
  MODIFY_REG(peripheral_->CR1, USART_CR1_TE | USART_CR1_RE, USART_CR1_TE | USART_CR1_RE);

  // Configure HW flow control disabled
  MODIFY_REG(peripheral_->CR3, USART_CR3_RTSE | USART_CR3_CTSE, USART_CR3_RTSE | USART_CR3_CTSE);

  // Compute baud rate
  peripheral_->BRR = (uint16_t)((Rcc::GetFrequency(Rcc::kClockPclk1) + baud_rate/2U) / baud_rate);
  // Enable peripheral
  SET_BIT(peripheral_->CR1, USART_CR1_UE);

  // Wait for peripheral to be enabled
  while (!READ_BIT(peripheral_->ISR, USART_ISR_TEACK)) {}
  while (!READ_BIT(peripheral_->ISR, USART_ISR_REACK)) {}

  // Enable the UART Error Interrupt: (Frame error, noise error, overrun error)
  SET_BIT(peripheral_->CR3, USART_CR3_EIE);
  // Enable the UART Data Register Not Empty interrupt
  SET_BIT(peripheral_->CR1, USART_CR1_RXNEIE_RXFNEIE);
}

bool Uart::Write(uint8_t *tx_data, uint16_t data_len) {
  return Write(tx_data, data_len, nullptr, nullptr);
}

bool Uart::Write(uint8_t *tx_data, uint16_t data_len, CallbackFunction cb_func, void *cb_arg) {
  if (busy_) {
    return false;
  }
  tx_buffer_ = tx_data;
  tx_length_ = data_len;
  callback_.cb_func = cb_func;
  callback_.cb_arg = cb_arg;

  busy_ = true;
  /* Enable the Transmit Data Register Empty interrupt */
  SET_BIT(peripheral_->CR1, USART_CR1_TXEIE_TXFNFIE);
  return true;
}

uint16_t Uart::Read(uint8_t *rx_data, uint16_t data_len) {
  uint16_t ret;
  for (ret = 0; ret < data_len; ++ret) {
    if (rx_buffer_.IsEmpty()) {
      // No further data to read!
      return ret;
    }
    rx_buffer_.ReadElement(&rx_data[ret]);
    // Disable the UART Receive Data Register Not Empty Interrupt if ringbuffer is full
    SET_BIT(peripheral_->CR1, USART_CR1_RXNEIE_RXFNEIE);
  }
  return ret;
}

void Uart::ClearRxBuffer() {
  SET_BIT(peripheral_->RQR, USART_RQR_RXFRQ);
  rx_buffer_.Clear();
}

bool Uart::IsBusy() {
  return busy_;
}

Uart::Error Uart::GetErrorStatus() {
  return error_;
}

/**************************************************************************************************
 *     Private member functions                                                                   *
 **************************************************************************************************/
void Uart::RxIsr8Bit() {
  if (rx_buffer_.IsFull()) {
    // Disable the UART Receive Data Register Not Empty Interrupt if ringbuffer is full
    CLEAR_BIT(peripheral_->CR1, USART_CR1_RXNEIE_RXFNEIE);
    error_ = kErrorRxBufferOverflow;
  } else {
    rx_buffer_.WriteElement((uint8_t) (uint16_t) READ_REG(peripheral_->RDR));
  }
}

void Uart::TxIsr8Bit() {
  if (tx_length_ == 0) {
    // Disable the UART Transmit Data Register Empty Interrupt
    CLEAR_BIT(peripheral_->CR1, USART_CR1_TXEIE_TXFNFIE);
    busy_ = false;
    if (callback_.cb_func) {
      callback_.cb_func(kErrorNone, callback_.cb_arg);
    }
  } else {
    tx_length_--;
    peripheral_->TDR = (uint8_t)(*tx_buffer_++);
  }
}

void Uart::IRQHandler() {
  if (READ_BIT(peripheral_->ISR, USART_ISR_FE)) {
    SET_BIT(peripheral_->ICR, USART_ICR_FECF);
    error_ = kErrorFrame;
    busy_ = false;
    if (callback_.cb_func) {
      callback_.cb_func(error_, callback_.cb_arg);
    }
    return;
  }
  if (READ_BIT(peripheral_->ISR, USART_ISR_NE)) {
    SET_BIT(peripheral_->ICR, USART_ICR_NECF);
    error_ = kErrorNoise;
    busy_ = false;
    if (callback_.cb_func) {
      callback_.cb_func(error_, callback_.cb_arg);
    }
    return;
  }
  if (READ_BIT(peripheral_->ISR, USART_ISR_ORE)) {
    error_ = kErrorOverrun;
    busy_ = false;
    SET_BIT(peripheral_->ICR, USART_ICR_ORECF);
    if (callback_.cb_func) {
      callback_.cb_func(error_, callback_.cb_arg);
    }
    return;
  }

  error_ = kErrorNone;

  if (READ_BIT(peripheral_->ISR, USART_ISR_RXNE_RXFNE)) {
    RxIsr8Bit();
  }
  if (READ_BIT(peripheral_->CR1, USART_CR1_TXEIE_TXFNFIE) &&
      READ_BIT(peripheral_->ISR, USART_ISR_TXE_TXFNF)) {
    TxIsr8Bit();
  }
}

}  // namespace hal

/**************************************************************************************************
 *     Global Interrupt handler implementations                                                   *
 **************************************************************************************************/
extern "C" {
//! Interrupt handler of UART1
void USART1_IRQHandler() { hal::Uart::device_registry_[hal::Uart::kPort1]->IRQHandler(); }

//! Interrupt handler of UART4
void UART4_IRQHandler() { hal::Uart::device_registry_[hal::Uart::kPort4]->IRQHandler(); }
}  // extern "C"
