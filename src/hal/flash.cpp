// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.

#include "hal/flash.hpp"
#include <stm32h7xx.h>
#include "hal/interrupts.hpp"
#include "utils/debug_utils.h"

#define FLASH_KEY1                 0x45670123U
#define FLASH_KEY2                 0xCDEF89ABU
namespace hal {

// Reads data from flash.
void Flash::Read(uint32_t flash_address, uint32_t *data, uint16_t data_length) {
  // Disable interrupts during blocking flash access
  Interrupts::Disable();

  for (int i = 0; i < data_length; i++) {
    data[i] = (reinterpret_cast<uint32_t *>(flash_address))[i];
  }
  __DSB();

  Interrupts::Enable();
}

// Erases flash sectors.
bool Flash::EraseSectors(Bank bank, Sector start_sector, uint32_t sector_cnt) {
  // Disable interrupts during blocking flash access
  Interrupts::Disable();

  // Unlock flash
  if (!Unlock()) {
    Interrupts::Enable();
    return false;
  }

  volatile uint32_t* status_reg;
  volatile uint32_t* control_reg;
  if (bank == kBank1) {
    status_reg = &FLASH->SR1;
    control_reg = &FLASH->CR1;
  } else {
    status_reg = &FLASH->SR2;
    control_reg = &FLASH->CR2;
  }

  // Wait for last operation to be completed on Bank
  while (READ_BIT(*status_reg, FLASH_SR_QW)) {}

  // Check if we need a mass erase
  if (start_sector == kSector0 && sector_cnt == 8) {
    // Configure bank erase
    SET_BIT(*control_reg, FLASH_CR_START | FLASH_CR_PSIZE | FLASH_CR_BER);

    // Wait for last operation to be completed on Bank
    while (READ_BIT(*status_reg, FLASH_SR_QW)) {}
    // if the erase operation is completed, disable the Bank BER Bit
    CLEAR_BIT(*control_reg, FLASH_CR_BER);
  } else {  // Normal sector erase
    for (uint32_t sector_idx = start_sector; sector_idx < start_sector + sector_cnt; sector_idx++) {
      // Configure sector index that should be erased
      MODIFY_REG(*control_reg, FLASH_CR_SNB, (sector_idx << FLASH_CR_SNB_Pos));
      SET_BIT(*control_reg, FLASH_CR_START | FLASH_CR_PSIZE | FLASH_CR_SER);

      // Wait for last operation to be completed on Bank
      while (READ_BIT(*status_reg, FLASH_SR_QW)) {}
      // If the erase operation is completed, disable the SER Bit
      CLEAR_BIT(*control_reg, FLASH_CR_SER);
    }
  }
  // Lock flash
  Lock();
  Interrupts::Enable();
  return true;
}

// Erases a memory area.
bool Flash::Erase(uint32_t start_address, uint32_t memory_size) {
  // Check if the address is in the program memory address range
  ASSERT(start_address  >= FLASH_BANK1_BASE &&
         start_address <= FLASH_END);

  // Compute flash bank and sector of start and end address
  Bank start_bank = (start_address < FLASH_BANK2_BASE) ? kBank1 :
                            kBank2;
  uint32_t start_address_on_bank = (start_bank == kBank1) ?
      (start_address - FLASH_BANK1_BASE) : (start_address - FLASH_BANK2_BASE);
  auto start_sector = (Sector)(start_address_on_bank / FLASH_SECTOR_SIZE);
  Bank end_bank = (start_address + memory_size - 1 < FLASH_BANK2_BASE) ? kBank1 :
                          kBank2;
  uint32_t end_address_on_bank = (end_bank == kBank1) ?
      (start_address + memory_size - 1 - FLASH_BANK1_BASE) :
      (start_address + memory_size - 1 - FLASH_BANK2_BASE);
  auto end_sector = (Sector)((end_address_on_bank) / FLASH_SECTOR_SIZE);

  // Erase bank 1 sectors
  if (start_bank == kBank1) {  // Sectors to erase on flash bank 1
    Sector bank_1_end_sector = (end_bank == kBank2) ? kSector7 : end_sector;
    if (!EraseSectors(kBank1, start_sector,
                              (uint32_t) bank_1_end_sector - (uint32_t) start_sector + 1)) {
      return false;
    }
  }

  // Erase bank 2 sectors
  if (end_bank == kBank2) {  // Sectors to erase on flash bank 2
    Sector bank_2_start_sector =
        (start_bank == kBank1) ? kSector0 : start_sector;
    if (!EraseSectors(kBank2, bank_2_start_sector,
                              (uint32_t) end_sector - (uint32_t) bank_2_start_sector + 1)) {
      return false;
    }
  }

  return true;
}

// Writes data to flash.
bool Flash::Write(uint32_t flash_address, uint32_t *data, uint16_t data_length) {
  // Disable interrupts during blocking flash access
  Interrupts::Disable();

    // Check if the address is in the program memory address range
  ASSERT(flash_address  >= FLASH_BANK1_BASE &&
         flash_address <= FLASH_END);
  // Unlock flash
  if (!Unlock()) {
    Interrupts::Enable();
    return false;
  }

  volatile uint32_t* status_reg;
  volatile uint32_t* control_reg;
  if (flash_address >= FLASH_BANK1_BASE &&
      flash_address < FLASH_BANK2_BASE) {
    status_reg = &FLASH->SR1;
    control_reg = &FLASH->CR1;
  } else if (flash_address >= FLASH_BANK2_BASE &&
             flash_address < FLASH_END) {
    status_reg = &FLASH->SR2;
    control_reg = &FLASH->CR2;
  } else {
    Lock();
    Interrupts::Enable();
    return false;
  }

  // Wait for last operation to be completed on Bank
  while (READ_BIT(*status_reg, FLASH_SR_QW)) {}

  /* Set PG bit */
  SET_BIT(*control_reg, FLASH_CR_PG);
  __ISB();
  __DSB();

  volatile auto* dest_addr = reinterpret_cast<volatile uint32_t*>(flash_address);
  volatile auto* src_addr = reinterpret_cast<volatile uint32_t*>(data);

  // Program the flash word
  for (int i = 0; i < data_length; i+=8) {
    for (int j = 0; j < 8; j++) {
      *dest_addr = *src_addr;
      dest_addr++;
      src_addr++;
    }
    __ISB();
    __DSB();
  }

  // Wait for last operation to be completed on Bank
  while (READ_BIT(*status_reg, FLASH_SR_QW)) {}
  /* Clear PG bit */
  CLEAR_BIT(*control_reg, FLASH_CR_PG);

  // Lock flash
  Lock();
  Interrupts::Enable();
  return true;
}

bool Flash::Unlock() {
  if (READ_BIT(FLASH->CR1, FLASH_CR_LOCK)) {
    /* Authorize the FLASH Bank1 Registers access */
    WRITE_REG(FLASH->KEYR1, FLASH_KEY1);
    WRITE_REG(FLASH->KEYR1, FLASH_KEY2);

    /* Verify Flash Bank1 is unlocked */
    if (READ_BIT(FLASH->CR1, FLASH_CR_LOCK)) {
      return false;
    }
  }

  if (READ_BIT(FLASH->CR2, FLASH_CR_LOCK)) {
    /* Authorize the FLASH Bank2 Registers access */
    WRITE_REG(FLASH->KEYR2, FLASH_KEY1);
    WRITE_REG(FLASH->KEYR2, FLASH_KEY2);

    /* Verify Flash Bank2 is unlocked */
    if (READ_BIT(FLASH->CR2, FLASH_CR_LOCK)) {
      return false;
    }
  }
  return true;
}

bool Flash::Lock() {
  /* Set the LOCK Bit to lock the FLASH Bank1 Control Register access */
  SET_BIT(FLASH->CR1, FLASH_CR_LOCK);

  /* Verify Flash Bank1 is locked */
  if (!READ_BIT(FLASH->CR1, FLASH_CR_LOCK)) {
    return false;
  }

  /* Set the LOCK Bit to lock the FLASH Bank2 Control Register access */
  SET_BIT(FLASH->CR2, FLASH_CR_LOCK);

  /* Verify Flash Bank2 is locked */
  if (!READ_BIT(FLASH->CR2, FLASH_CR_LOCK)) {
    return false;
  }

  return true;
}
}  // namespace hal
