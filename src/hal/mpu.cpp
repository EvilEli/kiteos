// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.

#include "hal/mpu.hpp"
#include <stm32h7xx.h>

#define  MPU_INSTRUCTION_ACCESS_ENABLE      ((uint8_t)0x00)
#define  MPU_INSTRUCTION_ACCESS_DISABLE     ((uint8_t)0x01)

#define  MPU_REGION_NO_ACCESS      ((uint8_t)0x00)
#define  MPU_REGION_PRIV_RW        ((uint8_t)0x01)
#define  MPU_REGION_PRIV_RW_URO    ((uint8_t)0x02)
#define  MPU_REGION_FULL_ACCESS    ((uint8_t)0x03)
#define  MPU_REGION_PRIV_RO        ((uint8_t)0x05)
#define  MPU_REGION_PRIV_RO_URO    ((uint8_t)0x06)

#define  MPU_TEX_LEVEL0    ((uint8_t)0x00)
#define  MPU_TEX_LEVEL1    ((uint8_t)0x01)
#define  MPU_TEX_LEVEL2    ((uint8_t)0x02)

#define  MPU_ACCESS_SHAREABLE        ((uint8_t)0x01)
#define  MPU_ACCESS_NOT_SHAREABLE    ((uint8_t)0x00)

#define  MPU_ACCESS_CACHEABLE         ((uint8_t)0x01)
#define  MPU_ACCESS_NOT_CACHEABLE     ((uint8_t)0x00)

#define  MPU_ACCESS_BUFFERABLE         ((uint8_t)0x01)
#define  MPU_ACCESS_NOT_BUFFERABLE     ((uint8_t)0x00)

#define   MPU_REGION_SIZE_1KB      ((uint8_t)0x09)
#define   MPU_REGION_SIZE_2KB      ((uint8_t)0x0A)
#define   MPU_REGION_SIZE_32KB     ((uint8_t)0x0E)
#define   MPU_REGION_SIZE_64KB     ((uint8_t)0x0F)
#define   MPU_REGION_SIZE_128KB    ((uint8_t)0x10)

#define  MPU_REGION_ENABLE     ((uint8_t)0x01)
#define  MPU_REGION_DISABLE    ((uint8_t)0x00)
namespace hal {

void Mpu::Init() {
  // Make sure outstanding transfers are done
  __DMB();
  // Disable fault exceptions
  CLEAR_BIT(SCB->SHCSR, SCB_SHCSR_MEMFAULTENA_Msk);
  // Disable the MPU and clear the control register
  WRITE_REG(MPU->CTRL, 0);

  //  ETH DMA descriptors:
  //  Sharable Device ->  TEX = 0, cacheable = 0, buffereable = 1
  //  MPU region 1 (higher region number -> higher priority of config when there is overlap)
  MPU->RNR = 0x01;
  MPU->RBAR = ETH_DESC_BASE_ADDR;
  MPU->RASR = (MPU_INSTRUCTION_ACCESS_ENABLE    << MPU_RASR_XN_Pos)   |
              (MPU_REGION_FULL_ACCESS           << MPU_RASR_AP_Pos)   |
              (MPU_TEX_LEVEL0                   << MPU_RASR_TEX_Pos)  |
              (MPU_ACCESS_NOT_SHAREABLE         << MPU_RASR_S_Pos)    |
              (MPU_ACCESS_NOT_CACHEABLE         << MPU_RASR_C_Pos)    |
              (MPU_ACCESS_BUFFERABLE            << MPU_RASR_B_Pos)    |
              (0x0                              << MPU_RASR_SRD_Pos)  |
              (MPU_REGION_SIZE_2KB              << MPU_RASR_SIZE_Pos) |
              (MPU_REGION_ENABLE                << MPU_RASR_ENABLE_Pos);

  // Buffers used by the Ethernet driver:
  // Non Cacheable -> TEX = 001 , cacheable = 0, buffereable = 0
  // MPU region 0 (higher region number -> higher priority of config when there is overlap)

  // Buffers are located after descriptors. This memory region is overlapping with the descriptor
  // memory region but also contains the buffers. This overlap is configured since it seems that
  // memory regions can only be configured in a certain grid.
  MPU->RNR = 0x00;
  MPU->RBAR = ETH_DESC_BASE_ADDR;
  MPU->RASR = (MPU_INSTRUCTION_ACCESS_ENABLE    << MPU_RASR_XN_Pos)   |
              (MPU_REGION_FULL_ACCESS           << MPU_RASR_AP_Pos)   |
              (MPU_TEX_LEVEL1                   << MPU_RASR_TEX_Pos)  |
              (MPU_ACCESS_NOT_SHAREABLE         << MPU_RASR_S_Pos)    |
              (MPU_ACCESS_NOT_CACHEABLE         << MPU_RASR_C_Pos)    |
              (MPU_ACCESS_NOT_BUFFERABLE        << MPU_RASR_B_Pos)    |
              (0x0                              << MPU_RASR_SRD_Pos)  |
              (MPU_REGION_SIZE_128KB            << MPU_RASR_SIZE_Pos) |
              (MPU_REGION_ENABLE                << MPU_RASR_ENABLE_Pos);

  // LwIP RAM heap which contains the Tx buffers:
  // Write-through -> TEX = 0, cacheable = 1, buffereable = 0
  // MPU region 2 (higher region number -> higher priority of config when there is overlap)
  MPU->RNR = 0x02;
  MPU->RBAR = ETH_RAM_HEAP_ADDR;
  MPU->RASR = (MPU_INSTRUCTION_ACCESS_ENABLE    << MPU_RASR_XN_Pos)   |
              (MPU_REGION_FULL_ACCESS           << MPU_RASR_AP_Pos)   |
              (MPU_TEX_LEVEL0                   << MPU_RASR_TEX_Pos)  |
              (MPU_ACCESS_NOT_SHAREABLE         << MPU_RASR_S_Pos)    |
              (MPU_ACCESS_CACHEABLE             << MPU_RASR_C_Pos)    |
              (MPU_ACCESS_NOT_BUFFERABLE        << MPU_RASR_B_Pos)    |
              (0x0                              << MPU_RASR_SRD_Pos)  |
              (MPU_REGION_SIZE_32KB             << MPU_RASR_SIZE_Pos) |
              (MPU_REGION_ENABLE                << MPU_RASR_ENABLE_Pos);

  // Enable the MPU
  WRITE_REG(MPU->CTRL , 0x04 | MPU_CTRL_ENABLE_Msk);
  // Enable fault exceptions
  SET_BIT(SCB->SHCSR, SCB_SHCSR_MEMFAULTENA_Msk);
  // Ensure MPU setting take effects
  __DSB();
  __ISB();

  // Check if I cache is already enabled and enable it if not
  if (!READ_BIT(SCB->CCR, SCB_CCR_IC_Msk)) {
    SCB_EnableICache();
  }

  // Check if D cache is already enabled and enable it if not
  if (!READ_BIT(SCB->CCR, SCB_CCR_DC_Msk)) {
    SCB_EnableDCache();
  }
}
}  // namespace hal
