// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#include "hal/ethernet.hpp"
#include <cstring>
#include "hal/rcc.hpp"
#include "hal/systick_timer.hpp"
#include "utils/debug_utils.h"


//! Bit definition of TDES2 RF register
#define ETH_DMATXNDESCRF_IOC   ((uint32_t)0x80000000U)  //!< Interrupt on Completion
#define ETH_DMATXNDESCRF_TTSE  ((uint32_t)0x40000000U)  //!< Transmit Timestamp Enable
#define ETH_DMATXNDESCRF_B1L   ((uint32_t)0x00003FFFU)  //!< Buffer 1 Length

//! Bit definition of TDES3 RF register
#define ETH_DMATXNDESCRF_OWN   ((uint32_t)0x80000000U)  //!< OWN bit: descriptor is owned by DMA
#define ETH_DMATXNDESCRF_FD    ((uint32_t)0x20000000U)  //!< First Descriptor
#define ETH_DMATXNDESCRF_LD    ((uint32_t)0x10000000U)  //!< Last Descriptor
#define ETH_DMATXNDESCRF_CIC   ((uint32_t)0x00030000U)  //!< Checksum Insertion Control: 4 cases
#define ETH_DMATXNDESCRF_FL    ((uint32_t)0x00007FFFU)  //!< Transmit End of Ring

//! Bit definition of TDES3 WBF register
#define ETH_DMATXNDESCWBF_OWN  ((uint32_t)0x80000000U)  //!< OWN bit: descriptor is owned by DMA
#define ETH_DMATXNDESCWBF_LD   ((uint32_t)0x10000000U)  //!< Last Descriptor
#define ETH_DMATXNDESCWBF_TTSS ((uint32_t)0x00020000U)  //!< Tx Timestamp Status

//! Bit definition of RDES1 WBF register
#define ETH_DMARXNDESCWBF_TSA  ((uint32_t)0x00004000U)  //!< Timestamp Available

//! Bit definition of RDES3 RF register
#define ETH_DMARXNDESCRF_OWN    ((uint32_t)0x80000000U)  //!< OWN bit: descriptor is owned by DMA
#define ETH_DMARXNDESCRF_IOC    ((uint32_t)0x40000000U)  //!< Interrupt Enabled on Completion
#define ETH_DMARXNDESCRF_BUF2V  ((uint32_t)0x02000000U)  //!< Buffer 2 Address Valid
#define ETH_DMARXNDESCRF_BUF1V  ((uint32_t)0x01000000U)  //!< Buffer 1 Address Valid

//! Bit definition of RDES3 WBF register
#define ETH_DMARXNDESCWBF_OWN   ((uint32_t)0x80000000U)  //!< Own Bit
#define ETH_DMARXNDESCWBF_FD    ((uint32_t)0x20000000U)  //!< First Descriptor
#define ETH_DMARXNDESCWBF_LD    ((uint32_t)0x10000000U)  //!< Last Descriptor
#define ETH_DMARXNDESCWBF_RS1V  ((uint32_t)0x04000000U)  //!< Receive Status RDES1 Valid
#define ETH_DMARXNDESCWBF_CE    ((uint32_t)0x01000000U)  //!< CRC Error
#define ETH_DMARXNDESCWBF_PL    ((uint32_t)0x00007FFFU)  //!< Packet Length

//! Bit definition of RDES3 CTX register
#define ETH_DMARXCDESC_CTXT     ((uint32_t)0x40000000U)  //!< Receive Context Descriptor

namespace hal {

/**************************************************************************************************
 *     Static member variables                                                                    *
 **************************************************************************************************/
  // Initialize GPIO pins for ethernet
#if USE_EVAL_BOARD
Gpio::Config Ethernet::gpio_config_[] = {
  // ETH_MII_RX_CLK/ETH_RMII_REF_CLK --> PA1
  // ETH_MII_RX_DV/ETH_RMII_CRS_DV ----> PA7
  // ETH_MII_TX_EN/ETH_RMII_TX_EN -----> PB11, PG11 (PG11 on Nucleo)
  // ETH_MII_TXD0/ETH_RMII_TXD0 -------> PB12, PG13 (PG13 on Nucleo)
  // ETH_MII_TXD1/ETH_RMII_TXD1 -------> PB13, PG12, PG14 (PB13 on Nucleo)
  // ETH_MII_RXD0/EHT_RMII_RXD0 -------> PC4
  // ETH_MII_RXD1/EHT_RMII_RXD1 -------> PC5

  // ETH_MII_CRS ----------------------> PA0, PH2
  // ETH_MII_COL ----------------------> PA3, PH3
  // ETH_MII_RXD2 ---------------------> PB0, PH6
  // ETH_MII_RXD3 ---------------------> PB1, PH7
  // ETH_MII_TXD3 ---------------------> PB8, PE2
  // ETH_MII_RX_ER --------------------> PB10, PI10
  // ETH_MII_TXD2 ---------------------> PC2
  // ETH_MII_TX_CLK -------------------> PC3

  // ETH_MDIO -------------------------> PA2
  // ETH_PPS_OUT ----------------------> PB5, PG8
  // ETH_MDC --------------------------> PC1

  // Initialize in RMII Mode, pinout of Nucleo board
  {Gpio::kPortA, hal::Gpio::kPin1 |
                 hal::Gpio::kPin2 |
                 hal::Gpio::kPin7, Gpio::Mode::kModeAF11Ethernet},
  // Configure Pin B5 as alternate function PPS
  {Gpio::kPortB, hal::Gpio::kPin5 |
                 hal::Gpio::kPin13, Gpio::Mode::kModeAF11Ethernet},
  {Gpio::kPortC, hal::Gpio::kPin1 |
                 hal::Gpio::kPin4 |
                 hal::Gpio::kPin5, Gpio::Mode::kModeAF11Ethernet},
  // Configure Pin G8 as alternate function PPS
  {Gpio::kPortG, hal::Gpio::kPin8 |
                 hal::Gpio::kPin11 |
                 hal::Gpio::kPin13, Gpio::Mode::kModeAF11Ethernet},
};
#else
Gpio::Config Ethernet::gpio_config_[] = {
  // ETH_MII_RX_CLK/ETH_RMII_REF_CLK --> PA1
  // ETH_MII_RX_DV/ETH_RMII_CRS_DV ----> PA7
  // ETH_MII_TX_EN/ETH_RMII_TX_EN -----> PB11, PG11 (PG11 on Nucleo, PB11 on FABO)
  // ETH_MII_TXD0/ETH_RMII_TXD0 -------> PB12, PG13 (PG13 on Nucleo, PB12 on FABO)
  // ETH_MII_TXD1/ETH_RMII_TXD1 -------> PB13, PG12, PG14 (PB13 on Nucleo, PB13 on FABO)
  // ETH_MII_RXD0/EHT_RMII_RXD0 -------> PC4
  // ETH_MII_RXD1/EHT_RMII_RXD1 -------> PC5

  // ETH_MII_CRS ----------------------> PA0, PH2 (PA0 on FABO)
  // ETH_MII_COL ----------------------> PA3, PH3 (PA3 on FABO)
  // ETH_MII_RXD2 ---------------------> PB0, PH6 (PB0 on FABO)
  // ETH_MII_RXD3 ---------------------> PB1, PH7 (PB1 on FABO)
  // ETH_MII_TXD3 ---------------------> PB8, PE2 (PE2 on FABO)
  // ETH_MII_RX_ER --------------------> PB10, PI10 (Not used on FABO)
  // ETH_MII_TXD2 ---------------------> PC2
  // ETH_MII_TX_CLK -------------------> PC3

  // ETH_MDIO -------------------------> PA2
  // ETH_PPS_OUT ----------------------> PB5, PG8 (PB5 on FABO)
  // ETH_MDC --------------------------> PC1

  // Initialize in MMII Mode, pinout of Nucleo board
  {Gpio::kPortA, hal::Gpio::kPin1 |
                 hal::Gpio::kPin7, Gpio::Mode::kModeAF11Ethernet},

  // Configure Pin B5 as alternate function PPS
  {Gpio::kPortB, hal::Gpio::kPin5 |
                 hal::Gpio::kPin11 |
                 hal::Gpio::kPin12 |
                 hal::Gpio::kPin13, Gpio::Mode::kModeAF11Ethernet},

  {Gpio::kPortC, hal::Gpio::kPin4 |
                 hal::Gpio::kPin5, Gpio::Mode::kModeAF11Ethernet},
};
#endif
Gpio* Ethernet::gpio_[4] = {nullptr};
uint64_t Ethernet::mac_address_ = 0x000000000002;
/**************************************************************************************************
 *     Structs                                                                                    *
 **************************************************************************************************/

//! Structure of an ethernet DMA descriptor
struct DmaDescriptor {
  uint32_t des_0_;  //!< Descriptor Field 0
  uint32_t des_1_;  //!< Descriptor Field 1
  uint32_t des_2_;  //!< Descriptor Field 2
  uint32_t des_3_;  //!< Descriptor Field 3
  //! Information regarding packet. data fileds:
  //! ------------------------------------------------------------------------------------
  //! | Data valid[31] | not used[30:12] | PTP sequence ID[11:4] | PTP message type[3:0] |
  //! ------------------------------------------------------------------------------------
  uint32_t packet_info_;
};


/**************************************************************************************************
 *     Global variables                                                                           *
 **************************************************************************************************/

//! Ethernet Rx DMA Descriptors
DmaDescriptor g_dma_rx_descriptors[HAL_ETHERNET_RX_DESCRIPTOR_COUNT]
    __attribute__((section(".RxDescriptorSection")));

//! Ethernet Tx DMA Descriptors
DmaDescriptor g_dma_tx_descriptors[HAL_ETHERNET_TX_DESCRIPTOR_COUNT]
    __attribute__((section(".TxDescriptorSection")));

//! Ethernet Receive Buffers
uint8_t g_rx_buffer[HAL_ETHERNET_RX_DESCRIPTOR_COUNT][HAL_ETHERNET_BUFFER_SIZE]
    __attribute__((section(".RxArraySection")));

//! Ethernet Transmit Buffers
uint8_t g_tx_buffer[HAL_ETHERNET_TX_DESCRIPTOR_COUNT][HAL_ETHERNET_BUFFER_SIZE]
    __attribute__((section(".TxArraySection")));


/**************************************************************************************************
 *     Static member variables                                                                    *
 **************************************************************************************************/

int Ethernet::dma_rx_descriptor_index_;  //!< Index of current Rx descriptor
int Ethernet::dma_tx_descriptor_index_;  //!< Index of current Tx descriptor

void (*Ethernet::tx_timestamp_callback_)(uint32_t, uint32_t, uint8_t, uint16_t)
= nullptr;
void (*Ethernet::rx_timestamp_callback_)(uint32_t, uint32_t, uint8_t, uint16_t, uint64_t,
                                         uint16_t)
= nullptr;

uint16_t Ethernet::ptp_event_message_port_ = standard_ptp_event_msg_port_;

// Addend is dividing HCLK in half per default
uint32_t Ethernet::ieee1588_addend_value_ = 0x80000000;


/**************************************************************************************************
 *     Public methods                                                                             *
 **************************************************************************************************/

void Ethernet::Init() {
  // Construct the Ethernet Gpios
  for (uint8_t i = 0; i < sizeof(gpio_config_) / sizeof(Gpio::Config); ++i) {
    gpio_[i] = new Gpio(gpio_config_[i]);
  }
  // Initialize descriptor indexes
  dma_rx_descriptor_index_ = 0;
  dma_tx_descriptor_index_ = 0;

  // Initialize Rx and Tx descriptors with zeros
  memset(g_dma_rx_descriptors, 0,
         sizeof(DmaDescriptor) * HAL_ETHERNET_RX_DESCRIPTOR_COUNT);
  memset(g_dma_tx_descriptors, 0,
         sizeof(DmaDescriptor) * HAL_ETHERNET_TX_DESCRIPTOR_COUNT);

  // Release all Rx descriptors to DMA
  for (int i = 0; i < HAL_ETHERNET_RX_DESCRIPTOR_COUNT; i++) {
    ReleaseRxDescriptor(i);
  }

  // Enable the Ethernet global interrupt
  NVIC_EnableIRQ(ETH_IRQn);
  // Enable Ethernet clocks
  Rcc::EnablePeripheralClock(Rcc::kEthernet);

  // Select RMII PHY interface
  MODIFY_REG(SYSCFG->PMCR, SYSCFG_PMCR_EPIS_SEL, SYSCFG_PMCR_EPIS_SEL_2);
  // Reset Ethernet DMA MTL and MAC
  ETH->DMAMR |= ETH_DMAMR_SWR;  // Start software reset of ethernet DMA MTL and MAC
  while (ETH->DMAMR & ETH_DMAMR_SWR) {}  // Wait for reset to finish

  // Initialize MAC
  MacInit();

  // Initialize MTL
  ETH->MTLTQOMR |= ETH_MTLTQOMR_TSF;  // Start transmission when full packet is in the MTL queue
  ETH->MTLRQOMR |= ETH_MTLRQOMR_RSF;  // Read packet from Rx queue when complete.

  // Initialize DMA
  DmaInit();

  // Initialize system time
  Ieee1588Init();

  // Set management clock divider
  SetMdioClockRange();
}

void Ethernet::Start() {
  // Enable MAC receiver and transmitter
  ETH->MACCR |= ETH_MACCR_TE | ETH_MACCR_RE;

  // FTQ bit should only be set if it is 0
  if (!(ETH->MTLTQOMR & ETH_MTLTQOMR_FTQ)) {
    // Flush transmit FIFO queue
    ETH->MTLTQOMR |= ETH_MTLTQOMR_FTQ;
  }

  // Re-initialize Rx descriptor ring indexes and release all Rx descriptors to DMA
  dma_rx_descriptor_index_ = 0;
  for (int i = 0; i < HAL_ETHERNET_RX_DESCRIPTOR_COUNT; i++) {
    ReleaseRxDescriptor(i);
  }
  ETH->DMACRDTPR =
      (uint32_t) &g_dma_rx_descriptors[HAL_ETHERNET_RX_DESCRIPTOR_COUNT - 1];

  // Re-initialize Tx and Rx DMA descriptor list address registers
  ETH->DMACTDLAR =
      (uint32_t) g_dma_tx_descriptors;  // Set DMA Tx descriptor list address
  ETH->DMACRDLAR =
      (uint32_t) g_dma_rx_descriptors;  // Set DMA Rx descriptor list address

  // Start Tx DMA transmission and Rx DMA reception
  ETH->DMACTCR |= ETH_DMACTCR_ST;
  ETH->DMACRCR |= ETH_DMACRCR_SR;

  // Clear Tx and Rx process stopped flags
  ETH->DMACSR |= (ETH_DMACSR_TPS | ETH_DMACSR_RPS);
}

void Ethernet::Stop() {
  // Stop Tx DMA transmission and Rx DMA reception
  ETH->DMACTCR &= ~ETH_DMACTCR_ST;
  ETH->DMACRCR &= ~ETH_DMACRCR_SR;

  // FTQ bit should only be set if it is 0
  if (!(ETH->MTLTQOMR & ETH_MTLTQOMR_FTQ)) {
    // Flush transmit FIFO queue
    ETH->MTLTQOMR |= ETH_MTLTQOMR_FTQ;
  }

  // Disable MAC receiver and transmitter
  ETH->MACCR &= ~(ETH_MACCR_TE | ETH_MACCR_RE);
}

bool Ethernet::Send(uint8_t *tx_buffer, uint32_t tx_buffer_length) {
  // Check if enough TX descriptors are available for sending the packet
  if ((GetAvailableTxDescriptorCount() - 1 * (uint32_t) HAL_ETHERNET_BUFFER_SIZE <
      tx_buffer_length) || (tx_buffer_length == 0)) {
    return false;  // Not enough Tx descriptors available or no data to send
  }

  // Prepare normal descriptors
  uint32_t bytes_to_send = tx_buffer_length;
  while (bytes_to_send) {
    // Copy data to the TX buffer
    uint32_t bytes_to_copy = tx_buffer_length < HAL_ETHERNET_BUFFER_SIZE ?
                             tx_buffer_length : HAL_ETHERNET_BUFFER_SIZE;
    for (uint32_t i = 0; i < bytes_to_copy; i++) {
      g_tx_buffer[dma_tx_descriptor_index_][i] = tx_buffer[i];
    }

    // Set normal descriptor pointer
    DmaDescriptor *normal_descriptor =
        &g_dma_tx_descriptors[dma_tx_descriptor_index_];

    // Set normal descriptor values
    // Set buffer 1 address pointer
    normal_descriptor->des_0_ =
        (uint32_t) (&g_tx_buffer[dma_tx_descriptor_index_][0]);
    normal_descriptor->des_2_ = 0;  // Reset des_2_ (this also sets buffer 2 length to 0)
    normal_descriptor->des_2_ |= ETH_DMATXNDESCRF_B1L & bytes_to_copy;  // Set buffer 1 length
    normal_descriptor->des_1_ = (uint32_t) NULL;  // Reset buffer 2 address pointer

    if (IsPtpEventMessage(tx_buffer)) {
      // Enable interrupt on completion and capture transmit timestamp
      normal_descriptor->des_2_ |= ETH_DMATXNDESCRF_IOC | ETH_DMATXNDESCRF_TTSE;

      // Set packet info
      normal_descriptor->packet_info_ =
          packet_info_data_valid_ |  // Packet info valid flag
              ((((uint32_t) tx_buffer[72] << 8u) + (uint32_t) tx_buffer[73]) <<
              packet_info_ptp_sequence_id_pos_)
              |  // Sequence ID
                  (uint32_t) (tx_buffer[42] & 0x0Fu);  // Message type
    } else {
      // Set packet info data to invalid
      normal_descriptor->packet_info_ = 0;
    }

    normal_descriptor->des_3_ = 0;  // Reset des_3_

    // If bytes to send was not decremented yet, the current descriptor is the first normal
    // descriptor of the packet
    if (bytes_to_send == tx_buffer_length) {
      // Set as first descriptor
      normal_descriptor->des_3_ |= ETH_DMATXNDESCRF_FD;
    }

    // Decrement bytes to send by the amount of bytes copied in the data buffer of the current
    // descriptor
    bytes_to_send -= bytes_to_copy;

    // If no more bytes are to be sent, the current descriptor is the last normal descriptor
    if (!bytes_to_send) {
      // Set as last descriptor
      normal_descriptor->des_3_ |= ETH_DMATXNDESCRF_LD;
    }

    // Enable checksum insertion
    normal_descriptor->des_3_ |= ETH_DMATXNDESCRF_CIC;

    // Set frame length
    normal_descriptor->des_3_ |= ETH_DMATXNDESCRF_FL & tx_buffer_length;

    // Release descriptor to DMA
    normal_descriptor->des_3_ |= ETH_DMATXNDESCRF_OWN;

    // Set next descriptor as current descriptor
    dma_tx_descriptor_index_++;
    if (dma_tx_descriptor_index_ >= HAL_ETHERNET_TX_DESCRIPTOR_COUNT) {
      dma_tx_descriptor_index_ = 0;
    }
  }

  // Set DMA TX tail pointer to first application descriptor
  ETH->DMACTDTPR = (uint32_t) &g_dma_tx_descriptors[dma_tx_descriptor_index_];

  return true;
}

bool Ethernet::Receive(Ethernet::RxBuffer *rx_buffer) {
  // Set initial return values to be returned per default
  rx_buffer->length = 0;
  rx_buffer->data = nullptr;

  // Check if an ethernet packed is available
  if (!RxPacketAvailable()) {
    // No data available
    return false;
  }

  // Go forward to the last normal descriptor
  while (!(g_dma_rx_descriptors[dma_rx_descriptor_index_].des_3_ &
      ETH_DMARXNDESCWBF_LD) &&
      !(g_dma_rx_descriptors[dma_rx_descriptor_index_].des_3_ &
          ETH_DMARXNDESCWBF_OWN)) {
    // Release descriptor to DMA
    ReleaseRxDescriptor(dma_rx_descriptor_index_);

    // Increment current descriptor index
    dma_rx_descriptor_index_++;
    if (dma_rx_descriptor_index_ >= HAL_ETHERNET_RX_DESCRIPTOR_COUNT) {
      dma_rx_descriptor_index_ = 0;
    }
  }

  // Parse normal descriptor
  bool context_descriptor_available = false;
  bool drop_package = false;
  if (!(g_dma_rx_descriptors[dma_rx_descriptor_index_].des_3_ &
      ETH_DMARXNDESCWBF_OWN)) {
    // Set normal descriptor pointer
    DmaDescriptor *normal_descriptor =
        &g_dma_rx_descriptors[dma_rx_descriptor_index_];

    // Check if package is oversized (packets which do not fit in 1 normal descriptor are oversized)
    // or if a checksum error occurred
    if ((!(normal_descriptor->des_3_ & ETH_DMARXNDESCWBF_FD)) ||
        (normal_descriptor->des_3_ & ETH_DMARXNDESCWBF_CE)) {
      drop_package = true;
    }

    // Parse descriptor data
    rx_buffer->length = normal_descriptor->des_3_ & ETH_DMARXNDESCWBF_PL;
    rx_buffer->data = &g_rx_buffer[dma_rx_descriptor_index_][0];
    rx_buffer->data_buffer_index = dma_rx_descriptor_index_;

    // Check if des_1_ is valid
    if (normal_descriptor->des_3_ & ETH_DMARXNDESCWBF_RS1V) {
      // Check if a timestamp is available in the context descriptor
      if (normal_descriptor->des_1_ & ETH_DMARXNDESCWBF_TSA) {
        context_descriptor_available = true;
      }
    }

    // Do not release the Rx descriptor to the DMA here. The descriptor is later released by the
    // HalEthernetReleaseReceiveBuffer function after the application processed its content.

    // Increment current descriptor index
    dma_rx_descriptor_index_++;
    if (dma_rx_descriptor_index_ >= HAL_ETHERNET_RX_DESCRIPTOR_COUNT) {
      dma_rx_descriptor_index_ = 0;
    }
  }

  // Parse context descriptor
  if (context_descriptor_available &&
      !(g_dma_rx_descriptors[dma_rx_descriptor_index_].des_3_ &
          ETH_DMARXNDESCWBF_OWN)) {
    // Set context descriptor pointer
    DmaDescriptor *context_descriptor =
        &g_dma_rx_descriptors[dma_rx_descriptor_index_];

    // Double check if the descriptor is really a context descriptor
    if (context_descriptor->des_3_ & ETH_DMARXCDESC_CTXT) {
      // Check if the message is a PTP event message
      if (IsPtpEventMessage(rx_buffer->data)) {
        // Check if an Rx timestamp callback is registered
        if (rx_timestamp_callback_ != nullptr) {
          // Parse descriptor data
          uint32_t timestamp_low = context_descriptor->des_0_;
          uint32_t timestamp_high = context_descriptor->des_1_;

          // Parse PTP message ID and sequence ID from received message
          uint8_t message_id = rx_buffer->data[42];
          uint16_t sequence_id = (((uint16_t) (rx_buffer->data[72])) << 8u) |
              ((uint16_t) (rx_buffer->data[73]));
          uint64_t clock_identity = (((uint64_t) rx_buffer->data[62]) << 56u) |
              (((uint64_t) rx_buffer->data[63]) << 48u) |
              (((uint64_t) rx_buffer->data[64]) << 40u) |
              (((uint64_t) rx_buffer->data[65]) << 32u) |
              (((uint64_t) rx_buffer->data[66]) << 24u) |
              (((uint64_t) rx_buffer->data[67]) << 16u) |
              (((uint64_t) rx_buffer->data[68]) << 8u) |
              ((uint64_t) rx_buffer->data[69]);
          uint16_t port_number = (((uint64_t) rx_buffer->data[70]) << 8u) |
              ((uint64_t) rx_buffer->data[71]);

          // Call Rx timestamp callback to hand over Rx timestamp
          rx_timestamp_callback_(timestamp_high, timestamp_low, message_id, sequence_id,
                                 clock_identity, port_number);
        }
      }
    }

    // Release descriptor to DMA
    ReleaseRxDescriptor(dma_rx_descriptor_index_);

    // Increment current descriptor index
    dma_rx_descriptor_index_++;
    if (dma_rx_descriptor_index_ >= HAL_ETHERNET_RX_DESCRIPTOR_COUNT) {
      dma_rx_descriptor_index_ = 0;
    }
  }

  // Set DMA RX tailpointer register
  int tailpointer_descriptor_index = dma_rx_descriptor_index_ - 1;
  if (tailpointer_descriptor_index < 0) {
    tailpointer_descriptor_index = HAL_ETHERNET_RX_DESCRIPTOR_COUNT - 1;
  }
  ETH->DMACRDTPR = (uint32_t) &g_dma_rx_descriptors[tailpointer_descriptor_index];

  return !drop_package;
}

void Ethernet::ReleaseReceiveBuffer(int data_buffer_index) {
  ASSERT(data_buffer_index < HAL_ETHERNET_RX_DESCRIPTOR_COUNT);
  ReleaseRxDescriptor(data_buffer_index);
}

void Ethernet::SetDuplexMode(Ethernet::DuplexMode duplex_mode) {
  switch (duplex_mode) {
    case kHalfDuplex:
      // Set duplex mode to half duplex
      ETH->MACCR &= ~ETH_MACCR_DM;
      break;

    case kFullDuplex:
      // Set duplex mode to full duplex
      ETH->MACCR |= ETH_MACCR_DM;
      break;

    default:break;
  }
}

void Ethernet::SetSpeed(Ethernet::EthernetSpeed speed) {
  switch (speed) {
    case k10Mbps:
      // Set ethernet speed to 10 Mbps
      ETH->MACCR &= ~ETH_MACCR_FES;
      break;

    case k100Mbps:
      // Set ethernet speed to 100 Mbps
      ETH->MACCR |= ETH_MACCR_FES;
      break;

    default:break;
  }
}

void Ethernet::SetMulticastEnabled(bool enabled) {
  if (enabled) {
    // Set pass all multicast messages bit
    ETH->MACPFR |= ETH_MACPFR_PM;
  } else {
    // Reset pass all multicast messages bit
    ETH->MACPFR &= ~ETH_MACPFR_PM;
  }
}

bool Ethernet::ReadPhyRegister(uint32_t phy_address, uint32_t phy_register,
                               uint32_t *register_value) {
  // Check if MII is busy
  if (ETH->MACMDIOAR & ETH_MACMDIOAR_MB) {
    return false;
  }

  // Read register from PHY by writing a read command to the address register
  MODIFY_REG(ETH->MACMDIOAR, ETH_MACMDIOAR_PA | ETH_MACMDIOAR_RDA | ETH_MACMDIOAR_MOC |
      ETH_MACMDIOAR_MB,
             (phy_address << ETH_MACMDIOAR_PA_Pos) |  // Set PHY device address
                 (phy_register << ETH_MACMDIOAR_RDA_Pos) |  // Set PHY register address
                 ETH_MACMDIOAR_MOC_RD |                // Read command
                 ETH_MACMDIOAR_MB);                    // Start read

  // Wait for read operation to finish
  uint32_t tick_start = SystickTimer::GetCount();
  while (ETH->MACMDIOAR & ETH_MACMDIOAR_MB) {
    if ((SystickTimer::GetCount() - tick_start) > mdio_bus_timeout_) {
      return false;
    }
  }

  // Read register value from data register
  *register_value = ETH->MACMDIODR & 0x0000FFFFu;

  return true;
}

bool Ethernet::WritePhyRegister(uint32_t phy_address, uint32_t phy_register,
                                uint32_t register_value) {
  // Check if MII is busy
  if (ETH->MACMDIOAR & ETH_MACMDIOAR_MB) {
    return false;
  }

  // Write register value to data register
  ETH->MACMDIODR = register_value & 0x0000FFFFu;

  // Write PHY register by writing a write command to the address register
  MODIFY_REG(ETH->MACMDIOAR, ETH_MACMDIOAR_PA | ETH_MACMDIOAR_RDA | ETH_MACMDIOAR_MOC |
      ETH_MACMDIOAR_MB,
             (phy_address << ETH_MACMDIOAR_PA_Pos) |  // Set PHY device address
                 (phy_register << ETH_MACMDIOAR_RDA_Pos) |  // Set PHY register address
                 ETH_MACMDIOAR_MOC_WR |                // Write command
                 ETH_MACMDIOAR_MB);                    // Start write

  // Wait for write operation to finish
  uint32_t tick_start = SystickTimer::GetCount();
  while (ETH->MACMDIOAR & ETH_MACMDIOAR_MB) {
    if ((SystickTimer::GetCount() - tick_start) > mdio_bus_timeout_) {
      return false;
    }
  }

  return true;
}

void Ethernet::RegisterTxTimestampCallback(
    void (*tx_timestamp_callback)(uint32_t, uint32_t, uint8_t, uint16_t)) {
  // Set Tx timestamp callback function pointer
  tx_timestamp_callback_ = tx_timestamp_callback;
}

void Ethernet::RegisterRxTimestampCallback(
    void (*rx_timestamp_callback)(uint32_t, uint32_t, uint8_t, uint16_t, uint64_t, uint16_t)) {
  // Set Rx timestamp callback function pointer
  rx_timestamp_callback_ = rx_timestamp_callback;
}

void Ethernet::SetPtpEventMessagePort(uint16_t event_message_port) {
  // Set PTP event message port variable
  ptp_event_message_port_ = event_message_port;
}

void Ethernet::Ieee1588GetSystemTime(uint32_t *seconds, uint32_t *nanoseconds) {
  // Read the current system time from the system time registers
  do {
    *seconds = ETH->MACSTSR;
    *nanoseconds = ETH->MACSTNR;
    // ensure that there was no nanoseconds rollover between reading seconds and nanoseconds
  } while (*seconds != ETH->MACSTSR);
}

bool Ethernet::Ieee1588CoarseSystemTimeCorrection(
    uint32_t seconds, uint32_t nanoseconds, bool subtract) {
  // Clip nanoseconds to avoid invalid values
  if (nanoseconds > 0x3B9AC9FF) {
    nanoseconds = 0x3B9AC9FF;
  }

  // Check if a system time update is already in progress
  if (ETH->MACTSCR & ieee1588_tsupdt_) {  // TSUPDT must be zero before setting it
    return false;  // TSUPDT is not zero, system time update not possible
  }

  // PPS pulse train must be restarted to be in sync with system time
  ETH->MACPPSCR |= 0x5u;  // Stop PPS pulse train immediately

  // Set system time update registers
  if (subtract) {
    ETH->MACSTSUR = 0x100000000 - seconds;
    ETH->MACSTNUR = 1000000000 - nanoseconds;
    ETH->MACSTNUR |= (1u << 31u);  // Bit 31 is add/subtract bit
  } else {
    ETH->MACSTSUR = seconds;
    ETH->MACSTNUR = nanoseconds;
  }

  // Update system time with values written in system time update registers
  ETH->MACTSCR |= ieee1588_tsupdt_;

  // Start pulse train 1 second after the next second increment (This is necessary since the next
  // second increment may occur before the start pulse train command is processed)
  ETH->MACPPSTTSR = ETH->MACSTSR + 2;
  ETH->MACPPSCR |= 0x2u;  // Start PPS pulse train again

  return true;
}

bool Ethernet::Ieee1588RescaleAddend(double scaling_factor) {
  // Check if the scaling factor value is valid
  ASSERT(scaling_factor > 0.0);

  // Rescale addend register value, prevent overflow by clipping at max value
  if (ieee1588_addend_value_ * scaling_factor > 0xFFFFFFFF) {
    ieee1588_addend_value_ = 0xFFFFFFFF;
  } else {
    ieee1588_addend_value_ *= scaling_factor;
  }

  // Write new register value to addend register
  ETH->MACTSAR = ieee1588_addend_value_;
  if (ETH->MACTSCR & ieee1588_tsaddreg_) {  // TSADDREG must be zero before setting it
    return false;
  }
  ETH->MACTSCR |= ieee1588_tsaddreg_;  // Update addend register in the PTP block

  return true;
}


/**************************************************************************************************
 *     Private methods                                                                            *
 **************************************************************************************************/
void Ethernet::MacInit() {
  // Generate the MAC address from each of the lower bytes of the Unique ID
  uint32_t unique_id[3] = {0};
  unique_id[0] = *(reinterpret_cast<uint32_t*>(UID_BASE));
  unique_id[1] = *(reinterpret_cast<uint32_t*>(UID_BASE) + 1);
  unique_id[2] = *(reinterpret_cast<uint32_t*>(UID_BASE) + 2);

  mac_address_ = (((uint64_t)unique_id[2] << 32) & 0xFFFF00000000) |
                 ((unique_id[1] << 16) & 0x0000FFFF0000) |
                 (unique_id[0] & 0x00000000FFFF);

  SET_BIT(mac_address_, 0x2);  // Locally administrated
  CLEAR_BIT(mac_address_, 0x1);  // Individual MAC address

  // Set MAC address
  MODIFY_REG(ETH->MACA0HR, ETH_MACA0HR_ADDRHI,
             (uint32_t) ((mac_address_ & 0x0000FFFF00000000ul) >> 32u));
  MODIFY_REG(ETH->MACA0LR, ETH_MACA0LR_ADDRLO,
             (uint32_t) (mac_address_ & 0x00000000FFFFFFFFul));

  // Set operating mode configuration register
  ETH->MACCR |= ETH_MACCR_SARC_REPADDR0 |  // Use MAC address 0 for all transmitted packages
      ETH_MACCR_IPC |            // Enable checksum offload
      ETH_MACCR_CST |            // Enable CRC stripping for ether type packets
      ETH_MACCR_ACS |            // Only strip packets up to 1536 bytes
      ETH_MACCR_FES |            // 100Mb/s
      ETH_MACCR_DM;              // Full duplex mode

  // Set Giant packet size limit
  MODIFY_REG(ETH->MACECR, ETH_MACECR_GPSL, 1560u);

  // Set management data clock (MDC) clock divider depending on hclk
  SetMdioClockRange();

  // Set 1 microsecond tick counter for low power idle (LPI) timers depending on hclk
  MODIFY_REG(ETH->MAC1USTCR, ETH_MAC1USTCR_TIC1USCNTR,
             ((Rcc::GetFrequency(Rcc::kClockHclk) / 1000000) - 1));
}

void Ethernet::DmaInit() {
  // Address-aligned beats enabled
  ETH->DMASBMR |= ETH_DMASBMR_AAL;
  // ETH->DMASBMR |= ETH_DMASBMR_AAL | ETH_DMASBMR_FB;
  // ETH->DMASBMR |= ETH_DMASBMR_AAL | ETH_DMASBMR_MB;

  // Initialize Tx DMA descriptor registers
  ETH->DMACTDRLR = HAL_ETHERNET_TX_DESCRIPTOR_COUNT - 1;  // Set Tx descriptor ring length
  ETH->DMACTDLAR =
      (uint32_t) g_dma_tx_descriptors;  // Set DMA Tx descriptor list address
  // Set DMA Tx tail pointer to first descriptor
  ETH->DMACTDTPR = (uint32_t) g_dma_tx_descriptors;

  // Initialize Rx DMA descriptor registers
  ETH->DMACRDRLR = HAL_ETHERNET_RX_DESCRIPTOR_COUNT - 1;  // Set Rx descriptor ring length
  ETH->DMACRDLAR =
      (uint32_t) g_dma_rx_descriptors;  // Set DMA Rx descriptor list address
  // Set DMA Rx tail pointer to last descriptor
  ETH->DMACRDTPR =
      (uint32_t) &g_dma_rx_descriptors[HAL_ETHERNET_RX_DESCRIPTOR_COUNT - 1];

  // Set maximum segment size
  MODIFY_REG(ETH->DMACCR, ETH_DMACCR_MSS, 536u);

  // Set descriptor skip length
  uint32_t descriptor_skip_length =
      (uint32_t) ((sizeof(g_dma_rx_descriptors[0]) / sizeof(uint32_t)) - 4);
  MODIFY_REG(ETH->DMACCR, ETH_DMACCR_DSL, (descriptor_skip_length << ETH_DMACCR_DSL_Pos));

  // Set burst lengths
  MODIFY_REG(ETH->DMACTCR, ETH_DMACTCR_TPBL, ETH_DMACTCR_TPBL_32PBL);  // Set transmit burst length
  MODIFY_REG(ETH->DMACRCR, ETH_DMACRCR_RPBL, ETH_DMACRCR_RPBL_32PBL);  // Set receive burst length

  // Set Rx buffer size
  MODIFY_REG(ETH->DMACRCR, ETH_DMACRCR_RBSZ, (HAL_ETHERNET_BUFFER_SIZE << ETH_DMACRCR_RBSZ_Pos));

  // Configure interrupt mode
  ETH->DMAMR &= ~ETH_DMAMR_INTM;

  // Enable transmit interrupt
  ETH->DMACIER |= ETH_DMACIER_NIE | ETH_DMACIER_TIE;
}

void Ethernet::ReleaseRxDescriptor(int descriptor_index) {
  // Set descriptor values
  DmaDescriptor *rx_descriptor_to_release =
      &g_dma_rx_descriptors[descriptor_index];
  // Set buffer 1 address pointer
  rx_descriptor_to_release->des_0_ = (uint32_t) (&g_rx_buffer[descriptor_index][0]);
  rx_descriptor_to_release->des_3_ |= ETH_DMARXNDESCRF_BUF1V;  // Set buffer 1 address to valid
  rx_descriptor_to_release->des_2_ = (uint32_t) NULL;  // Reset buffer 2 address pointer
  rx_descriptor_to_release->des_3_ &= ~ETH_DMARXNDESCRF_BUF2V;  // Set buffer 2 address to invalid
  rx_descriptor_to_release->des_3_ &= ~ETH_DMARXNDESCRF_IOC;  // Disable interrupt on completion

  // Release descriptor to DMA
  rx_descriptor_to_release->des_3_ |= ETH_DMARXNDESCRF_OWN;
}

int Ethernet::GetAvailableTxDescriptorCount() {
  int cnt = 0;  // Counter
  int index = dma_tx_descriptor_index_;  // Iterator

  do {
    // Check if descriptor is owned by the DMA
    if (g_dma_tx_descriptors[index].des_3_ & ETH_DMATXNDESCRF_OWN) {
      break;
    }

    // Increment counter
    cnt++;

    // Increment iterator
    index++;
    if (index >= HAL_ETHERNET_TX_DESCRIPTOR_COUNT) index = 0;
  } while (cnt < HAL_ETHERNET_TX_DESCRIPTOR_COUNT);

  return cnt;
}

bool Ethernet::RxPacketAvailable() {
  // Loop through RX descriptors starting from current descriptor
  int descriptor_index_iterator = dma_rx_descriptor_index_;
  for (int i = 0; i < HAL_ETHERNET_RX_DESCRIPTOR_COUNT; i++) {
    // Check if descriptor is owned by the DMA
    if (g_dma_rx_descriptors[descriptor_index_iterator].des_3_ & ETH_DMARXNDESCWBF_OWN) {
      break;
    }

    // Check last descriptor bit (if the last descriptor bit is set, this means the full packet has
    // been received)
    if (g_dma_rx_descriptors[descriptor_index_iterator].des_3_ & ETH_DMARXNDESCWBF_LD) {
      return true;
    }

    // Set iterator to next descriptor in ring buffer
    descriptor_index_iterator++;
    if (descriptor_index_iterator >= HAL_ETHERNET_RX_DESCRIPTOR_COUNT) {
      descriptor_index_iterator = 0;
    }
  }

  // No last descriptor found, therefore no full package available
  return false;
}

bool Ethernet::IsPtpEventMessage(uint8_t *message_buffer) {
  // Check ethernet type
  uint16_t ethernet_type = (((uint16_t) (message_buffer[12])) << 8u) |
      ((uint16_t) (message_buffer[13]));
  if (ethernet_type == ethernet_type_ipv4_) {
    // Check IPv4 protocol
    uint8_t ipv4_protocol = message_buffer[23];
    if (ipv4_protocol == ipv4_protocol_udp_) {
      // Check UDP source and destination Port
      uint16_t source_port = (((uint16_t) (message_buffer[34])) << 8u) |
          ((uint16_t) (message_buffer[35]));
      uint16_t destination_port = (((uint16_t) (message_buffer[36])) << 8u) |
          ((uint16_t) (message_buffer[37]));
      if (source_port == ptp_event_message_port_ &&
          destination_port == ptp_event_message_port_) {
        // message is an IPv4 PTP event message
        return true;
      }
    }
  }

  // message is no IPv4 PTP event message
  return false;
}

void Ethernet::Ieee1588Init() {
  // Disable timestamp trigger interrupt
  ETH->MACIER &= ~ETH_MACIER_TSIE;

  // Enable timestamping
  ETH->MACTSCR |= ieee1588_tsena_;

  // Configure time update
  // Select digital rollover (Timestamp low register in nanoseconds)
  ETH->MACTSCR |= ieee1588_tsctrlssr_;
  // PTP clock period in nanoseconds
  double accumulator_output_clock =
      1000000000.0 * (static_cast<float>(0xFFFFFFFF) / ieee1588_addend_value_) /
      (static_cast<double>(Rcc::GetFrequency(Rcc::kClockHclk)));
  // Ensure that 1000000000 % ssinc == 0 (necessary for PPS output), First try to find next higher
  // valid ssinc value to ensure a wide clock frequnecy adjustment range in both directions.
  uint8_t ssinc = 0;
  for (int i = static_cast<uint8_t>(accumulator_output_clock + 1); i < 256; i++) {
    if (1000000000 % i == 0) {
      ssinc = i;
      break;
    }
  }
  // If no higher valid ssinc value was found use the next lower one.
  if (ssinc == 0) {
    for (auto i = static_cast<uint8_t>(accumulator_output_clock + 1); i > 0; i--) {
      if (1000000000 % i == 0) {
        ssinc = i;
        break;
      }
    }
  }
  ETH->MACSSIR |= ((uint32_t) ssinc) << 16u;
  ETH->MACSSIR &= 0xff00ffff | ((uint32_t) ssinc) << 16u;
  // Correct addend for the fractional part that cannot be represented by the subsecond increment
  // register
  ieee1588_addend_value_ = static_cast<float>(0xFFFFFFFF) * 1000000000.0 /
      (static_cast<float>(ssinc) * Rcc::GetFrequency(Rcc::kClockHclk));
  ETH->MACTSAR = ieee1588_addend_value_;  // Initial addend register value
  // TSADDREG must be zero before setting it
  while (ETH->MACTSCR & ieee1588_tsaddreg_) {}
  ETH->MACTSCR |= ieee1588_tsaddreg_;  // Update addend register in the PTP block
  // Wait for addend register update do finish
  while (ETH->MACTSCR & ieee1588_tsaddreg_) {}

  // Select fine correction method
  ETH->MACTSCR |= ieee1588_tscfupdt_;

  // Initialize system time
  ETH->MACSTSUR = 0;
  ETH->MACSTNUR = 0;  // Bit 31 is add/subtract bit
  while (ETH->MACTSCR & ieee1588_tsinit_) {}  // TSINIT must be zero before setting it
  // Initialize system time with values written in system time update registers
  ETH->MACTSCR |= ieee1588_tsinit_;
  // Wait for system time initialization to finish
  while (ETH->MACTSCR & ieee1588_tsinit_) {}

  // Enable MAC receiver and transmitter.
  ETH->MACCR |= ETH_MACCR_TE | ETH_MACCR_RE;

  // Transmit timestamp status mode
  ETH->MACTSCR |= ieee1588_txtsstsm_;

  // Enable PTP packet processing for version 2 format
  ETH->MACTSCR |= ieee1588_tsver2ena_;

  // Enable timestamp for all packets
  ETH->MACTSCR |= ieee1588_tsenall_;

  // Initializes PPS output
  Ieee1588PpsInit(ssinc);
}

void Ethernet::Ieee1588PpsInit(uint8_t ssinc) {
  ASSERT(ssinc > 0);
  ASSERT(1000000000 % ssinc == 0);

  ETH->MACPPSCR |= ieee1588_ppsen0_;  // Flexible PPS output mode
  // Start pulse train 1 second after the next second increment (This is necessary since the next
  // second increment may occur before the start pulse train command is processed)
  ETH->MACPPSTTSR = ETH->MACSTSR + 2;
  ETH->MACPPSIR = (1000000000 / ssinc) - 1;  // PPS interval 1 second
  ETH->MACPPSWR = (1000000 / ssinc) - 1;  // PPS width 1 millisecond
  ETH->MACPPSCR |= 0x2u;  // Start pulse train
}

void Ethernet::SetMdioClockRange() {
  // Get hclk frequency
  uint32_t hclk = Rcc::GetFrequency(Rcc::kClockHclk);

  // Set management data clock (MDC) clock divider depending on hclk
  if ((hclk >= 20000000U) && (hclk < 35000000U)) {
    MODIFY_REG(ETH->MACMDIOAR, ETH_MACMDIOAR_CR, (uint32_t) ETH_MACMDIOAR_CR_DIV16);
  } else if ((hclk >= 35000000U) && (hclk < 60000000U)) {
    MODIFY_REG(ETH->MACMDIOAR, ETH_MACMDIOAR_CR, (uint32_t) ETH_MACMDIOAR_CR_DIV26);
  } else if ((hclk >= 60000000U) && (hclk < 100000000U)) {
    MODIFY_REG(ETH->MACMDIOAR, ETH_MACMDIOAR_CR, (uint32_t) ETH_MACMDIOAR_CR_DIV42);
  } else if ((hclk >= 100000000U) && (hclk < 150000000U)) {
    MODIFY_REG(ETH->MACMDIOAR, ETH_MACMDIOAR_CR, (uint32_t) ETH_MACMDIOAR_CR_DIV62);
  } else if ((hclk >= 150000000U) && (hclk < 250000000U)) {
    MODIFY_REG(ETH->MACMDIOAR, ETH_MACMDIOAR_CR, (uint32_t) ETH_MACMDIOAR_CR_DIV102);
  } else {  // hclk >= 250000000U
    MODIFY_REG(ETH->MACMDIOAR, ETH_MACMDIOAR_CR, (uint32_t) ETH_MACMDIOAR_CR_DIV124);
  }
}
}  // namespace hal


/**************************************************************************************************
 *     Global Interrupt handler implementations                                                   *
 **************************************************************************************************/

extern "C" {
//! Ethernet ISR
void ETH_IRQHandler() {
  // Receive interrupt
  if (ETH->DMACSR & ETH_DMACSR_RI) {
    // Clear interrupt flag
    ETH->DMACSR = (ETH_DMACSR_RI | ETH_DMACSR_NIS);
  }

  // Transmit interrupt
  if (ETH->DMACSR & ETH_DMACSR_TI) {
    // Only get timestamp if timestamp interrupt is available
    if (hal::Ethernet::tx_timestamp_callback_ != nullptr) {
      // Iterate backwards over Rx descriptors to find the last normal descriptor
      int descriptor_index_iterator = hal::Ethernet::dma_tx_descriptor_index_;
      for (int i = 0; i < HAL_ETHERNET_TX_DESCRIPTOR_COUNT; i++) {
        // Decrement iterator
        descriptor_index_iterator--;
        if (descriptor_index_iterator < 0) {
          descriptor_index_iterator = HAL_ETHERNET_TX_DESCRIPTOR_COUNT - 1;
        }

        // Last normal descriptor found
        if (hal::g_dma_tx_descriptors[descriptor_index_iterator].des_3_ &
            ETH_DMATXNDESCWBF_LD &&
            !(hal::g_dma_tx_descriptors[descriptor_index_iterator].des_3_ &
                ETH_DMATXNDESCWBF_OWN)) {
          // Check if a timestamp and packet info is available
          if (hal::g_dma_tx_descriptors[descriptor_index_iterator].des_3_ &
              ETH_DMATXNDESCWBF_TTSS &&
              hal::g_dma_tx_descriptors[descriptor_index_iterator].packet_info_ &
                  hal::Ethernet::packet_info_data_valid_) {
            uint32_t timestamp_low =
                hal::g_dma_tx_descriptors[descriptor_index_iterator].des_0_;
            uint32_t timestamp_high =
                hal::g_dma_tx_descriptors[descriptor_index_iterator].des_1_;
            uint8_t message_type =
                hal::g_dma_tx_descriptors[descriptor_index_iterator].packet_info_ &
                    hal::Ethernet::packet_info_ptp_message_type_;
            uint16_t sequence_id =
                (hal::g_dma_tx_descriptors[descriptor_index_iterator].packet_info_ &
                    hal::Ethernet::packet_info_ptp_sequence_id_) >>
                    hal::Ethernet::packet_info_ptp_sequence_id_pos_;
            hal::Ethernet::tx_timestamp_callback_(timestamp_high, timestamp_low, message_type,
                                                  sequence_id);
          }
          break;
        }
      }
    }

    // Clear interrupt flag
    ETH->DMACSR = (ETH_DMACSR_TI | ETH_DMACSR_NIS);
  }
}
}
