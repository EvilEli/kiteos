// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#ifndef SRC_HAL_HAL_HPP_
#define SRC_HAL_HAL_HPP_
//*****************************************************************************
//
//! \addtogroup hal HAL API
//! @{
//!
//! \addtogroup hal_init HAL Init
//! @{
//
//*****************************************************************************

#include <cstdint>

namespace hal {
/*!
 *  \brief   Implements the HAL System class.
 *  \details This class is used to setup the basic HAL system and configure the clock tree.
 *
 *  \author   Elias Rosch
 *  \date     2020
 */
class System {
 public:
  /*!
  * \brief   Main function to initialize the hardware abstraction layer system.
  *
  * \return  Returns true if HAL is initialized successfully. false otherwise.
  */
  static bool Init();

 private:
  /*!
  * \brief   Configures the oscillators and system clock tree.
  */
  static void InitClockTree();
};
}  // namespace hal

//*****************************************************************************
//
// Close the Doxygen groups.
//! @}
//! @}
//
//*****************************************************************************
#endif  // SRC_HAL_HAL_HPP_
