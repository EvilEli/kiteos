// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#include <stm32h7xx.h>
#include "hal/systick_timer.hpp"
#include "utils/debug_utils.h"

namespace hal {
/**************************************************************************************************
 *     Static private member variables                                                            *
 **************************************************************************************************/
uint64_t SystickTimer::tick_count_ = 0;
SystickTimer::CallbackFunction SystickTimer::systick_callback_ = nullptr;
void* SystickTimer::systick_callback_argument_ = nullptr;

/**************************************************************************************************
 *     Public methods                                                                             *
 **************************************************************************************************/
bool SystickTimer::Init() {
  return (SysTick_Config(SystemCoreClock / (1000UL)) == 0);
}

uint64_t SystickTimer::GetCount() {
  return tick_count_;
}

void hal::SystickTimer::RegisterSystickCallback(CallbackFunction callback,
                                                void *callback_argument) {
  // Ensure that the callback is only registered once
  ASSERT(systick_callback_ == nullptr);
  ASSERT(callback_argument == nullptr);

  // Register callback
  systick_callback_ = callback;
  systick_callback_argument_ = callback_argument;
}

}  // namespace hal

/**************************************************************************************************
 *     Callbacks                                                                                  *
 **************************************************************************************************/
extern "C" {
//! Interrupt handler of System Ticks
void SysTick_Handler(void) {
  hal::SystickTimer::tick_count_++;
  if (hal::SystickTimer::systick_callback_) {
    hal::SystickTimer::systick_callback_(hal::SystickTimer::systick_callback_argument_);
  }
}
}  // extern "C"
