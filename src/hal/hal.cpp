// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#include "hal/hal.hpp"
#include "hal/mpu.hpp"
#include "hal/rcc.hpp"
#include "hal/systick_timer.hpp"
#include "hal/ethernet.hpp"
#include "hal/rng.hpp"

namespace hal {

bool System::Init() {
  // Use 0 bits for group priority and 8 bits for sub priority. This effectively disables nested
  // interrupts. When changing this, it may be necessary to adjust MAX_PROCESS_CNT in the thread
  // safe buffer class.
  NVIC_SetPriorityGrouping(0x7);

  Mpu::Init();

  InitClockTree();

  if (!SystickTimer::Init()) {
    return false;
  }

  Ethernet::Init();
  Ethernet::SetDuplexMode(Ethernet::kFullDuplex);
  Ethernet::SetSpeed(Ethernet::k100Mbps);

  // Initialize random number generator
  if (!Rng::Init()) {
    return false;
  }

  return true;
}

void System::InitClockTree() {
  MODIFY_REG(PWR->CR3, PWR_CR3_SCUEN, 0);  // Supply configuration update locked

  // Set voltage scaling to VOS1 which is needed for maximum performance (must be set to VOS1 before
  // activating overdrive mode)
  MODIFY_REG(PWR->D3CR, PWR_D3CR_VOS, PWR_D3CR_VOS_1 | PWR_D3CR_VOS_0);
  // Enable the SYSCFG clock
  Rcc::EnablePeripheralClock(Rcc::kSysCfg);
  // Overdrive mode enabled (boosts the voltage scaling level to VOS0). This is necessary to set the
  // clock to 480MHz (the junction temperature should be supervised in this mode and not get higher
  // than 105°C)
  SET_BIT(SYSCFG->PWRCR, SYSCFG_PWRCR_ODEN);
  // Wait for the core to reach the selected voltage level
  while (!((PWR->D3CR & PWR_D3CR_VOSRDY) == PWR_D3CR_VOSRDY)) {}

#if USE_EVAL_BOARD
  Rcc::EnableOscillator(Rcc::kOscillatorCsi);
  Rcc::SetClockSource(Rcc::kPllCsi);
  // Configure PLL1P 480 MHz
  Rcc::PllConfig pll1_config = {1, 240, 2, 2, 2, 0};
  // Configure PLL2P 100 MHz
  Rcc::PllConfig pll2_config = {1, 50, 2, 2, 2, 0};
  // Configure PLL3P 64 MHz
  Rcc::PllConfig pll3_config = {1, 48, 3, 3, 3, 0};
#else
  Rcc::EnableOscillator(Rcc::kOscillatorHse);
  Rcc::SetClockSource(Rcc::kPllHse);
  // Configure PLL1P 480 MHz
  Rcc::PllConfig pll1_config = {5, 192, 2, 2, 2, 0};
  // Rcc::PllConfig pll_config = {10, 312, 2, 2, 2, 4096};  // HSE 30.72 MHz
  // Configure PLL2P 100 MHz
  Rcc::PllConfig pll2_config = {4, 48, 3, 1, 1, 0};
  // Configure PLL3P 64 MHz
  Rcc::PllConfig pll3_config = {5, 64, 5, 1, 1, 0};
#endif
  Rcc::EnablePll(Rcc::kPll1, &pll1_config);

  // Configure Sysclock Tree
  Rcc::ConfigureSysclkDomain();
  // Set PLL as system clock source
  Rcc::SetClockSource(Rcc::kSysclkPll);

  // Update clock value
  SystemCoreClockUpdate();

  // pll_config = {12, 312, 8, 2, 2, 4096};  // HSE 30.72 MHz
  Rcc::EnablePll(Rcc::kPll2, &pll2_config);

  // pll_config = {15, 312, 10, 10, 10, 4096};  // HSE 30.72 MHz
  Rcc::EnablePll(Rcc::kPll3, &pll3_config);

  // Configure peripheral clock sources
  Rcc::SetClockSource(Rcc::kMco2Pll2P);
  Rcc::SetClockSource(Rcc::kSpi123Pll3P);
  Rcc::SetClockSource(Rcc::kI2c123Pclk1);
  Rcc::SetClockSource(Rcc::kI2c4Pclk4);
  Rcc::SetClockSource(Rcc::kUsart16Pclk2);
  Rcc::SetClockSource(Rcc::kUsart234578Pclk1);
  Rcc::SetClockSource(Rcc::kAdcPll2P);
  Rcc::SetClockSource(Rcc::kRngPll1Q);
}
}  // namespace hal
