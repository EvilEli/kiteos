// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.

#include "hal/rng.hpp"
#include "hal/rcc.hpp"

namespace hal {

uint32_t Rng::latest_random_;  //!< latest returned random value.
uint32_t Rng::lfsr_value_;  //!< Value of the lfsr used for pseudo random number generation.
/**************************************************************************************************
 *     API function implementations                                                               *
 **************************************************************************************************/
bool Rng::Init(void) {
  // Enable Rng peripheral clock
  Rcc::EnablePeripheralClock(Rcc::kRng);

  // Disable the RNG Peripheral
  CLEAR_BIT(RNG->CR, RNG_CR_RNGEN);

  // Disable clock error detection
  SET_BIT(RNG->CR, RNG_CR_CED);

  // Enable the RNG Peripheral
  SET_BIT(RNG->CR, RNG_CR_RNGEN);

  // Verify that no seed error interrupt
  while (READ_BIT(RNG->SR, RNG_SR_SEIS)) {
    // Recover the module
    CLEAR_BIT(RNG->SR, RNG_SR_SEIS);
    for (uint8_t i = 0; i < 12; i++) {
      GetNumberBlocking();
    }
  }

  lfsr_value_ = GetNumberBlocking();
  return true;
}

uint32_t Rng::GetNumberBlocking() {
    // Check that data register contains valid random data
  while (!READ_BIT(RNG->SR, RNG_SR_DRDY)) {}
  latest_random_ = RNG->DR;
  return latest_random_;
}

uint32_t Rng::GetPseudoRandomNumber(void) {
  uint32_t lsb = lfsr_value_ & 1u;
  lfsr_value_ >>= 1u;
  if (lsb)
    lfsr_value_ ^= 0xB400u;
  return lfsr_value_;
}
};  // namespace hal
