// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#ifndef SRC_HAL_RCC_HPP_
#define SRC_HAL_RCC_HPP_

//*****************************************************************************/
//! \addtogroup hal HAL API
//! @{
//! \addtogroup hal_rcc_api HAL RCC API
//! @{
///
//*****************************************************************************/
#include <hal/register.hpp>
namespace hal {
/*!
 *  \brief   Implements the HAL Rcc class.
 *  \details This class is used to setup the RCC for peripherals.
 *
 *  \author   Elias Rosch
 *  \date     2020
 */
class Rcc {
 public:
  //! Clock source options available to configure
  enum Oscillator {
    kOscillatorHsi,
    kOscillatorCsi,
    kOscillatorHse
  };

  //! Pll options available to configure
  enum Pll {
    kPll1,
    kPll2,
    kPll3
  };
  //! Struct containing multiplier and divider for a PLL
  struct PllConfig {
    uint32_t divm;  //!< M parameter can be a value between 0 and 63
    uint32_t divn;  //!< N parameter can be a value between 4 and 512
    uint32_t divp;  //!< P parameter can be a value between 1 and 128
    uint32_t divq;  //!< Q parameter can be a value between 1 and 128
    uint32_t divr;  //!< R parameter can be a value between 1 and 128
    uint32_t fracn;  //!< FRAC parameter can be a value between 0 and 8191
  };

  //! Clock source options available to configure
  enum ClockSource {
    kPllHsi,
    kPllCsi,
    kPllHse,
    kSysclkPll,
    kMco2Pll2P,
    kSpi123Pll2P,
    kSpi123Pll3P,
    kI2c123Pclk1,
    kI2c4Pclk4,
    kUsart16Pclk2,
    kUsart234578Pclk1,
    kAdcPll2P,
    kRngPll1Q,
  };

  //! Peripheral clocks available on the microcontroller.
  enum Peripheral {
    kSysCfg,
    kGpioA,
    kGpioB,
    kGpioC,
    kGpioD,
    kGpioE,
    kGpioF,
    kGpioG,
    kGpioH,
    kGpioI,
    kGpioJ,
    kGpioK,
    kEthernet,
    kSpi1,
    kSpi2,
    kI2c1,
    kI2c3,
    kI2c4,
    kUsart1,
    kUsart2,
    kUsart3,
    kUart4,
    kUart5,
    kUsart6,
    kTim1,
    kTim2,
    kTim3,
    kTim4,
    kTim5,
    kAdc12,
    kAdc3,
    kRng,
    kHsem
  };

  //! Clocks available on the microcontroller.
  enum Clock {
    kClockSysclk,
    kClockD1Sysclk,
    kClockHclk,
    kClockPclk1,
  };

  //! Sets the clock source configuration as provided by clock sourcee
  // \param clock_source enum containing the clock source to configure
  static void EnableOscillator(Oscillator oscillator);

  //! Enables a specific Pll with given parameters
  // \param pll enum which determines the pll to configure
  // \param pll_config struct which determines the multiplier and divider values
  static void EnablePll(Pll pll, PllConfig* pll_config);

  //! Sets the clock source configuration as provided by clock sourcee
  // \param clock_source enum containing the clock source to configure (for a respective input)
  static void SetClockSource(ClockSource clock_source);

  //! Sets the clock dividers of the sysclk domain
  static void ConfigureSysclkDomain();

  //! Enables the peripheral clock for provided peripheral
  // \param peripheral enum which determines the clock to be enabled
  static void EnablePeripheralClock(Peripheral peripheral);

  //! Disables the peripheral clock for provided peripheral
  // \param peripheral enum which determines the clock to be enabled
  static void DisablePeripheralClock(Peripheral peripheral);

  //! Gets the frequency of provided clock type
  // \param clock enum which determines frequency to get
  // \return frequency of clock type in Hz
  static uint32_t GetFrequency(Clock clock);

 private:
  //! Map from clock source enum to a RegisterConfig structure
  static Register::Config clock_source_config_[];
  //! Map from peripheral clock enum to a RegisterConfig structure
  static Register::Config peripheral_clock_config_[];
};
}  // namespace hal
/*! @} */
/*! @} */
#endif  // SRC_HAL_RCC_HPP_
