// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.

#ifndef SRC_UTILS_DEBUG_UTILS_H_
#define SRC_UTILS_DEBUG_UTILS_H_
#ifdef __cplusplus
extern "C" {
#endif

//! Defines an assert which checks the response of a function, but does not delete the function in a
//! release version. Usage: ASSERT_RESPONSE(foo(), 1);
#if defined(DEBUG) || defined(X86_SYSTEM)
#define ASSERT_RESPONSE(function, expected_response) ASSERT(function == expected_response)
#else
#define ASSERT_RESPONSE(function, expected_response) function
#endif

/*!*********************************************************
 *   This function is called if an assertion has failed.   *
 *   Traps the microcontroller in debug version.           *
 *   Prints the error source in  unit test version.        *
 *   Does nothing in a release version.                    *
 *   Usage: ASSERT(foo == 1);                              *
 ***********************************************************/
#ifdef DEBUG
#define ASSERT(statement_expected_to_be_true) \
        if (!(statement_expected_to_be_true)) while (1)  // NOLINT
#elif defined(X86_SYSTEM)
#include <stdio.h>
#include <stdlib.h>
#define ASSERT(statement_expected_to_be_true) \
        if (!(statement_expected_to_be_true)) { \
          printf("\n\nASSERT FAILED\n File: %s, Line:%d \n\n", __FILE__, __LINE__); \
          exit(1); \
        }
#else
#define ASSERT(statement_expected_to_be_true)
#endif

#ifdef __cplusplus
}
#endif
#endif  // SRC_UTILS_DEBUG_UTILS_H_
