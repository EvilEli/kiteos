// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.

#ifndef SRC_PULICAST_PORT_CUSTOM_RAND_H_
#define SRC_PULICAST_PORT_CUSTOM_RAND_H_

#include "hal/rng.hpp"


namespace pulicast::core {
    /*!
   * \brief  Generate a true random number.
   *
   * \return True random 64 bit number.
   */
  inline int64_t pulicast_rand() {
    return (static_cast<uint64_t>(hal::Rng::GetPseudoRandomNumber()) << 32u) |
           static_cast<uint64_t>(hal::Rng::GetPseudoRandomNumber());
  }
}  // namespace pulicast::core

#endif  // SRC_PULICAST_PORT_CUSTOM_RAND_H_
