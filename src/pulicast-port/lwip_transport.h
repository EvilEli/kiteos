// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.

#ifndef SRC_PULICAST_PORT_LWIP_TRANSPORT_H_
#define SRC_PULICAST_PORT_LWIP_TRANSPORT_H_

#include <memory>
#include <string>
#include <functional>
#include <unordered_map>

#include <asio/buffer.hpp>

#include "lwip/udp.h"
#include "lwip/igmp.h"

#include "pulicast/address.h"
#include "pulicast/core/transport.h"
#include "pulicast/core/utils/scatter_gather_buffer.h"

#include "pulicast/transport/udp_transport.h"

namespace pulicast::transport {

//! Type of callbacks delivering pulicast payload
using PulicastCallback = std::function<void(asio::mutable_buffer &, Address)>;

/*!
 *  \brief   Implements the multicast udp sender using the lwIP library.
 *
 *  \author   Elias Rosch
 *  \date     2020
 */
class LwipChannelSender : public UDPChannelSender<LwipChannelSender> {
 public:
  /*!
   * \brief  Constructor of the LwipChannelSender.
   *
   * \param  channel_name to publish on.
   * \param  pcb lwIP protocol control block used for sending.
   * \param  port Multicast UDP port to publish on.
   *
   * \return Returns constructed LwipChannelSender-instance.
   */
  LwipChannelSender(const std::string &channel_name,
                    udp_pcb* pcb,
                    uint16_t port) : UDPChannelSender<LwipChannelSender>(channel_name),
                                     send_pcb_(pcb),
                                     ipaddr_{htonl(MulticastGroupForChannel(channel_name))},
                                     port_(port) {}

  /*!
   * \brief Send out a pulicast message.
   *
   * \param data scatter-gather buffer of payload.
   */
  void SendToSocket(const core::ScatterGatherBuffer &data) {
    struct pbuf *p = nullptr;

    auto total_size = asio::buffer_size(data);

    // Allocate a new pbuf for sending.
    p = pbuf_alloc(PBUF_TRANSPORT, total_size, PBUF_RAM);

    if (!p) {
      send_errors_++;
      return;
    }

    // Copy the data into the pbuf, i.e. to lwIP stack memory.
    uint16_t offset = 0;
    for (auto& buffer : data) {
      if (ERR_OK != pbuf_take_at(p, asio::buffer_cast<const void*>(buffer),
                      asio::buffer_size(buffer), offset)) {
        send_errors_++;
        break;
      }
      offset += asio::buffer_size(buffer);
    }

    udp_set_multicast_netif_addr(send_pcb_, &ipaddr_);
    if (ERR_OK != udp_sendto(send_pcb_, p, &ipaddr_, port_)) {
      // Dont't publish logs here since that would likely cause another send error.
      send_errors_++;
    }
    // Free the pbuf.
    pbuf_free(p);
  }

 private:
  udp_pcb* send_pcb_;  //!< lwIP UDP protocol control block.
  ip_addr_t ipaddr_;   //!< multicast IP to be used for channel
  uint16_t port_;      //!< destination port to be used for multicast messages
  uint32_t send_errors_ = 0;  //!< counter for send error occurences.
};

/*!
 *  \brief   Pulicast transport interface for lwIP udp senderin and reading.
 *
 *  \author   Elias Rosch
 *  \date     2020
 */
class LwipTransport : public pulicast::core::Transport {
 public:
  /*!
   * \brief  Constructor of the LwipTransport.
   *
   * \param port Multicast UDP port to use.
   * \param ttl Multicast UDP ttl to use.
   *
   * \return Returns constructed LwipTransport-instance.
   */
  LwipTransport(uint16_t port, uint8_t ttl)
      : pcb_(nullptr),
        port_(port) {
    pcb_ = udp_new();
    udp_set_multicast_ttl(pcb_, ttl);
    ip_addr_t ipaddr = {IPADDR_ANY};  // all network interfaces should listen to multicast
    udp_bind(pcb_, &ipaddr, port_);
    udp_recv(pcb_,
      [](void *arg, struct udp_pcb *pcb, struct pbuf *p, const ip_addr_t *addr, u16_t port) {
      reinterpret_cast<LwipTransport*>(arg)->CallbackUdpRx(p, addr, port);},
      this);
  }

  /*!
   * \brief Create a new ChannelSender object to send data on a channel.
   * \param channel_name The name of the channel to send on.
   * \return A unique ptr to a new channel sender object.
   */
  std::unique_ptr<core::ChannelSender> MakeChannelSender(const std::string &channel) override {
    return std::make_unique<LwipChannelSender>(channel, pcb_, port_);
  }

  /*!
   * \brief Starts receiving messages on the given channel and calls the passed callback whenever
   *        a new packet arrives. This is assumed to be called only once per channel.
   *        Multiple subscriptions are then handled by pulicast itself.
   *
   * \param channel_name The name of the channel to receive from.
   * \param packet_callback The callback to call when a new packet arrived.
   * It takes an asio mutable buffer and the address of the packet source as parameters.
   */
  void StartReceivingMessagesFor(const std::string &channel, PulicastCallback cb) override {
    ip_addr_t ipaddr = {IPADDR_ANY};  // all network interfaces should listen to multicast
    ip_addr_t ipaddr_group = {htonl(MulticastGroupForChannel(channel))};
    err_t err = igmp_joingroup(&ipaddr, &ipaddr_group);
    ASSERT(err == ERR_OK);
    if (err != ERR_OK) {
      return;
    }
    subscribed_channels_.insert({channel, cb});
  }

 private:
   /*!
   * \brief Called upon lwIP input handling (new UDP arrived).
   *
   * \param p the packet buffer that was received
   * \param addr the remote IP address from which the packet was received
   * \param port the remote port from which the packet was received
   *
   * \sa see documentation of lwIP (http://www.nongnu.org/lwip/2_0_x/index.html)
   * for further information.
   *
   */
  void CallbackUdpRx(struct pbuf *p, const ip_addr_t *addr, u16_t port) {
    uint16_t tot_len = p->tot_len;

    auto buffer_store = std::make_unique<char[]>(tot_len);

    if (!buffer_store) {
      pbuf_free(p);  // free before exit
      return;
    }

    if (pbuf_copy_partial(p, buffer_store.get(), tot_len, 0) != tot_len) {
      pbuf_free(p);  // free before exit
      return;
    }

    pbuf_free(p);  // free before packet handling

    auto buffer = asio::buffer(buffer_store.get(), tot_len);
    auto channel_name = ExtractHeaderAndChannelName(buffer);
    if (channel_name.has_value() && subscribed_channels_.count(channel_name.value()) > 0) {
      subscribed_channels_.at(channel_name.value())(buffer, Address{addr->addr, port});
    }
  }

  udp_pcb *pcb_;   //!< lwIP UDP protocol control block.
  uint16_t port_;  //!< port to be used for multicast messages.
  //! Map holding all subscribed channels.
  std::unordered_map<std::string_view, PulicastCallback> subscribed_channels_;
};
}  // namespace pulicast::transport

#endif  // SRC_PULICAST_PORT_LWIP_TRANSPORT_H_
