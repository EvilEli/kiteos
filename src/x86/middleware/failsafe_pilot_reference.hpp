// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#ifndef SRC_X86_MIDDLEWARE_FAILSAFE_PILOT_REFERENCE_HPP_
#define SRC_X86_MIDDLEWARE_FAILSAFE_PILOT_REFERENCE_HPP_

/**
 * @brief Pilot Reference class dummy for x86 builds.
 */
class FailsafePilotReference {
 public:
  /*!
   * \brief Constructor of the failsafe pilot reference component.
   */
  FailsafePilotReference() = default;

  /*!
   * \brief Destructor of the failsafe pilot reference component.
   */
  ~FailsafePilotReference() = default;
};

#endif  // SRC_X86_MIDDLEWARE_FAILSAFE_PILOT_REFERENCE_HPP_
