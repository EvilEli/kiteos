// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#ifndef SRC_X86_DRIVERS_BUZZER_HPP_
#define SRC_X86_DRIVERS_BUZZER_HPP_

/*!
 *  \brief  Implements the Buzzer driver.
 */
class Buzzer {
 public:
  /*!
   * \brief  Constructor of the Buzzer-Component.
   */
  Buzzer() {}

   /*!
   * \brief Destructor of the Buzzer-Component.
   */
  ~Buzzer() = default;

  //! \brief Enables the buzzer sound.
  void Enable() {}
  //! \brief Disables the buzzer sound.
  void Disable() {}
};

#endif  // SRC_X86_DRIVERS_BUZZER_HPP_
