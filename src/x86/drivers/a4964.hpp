// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#ifndef SRC_X86_DRIVERS_A4964_HPP_
#define SRC_X86_DRIVERS_A4964_HPP_

#include <string>
#include "core/state_node.hpp"
#include "kitecom/timestamped_vector_double.hpp"

/*!
 *  \brief   Implements the A4964 motor controller driver.
 *
 *  \details This class is used to control the A4964 motor drivers on Axon3 boards.
 */
class A4964 {
 public:
  //! Struct used for communication with the plugin that calls the driver.
  struct MeasurementData {
    double temperature;      //!< Motor temperature.
    bool temperature_valid;  //!< Flag if there is a new temperature value.
    double speed;            //!< Motor speed.
    bool speed_valid;        //!< flag if there is a new speed value.
    uint64_t timestamp;      //!< Timestamp when the measurement data was measured.
  };

  /*!
   * \brief  Constructor of the A4964-Component.
   *
   * \param  plugin_str  String that names the plugin.
   *
   * \return Returns constructed A4964-instance.
   */
  explicit A4964(StateNode* state_node);


  /*!
   * \brief Destructor of the A4964-Component.
   */
  ~A4964();

  /*!
   * \brief  Lets the driver do its work, this should be called periodically.
   *
   * \param  measurement_data  Structure to be filled with the latest measurement if new measurement
   *                           data is available.
   */
  void Run(MeasurementData* measurement_data);

  /*!
   * \brief Sets speed of a4964.
   *
   * \param setpoint  New speed set point.
   */
  void SetSpeed(double setpoint);

  /**
   * @brief Callback for arrived commands
   *
   * @param command which arrives via the state node
   */
  void CommandCallback(std::string_view command);

 private:
  /*!
   * \brief Callback executed when a new true speed message arrives
   *
   * \param msg     KITECOM message
   */
  void MsgHandlerTrueSpeed(const kitecom::timestamped_vector_double& msg);

  /*!
   * \brief Get the right namespace for the motor (using info from emulatedpersistent memory)
   */
  std::string GetMotorNamespace();

  StateNode* state_node_;  //!< Callback to report internal state.
  pulicast::Channel& true_setpoint_channel_;  //!< Output channel of true setpoint data
  pulicast::Channel& true_speed_channel_;  //!< Input channel of true speed data
  double upper_limit_;               //!< Value of upper set point limit (above is clipped).
  double lower_limit_;               //!< Value of lower set point limit (below is clipped)
  MeasurementData measurement_data_;  //!< Last measurement data
};
/*! @} */
#endif  // SRC_X86_DRIVERS_A4964_HPP_
