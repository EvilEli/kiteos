// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#ifndef SRC_X86_DRIVERS_FAILSAFE_SWITCH_HPP_
#define SRC_X86_DRIVERS_FAILSAFE_SWITCH_HPP_

#include <cstdint>

/*!
 *  \brief Implements a driver to easily access the FailsafeSwitch.
 *         The driver uses a hardware timer which triggers a GPIO if the Set()
 *         method is not called for a specific period.
 */
class FailsafeSwitch {
 public:
  //! \brief Constructor of the FailsafeSwitch driver component.
  FailsafeSwitch() {}

  //! \brief Destructor of the FailsafeSwitch driver component.
  ~FailsafeSwitch() = default;

  //! \brief Sets the value of the failsafe gpio.
  //         Must be called periodically.
  //         If not the driver shuts down the GPIO after a defined timeout.
  void Set(bool value) {}
};

#endif  // SRC_X86_DRIVERS_FAILSAFE_SWITCH_HPP_
