// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#ifndef SRC_X86_DRIVERS_MPL3115A2_HPP_
#define SRC_X86_DRIVERS_MPL3115A2_HPP_

#include "pulicast-port/pulicast_embedded.h"
#include "kitecom/timestamped_vector_double.hpp"

/*!
 *  \brief  Implements the Mpl3115a2 driver.
 */
class Mpl3115a2 {
 public:
  //!< callback that is called if all channels are finished reading.
  typedef void (*DataReadyCallback)(double pressure, double temperature, void *ctx);

  /*!
   * \brief  Constructor of the Mpl3115a2-Component.
   *
   * \param  cb  callback that is called if all channels are finished reading.
   * \param  ctx  instance pointer.
   * \return Returns constructed Mpl3115a2-instance.
   */
  Mpl3115a2(Mpl3115a2::DataReadyCallback data_callback_, void *callback_argument_);

   /*!
   * \brief Destructor of the Mpl3115a2-Component.
   */
  ~Mpl3115a2();

 private:
  /*!
   * \brief Callback executed when a new accel message arrives
   *
   * \param msg     KITECOM message
   */
  void MsgHandlerPressure(const kitecom::timestamped_vector_double& message);
  //! Callback that is called when measurement data is available.
  DataReadyCallback data_callback_;
  void *callback_argument_;  //!< Argument that is passed to the data callback.
  pulicast::Channel& true_pressure_channel_;  //!< Input channel of true pressure data
  double dummy_temperature_ = 0.0;  //!< Dummy temperature measurement [degree Celsius].
};

#endif  // SRC_X86_DRIVERS_MPL3115A2_HPP_
