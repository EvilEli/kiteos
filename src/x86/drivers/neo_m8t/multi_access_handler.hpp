// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#ifndef SRC_X86_DRIVERS_NEO_M8T_MULTI_ACCESS_HANDLER_HPP_
#define SRC_X86_DRIVERS_NEO_M8T_MULTI_ACCESS_HANDLER_HPP_

#include <vector>
#include <functional>
#include "drivers/neo_m8t/ubx-utils/messages.hpp"
#include "drivers/neo_m8t/ubx-utils/dispatcher.hpp"
#include "pulicast-port/pulicast_embedded.h"
#include "core/software_timer.hpp"
#include "kitecom/timestamped_vector_double.hpp"

namespace neo_m8t {

//! Class providing access to a single Neo-M8T device driver instance for multiple users.
class MultiAccessHandler {
 public:
  /*!
   * \brief Constructor of the MultiAccessHandler class.
   */
  MultiAccessHandler();
  /*!
   * \brief Destructor of the MultiAccessHandler class.
   */
  ~MultiAccessHandler() = default;

  /*!
   * \brief  Function used to register callback for different messages with user provided ctx.
   *         The provided ctx pointer will be provided in the invoked callback.
   *
   * \param  class_id  ClassId to subscribe to
   * \param  cb_func   callback function with matching signature for desired message type
   *
   * \return True if successfully registered. False otherwise.
   */
  bool Subscribe(ubx::ClassId class_id, ubx::Dispatcher::RawCallbackFunction cb_func) {
    return true;
  }

  /*!
   * \brief  Function used to register callback for different messages with user provided ctx.
   *         The provided ctx pointer will be provided in the invoked callback.
   *
   * \param  class_id  ClassId to subscribe to
   * \param  cb_func   callback function with matching signature for desired message type
   * \param  ctx       pointer to user context which will be provided in callback
   *
   * \return True if successfully registered. False otherwise.
   */
  template<typename ctx_type>
  bool Subscribe(ubx::ClassId class_id, ubx::Dispatcher::RawCallbackCtxFunction<ctx_type> cb_func,
                 ctx_type *ctx) {
    return true;
  }

  /*!
   * \brief  Function used to register callback for different messages without ctx.
   *
   * \param  cb_func  callback function with matching signature for desired message type
   *
   * \return True if successfully registered. False otherwise.
   */
  template<typename message_type>
  bool Subscribe(ubx::Dispatcher::CallbackFunction<message_type> cb_func) {
    return true;
  }

  /*!
   * \brief  Function used to register callback for different messages with user provided ctx.
   *         The provided ctx pointer will be provided in the invoked callback.
   *
   * \param  cb_func  callback function with matching signature for desired message type
   * \param  ctx      pointer to user context which will be provided in callback
   *
   * \return True if successfully registered. False otherwise.
   */
  template<typename message_type, typename ctx_type>
  bool Subscribe(ubx::Dispatcher::CallbackCtxFunction<message_type, ctx_type> cb_func,
                 ctx_type *ctx) {
    return true;
  }

  /*!
   * \brief  Function used to register callback for different messages with user provided ctx.
   *         The provided ctx pointer will be provided in the invoked callback.
   *
   * \param  cb_func  callback function with matching signature for desired message type
   * \param  ctx      pointer to user context which will be provided in callback
   *
   * \return True if successfully registered. False otherwise.
   */
  template<typename ctx_type>
  bool Subscribe(ubx::Dispatcher::CallbackCtxFunction<ubx::NavPvt, ctx_type> cb_func,
                 ctx_type *ctx) {
    callback_function_ =
      [cb_func, ctx, this](ubx::NavPvt* data) {
        cb_func(data, reinterpret_cast<ctx_type*>(ctx));
      };
    return true;
  }

  /*!
   * \brief Enables raw messages
   */
  void EnableRaw() {}

  /*!
   * \brief Disables raw messages
   */
  void DisableRaw() {}

 private:
  //! Task executed or enqueued  by the callbacks if something has to be done.
  void Dispatch();
  /*!
   * \brief Callback executed when a new accel message arrives
   *
   * \param msg     KITECOM message
   */
  void MsgHandlerPosition(const kitecom::timestamped_vector_double& message);
  /*!
   * \brief Callback executed when a new accel message arrives
   *
   * \param msg     KITECOM message
   */
  void MsgHandlerAltitude(const kitecom::timestamped_vector_double& message);
  /*!
   * \brief Callback executed when a new accel message arrives
   *
   * \param msg     KITECOM message
   */
  void MsgHandlerVelocity(const kitecom::timestamped_vector_double& message);
  //! Callback that is called when measurement data is available.
  std::function<void(ubx::NavPvt*)> callback_function_;
  pulicast::Channel& true_geographic_position_channel_;  //!< Input channel of true pos data
  pulicast::Channel& true_altitude_channel_;  //!< Input channel of true pos data
  pulicast::Channel& true_velocity_channel_;  //!< Input channel of true pos data
  ubx::NavPvt nav_pvt_ = {0};  //!< Last measurement data
  bool geographic_position_available_ = false;  //!< Flag indicating available measurement data.
  bool altitude_available_ = false;  //!< Flag indicating available measurement data.
  bool velocity_available_ = false;  //!< Flag indicating available measurement data.
  SoftwareTimer update_timer_;  //!< Timer used to publishing rate.
};

}  // namespace neo_m8t

#endif  // SRC_X86_DRIVERS_NEO_M8T_MULTI_ACCESS_HANDLER_HPP_
