// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#ifndef SRC_X86_DRIVERS_AK09916_HPP_
#define SRC_X86_DRIVERS_AK09916_HPP_

#include <cstdint>

/*!
 *  \brief  Implements the Ak09916 driver.
 *  \details  This class reads Ak09916 magnetometer data
 */
class Ak09916 {
 public:
  //! Available measurement frequencies.
  enum MeasurementFrequency {
    kMeasurementFrequency10Hz = 10,   //!< Continuous Measurement Mode 1.
    kMeasurementFrequency20Hz = 20,   //!< Continuous Measurement Mode 2.
    kMeasurementFrequency50Hz = 50,   //!< Continuous Measurement Mode 3.
    kMeasurementFrequency100Hz = 100   //!< Continuous Measurement Mode 4.
  };
  //! Struct used for communication with the plugin that calls the driver.
  struct MeasurementData {
    double magnetic_flux_density[3] = {0.0};  //!< MagneticFluxDensity of the 3 axis.
    uint64_t timestamp = 0;   //!< Timestamp when the set point data was measured.
  };
  //! Callback that is called if measurements are finished reading.
  typedef void (*DataReadyCallback)(MeasurementData measurement_data, void *ctx);

  /*!
   * \brief  Constructor of the Ak09916-Component.
   *
   * \param  cb  callback that is called if all channels are finished reading.
   * \param  ctx  instance pointer.
   * \return Returns constructed Ak09916-instance.
   */
  Ak09916(MeasurementFrequency frequency,  Ak09916::DataReadyCallback cb, void *ctx) {}

   /*!
   * \brief Destructor of the Ak09916-Component.
   */
  ~Ak09916() = default;
};

#endif  // SRC_X86_DRIVERS_AK09916_HPP_
