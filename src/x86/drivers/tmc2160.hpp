// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#ifndef SRC_X86_DRIVERS_TMC2160_HPP_
#define SRC_X86_DRIVERS_TMC2160_HPP_

//! \brief Implements a driver to access the TMC2160 stepper motor driver via SPI.
class Tmc2160 {
 public:
  //! Typedef of function pointer used as data callback.
  typedef void (*MeasurementDataCallbackFunction)(float current_speed, float current_acceleration,
                                                  void *callback_argument);

  /*!
   * \brief Constructor of the TMC2160 component.
   *
   * \param steps_per_rev              Steps per rev of the stepper motor driven by the TMC2160.
   * \param measurement_data_callback  Callback called when measurement data is available.
   * \param callback_argument           Argument passed to the measurement callback.
   */
  Tmc2160(uint32_t steps_per_rev, MeasurementDataCallbackFunction measurement_data_callback,
          void *callback_argument) {}

  /*!
   * \brief Destructor of the TMC2160 component.
   */
  ~Tmc2160() = default;

  /*!
   * \brief  Sets the stepper motor speed.
   *
   * \param  speed  Target speed of the stepper motor.
   *
   * \return True on success, false if the stepper motor driver is not yet initialized.
   */
  bool SetSpeed(float speed) {
    return false;
  }

  /*!
   * \brief  Sets the stepper motor acceleration.
   *
   * \param  acceleration  Target acceleration of the stepper motor.
   *
   * \return True on success, false if the stepper motor driver is not yet initialized.
   */
  bool SetAcceleration(float acceleration) {
    return false;
  }
};

#endif  // SRC_X86_DRIVERS_TMC2160_HPP_
