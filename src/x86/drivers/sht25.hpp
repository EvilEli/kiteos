// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#ifndef SRC_X86_DRIVERS_SHT25_HPP_
#define SRC_X86_DRIVERS_SHT25_HPP_

/*!
 *  \brief  Implements the Sht25 driver.
 */
class Sht25 {
 public:
  //! Structure containing the data measured by the Max11040k including the timestamp of the
  //! measurement.
  struct MeasurementData {
    double temperature = 0.0;  //!< Measured temperature.
    uint64_t temperature_timestamp = 0;  //!< Timestamp when the temperature was measured.
    double humidity = 0.0;  //!< Measured humidity.
    uint64_t humidity_timestamp = 0;  //!< Timestamp when the humidity was measured.
  };
  //! callback that is called if all channels are finished reading.
  typedef void (*DataReadyCallback)(MeasurementData measurement_data, void *ctx);

  /*!
   * \brief  Constructor of the Sht25-Component.
   *
   * \param  cb   callback that is called if all channels are finished reading.
   * \param  ctx  instance pointer.
   *
   * \return Returns constructed Sht25-instance.
   */
  Sht25(Sht25::DataReadyCallback cb, void *ctx) {}

   /*!
   * \brief Destructor of the Sht25-Component.
   */
  ~Sht25() = default;
};
#endif  // SRC_X86_DRIVERS_SHT25_HPP_
