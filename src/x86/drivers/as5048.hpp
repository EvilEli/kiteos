// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#ifndef SRC_X86_DRIVERS_AS5048_HPP_
#define SRC_X86_DRIVERS_AS5048_HPP_

#include "core/software_timer.hpp"
#include "hal/i2c/definitions.hpp"

//! Implements a driver for the AS5048 magnetometer.
class As5048 {
 public:
  //! Function type of the callback which is called when measurement data is ready.
  typedef void (*DataReadyCallback)(float angle, uint64_t timestamp, void *callback_argument);

  /*!
   * \brief Constructor of the AS5048 driver.
   *
   * \param port                 I2C port connected to the AS5048 chip.
   * \param slave_address        I2C slave address of the AS5048 chip. May be 0x40, 0x41, 0x42 or
   *                             0x43.
   * \param data_ready_callback  Callback called when measurement data is ready.
   * \param callback_argument    Argument passed to the data ready callback
   */
  As5048(hal::i2c::Port port, uint8_t slave_address,
         As5048::DataReadyCallback data_ready_callback, void *callback_argument) {}

   /*!
    * \brief Destructor of the AS5048 driver.
    */
  ~As5048() = default;
};

#endif  // SRC_X86_DRIVERS_AS5048_HPP_
