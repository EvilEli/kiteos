// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#ifndef SRC_X86_DRIVERS_INA3221_HPP_
#define SRC_X86_DRIVERS_INA3221_HPP_

/*!
 *  \brief  Implements the Ina3221 driver.
 *  \details  This class reads Ina3221 shunt voltage
 */
class Ina3221 {
 public:
  //!< callback that is called if all channels are finished reading.
  typedef void (*DataReadyCallback)(double voltage_channels_[3], void *ctx);

  /*!
   * \brief  Constructor of the Ina3221-Component.
   *
   * \param  cb  callback that is called if all channels are finished reading.
   * \param  ctx  instance pointer.
   * \return Returns constructed Ina3221-instance.
   */
  Ina3221(Ina3221::DataReadyCallback cb, void *ctx) {}

   /*!
   * \brief Destructor of the Ina3221-Component.
   */
  ~Ina3221() = default;
};

#endif  // SRC_X86_DRIVERS_INA3221_HPP_
