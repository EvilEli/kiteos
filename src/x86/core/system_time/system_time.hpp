// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#ifndef SRC_X86_CORE_SYSTEM_TIME_SYSTEM_TIME_HPP_
#define SRC_X86_CORE_SYSTEM_TIME_SYSTEM_TIME_HPP_

#include <cstdint>
#include <chrono>

//! Implemets a service providing the system time. This service wraps several synchronization
//! methods and provides a unified interface.
class SystemTime {
 public:
  /*!
   * \brief  Gets a timestamp from the system time register.
   *
   * \return Current timestamp in nanoseconds.
   */
  static uint64_t GetTimestamp() {
    auto now = std::chrono::steady_clock::now();
    auto nowInNanos = std::chrono::time_point_cast<std::chrono::nanoseconds>(now);
    auto durationInNanos = nowInNanos.time_since_epoch();
    return durationInNanos.count();
  }

  /*!
   * \brief  Checks if the device is currently synchronized to its synchronization source.
   *
   * @return True if the device is currently synchronized to its synchronization source, false
   *         otherwise. If The device is configured as PTP master with local time source, this
   *         always returns true.
   */
  static bool IsSynchronized() {return false;}

  /*!
   * \brief   Gets the current clock accuracy.
   *
   * \details Gets the current clock accuracy. The accuracy is returned as a clock error which is
   *          the time offset calculated for the last clock correction. In PTP master mode, this is
   *          the accuracy with respect to the GPS time. In PTP slave mode, this is the accuracy
   *          with respect to the Master time. In Fallback mode this is always 0xFFFFFFFFFFFFFFFF.
   *
   * \return  Current clock error in nanoseconds.
   */
  static uint64_t GetCurrentClockAccuracy() {return 0xFFFFFFFFFFFFFFFF;}

  /*!
   * \brief   Sets the time of the last received PPS pulse.
   *
   * \details Corrects the system time by defining the time of the last received PPS pulse. This
   *          method is only available in PPS synchronization mode.
   *
   * \param   last_pps_time  Time of the last PPS pulse.
   *
   * \return  True on success, false when called in the wrong synchronization mode.
   */
  static bool SetLastPpsTime(uint64_t last_pps_time) {return true;}
};
#endif  // SRC_X86_CORE_SYSTEM_TIME_SYSTEM_TIME_HPP_
