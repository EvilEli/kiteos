// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#ifndef SRC_X86_CORE_PERSISTENT_MEMORY_FRAM_EMULATOR_HPP_
#define SRC_X86_CORE_PERSISTENT_MEMORY_FRAM_EMULATOR_HPP_

#include <rapidjson/document.h>
#include <vector>
#include <utility>
#include <memory>
#include <string>

/*! \brief     FramEmulator class that creates a key value map from a provoded json file.
 *             Interpretation of content is done according to the system provided
 *             persistent-memory-definition file.
 *
 *  \author    Elias Rosch
 *
 *  \date      2020
 */
class FramEmulator {
 public:
 /*! \brief   Initializes the FramEmulator with the provided .json file.
  *  \param   config_json_path path to the configuration .json
  */
  static void Init(const std::string& config_json_path);

 /*! \brief   Initializes the FramEmulator with the provided config .json file.
  *  \param   config_json_path path to the configuration .json
  */
  static bool LoadConfig(const std::string& config_json_path);

 /*! \brief   Initializes the FramEmulator with the provided definition .json file.
  *  \param   definition_json_path path to the configuration .json
  */
  static bool LoadDefinition(const std::string& definition_json_path);

 /*! \brief   Gets the content of the emulated fram as a vector of key-value pairs.
  *  \returns std::vector containing key-value pairs
  */
  static std::unique_ptr<std::vector<std::pair<uint32_t, uint32_t>>> GetContent();

 private:
  static rapidjson::Document definitions_;  //!< .json content of memory-definitions
  static rapidjson::Document config_;       //!< .json content of configuration
  static bool initialized_;  //!< Flag denoting a successful initialization.
};

static constexpr const uint16_t kMb85Rs64TSize = 0xFFFF;  //!< Size of the FRAM in bytes
class Mb85Rs64T;  //!< Dummy class providing an empty class definition of the Mb85Rs64T driver

#endif  // SRC_X86_CORE_PERSISTENT_MEMORY_FRAM_EMULATOR_HPP_
