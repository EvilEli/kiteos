// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#include "core/persistent_memory/fram_emulator.hpp"
#include <unistd.h>
#include <pwd.h>
#include <rapidjson/istreamwrapper.h>
#include <string>
#include <iostream>
#include <iomanip>
#include <fstream>

rapidjson::Document FramEmulator::definitions_;
rapidjson::Document FramEmulator::config_;
bool FramEmulator::initialized_ = false;

void FramEmulator::Init(const std::string& config_json_path) {
  // Try loading user config
  if (!FramEmulator::LoadConfig(config_json_path)) {
    exit(1);
  }

  // First attempt try to get memory-definition from virtual environment
  const char* search_dir = std::getenv("VIRTUAL_ENV");
  if (search_dir) {
    std::cout << "Found VIRTUAL_ENV environment variable. "
                 "Trying to locate 'memory-definition.json'." << std::endl;
    std::string memory_definitions_json = std::string(search_dir) +
      "/lib/python3.8/site-packages/persistent_memory_interface/memory-definition.json";
    if (FramEmulator::LoadDefinition(memory_definitions_json)) {
      initialized_ = true;
      return;
    }
  }
  // Second attempt try to get memory-definition via home dir from home environment variable
  search_dir = std::getenv("HOME");
  if (search_dir) {
    std::cout << "Found HOME environment variable. "
                 "Trying to locate 'memory-definition.json'." << std::endl;
    std::string memory_definitions_json = std::string(search_dir) +
      "/.local/lib/python3.8/site-packages/persistent_memory_interface/memory-definition.json";
    if (FramEmulator::LoadDefinition(memory_definitions_json)) {
      initialized_ = true;
      return;
    }
  }
  // Second attempt try to get memory-definition via home dir from user id
  search_dir = getpwuid(getuid())->pw_dir;
  if (search_dir) {
    std::cout << "The env variable HOME is not set. "
                 "Trying to find out the home directory from the user id." << std::endl;
    std::string memory_definitions_json = std::string(search_dir) +
      "/.local/lib/python3.8/site-packages/persistent_memory_interface/memory-definition.json";
    if (FramEmulator::LoadDefinition(memory_definitions_json)) {
      initialized_ = true;
      return;
    }
  }
  std::cout << "Could not find the persistent-memory-interface package directory. "
               "This is needed in order to locate 'memory-definition.json'." << std::endl;
  exit(1);
}

bool FramEmulator::LoadConfig(const std::string& config_json_path) {
  std::cout << "Loading config from: " << config_json_path << "... ";
  std::ifstream cifs(config_json_path);
  if (!cifs) {
    std::cout << "\033[1;31mNo such file:\033[0m '" << config_json_path << "'" << std::endl;
    return false;
  }
  rapidjson::IStreamWrapper cisw(cifs);
  rapidjson::ParseResult ok = config_.ParseStream(cisw);
  if (!ok) {
    std::cout << "\033[1;31mThe file '" << config_json_path
              << "' could not be parsed.\033[0m" << std::endl;
    return false;
  }
  std::cout << "\033[1;32msuccess!\033[0m" << std::endl;
  return true;
}

bool FramEmulator::LoadDefinition(const std::string& definition_json_path) {
  std::cout << "Loading definitions from: " << definition_json_path << "... ";
  std::ifstream difs(definition_json_path);
  if (!difs) {
    std::cout << "\033[1;31mNo such file:\033[0m '" << definition_json_path << "'" << std::endl;
    return false;
  }
  rapidjson::IStreamWrapper disw(difs);
  definitions_.ParseStream(disw);
  std::cout << "\033[1;32msuccess!\033[0m" << std::endl;
  return true;
}

std::unique_ptr<std::vector<std::pair<uint32_t, uint32_t>>> FramEmulator::GetContent() {
  std::unique_ptr<std::vector<std::pair<uint32_t, uint32_t>>> ptr =
    std::make_unique<std::vector<std::pair<uint32_t, uint32_t>>>();

  if (!initialized_) {
    return ptr;
  }

  for (auto& m : config_.GetObject()) {
    const char* config_key_name = m.name.GetString();
    if (!definitions_.HasMember(config_key_name)) {
      std::cout << config_key_name << " is not a valid key name." << std::endl;
      exit(1);
    }

    auto key_array = definitions_[config_key_name]["keys"].GetArray();
    const char* type = definitions_[config_key_name]["type"].GetString();

    for (uint8_t i = 0; i < key_array.Size(); ++i) {
      const char* key_string = key_array[i].GetString();
      uint32_t key = std::stoul(key_string, nullptr, 16);
      uint32_t value = 0;

      if (std::string(type) == "string") {
        const char* config_value = m.value.GetString();
        value = __builtin_bswap32(*reinterpret_cast<const uint32_t*>(config_value + i * 4));
      } else if (std::string(type) == "bool") {
        if (m.value.GetBool()) {
          value = 0x80000000;
        }
      } else if (std::string(type) == "uint32") {
        value = m.value.GetUint();
      } else if (std::string(type) == "uint8") {
        auto config_value_array = m.value.GetArray();
        for (uint8_t j = 0; j < config_value_array.Size() && j < 4; ++j) {
          value |= config_value_array[j+i].GetUint() << (3 - j) * 8;
        }
      } else if (std::string(type) == "enum") {
        bool config_value_is_legal = false;
        const char* config_value = m.value.GetString();
        for (auto& legal_enum : definitions_[config_key_name]["values"].GetObject()) {
          if (std::string(config_value) == std::string(legal_enum.value.GetString())) {
            value = std::stoul(legal_enum.name.GetString());
            config_value_is_legal = true;
            break;
          }
        }
        if (!config_value_is_legal) {
          std::cout << config_value << " is not a valid enum name." << std::endl;
          exit(1);
        }
      }

      std::cout << "Found " << config_key_name <<  " ("<< type << ")" << " \t"
      // Add conditional tab for long strings
      << ((strlen(config_key_name) + strlen(type) > 12)? "" : "\t")
      // Add conditional tab for extra long strings
      << ((strlen(config_key_name) + strlen(type) > 24)? "" : "\t")
      << key_string << " \t0x" << std::hex << std::setw(8) << std::setfill('0') << value
      << std::endl;

      ptr.get()->push_back(std::make_pair(key, value));
    }
  }
  return ptr;
}
