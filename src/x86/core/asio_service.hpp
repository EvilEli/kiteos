// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.

#ifndef SRC_X86_CORE_ASIO_SERVICE_HPP_
#define SRC_X86_CORE_ASIO_SERVICE_HPP_

#include <memory>
#include <asio/io_service.hpp>

//! Holds an asio service used for scheduling and communication.
class AsioService {
 public:
  /*!
   * \brief  Gets the service instance hold by the class.
   *
   * \return Asio IO service instance.
   */
  static std::shared_ptr<asio::io_service> GetService();

 private:
  static std::shared_ptr<asio::io_service> service_;  //!< Reference to asio service.
};

#endif  // SRC_X86_CORE_ASIO_SERVICE_HPP_
