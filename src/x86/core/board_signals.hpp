// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#ifndef SRC_X86_CORE_BOARD_SIGNALS_HPP_
#define SRC_X86_CORE_BOARD_SIGNALS_HPP_

//*****************************************************************************/
//! \addtogroup services
//! @{
///
//*****************************************************************************/
/*!
 * \brief      Implements the BoardSignals Class.
 *
 * \details    This class is used to initialize all signals that establish nominal board operation.
 *
 * \author     Elias Rosch
 *
 * \date       2020
 */
class BoardSignals {
 public:
  //! Board signal types available on KiteOS boards.
  enum Type {
    kTopStackDetection,
    kBottomStackDetection,
    kEthMuxSelect,
    kPpsNotRerouting,
    kPpsTermination
  };
  /*!
   * \brief Initializes the Board signals.
   */
  static void Init() {}

  /*!
   * \brief Reads the board signals.
   *
   * \param type enum that selects the signal to be read
   * \return true if signal is high, false otherwise.
   */
  static bool GetValue(Type type) {return false;}
};
/*! @} */
#endif  // SRC_X86_CORE_BOARD_SIGNALS_HPP_
