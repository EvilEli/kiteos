// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#include "hal/systick_timer.hpp"

#include "core/asio_service.hpp"
#include "utils/debug_utils.h"
namespace hal {


/**************************************************************************************************
 *     Static private member variables                                                            *
 **************************************************************************************************/

uint64_t SystickTimer::tick_count_ = 0;
SystickTimer::CallbackFunction SystickTimer::systick_callback_ = nullptr;
void* SystickTimer::systick_callback_argument_ = nullptr;

asio::basic_waitable_timer<std::chrono::system_clock> *SystickTimer::asio_timer_;
std::function<void(const asio::error_code &)> SystickTimer::systick_lambda;


/**************************************************************************************************
 *     Public methods                                                                             *
 **************************************************************************************************/

bool SystickTimer::Init() {
  asio_timer_ = new asio::basic_waitable_timer<std::chrono::system_clock>(
      *(AsioService::GetService()), std::chrono::system_clock::duration::zero());

  systick_lambda = [](const asio::error_code &){
    tick_count_++;
    if (systick_callback_) {
      systick_callback_(systick_callback_argument_);
    }

    asio_timer_->expires_at(asio_timer_->expires_at()+ std::chrono::microseconds(1000));
    asio_timer_->async_wait(systick_lambda);
  };
  asio_timer_->async_wait(systick_lambda);

  return true;
}

uint64_t SystickTimer::GetCount() {
  return tick_count_;
}

void hal::SystickTimer::RegisterSystickCallback(CallbackFunction callback,
                                                void *callback_argument) {
  // Ensure that the callback is only registered once
  ASSERT(systick_callback_ == nullptr);
  ASSERT(callback_argument == nullptr);

  // Register callback
  systick_callback_ = callback;
  systick_callback_argument_ = callback_argument;
}

}  // namespace hal
