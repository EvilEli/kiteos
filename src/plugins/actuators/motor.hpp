// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#ifndef SRC_PLUGINS_ACTUATORS_MOTOR_HPP_
#define SRC_PLUGINS_ACTUATORS_MOTOR_HPP_

#include <string>
#include "core/plugin_manager/plugin.hpp"
#include "core/state_node.hpp"
#include "drivers/a4964.hpp"
#include "drivers/ina226.hpp"

/*!
 *  \brief Implements a plug-in containing and handling an MOTOR object.
 */
class Motor : public Plugin {
 public:
  /*!
   * \brief Constructor of the MOTOR component.
   *
   * \param puli_namespace  The pulicast namespace the motor operates in.
   */
  explicit Motor(pulicast::Namespace puli_namespace);

  /*!
   * \brief Destructor of the MOTOR component.
   */
  ~Motor() override;

 private:
  static void Run(Motor *instance);  //!< Lets the MOTOR component do its work.
  static void ShuntVoltageCallback(double shunt_voltage, void *ctx);  //!< Invoked when new value

  StateNode state_node_;  //!< StateNode interface to report states
  A4964 a4964_;  //!< A4964 driver instance which handles the MOTOR communication.
  Ina226 ina226_;  //!< Ina226 driver instance which reads out the motor shunt voltage.
  pulicast::Channel& speed_channel_;  //!< Output channel for motor speed.
  pulicast::Channel& temperature_channel_;  //!< Output channel for motor temperature.
  pulicast::Channel& shunt_channel_;  //!< Output channel for motor shunt voltage.
  pulicast::Channel& set_speed_chan_;  //!< Input channel for motor speed
  A4964::MeasurementData measurement_data_;  //!< Data struct that gets updated with a4964.Run()
};

#endif  // SRC_PLUGINS_ACTUATORS_MOTOR_HPP_
