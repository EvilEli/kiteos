// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#include "motor.hpp"
#include "core/scheduler.hpp"
#include "utils/debug_utils.h"

/**************************************************************************************************
 *     Public methods                                                                             *
 **************************************************************************************************/
Motor::Motor(pulicast::Namespace puli_namespace):
state_node_("UNINITIALIZED", static_cast<std::string>(puli_namespace),
            [this](std::string_view command) {
              a4964_.CommandCallback(command);
            }),
a4964_(&state_node_),
ina226_(ShuntVoltageCallback, this),
speed_channel_(puli_namespace["Speed"]),
temperature_channel_(puli_namespace["Temperature"]),
shunt_channel_(puli_namespace["Voltage"]),
set_speed_chan_(puli_namespace["Setpoint"]) {
  set_speed_chan_.Subscribe<kitecom::timestamped_vector_double>(
      [this](const kitecom::timestamped_vector_double& msg) {
        a4964_.SetSpeed(msg.values[0]);
      });
  Scheduler::EnqueueTask(Run, this);
}

Motor::~Motor() = default;

/**************************************************************************************************
 *     Callbacks                                                                                  *
 **************************************************************************************************/
void Motor::Run(Motor *instance) {
  instance->a4964_.Run(&instance->measurement_data_);

  if (instance->measurement_data_.speed_valid) {
    Publish(instance->speed_channel_, instance->measurement_data_.timestamp,
                      instance->measurement_data_.speed);
  }
  if (instance->measurement_data_.temperature_valid) {
    Publish(instance->temperature_channel_, instance->measurement_data_.timestamp,
                      instance->measurement_data_.temperature);
  }

  Scheduler::EnqueueTask(Run, instance);
}

void Motor::ShuntVoltageCallback(double shunt_voltage, void *ctx) {
  auto inst = reinterpret_cast<Motor*>(ctx);
  // Publish shunt values
  Publish(inst->shunt_channel_, SystemTime::GetTimestamp(), shunt_voltage);
}
