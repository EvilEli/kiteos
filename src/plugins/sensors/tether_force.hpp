// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#ifndef SRC_PLUGINS_SENSORS_TETHER_FORCE_HPP_
#define SRC_PLUGINS_SENSORS_TETHER_FORCE_HPP_

#include "core/plugin_manager/plugin.hpp"
#include "drivers/max11040k.hpp"
#include "drivers/as5048.hpp"

//! Implements a plugin representing a tether force sensor which can also sense the direction of the
//! force
class TetherForce : public Plugin {
 public:
  /*!
   * \brief Constructor of the tether force sensor component.
   *
   * \param puli_namespace  The pulicast namespace the tether force sensor operates in.
   */
  explicit TetherForce(pulicast::Namespace puli_namespace);

  /*!
   * \brief Destructor of the tether force sensor component.
   */
  ~TetherForce() override = default;

 private:
  //! Callback called when Adc measurement data is available.
  static void AdcCallback(Max11040k::MeasurementData measurement_data, void* instance);
  //! Callback called when encoder data of the X axis angle sensor is available.
  static void XAngleCallback(float angle, uint64_t timestamp, void *instance);
  //! Callback called when encoder data of the Y axis angle sensor is available.
  static void YAngleCallback(float angle, uint64_t timestamp, void *instance);

  //! Output channel for the raw sensor value measured by the x axis encoder.
  pulicast::Channel& raw_x_angle_channel_;
  //! Output channel for the raw sensor value measured by the y axis encoder.
  pulicast::Channel& raw_y_angle_channel_;
  //! Output channel for the raw sensor value measured by the force sensor.
  pulicast::Channel& raw_force_channel_;
  //! Output channel for the force vector calculated from the raw sensor values.
  pulicast::Channel& force_vector_channel_;

  Max11040k max11040k_;  //!< Max11040k driver instance which handles the Adc communication.

  As5048 as5048_x_;  //!< X axis encoder.
  As5048 as5048_y_;  //!< Y axis encoder.

  double adc_voltage_ = 0;  //!< Latest ADC voltage measurement.
  uint64_t adc_voltage_timestamp_ = 0;  //!< Timestamp of the latest ADC voltage measurement.
  float x_angle_ = 0;  //!< Latest X axis angle measurement.
  uint64_t x_angle_timestamp_ = 0;  //!< Timestamp of the latest X axis angle measurement.
  float y_angle_ = 0;  //!< Latest Y axis angle measurement.
  uint64_t y_angle_timestamp_ = 0;  //!< Timestamp of the latest Y axis angle measurement.
};

#endif  // SRC_PLUGINS_SENSORS_TETHER_FORCE_HPP_
