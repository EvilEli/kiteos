// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#include <vector>
#include "adc.hpp"
#include "core/scheduler.hpp"
#include "core/logger.hpp"

#define CH3_TRANSFER_FN (2120.0 / 120.0)
/**************************************************************************************************
 *     Public Methods                                                                             *
 **************************************************************************************************/
Adc::Adc(pulicast::Namespace puli_namespace):
max11040k_(MeasurementCallback, this),
voltage_channel_(puli_namespace["Voltage"]) {
  timeout_timer_.SetCallback(NoDataTimeoutCallback, this);
  timeout_timer_.SetTimeout(no_data_timeout_);
}

Adc::~Adc() = default;


/**************************************************************************************************
 *     Callbacks                                                                                  *
 **************************************************************************************************/
void Adc::MeasurementCallback(Max11040k::MeasurementData measurement_data, void* ctx) {
  auto inst = reinterpret_cast<Adc*>(ctx);

  // Publish acceleration values
  std::vector<double> voltage_values{
    measurement_data.voltage[0],
    measurement_data.voltage[1],
    measurement_data.voltage[2] * CH3_TRANSFER_FN,
    measurement_data.voltage[3]
  };
  // Measurement available, restart timeout timer
  inst->timeout_timer_.SetTimeout(no_data_timeout_);
  Publish(inst->voltage_channel_, measurement_data.timestamp, voltage_values);
}

void Adc::NoDataTimeoutCallback(Adc *inst) {
  Logger::Report("Max11040k timed out.", Logger::kWarning);
  inst->timeout_timer_.SetTimeout(no_data_timeout_);
}
