// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#ifndef SRC_PLUGINS_SENSORS_RC_RECEIVER_HPP_
#define SRC_PLUGINS_SENSORS_RC_RECEIVER_HPP_

#include "core/plugin_manager/plugin.hpp"
#include "drivers/spektrum_receiver/definitions.hpp"
#include "drivers/spektrum_receiver/multi_access_handler.hpp"
#include "failsafe_pilot_reference.hpp"

/*!
 * \brief Implements a plug-in containing and handling an RC receiver object.
 */
class RcReceiver : public Plugin {
 public:
  /*!
   * \brief Constructor of the RC receiver component.
   *
   * \param puli_namespace  The pulicast namespace the RC receiver operates in.
   */
  explicit RcReceiver(pulicast::Namespace puli_namespace);

  /*!
   * \brief Destructor of the RC receiver component.
   */
  ~RcReceiver() override;

 private:
  //!< New measurement data arrived
  static void ChannelDataCallback(spektrum_receiver::ChannelData channel_data, void* ctx);

  //! Spektrum receiver driver handler instance which handles the RC receiver communication.
  spektrum_receiver::MultiAccessHandler spektrum_receiver_handler_;
  //! Middleware component forwarding the arm switch from the Spektrum receiver to the failsafe
  //! switch.
  FailsafePilotReference failsafe_pilot_reference_;

  pulicast::Channel& roll_channel_;      //!< Output channel for roll values.
  pulicast::Channel& pitch_channel_;     //!< Output channel for pitch values.
  pulicast::Channel& yaw_channel_;       //!< Output channel for yaw values.
  pulicast::Channel& throttle_channel_;  //!< Output channel for throttle values.
  pulicast::Channel& gear_channel_;       //!< Output channel for gear values.
  pulicast::Channel& aux1_channel_;       //!< Output channel for aux1 values.
  pulicast::Channel& aux2_channel_;       //!< Output channel for aux2 values.
  pulicast::Channel& aux3_channel_;       //!< Output channel for aux3 values.
};

#endif  // SRC_PLUGINS_SENSORS_RC_RECEIVER_HPP_
