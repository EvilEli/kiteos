// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#include <vector>
#include "imu.hpp"
#include "core/scheduler.hpp"
#include "core/logger.hpp"

/**************************************************************************************************
 *     Public Methods                                                                             *
 **************************************************************************************************/
Imu::Imu(pulicast::Namespace puli_namespace):
icm20948_(icm20948::Device::kGyroRange2000Dps, icm20948::Device::kGyroBandwidth361Hz4,
          icm20948::Device::kAccelRange16G, icm20948::Device::kAccelBandwidth473Hz,
          Icm20948MeasurementCallback, this),
ak09916_(Ak09916::kMeasurementFrequency20Hz, Ak09916MeasurementCallback, this),
acceleration_channel_(puli_namespace["Acceleration"]),
gyro_channel_(puli_namespace["AngularVelocity"]),
temperature_channel_(puli_namespace["Temperature"]),
magnetic_flux_density_channel_(puli_namespace["MagneticFluxDensity"]) {
  // Start timeout timer, add 2 seconds since in the first seconds after power up, the IMU is busy
  // with idle detection and self test
  icm20948_timeout_timer_.SetCallback(Imu::Icm20948NoDataTimeoutCallback, this);
  icm20948_timeout_timer_.SetTimeout(no_data_timeout_ + 2000);
  ak09916_timeout_timer_.SetCallback(Imu::Ak09916NoDataTimeoutCallback, this);
  ak09916_timeout_timer_.SetTimeout(no_data_timeout_ + 2000);
}

Imu::~Imu() = default;


/**************************************************************************************************
 *     Callbacks                                                                                  *
 **************************************************************************************************/
void Imu::Icm20948MeasurementCallback(icm20948::Device::MeasurementData measurement_data,
                                      void* ctx) {
  auto inst = reinterpret_cast<Imu*>(ctx);

  // Measurement available, restart timeout timer
  inst->icm20948_timeout_timer_.SetTimeout(no_data_timeout_);

  // Publish acceleration values
  std::vector<double> acceleration_values{measurement_data.acceleration[0],
                                          measurement_data.acceleration[1],
                                          measurement_data.acceleration[2]};
  Publish(inst->acceleration_channel_, measurement_data.timestamp, acceleration_values);

  // Publish gyro values
  std::vector<double> gyro_values{measurement_data.gyro[0], measurement_data.gyro[1],
                                  measurement_data.gyro[2]};
  Publish(inst->gyro_channel_, measurement_data.timestamp, gyro_values);

  // Publish temperature values
  Publish(inst->temperature_channel_, measurement_data.timestamp, measurement_data.temperature);
}

void Imu::Icm20948NoDataTimeoutCallback(Imu *inst) {
  Logger::Report("Icm20948 timed out.", Logger::kWarning);
  inst->icm20948_timeout_timer_.SetTimeout(no_data_timeout_);
}

void Imu::Ak09916MeasurementCallback(Ak09916::MeasurementData measurement_data, void *ctx) {
  auto inst = reinterpret_cast<Imu*>(ctx);

  // Measurement available, restart timeout timer
  inst->ak09916_timeout_timer_.SetTimeout(no_data_timeout_);

  // Publish magnetometer values
  std::vector<double> magnetometer_values{measurement_data.magnetic_flux_density[0],
                                          measurement_data.magnetic_flux_density[1],
                                          measurement_data.magnetic_flux_density[2]};
  Publish(inst->magnetic_flux_density_channel_,
          measurement_data.timestamp, magnetometer_values);
}

void Imu::Ak09916NoDataTimeoutCallback(Imu *inst) {
  Logger::Report("Ak09916 timed out.", Logger::kWarning);
  inst->ak09916_timeout_timer_.SetTimeout(no_data_timeout_);
}
