// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#ifndef SRC_PLUGINS_SENSORS_UWB_HPP_
#define SRC_PLUGINS_SENSORS_UWB_HPP_

#include "core/plugin_manager/plugin.hpp"
#include "drivers/telocate_uwb.hpp"

//! Implements a plug-in containing and handling an UWB object.
class Uwb : public Plugin {
 public:
  /*!
   * \brief Constructor of the UWB component.
   *
   * \param puli_namespace  The pulicast namespace the UWB plugin operates in.
   */
  explicit Uwb(pulicast::Namespace puli_namespace);

  /*!
   * \brief Destructor of the UWB component.
   */
  ~Uwb() override = default;

 private:
  //! Callback called by the Telocate UWB driver when the initialization of the module finished.
  static void UwbInitCallback(uint32_t node_address, void *instance);

  //! Callback called by the Telocate UWB driver when a range measurement is ready.
  static void MeasurementDataCallback(uint32_t remote_node_index, float distance, float rssi,
                                      uint64_t measurement_timestamp, TelocateUwb::Status status,
                                      void *ctx);

  //! The pulicast namespace the UWB plugin operates in.
  pulicast::Namespace puli_namespace_;

  //! Telocate UWB driver instance, handling the communication with the Telocate UWB hardware.
  TelocateUwb telocate_uwb_;

  //! Output channel for range 1 measurement values.
  pulicast::Channel& range_1_channel_;
  //! Output channel for range 2 measurement values.
  pulicast::Channel& range_2_channel_;
  //! Output channel for range 3 measurement values.
  pulicast::Channel& range_3_channel_;
  //! Output channel for range 4 measurement values.
  pulicast::Channel& range_4_channel_;

  //! Measurement interval of UWB cycle.
  pulicast::experimental::distributed_settings::DistributedSetting<double> measurement_interval_;
  //! Node address of this UWB node.
  std::optional<pulicast::experimental::distributed_settings::DistributedSetting<int32_t>>
    node_address_;
  //! Node addresses to range to.
  pulicast::experimental::distributed_settings::DistributedSetting<kitecom::timestamped_vector_uint>
    remote_node_addresses_;
};

#endif  // SRC_PLUGINS_SENSORS_UWB_HPP_
