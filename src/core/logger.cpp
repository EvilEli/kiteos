// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#include "logger.hpp"
#include <string>
#include "core/scheduler.hpp"
#include "core/name_provider.hpp"
#include "core/system_time/system_time.hpp"
#include "kitecom/logmessage_lvl_origin.hpp"

// The startup time in milliseconds. During the startup time, log messages are not published. All
// pending log messages are published after the startup time elapsed. This is done to reduce the
// risk of log messages getting lost due to a missing ethernet link.
#define LOGGER_STARTUP_TIME 6000

/**************************************************************************************************
 *     Static private member variables                                                            *
 **************************************************************************************************/
pulicast::Channel* Logger::log_channel_;
ThreadSafeBuffer<Logger::LogMessage, LOGGER_BUFFER_SIZE, LOGGER_BUFFER_PROCESSES>
    Logger::log_message_buffer_;
SoftwareTimer Logger::init_timer_;
bool Logger::ready_ = false;

/**************************************************************************************************
 *     Public methods                                                                             *
 **************************************************************************************************/

void Logger::Init(pulicast::Namespace puli_namespace) {
  log_channel_ = &puli_namespace["/log"];

  // Initialize timer used to print startup log messages
  init_timer_.SetCallback(InitTimerCallback, nullptr);
  init_timer_.SetTimeout(LOGGER_STARTUP_TIME);
}

void Logger::Report(const char *message, LogLevel level) {
  LogMessage log_message;
  snprintf(log_message.message, MAX_MESSAGE_LENGTH, "%s", message);
  log_message.level = level;
  log_message_buffer_.WriteElement(log_message);

  if (ready_) {
    Scheduler::EnqueueTask(Logger::PublishTask, nullptr);
  }
}

/**************************************************************************************************
 *     Callbacks                                                                                  *
 **************************************************************************************************/
void Logger::PublishTask(void *arg) {
  LogMessage log_message;
  while (log_message_buffer_.ReadElement(&log_message)) {
    // Publish log message
    kitecom::logmessage_lvl_origin packet;
    packet.ts = SystemTime::GetTimestamp();
    packet.level = (int8_t)log_message.level;
    packet.msg = std::string(log_message.message);
    packet.origin = NameProvider::GetFullBoardNamespace();
    *log_channel_ << packet;
  }
}

void Logger::InitTimerCallback(void *arg) {
  ready_ = true;
  PublishTask(nullptr);
}
