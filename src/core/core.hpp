// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#ifndef SRC_CORE_CORE_HPP_
#define SRC_CORE_CORE_HPP_

//*****************************************************************************/
//! \addtogroup services
//! @{
///
//*****************************************************************************/
/*!
 * \brief      Implements the Core Class.
 *
 * \details    This class is used to initialize and run all classes implementing core functionality.
 *
 * \author     Jan Lehmann
 *
 * \date       2020
 */
class Core {
 public:
  /*!
   * \brief Initializes the KiteOS core and keeps running it.
   */
  static void Start();
};
/*! @} */
#endif  // SRC_CORE_CORE_HPP_
