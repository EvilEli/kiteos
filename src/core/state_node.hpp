// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#ifndef SRC_CORE_STATE_NODE_HPP_
#define SRC_CORE_STATE_NODE_HPP_

#include <string>
#include "pulicast-port/pulicast_embedded.h"
#include <kitecom/statemessage.hpp>
#include <kitecom/commandmessage.hpp>
#include "core/software_timer.hpp"

#define STATE_NODE_UPDATE_RATE 100
#define MAX_NODE_CNT 5

/*!
 *  \brief     Implements the StateNode.
 *  \details   This service is used by everything that uses kitecom states. A plugin registers itself
 *             by providing a name, callbacks for state request and receiving of commands as well as
 *             an optional user context. As soon as a given command is handled by the StateNode,
 *             the respective callback is invoked. Additionally the StateNode periodically
 *             requests the state callback.
 *
 *  \author    Elias Rosch
 *  \date      2020
 *
 */
class StateNode {
 public:
 /*!
  *  \brief Type used for command callbacks.
  *  \param command  Received command string.
  */
  using CommandCallback = std::function<void (std::string_view command)>;

  /*!
   * \brief  Constructor of the StateNode-Component.
   *
   *  \param state  String of initial state.
   *  \param origin  String denoting the origin name.
   *  \param command_cb  Received command string.
   */
  StateNode(std::string_view state, const std::string& origin, CommandCallback command_cb);

  /*!
  * \brief Destructor of the StateNode-Component.
  */
  ~StateNode();

  /*!
   * \brief   Initializes the state node class.
   *
   * \details This function has to be executed once before using the state node class.
   */
  static void Init(pulicast::Namespace puli_namespace);

  /*!
   *  \brief Function to publish a node state.
   *
   *  \param node  Context of the node to be published.
   */
  void SetState(std::string_view state);

 private:
  /*!
   * \brief  Adds itself to the static state node list.
   *
   * \return true if successful, false otherwise.
   */
  bool Register();

  /*!
   * \brief Publishes the current state of the node in a state message.
   */
  void Publish();

  /*!
   *  \brief Callback when a commandmessage is received.
   *
   *  \param msg  Command message structure.
   */
  static void MsgHandlerCommand(const kitecom::commandmessage &msg);

  /*!
   *  \brief   Functions that returns a state node with origin name that is provided via destination
   *           name.
   *
   *  \param   destination  String that is searched for in all state node origins.
   *
   *  \returns Pointer to state node context.
   */
  static StateNode *FindNode(std::string_view destination);

  //! Publishes the state of all registered state node instances. This method is called periodically
  //! by the update timer.
  static void PublishAll(void *arg);

  static StateNode* node_registry_[MAX_NODE_CNT];  //!< list containing all nodes.
  static SoftwareTimer update_timer_;              //!< Timer used for periodic state polling.
  static pulicast::Channel* state_channel_;    //!< Output channel for motor speed.
  static pulicast::Channel* command_channel_;  //!< Output channel for motor temperature.

  std::string_view state_;        //!< Name of the current state of the state node.
  const std::string origin_;       //!< Name of the state node origin.
  CommandCallback command_cb_;    //!< Callback for when a command for the origin is received.

  uint8_t node_index_;  //!< Index of the node in the node registry.
};

#endif  // SRC_CORE_STATE_NODE_HPP_
