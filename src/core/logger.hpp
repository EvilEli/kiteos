// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#ifndef SRC_CORE_LOGGER_HPP_
#define SRC_CORE_LOGGER_HPP_

#include "utils/thread_safe_buffer.hpp"
#include "software_timer.hpp"
#include "pulicast-port/pulicast_embedded.h"

#define LOGGER_BUFFER_SIZE (THREAD_SAFE_BUFFER_MAX_PROCESS_CNT * 16)
#define LOGGER_BUFFER_PROCESSES THREAD_SAFE_BUFFER_MAX_PROCESS_CNT
#define MAX_MESSAGE_LENGTH 128

/*!
 *  \brief  This class is used to manage logging globally.
 *
 *  \author Julian Reimer
 *
 *  \date   2020
 */
class Logger {
 public:
  //! Use boost log severity values (boost/log/trivial.hpp)
  enum LogLevel{
    kTrace = 0,
    kDebug = 1,
    kInfo = 2,
    kWarning = 3,
    kError = 4,
    kFatal = 5,
  };

  /*!
   * \brief Initializes the logger class.
   *
   * \param puli_namespace  The pulicast namespace the logger operates in.
   */
  static void Init(pulicast::Namespace puli_namespace);

  /*!
  * \brief Function to report log messages. Currently the logs get published via pulicast.
  *
  * \param message    The message string reference.
  * \param level  The log level enum, see "LogLevel" above.
  */
  static void Report(const char *message, LogLevel level);

 private:
  //! Structure defining a log message including its log level.
  struct LogMessage {
    char message[MAX_MESSAGE_LENGTH];  //!< Log message to be sent.
    LogLevel level;                    //!< Log level of the log message.
  };

  //! Callback called by the scheduler in synchronous context after it was enqueued by the report
  //! method. Publishes queued log messages.
  static void PublishTask(void *arg);

  //! Callback called when the init timer elapsed, publishes all pending messages.
  static void InitTimerCallback(void *arg);

  //! Channel used for publishing log message.
  static pulicast::Channel* log_channel_;

  //! Buffer containing pending log messages.
  static ThreadSafeBuffer<LogMessage, LOGGER_BUFFER_SIZE, LOGGER_BUFFER_PROCESSES>
      log_message_buffer_;

  //! Timer used for delaying the messages published during startup.
  static SoftwareTimer init_timer_;

  //! Set after startup timer elapsed to indicate that logger is ready to publish log messages.
  static bool ready_;
};

#endif  // SRC_CORE_LOGGER_HPP_
