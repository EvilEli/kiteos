// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#include "board_monitor.hpp"
#include <vector>
#include "core/kiteos-version.hpp"
#include "core/system_time/system_time.hpp"
#include "core/scheduler.hpp"
#include "core/board_signals.hpp"
#include "core/name_provider.hpp"
#include "core/logger.hpp"

/**************************************************************************************************
 *     Public methods                                                                             *
 **************************************************************************************************/

BoardMonitor::BoardMonitor(pulicast::Namespace puli_namespace):
version_channel_(puli_namespace["Version"]),
stack_detection_channel_(puli_namespace["Stack"]),
mainloops_channel_(puli_namespace["Mainloops"]),
maxcycle_channel_(puli_namespace["MaxCycle"]),
temperature_channel_(puli_namespace["Temperature"]),
clock_accuracy_channel_(puli_namespace["ClockAccuracy"]),
ping_channel_((puli_namespace / "..")[NameProvider::GetBoardName() + "ping"]),
pong_channel_((puli_namespace / "..")[NameProvider::GetBoardName() + "pong"]) {
  update_timer_.SetTimeoutPeriodic(1000, UpdateTimerElapsedCallback, this);
  system_time_is_synchronized_ = SystemTime::IsSynchronized();

  ping_channel_.Subscribe<kitecom::timestamped_vector_uint>(
    [this](const kitecom::timestamped_vector_uint& message) {
      this->MsgHandlerPing(message);});
}

/**************************************************************************************************
 *     Private methods                                                                            *
 **************************************************************************************************/
void BoardMonitor::SendVersion() {
    kitecom::timestamped_vector_string msg;
    msg.ts = SystemTime::GetTimestamp();
    msg.msg = GIT_TAG;
    msg.msg += (GIT_IS_DIRTY ? "-dirty" : "");
    version_channel_ << msg;
}

void BoardMonitor::SendPpsMode() {
  bool is_synchronized = SystemTime::IsSynchronized();
  if (is_synchronized != system_time_is_synchronized_) {
    if (is_synchronized) {
      Logger::Report("System time is now synchronized.", Logger::LogLevel::kInfo);
    } else {
      Logger::Report("System time is no longer synchronized.", Logger::LogLevel::kWarning);
    }
    system_time_is_synchronized_ = is_synchronized;
  }

  Publish(clock_accuracy_channel_, SystemTime::GetTimestamp(),
          static_cast<double>(SystemTime::GetCurrentClockAccuracy()) / 1000000000);
}

void BoardMonitor::SendStackDetection() {
  std::vector<uint8_t> stack_detection_signals = {
    BoardSignals::GetValue(BoardSignals::kTopStackDetection),
    BoardSignals::GetValue(BoardSignals::kBottomStackDetection),
  };
  Publish(stack_detection_channel_, SystemTime::GetTimestamp(), stack_detection_signals);
}

void BoardMonitor::SendCoreTemperature() {
  double temp = core_temperature_.GetValue();
  Publish(temperature_channel_, SystemTime::GetTimestamp(), temp);
  if (temp > 105) {
    Logger::Report("Core temperature critical.", Logger::LogLevel::kWarning);
  } else if (temp > 80) {
    Logger::Report("Core temperature high.", Logger::LogLevel::kWarning);
  }
}
/**************************************************************************************************
 *     Callbacks                                                                                  *
 **************************************************************************************************/
void BoardMonitor::UpdateTimerElapsedCallback(BoardMonitor *inst) {
  uint64_t timestamp = SystemTime::GetTimestamp();

  uint32_t max_cycle_time, mainloops_passed;
  Scheduler::GetPerformanceStats(&mainloops_passed, &max_cycle_time);

  Publish(inst->mainloops_channel_, timestamp, mainloops_passed);
  Publish(inst->maxcycle_channel_, timestamp, max_cycle_time);

  inst->SendPpsMode();
  inst->SendVersion();
  inst->SendStackDetection();
  inst->SendCoreTemperature();
}

void BoardMonitor::MsgHandlerPing(const kitecom::timestamped_vector_uint &msg) {
  Publish(pong_channel_, SystemTime::GetTimestamp(), msg.values);
}
