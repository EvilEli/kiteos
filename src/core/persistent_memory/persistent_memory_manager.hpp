// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#ifndef SRC_CORE_PERSISTENT_MEMORY_PERSISTENT_MEMORY_MANAGER_HPP_
#define SRC_CORE_PERSISTENT_MEMORY_PERSISTENT_MEMORY_MANAGER_HPP_

//*************************************************************************************************/
//! \addtogroup plugins
//! @{
//! \addtogroup misc
//! @{
///
//*************************************************************************************************/

#include <cstdint>
#include <cstdbool>
#include <vector>
#include "core/software_timer.hpp"
#include "pulicast-port/pulicast_embedded.h"
#include "kitecom/timestamped_vector_byte.hpp"


/*!
 *  \brief Implements a  class to directly access the persistent memory via pulicast.
 */
class PersistentMemoryManager {
 public:
  // Public methods
  /*!
   * \brief Constructor of the persistent memory manager component.
   *
   * \param puli_namespace  The pulicast namespace the persistent memory manager operates in.
   */
  explicit PersistentMemoryManager(pulicast::Namespace puli_namespace);

 private:
  /*!
   * \brief This method handles the command to process.
   */
  void HandleCommand(const std::vector<uint8_t > &command);

  /*!
   * \brief Handles an incoming get size command.
   */
  void HandleGetSizeCommand(const std::vector<uint8_t > &command);

  /*!
   * \brief Handles an incoming read command.
   */
  void HandleReadCommand(const std::vector<uint8_t > &command);

  /*!
   * \brief Handles an incoming read all command.
   */
  void HandleReadAllCommand(const std::vector<uint8_t > &command);

  /*!
   * \brief Handles an incoming write command.
   */
  void HandleWriteCommand(const std::vector<uint8_t > &command);

  /*!
   * \brief Handles an incoming erase command.
   */
  void HandleEraseCommand(const std::vector<uint8_t > &command);

  /*!
   * \brief Publishes an error response.
   *
   * \param error_type  Error type to be encoded in the error response.
   */
  void PublishErrorResponse(const std::vector<uint8_t> &command, uint8_t error_type);

  /*!
   * \brief Sets the source and destination fields of a response message.
   *
   * \param command  Command to respond to.
   * \param response Response whichs source and destination fields should be updated.
   */
  void SetResponseSourceAndDestination(const std::vector<uint8_t > &command,
                                              std::vector<uint8_t > &response);

  /*!
   * \brief Publishes a response via pulicast.
   *
   * \param response  Response to be published.
   */
  void PublishResponse(const std::vector<uint8_t > &response);

  // Callback functions

  /*!
  * \brief Callback that handles incoming pulicast command messages.
  *
  * \param msg     Raw undecoded pulicast receive buffer
  * \param source  Indication where the message is originated
  * \param lead    Indicator for dropped/oos messages
  */
  void CommandHandler(const kitecom::timestamped_vector_byte &msg);

  //! Callback called when the update timer elapsed, publishes the errors.
  static void UpdateTimerCallback(PersistentMemoryManager *instance);

  //! Timer used for periodically reading memory access errors.
  SoftwareTimer update_timer_;

  pulicast::Channel& communication_channel_;  //!< Pulicast communication channel.
};
/*! @} */
/*! @} */

#endif  // SRC_CORE_PERSISTENT_MEMORY_PERSISTENT_MEMORY_MANAGER_HPP_
