// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#ifndef SRC_CORE_PERSISTENT_MEMORY_FRAM_ACCESS_HPP_
#define SRC_CORE_PERSISTENT_MEMORY_FRAM_ACCESS_HPP_
/**************************************************************************************************
 *                                                                                                *
 *        '|| '||'  '|'    |      '||''|.    '|.   '|'  '||'  '|.   '|'   ..|'''.|                *
 *         '|. '|.  .'    |||      ||   ||    |'|   |    ||    |'|   |   .|'     '                *
 *          ||  ||  |    |  ||     ||''|'     | '|. |    ||    | '|. |   ||    ....               *
 *           ||| |||    .''''|.    ||   |.    |   |||    ||    |   |||   '|.    ||                *
 *            |   |    .|.  .||.  .||.  '|'  .|.   '|   .||.  .|.   '|    ''|...'|                *
 *                                                                                                *
 *        !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!               *
 *        !!   Do not directly use this class to access the persistent memory.   !!               *
 *        !!              Use The PersistentMemory class instead.                !!               *
 *        !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!               *
 *                                                                                                *
 **************************************************************************************************/

//*****************************************************************************/
//! \addtogroup services
//! @{
//! \addtogroup persistent-memory
//! @{
///
//*****************************************************************************/

// Define macro for poisoned MB85RS64T class.
#define MB85RS64T Mb85Rs64T

// Only for test purposes. Uncomment this if you are using an evaluation board with no FRAM
// available, to simulate the FRAM.
#if USE_EVAL_BOARD
#define MOCK_FRAM
#endif

#include <map>
#include <forward_list>
#ifdef MOCK_FRAM
#include "core/persistent_memory/fram_emulator.hpp"
#include <vector>
#include <utility>
#else
#include "drivers/mb85rs64t.hpp"
#endif

//! Number of key value pairs that can be stored in the FRAM
static constexpr const uint16_t kFramSize = (kMb85Rs64TSize - 8) / 8;


/*!
 * \brief      Implements the FRAM access class.
 *
 * \details    This class is used to access the FRAM. It implements a system to store key value
 *             pairs as well as write protection and ensures that the FRAM is accessed in the
 *             correct way to prevent accidental corruption of the data stored in the FRAM.
 *
 * \author     Jan Lehmann
 *
 * \date       2020
 *
 */
class FramAccess {
 public:
  // Public methods

  /*!
   * \brief  Initializes the FRAM access class.
   *
   * \return Returns true on success, false otherwise.
   */
  static bool Init();

  /*!
   * \brief Lets the FRAM access class do its work. This method should be called as often as
   *        possible.
   */
  static void Run();

  /*!
   * \brief  Gets the occupied space of the FRAM.
   *
   * \return Returns the number of key value pairs that are stored in the FRAM.
   */
  static uint32_t GetOccupiedSpace();

  /*!
   * \brief  Gets the free space availabe in the FRAM.
   *
   * \return Returns the remaining number of key value pairs that can be stored in the FRAM.
   */
  static uint32_t GetFreeSpace();

  /*!
   * \brief  Checks if there is a value stored in the FRAM corresponding to a given key.
   *
   * \param  key  Key which should be checked.
   *
   * \return Returns True if there is a value stored in the FRAM corresponding to the given key,
   *         false otherwise.
   */
  static bool KeyExists(uint32_t key);

  /*!
   * \brief  Gets the value corresponding to a given key.
   *
   * \param  key    Key of the value to get.
   * \param  value  Pointer to the memory location where the value should be stored.
   *
   * \return Returns true on success, false otherwise.
   */
  static bool GetValue(uint32_t key, uint32_t *value);

  /*!
   * \brief   Gets a key value pair from the FRAM.
   *
   * \details Repeatedly calling this method will iterate over the FRAM elements.
   *
   * \param   key    Pointer to the memory location where the key should be stored.
   * \param   value  Pointer to the memory location where the value should be stored.
   *
   * \return  Returns true on success, false if there are no elements stored in the FRAM.
   */
  static bool GetNextKeyValuePair(uint32_t *key, uint32_t *value);

  /*!
   * \brief   Sets the value corresponding to a given key.
   *
   * \details If the given key already exists, the value assigned to the key is overwritten. If the
   *          key does not exist, a new key value pair is stored in the FRAM.
   *
   * \warning If a new key is added to the FRAM by this function, a dynamic memory allocation is
   *          carried out. This happens when a value is set for a key which was not present in the
   *          FRAM before. This function should not be called within an interrupt.
   *
   * \param   key    Key of the value to set. BIT32 acts as fuse bit. A key with a set fuse bit can
   *                 only be written once and not be erased.
   * \param   value  Value to set.
   *
   * \return  Returns true on success, false otherwise.
   */
  static bool SetValue(uint32_t key, uint32_t value);

  /*!
   * \brief   Erases a key value pair from the FRAM.
   *
   * \warning This function should not be called within an interrupt.
   *
   * \param   key  Key of the key value pair to erase. BIT32 acts as fuse bit. If this bit is set,
   *               the key value pair cannot be erased.
   *
   * \return  Returns true on success, false otherwise.
   */
  static bool EraseKeyValuePair(uint32_t key);

  /*!
   * \brief  Returns the internal write error counter and clears it.
   *
   * \return Number of write errors occurred since the last call of this function. If this function
   *         is called the first time, it returns the number of write errors occurred since the
   *         initialization.
   */
  static uint32_t ClearWriteErrorCounter();

  /************************************************************************************************
   *                                                                                              *
   *       '|| '||'  '|'    |      '||''|.    '|.   '|'  '||'  '|.   '|'   ..|'''.|               *
   *        '|. '|.  .'    |||      ||   ||    |'|   |    ||    |'|   |   .|'     '               *
   *         ||  ||  |    |  ||     ||''|'     | '|. |    ||    | '|. |   ||    ....              *
   *          ||| |||    .''''|.    ||   |.    |   |||    ||    |   |||   '|.    ||               *
   *           |   |    .|.  .||.  .||.  '|'  .|.   '|   .||.  .|.   '|    ''|...'|               *
   *                                                                                              *
   *       !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!              *
   *       !!  SetProtectedValue and EraseProtectedKeyValuePair will ignore the   !!              *
   *       !! fuse bit. These functions should only be used by maintenance tools. !!              *
   *       !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!              *
   *                                                                                              *
   ************************************************************************************************/

  /*!
   * \brief   Sets the value corresponding to a given key.
   *
   * \details If the given key already exists, the value assigned to the key is overwritten. If the
   *          key does not exist, a new key value pair is stored in the FRAM. This method will also
   *          modify values protected by a fuse bit.
   *
   * \warning If a new key is added to the FRAM by this function, a dynamic memory allocation is
   *          carried out. This happens when a value is set for a key which was not present in the
   *          FRAM before. Since also protected values will be modified, this method should only be
   *          used by maintenance tools. In most cases the SetValue method should be used instead.
   *          This function should not be called within an interrupt.
   *
   * \param   key    Key of the value to set. BIT32 acts as fuse bit. A key with a set fuse bit can
   *                 only be written once and not be erased.
   * \param   value  Value to set.
   *
   * \return  Returns true on success, false otherwise.
   */
  static bool SetProtectedValue(uint32_t key, uint32_t value);

  /*!
   * \brief   Erases a key value pair from the FRAM.
   *
   * \details Erases a key value pair from the FRAM. This method will also erase values protected by
   *          a fuse bit.
   *
   * \warning Since also protected values will be erased, this method should only be used by
   *          maintenance tools. In most cases the EraseKeyValuePair method should be used instead.
   *          This function should not be called within an interrupt.
   *
   * \param   key  Key of the key value pair to erase. BIT32 acts as fuse bit. If this bit is set,
   *               the key value pair cannot be erased.
   *
   * \return  Returns true on success, false otherwise.
   */
  static bool EraseProtectedKeyValuePair(uint32_t key);

 private:
  /*!
   * \brief Enum containing data associated to a certain key.
   */
  struct FramEntry {
    uint16_t key_address;  //!< FRAM address where the key value pair is stored.
    uint32_t value;  //!< Value associated with the key.
    //! Flag to notify the main routine that the key value pair does not exist in the FRAM yet and
    //! should be added.
    bool create_entry;
    //! Flag to notify the main routine that the value should be updated in the FRAM.
    int update_value;
    //! Flag to notify the main routine that the key value pair should be erased from the FRAM.
    bool erase_entry;
  };

  // Private variables

  //! Map mapping the values stored in the FRAM to an FRAM entry structure.
  static std::map<uint32_t, FramEntry> fram_entries_;
  //! Iterator used by the GetNextKeyValuePair method to iterate over the FRAM entries.
  static std::map<uint32_t, FramEntry>::iterator fram_entries_iterator_;
  static std::forward_list<uint16_t> occupied_memory_;  //!< List of the occupied FRAM memory slots.
  static MB85RS64T* mb85rs64t_;  //!< MB85RS64T (FRAM driver) object used to access the FRAM.
  static bool action_pending_;  //!< Flag indicating that something has to be done.
  static uint32_t write_failures_;  //!< Counter for failed write operations.

  // Private methods
#ifdef MOCK_FRAM
  /*!
   * \brief  Implements an init function which mocks the FRAM. This mock init function replaces the
   *         implementation of the init function when MOCK_FRAM is set.
   *
   * \return Returns true on success, false otherwise.
   */
  static bool MockInit();

  /*!
   * \brief  Implements a run function which mocks the FRAM. This mock run function replaces the
   *         implementation of the run function when MOCK_FRAM is set.
   */
  static void MockRun();
#endif

  /*!
   * \brief   Sets the value corresponding to a given key.
   *
   * \details If the given key already exists, the value assigned to the key is overwritten. If the
   *          key does not exist, a new key value pair is stored in the FRAM.
   *
   * \warning If a new key is added to the FRAM by this function, a dynamic memory allocation is
   *          carried out. This happens when a value is set for a key which was not present in the
   *          FRAM before.
   *
   * \param   key              Key of the value to set. BIT32 acts as fuse bit. A key with a set
   *                           fuse bit can only be written once and not be erased.
   * \param   value            Value to set.
   * \param   ignore_fuse_bit  If true, this method will also set values protected by a fuse bit.
   *                           Otherwise it will return false when trying to modify a value
   *                           protected by a fuse bit.
   *
   * \return  Returns true on success, false otherwise.
   */
  static bool SetValue(uint32_t key, uint32_t value, bool ignore_fuse_bit);

  /*!
   * \brief  Erases a key value pair from the FRAM.
   *
   * \param  key              Key of the key value pair to erase. BIT32 acts as fuse bit. If this
   *                          bit is set, the key value pair cannot be erased.
   * \param  ignore_fuse_bit  If true, this method will also erase values protected by a fuse bit.
   *                          Otherwise it will return false when trying to erase a value protected
   *                          by a fuse bit.
   *
   * \return Returns true on success, false otherwise.
   */
  static bool EraseKeyValuePair(uint32_t key, bool ignore_fuse_bit);

  // Callback functions

  /*!
   * \brief     Callback called when the write operation to create an FRAM entry finished.
   *
   * \attention This callback is also called when the write operation failed. Read the MB85RS64T
   *            state to check if the write operation succeeded.
   * \param     instance  Pointer to the FramEntry object which was written to the FRAM.
   */
  static void CreateEntryCallback(void *instance);

  /*!
   * \brief     Callback called when the write operation to update an FRAM entry finished.
   *
   * \attention This callback is also called when the write operation failed. Read the MB85RS64T
   *            state to check if the write operation succeeded.
   * \param     instance  Pointer to the FramEntry object which was written to the FRAM.
   */
  static void UpdateValueCallback(void *instance);

  /*!
   * \brief     Callback called when the write operation to erase an FRAM entry finished.
   *
   * \attention This callback is also called when the write operation failed. Read the MB85RS64T
   *            state to check if the write operation succeeded.
   * \param     instance  Pointer to the FramEntry object which was erased from the FRAM.
   */
  static void EraseEntryCallback(void *instance);
};

// Poison MB85RS64T class to ensure that it is not directly used outside of the core.
#pragma GCC poison  FramAccess

// Poison methods accessing values protected by a fuse bit to ensure that these methods are not used
// accidentally.
#pragma GCC poison  SetProtectedValue EraseProtectedKeyValuePair
/*! @} */
/*! @} */
#endif  // SRC_CORE_PERSISTENT_MEMORY_FRAM_ACCESS_HPP_
