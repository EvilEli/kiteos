// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#ifndef SRC_CORE_PERSISTENT_MEMORY_PERSISTENT_MEMORY_HPP_
#define SRC_CORE_PERSISTENT_MEMORY_PERSISTENT_MEMORY_HPP_

#include <string>

//! Class implementing the definitions of persistent memory keys and their interpretation.
class PersistentMemory {
 public:
  //! Persistent memory key definitions according to the persistent memory manual.
  enum Key {
    kIpAddress = 0x00000001,
    kBoardIndex = 0x00000010,
    kKiteName = 0x00000020,
    kNetmask = 0x00000030,
    kGatewayAddress = 0x00000040,
    kBoardName = 0x00000050,
    kSystemTimeMode = 0x00000060,
    kSwitchHardware = 0x00000080,
    kUseMotorPlugin = 0x00001000,
    kUseImuPlugin = 0x00001011,
    kUseGnssPlugin = 0x00001020,
    kUseRcReceiverPlugin = 0x00001030,
    kUseInePlugin = 0x00001040,
    kUsePowerMonitorPlugin = 0x00001050,
    kUseAclPlugin = 0x00001060,
    kUseUwbPlugin = 0x00001070,
    kUseBaroPlugin = 0x00001080,
    kUseAdcPlugin = 0x00001090,
    kUseHygrometerPlugin = 0x000010A0,
    kUseStepperMotorPlugin = 0x000010B0,
    kModelParameterSet = 0x00002000,
    kStayInBootloader = 0x40003000,
    kFirmwareBroken = 0x40003010,
    kPcbVersion = 0x80000011,
    kPcbId = 0x80000020,
    kPcbType0 = 0x80000030,
    kPcbType1 = 0x80000031,
    kPcbType2 = 0x80000032,
    kPcbType3 = 0x80000033
  };

  //! Operation mode of the system time module defining how to synchronize the system time.
  enum SystemTimeMode {
    kSystemTimeModePtpSlave = 0,
    kSystemTimeModePtpMasterLocal = 1,
    kSystemTimeModePtpMasterGnss = 2,
    kSystemTimeModePpsSlave = 3,
    kSystemTimeModePpsSlaveGnss = 4,
    kSystemTimeModePtpMasterPpsSlave = 5,
    kSystemTimeModePtpMasterPpsSlaveGnss = 6
  };

  //! Switch hardware that is available on the board.
  enum SwitchHardware {
    kSwitchHardwareNone = 0,
    kSwitchHardwareKsz8563R = 1,
    kSwitchHardwareDualKsz8567R = 2
  };

  //! Parameter Set for the different Kite models.
  enum ModelParameterSet {
    kModelParameterSetKs0Amelia = 0,
    kModelParameterSetKs0Ulw001 = 1,
    kModelParameterSetKs0Ulw002 = 2,
    kModelParameterSetKs0Ulw003 = 3,
    kModelParameterSetNumber = 4
  };

  /*!
     * \brief  Gets the occupied space of the persistent memory.
     *
     * \return Returns the number of key value pairs that are stored in the persistent memory.
     */
  static uint32_t GetOccupiedSpace();

  /*!
   * \brief  Gets the free space availabe in the persistent memory.
   *
   * \return Returns the remaining number of key value pairs that can be stored in the persistent
   *         memory.
   */
  static uint32_t GetFreeSpace();

  /*!
   * \brief  Checks if there is a value stored in the persistent memory corresponding to a given
   *         key.
   *
   * \param  key  Key which should be checked.
   *
   * \return Returns True if there is a value stored in the persistent memory corresponding to the
   *         given key, false otherwise.
   */
  static bool KeyExists(Key key);

  /*!
   * \brief  Gets the value corresponding to a given key.
   *
   * \param  key    Key of the value to get.
   * \param  value  Pointer to the memory location where the value should be stored.
   *
   * \return Returns true on success, false otherwise.
   */
  template<class T>
  static bool GetValue(Key key, T value);

  /*!
   * \name   GetValuesFunctionGroup
   */
  ///@{
  /*!
   * \brief   Gets multiple values starting at a given key.
   *
   * \details For data types smaller than 32 bit, multiple values may fit in one key. If multiple
   *          keys are needed, the next read key is always incremented by one.
   *
   * \param   start_key  First key of the values to get.
   * \param   values     Pointer to the memory location where the values should be stored.
   * \param   length     Number of values to get.
   *
   * \return  Returns true on success, false otherwise.
   */
  static bool GetValues(Key start_key, uint64_t* values, uint32_t length);
  static bool GetValues(Key start_key, uint32_t* values, uint32_t length);
  static bool GetValues(Key start_key, uint16_t* values, uint32_t length);
  static bool GetValues(Key start_key, uint8_t* values, uint32_t length);
  static bool GetValues(Key start_key, int64_t* values, uint32_t length);
  static bool GetValues(Key start_key, int32_t* values, uint32_t length);
  static bool GetValues(Key start_key, int16_t* values, uint32_t length);
  static bool GetValues(Key start_key, int8_t* values, uint32_t length);
  static bool GetValues(Key start_key, double* values, uint32_t length);
  static bool GetValues(Key start_key, float* values, uint32_t length);
  static bool GetValues(Key start_key, bool* values, uint32_t length);
  ///@}

  /*!
   * \brief  Gets a string value from the persistent memory.
   *
   * \tparam LENGTH  Maximum length of the string.
   *
   * \param  start_key First key of the string to get.
   * \param  value     Pointer to the memory location where the string should be stored.
   *
   * \return Returns true on success, false otherwise.
   */
  static bool GetStringValue(Key start_key, std::string &value, uint8_t len);

  /*!
   * \brief   Sets the value corresponding to a given key.
   *
   * \details If the given key already exists, the value assigned to the key is overwritten. If the
   *          key does not exist, a new key value pair is stored in the persistent memory.
   *
   * \warning If a new key is added to the persistent memory by this function, a dynamic memory
   *          allocation is carried out. This happens when a value is set for a key which was not
   *          present in the persistent memory before. This function should not be called within an
   *          interrupt.
   *
   * \param   key    Key of the value to set. BIT32 acts as fuse bit. A key with a set fuse bit can
   *                 only be written once and not be erased.
   * \param   value  Value to set.
   *
   * \return  Returns true on success, false otherwise.
   */
  template<class T>
  static bool SetValue(Key key, T value);

  /*!
   * \name   SetValuesFunctionGroup
   */
  ///@{
  /*!
   * \brief   Sets multiple values starting at a given key.
   *
   * \details For data types smaller than 32 bit, multiple values may fit in one key. If multiple
   *          keys are needed, the next set key is always incremented by one.
   *
   * \param   start_key  First key of the values to set.
   * \param   values     Pointer to the memory location where the values to set are stored.
   * \param   length     Number of values to set.
   *
   * \return  Returns true on success, false otherwise.
   */
  static bool SetValues(Key start_key, const uint64_t* values, uint32_t length);
  static bool SetValues(Key start_key, const uint32_t* values, uint32_t length);
  static bool SetValues(Key start_key, const uint16_t* values, uint32_t length);
  static bool SetValues(Key start_key, const uint8_t* values, uint32_t length);
  static bool SetValues(Key start_key, const int64_t* values, uint32_t length);
  static bool SetValues(Key start_key, const int32_t* values, uint32_t length);
  static bool SetValues(Key start_key, const int16_t* values, uint32_t length);
  static bool SetValues(Key start_key, const int8_t* values, uint32_t length);
  static bool SetValues(Key start_key, const char* values, uint32_t length);
  static bool SetValues(Key start_key, const double* values, uint32_t length);
  static bool SetValues(Key start_key, const float* values, uint32_t length);
  static bool SetValues(Key start_key, const bool* values, uint32_t length);
  ///@}

  /*!
   * \brief  Sets a given string value in the persistent memory
   *
   * \tparam LENGTH  Maximum length of the string.
   *
   * \param  start_key  First key of the string to write.
   * \param  value      String to write to the persistent memory.
   *
   * \return Returns true on success, false otherwise.
   */
  static bool SetStringValue(Key start_key, const std::string &value);

  /*!
   * \brief   Erases a key value pair from the persistent memory.
   *
   * \warning This function should not be called within an interrupt.
   *
   * \param   key  Key of the key value pair to erase. BIT32 acts as fuse bit. If this bit is set,
   *               the key value pair cannot be erased.
   *
   * \return  Returns true on success, false otherwise.
   */
  static bool EraseKeyValuePair(Key key);

  /*!
   * \brief   Erases a multiple key value pairs from the persistent memory.
   *
   * \warning This function should not be called within an interrupt.
   *
   * \param   start key  First key of the key value pairs to erase. BIT32 acts as fuse bit. If this
   *                     bit is set, the key value pair cannot be erased. If cnt is greater than 1,
   *                     the next read key is always incremented by one.
   *
   * \return  Returns true on success, false otherwise.
   */
  static bool EraseKeyValuePairs(Key start_key, uint32_t cnt);

  /*!
   * \brief   Checks if a given bool key is present in the persistent memory and true.
   *
   * \param   key  Key of the key value pair to check.
   *
   * \return  Returns true on success, false otherwise.
   */
  static bool ValueIsPresentAndTrue(Key key);

 private:
  //! This method is a wrapper of FramAccess::GetValue to make the method usable by the template
  //! method implementations in the header without defining a macro for the poisoned FramAccess +
  //! class in the header.
  static bool GetValueFromFram(uint32_t key, uint32_t *value);
};


/**************************************************************************************************
 *     Public method implementations                                                              *
 **************************************************************************************************/

template<class T>
bool PersistentMemory::GetValue(PersistentMemory::Key key, T value) {
  return GetValues(key, value, 1);
}

template<class T>
bool PersistentMemory::SetValue(PersistentMemory::Key key, T value) {
  return SetValues(key, &value, 1);
}
#endif  // SRC_CORE_PERSISTENT_MEMORY_PERSISTENT_MEMORY_HPP_
