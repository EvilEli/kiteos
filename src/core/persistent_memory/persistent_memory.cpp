// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
// Define macro for poisoned FramAccess class.
#define FRAM_ACCESS FramAccess

#include "persistent_memory.hpp"
#include "fram_access.hpp"

/**************************************************************************************************
 *     Public methods                                                                             *
 **************************************************************************************************/
uint32_t PersistentMemory::GetOccupiedSpace() {
  return FRAM_ACCESS::GetOccupiedSpace();
}
uint32_t PersistentMemory::GetFreeSpace() {
  return FRAM_ACCESS::GetFreeSpace();
}
bool PersistentMemory::KeyExists(PersistentMemory::Key key) {
  return FRAM_ACCESS::KeyExists(key);
}

bool PersistentMemory::GetValues(Key start_key, uint64_t* values, uint32_t length) {
  for (uint32_t i = 0; i < length; i++) {
    uint32_t fram_values[2] = {0};
    if (!FRAM_ACCESS::GetValue(start_key + (2 * i), &fram_values[0])) {
      return false;
    }
    if (!FRAM_ACCESS::GetValue(start_key + (2 * i) + 1, &fram_values[1])) {
      return false;
    }
    values[i] =
        (static_cast<uint64_t >(fram_values[0]) << 32) | static_cast<uint64_t >(fram_values[1]);
  }
  return true;
}

bool PersistentMemory::GetValues(Key start_key, uint32_t* values, uint32_t length) {
  for (uint32_t i = 0; i < length; i++) {
    if (!FRAM_ACCESS::GetValue(start_key + i, &values[i])) {
      return false;
    }
  }
  return true;
}

bool PersistentMemory::GetValues(Key start_key, uint16_t* values, uint32_t length) {
  for (uint32_t i = 0; i < (length + 1) / 2; i++) {
    uint32_t fram_value = 0;
    if (!FRAM_ACCESS::GetValue(start_key + i, &fram_value)) {
      return false;
    }
    values[i * 2] = fram_value >> 16u;
    if (length > i * 2 + 1) {
      values[i * 2 + 1] = fram_value;
    }
  }
  return true;
}

bool PersistentMemory::GetValues(Key start_key, uint8_t* values, uint32_t length) {
  for (uint32_t i = 0; i < (length + 3) / 4; i++) {
    uint32_t fram_value = 0;
    if (!FRAM_ACCESS::GetValue(start_key + i, &fram_value)) {
      return false;
    }
    values[i * 4] = fram_value >> 24u;
    if (length > i * 4 + 1) {
      values[i * 4 + 1] = fram_value >> 16u;
    }
    if (length > i * 4 + 2) {
      values[i * 4 + 2] = fram_value >> 8u;
    }
    if (length > i * 4 + 3) {
      values[i * 4 + 3] = fram_value;
    }
  }
  return true;
}

bool PersistentMemory::GetValues(Key start_key, int64_t* values, uint32_t length) {
  return GetValues(start_key, reinterpret_cast<uint64_t*>(values), length);
}

bool PersistentMemory::GetValues(Key start_key, int32_t* values, uint32_t length) {
  return GetValues(start_key, reinterpret_cast<uint32_t*>(values), length);
}

bool PersistentMemory::GetValues(Key start_key, int16_t* values, uint32_t length) {
  return GetValues(start_key, reinterpret_cast<uint16_t*>(values), length);
}

bool PersistentMemory::GetValues(Key start_key, int8_t* values, uint32_t length) {
  return GetValues(start_key, reinterpret_cast<uint8_t*>(values), length);
}

bool PersistentMemory::GetValues(Key start_key, double* values, uint32_t length) {
  return GetValues(start_key, reinterpret_cast<uint64_t*>(values), length);
}

bool PersistentMemory::GetValues(Key start_key, float* values, uint32_t length) {
  return GetValues(start_key, reinterpret_cast<uint32_t*>(values), length);
}

bool PersistentMemory::GetValues(Key start_key, bool* values, uint32_t length) {
  for (uint32_t i = 0; i < (length + 31) / 32; i++) {
    uint32_t fram_value = 0;
    if (!FRAM_ACCESS::GetValue(start_key + i, &fram_value)) {
      return false;
    }
    for (int j = 0; j < 32; j++) {
      if (length > i * 32 + j) {
        values[i * 32 + j] = fram_value >> (31u - j);
      } else {
        return true;
      }
    }
  }
  return true;
}

bool PersistentMemory::GetStringValue(PersistentMemory::Key start_key,
                                      std::string &value,
                                      uint8_t len) {
  for (uint8_t i = 0; i < (len + 3) / 4; i++) {
    uint32_t fram_value = 0;
    if (!GetValueFromFram(start_key + i, &fram_value)) {
      return false;
    }
    char c = fram_value >> 24u;
    if (c) {
      value += c;
    }
    if (static_cast<uint8_t>(fram_value >> 24u) == 0) {
      return true;
    }
    if (len > i * 4 + 1) {
      char c = fram_value >> 16u;
      if (c) {
        value += c;
      }
      if (static_cast<uint8_t>(fram_value >> 16u) == 0) {
        return true;
      }
    }
    if (len > i * 4 + 2) {
      char c = fram_value >> 8u;
      if (c) {
        value += c;
      }
      if (static_cast<uint8_t>(fram_value >> 8u) == 0) {
        return true;
      }
    }
    if (len > i * 4 + 3) {
      char c = fram_value;
      if (c) {
        value += c;
      }
      if (static_cast<uint8_t>(fram_value) == 0) {
        return true;
      }
    }
  }
  return true;
}

bool PersistentMemory::SetValues(Key start_key, const uint64_t* values, uint32_t length) {
  for (uint32_t i = 0; i < length; i++) {
    if (!FRAM_ACCESS::SetValue(start_key + (2 * i), static_cast<uint32_t >(values[i] >> 32))) {
      return false;
    }
    if (!FRAM_ACCESS::SetValue(start_key + (2 * i) + 1, static_cast<uint32_t >(values[i]))) {
      return false;
    }
  }
  return true;
}

bool PersistentMemory::SetValues(PersistentMemory::Key start_key, const uint32_t *values,
                                 uint32_t length) {
  for (uint32_t i = 0; i < length; i++) {
    if (!FRAM_ACCESS::SetValue(start_key + i, values[i])) {
      return false;
    }
  }
  return true;
}

bool PersistentMemory::SetValues(PersistentMemory::Key start_key, const uint16_t *values,
                                 uint32_t length) {
  for (uint32_t i = 0; i < (length + 1) / 2; i++) {
    uint32_t fram_value = static_cast<uint32_t >(values[i * 2]) << 16u;
    if (length > i * 2 + 1) {
      fram_value |= static_cast<uint32_t >(values[i * 2 + 1]);
    }
    if (!FRAM_ACCESS::SetValue(start_key + i, fram_value)) {
      return false;
    }
  }
  return true;
}

bool PersistentMemory::SetValues(PersistentMemory::Key start_key, const uint8_t *values,
                                 uint32_t length) {
  for (uint32_t i = 0; i < (length + 3) / 4; i++) {
    uint32_t fram_value = static_cast<uint32_t >(values[i * 4]) << 24u;
    if (length > i * 4 + 1) {
      fram_value |= static_cast<uint32_t >(values[i * 4 + 1]) << 16u;
    }
    if (length > i * 4 + 2) {
      fram_value |= static_cast<uint32_t >(values[i * 4 + 2]) << 8u;
    }
    if (length > i * 4 + 3) {
      fram_value |= static_cast<uint32_t >(values[i * 4 + 3]);
    }
    if (!FRAM_ACCESS::SetValue(start_key + i, fram_value)) {
      return false;
    }
  }
  return true;
}

bool PersistentMemory::SetValues(Key start_key, const int64_t* values, uint32_t length) {
  return SetValues(start_key, reinterpret_cast<const uint64_t*>(values), length);
}

bool PersistentMemory::SetValues(PersistentMemory::Key start_key, const int32_t *values,
                                 uint32_t length) {
  return SetValues(start_key, reinterpret_cast<const uint32_t*>(values), length);
}

bool PersistentMemory::SetValues(PersistentMemory::Key start_key, const int16_t *values,
                                 uint32_t length) {
  return SetValues(start_key, reinterpret_cast<const uint16_t*>(values), length);
}

bool PersistentMemory::SetValues(PersistentMemory::Key start_key, const int8_t *values,
                                 uint32_t length) {
  return SetValues(start_key, reinterpret_cast<const uint8_t*>(values), length);
}

bool PersistentMemory::SetValues(Key start_key, const double* values, uint32_t length) {
  return SetValues(start_key, reinterpret_cast<const uint64_t*>(values), length);
}

bool PersistentMemory::SetValues(PersistentMemory::Key start_key, const float *values,
                                 uint32_t length) {
  return SetValues(start_key, reinterpret_cast<const uint32_t*>(values), length);
}

bool PersistentMemory::SetValues(PersistentMemory::Key start_key, const bool *values,
                                 uint32_t length) {
  for (uint32_t i = 0; i < (length + 31) / 32; i++) {
    uint32_t fram_value = 0;
    for (int j = 0; j < 32; j++) {
      if (length > i * 32 + j) {
        if (values[i * 32 + j]) {
          fram_value |= 1 << (31u - j);
        }
      } else {
        break;
      }
      if (!FRAM_ACCESS::SetValue(start_key + i, fram_value)) {
        return false;
      }
    }
  }
  return true;
}

bool PersistentMemory::SetStringValue(PersistentMemory::Key start_key,
                                      const std::string &value) {
  return SetValues(start_key, value.c_str(), value.size());
}


bool PersistentMemory::EraseKeyValuePair(PersistentMemory::Key key) {
  return FRAM_ACCESS::EraseKeyValuePair(key);
}

bool PersistentMemory::EraseKeyValuePairs(PersistentMemory::Key start_key, uint32_t cnt) {
  bool ret = true;
  for (uint32_t i = 0; i < cnt; cnt++) {
    ret &= FRAM_ACCESS::EraseKeyValuePair(start_key + i);
  }
  return false;
}

bool PersistentMemory::ValueIsPresentAndTrue(PersistentMemory::Key key) {
  uint32_t value = 0;
  if (GetValue(key, &value)) {
    if (value) {
      return true;
    }
  }
  return false;
}

/**************************************************************************************************
 *     Private methods                                                                            *
 **************************************************************************************************/
bool PersistentMemory::GetValueFromFram(uint32_t key, uint32_t *value) {
  return FRAM_ACCESS::GetValue(key, value);
}
