// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
// Define macro for poisoned class.
#define FRAM_ACCESS FramAccess

// Define macros for poisoned methods.
#define FRAM_ACCESS_SET_PROTECTED_VALUE SetProtectedValue
#define FRAM_ACCESS_ERASE_PROTECTED_KEY_VALUE_PAIR EraseProtectedKeyValuePair

#include "core/persistent_memory/fram_access.hpp"
#include "core/logger.hpp"
#include "utils/debug_utils.h"

//! Magic word which identifies the FRAM as initialized. This word is stored at the first memory
//! address of the FRAM when the FRAM is accessed by this plugin for the first time.
static constexpr const uint64_t kFramAccessMagicWord = 0x0042004242004200;
//! When This bit is true in a FRAM key, the key counts as read only.
static constexpr const uint32_t kFramAccessFuseBit = 0x80000000;

/**************************************************************************************************
 *     Private static member variables                                                            *
 **************************************************************************************************/
std::map<uint32_t, FRAM_ACCESS::FramEntry> FRAM_ACCESS::fram_entries_ = {};
std::map<uint32_t, FRAM_ACCESS::FramEntry>::iterator FRAM_ACCESS::fram_entries_iterator_ =
    fram_entries_.end();
std::forward_list<uint16_t> FRAM_ACCESS::occupied_memory_ = {};
MB85RS64T* FRAM_ACCESS::mb85rs64t_ = nullptr;
bool FRAM_ACCESS::action_pending_ = false;
uint32_t FRAM_ACCESS::write_failures_ = 0;

/**************************************************************************************************
 *     Public methods                                                                             *
 **************************************************************************************************/
bool FRAM_ACCESS::Init() {
#ifdef MOCK_FRAM
  return FRAM_ACCESS::MockInit();
#else
  // Initialize FRAM
  mb85rs64t_ = new MB85RS64T(nullptr, nullptr);

  // Wait for initialization to finish
  while (mb85rs64t_->IsBusy()) {}

  // Check if magic word is stored in the FRAM
  uint64_t  magic_word_read;
  ASSERT_RESPONSE(mb85rs64t_->Read(0, reinterpret_cast<uint8_t*>(&magic_word_read), 8,
                  nullptr, nullptr), true);
  while (mb85rs64t_->IsBusy()) {}

  if (magic_word_read != kFramAccessMagicWord) {  // Magic word not found in the FRAM
    // Try to read the magic word a second time just to be sure that it was not corrupted by a read
    // error when reading the first time
    ASSERT_RESPONSE(mb85rs64t_->Read(0, reinterpret_cast<uint8_t*>(&magic_word_read), 8,
                                     nullptr, nullptr), true);
    while (mb85rs64t_->IsBusy()) {}
  }

  if (magic_word_read != kFramAccessMagicWord) {  // Magic word still not found in the FRAM
    Logger::Report("Magic word was not found in the FRAM. Initializing Fram.",
                   Logger::LogLevel::kInfo);

    // Erase FRAM
    for (int j = 0; j < kMb85Rs64TSize; j += kMb85Rs64TBufferLength) {
      uint8_t write_data[kMb85Rs64TBufferLength] = {0};
      ASSERT_RESPONSE(mb85rs64t_->Write(j, write_data, kMb85Rs64TBufferLength, nullptr, nullptr),
                      true);
      while (mb85rs64t_->IsBusy()) {}
      if (mb85rs64t_->GetState() == MB85RS64T::kStateWriteFailed) {
        return false;
      }
    }
    // Write magic word to FRAM
    ASSERT_RESPONSE(mb85rs64t_->Write(0, reinterpret_cast<const uint8_t*>(&kFramAccessMagicWord), 8,
                                     nullptr, nullptr), true);
    while (mb85rs64t_->IsBusy()) {}
    if (mb85rs64t_->GetState() == MB85RS64T::kStateWriteFailed) {
      return false;
    }
  } else {  // Magic word found in the FRAM
    // Read existing key value pairs from FRAM by looping through the whole memory
    auto occupied_memory_iterator = occupied_memory_.before_begin();
    for (int i = 0; i < kFramSize; i++) {
      // Read key value pair
      uint32_t fram_key_value_pair_read[2];
      ASSERT_RESPONSE(mb85rs64t_->Read(i * 8 + 8,
                                      reinterpret_cast<uint8_t*>(fram_key_value_pair_read), 8,
                                      nullptr, nullptr), true);
      while (mb85rs64t_->IsBusy()) {}

      // Check if memory slot is occupied with a key value pair
      if (fram_key_value_pair_read[0] != 0) {
        // Insert key value pair in map
        FramEntry fram_entry = {.key_address = static_cast<uint16_t>(i * 8 + 8),
                                .value = fram_key_value_pair_read[1], .create_entry = false,
                                .update_value = 0, .erase_entry = false};
        fram_entries_.insert(std::make_pair(fram_key_value_pair_read[0], fram_entry));
        occupied_memory_iterator = occupied_memory_.insert_after(occupied_memory_iterator,
                                                                 i * 8 + 8);
      }
    }
  }

  return true;
#endif
}

void FRAM_ACCESS::Run() {
#ifdef MOCK_FRAM
  FRAM_ACCESS::MockRun();
#else
  // Check if something has to be done
  if (action_pending_) {
    // Check if FRAM is ready to write
    if (!mb85rs64t_->IsBusy()) {
      // Loop through FRAM entry map
      for (auto &map_element : fram_entries_) {
        // Check if the key has to be added to the FRAM
        if (map_element.second.create_entry) {
          // Find a free memory slot
          auto it = occupied_memory_.begin();
          for (int i = 0; i < kFramSize; i++) {
            if ((it == occupied_memory_.end()) || (*it != i * 8 + 8)) {
              map_element.second.key_address = i * 8 + 8;
              break;
            }
            it++;
          }

          // Write key value pair to FRAM
          uint32_t key_value_pair[2] = {map_element.first, map_element.second.value};
          ASSERT_RESPONSE(mb85rs64t_->Write(map_element.second.key_address,
                                           reinterpret_cast<uint8_t *>(key_value_pair), 8,
                                           CreateEntryCallback, &map_element), true);
          return;
        }

        // Check if value was updated
        if (map_element.second.update_value > 0) {
          // Write new value to FRAM
          ASSERT_RESPONSE(
              mb85rs64t_->Write(map_element.second.key_address + 4,
                                reinterpret_cast<uint8_t *>(&(map_element.second.value)), 4,
                                UpdateValueCallback, &map_element), true);
          return;
        }

        // Check if the key value pair should be erased from the FRAM
        if (map_element.second.erase_entry) {
          // Erase key value pair from the FRAM
          uint8_t key_value_pair[8] = {0};
          ASSERT_RESPONSE(mb85rs64t_->Write(map_element.second.key_address, key_value_pair, 8,
                                            EraseEntryCallback, &map_element), true);
          return;
        }
      }
    } else {  // EEPROM is not ready to write
      return;
    }
  }

  // Reset action pending flag since nothing has to be done at the moment
  action_pending_ = false;
#endif
}

uint32_t FRAM_ACCESS::GetOccupiedSpace() {
  return fram_entries_.size();
}

uint32_t FRAM_ACCESS::GetFreeSpace() {
  return kFramSize - GetOccupiedSpace();
}

bool FRAM_ACCESS::KeyExists(uint32_t key) {
  return fram_entries_.count(key) != 0;
}

bool FRAM_ACCESS::GetValue(uint32_t key, uint32_t *value) {
  // Check if value exists
  if (!KeyExists(key)) {
    return false;
  }

  // Get value from map and write it to value pointer
  *value = fram_entries_[key].value;

  return true;
}

bool FRAM_ACCESS::GetNextKeyValuePair(uint32_t *key, uint32_t *value) {
  // Check if there are elements in the FRAM map
  if (GetOccupiedSpace() == 0) {
    return false;
  }

  // Increment iterator
  fram_entries_iterator_++;
  if (fram_entries_iterator_ == fram_entries_.end()) {
    fram_entries_iterator_ = fram_entries_.begin();
  }

  // Get current key value pair
  *key = fram_entries_iterator_->first;
  *value = fram_entries_iterator_->second.value;

  return true;
}

bool FRAM_ACCESS::SetValue(uint32_t key, uint32_t value) {
  // Set value considering the fuse bit
  return SetValue(key, value, false);
}

bool FRAM_ACCESS::EraseKeyValuePair(uint32_t key) {
  // Erase key value pair considering the fuse bit
  return EraseKeyValuePair(key, false);
}

uint32_t FRAM_ACCESS::ClearWriteErrorCounter() {
  uint32_t ret = write_failures_;
  write_failures_ = 0;
  return ret;
}

bool FRAM_ACCESS::FRAM_ACCESS_SET_PROTECTED_VALUE(uint32_t key, uint32_t value) {
  // Set value ignoring the fuse bit
  return SetValue(key, value, true);
}

bool FRAM_ACCESS::FRAM_ACCESS_ERASE_PROTECTED_KEY_VALUE_PAIR(uint32_t key) {
  // Erase key value pair ignoring the fuse bit
  return EraseKeyValuePair(key, true);
}


/**************************************************************************************************
 *     Private methods                                                                            *
 **************************************************************************************************/
#ifdef MOCK_FRAM
bool FRAM_ACCESS::MockInit() {
  std::vector<std::pair<uint32_t, uint32_t>> mock_key_value_pairs =
    *FramEmulator::GetContent().get();

  for (auto const& key_value_pair : mock_key_value_pairs) {
    FramEntry fram_entry = {.key_address = 0, .value = key_value_pair.second,
                            .create_entry = false, .update_value = 0, .erase_entry = false};
    fram_entries_.insert(std::make_pair(key_value_pair.first, fram_entry));
  }
  return true;
}

void FRAM_ACCESS::MockRun() {
  // Check if something has to be done
  if (action_pending_) {
    for (auto &map_element : fram_entries_) {
      // Check if the key has to be added to the FRAM
      if (map_element.second.create_entry) {
        // Simulate entry creation in the FRAM by resetting the flag
        map_element.second.create_entry = false;
        return;
      }

      // Check if value was updated
      if (map_element.second.update_value > 0) {
        // Simulate updating the value in the FRAM by setting the pending update counter to zero
        map_element.second.update_value = 0;
        return;
      }

      // Check if the key value pair should be erased from the FRAM
      if (map_element.second.erase_entry) {
        // Simulate erasing key value pair from the FRAM by removing the map element
        // Check if iterator points to this key value pair
        if (&*fram_entries_iterator_ == &map_element) {
          // Increment iterator to ensure that it still points to a valid address
          fram_entries_iterator_++;
        }

        // Remove key value pair from the FRAM map
        fram_entries_.erase(map_element.first);
        return;
      }
    }
  }

  // Reset action pending flag since nothing has to be done at the moment
  action_pending_ = false;
}
#endif

bool FRAM_ACCESS::SetValue(uint32_t key, uint32_t value, bool ignore_fuse_bit) {
  // Check if the key is valid
  if (key == 0xffffffff || key == 0x00000000) {
    return false;  // Key is reserved for empty space
  }

  // Check if key already exists
  if (!KeyExists(key)) {  // Key does not exist
    // Check if there is free space to store the key value pair
    if (GetFreeSpace() == 0) {
      return false;  // No free space to store the value
    }

    // Add an entry to the FRAM map
    FramEntry fram_entry = {.key_address = 0, .value = value, .create_entry = true,
                            .update_value = 0, .erase_entry = false};
    fram_entries_.insert(std::make_pair(key, fram_entry));
  } else {  // Key exists
    // Check if the value is protected by a fuse bit
    if ((!ignore_fuse_bit) && (key & kFramAccessFuseBit)) {
      return false;  // Value is protected by a fuse bit and cannot be written
    }

    // Update value
    fram_entries_[key].value = value;
    fram_entries_[key].update_value += 1;

    // Clear erase flag to prevent erasing if the value was issued to be erased before but is still
    // in the FRAM
    fram_entries_[key].erase_entry = false;
  }

  // Set action pending flag to notify the main routine that something has to be done
  action_pending_ = true;

  return true;
}

bool FRAM_ACCESS::EraseKeyValuePair(uint32_t key, bool ignore_fuse_bit) {
  // Check if value exists
  if (!KeyExists(key)) {
    return false;
  }

  // Check if the value is protected by a fuse bit
  if ((!ignore_fuse_bit) && (key & kFramAccessFuseBit)) {
    return false;  // Value is protected by a fuse bit and cannot be erased
  }

  // Set erase flag
  fram_entries_[key].erase_entry = true;

  // Set action pending flag to notify the main routine that something has to be done
  action_pending_ = true;

  return true;
}


/**************************************************************************************************
 *     Callback functions                                                                         *
 **************************************************************************************************/
#ifndef MOCK_FRAM

void FRAM_ACCESS::CreateEntryCallback(void *instance) {
  auto fram_entry = reinterpret_cast<std::pair<const uint32_t, FramEntry>*>(instance);

  if (mb85rs64t_->GetState() == MB85RS64T::kStateWriteFailed) {
    write_failures_++;
  } else {
    // Add memory slot to the occupied memory list
    auto it = occupied_memory_.before_begin();
    for (int i = 0; i < kFramSize; i++) {
      auto it_next = it;
      it_next++;
      if ((it_next == occupied_memory_.end()) ||
          ((it == occupied_memory_.before_begin() || (*it < fram_entry->second.key_address)) &&
           (*it_next > fram_entry->second.key_address))) {
        occupied_memory_.insert_after(it, fram_entry->second.key_address);
        break;
      }
      it++;
    }

    fram_entry->second.create_entry = false;
  }
}

void FRAM_ACCESS::UpdateValueCallback(void *instance) {
  auto fram_entry = reinterpret_cast<std::pair<const uint32_t, FramEntry>*>(instance);

  if (mb85rs64t_->GetState() == MB85RS64T::kStateWriteFailed) {
    write_failures_++;
  } else {
    fram_entry->second.update_value -= 1;
  }
}

void FRAM_ACCESS::EraseEntryCallback(void *instance) {
  auto fram_entry = reinterpret_cast<std::pair<const uint32_t, FramEntry>*>(instance);

  // Check if the value was updated during the erase process
  if (fram_entry->second.update_value > 0) {
    // value was updated while it was erased, so it must be created again
    fram_entry->second.erase_entry = false;
    fram_entry->second.create_entry = true;
  } else {
    // Remove key value pair memory address from the occupied memory list
    occupied_memory_.remove(fram_entry->second.key_address);

    // Check if iterator points to this key value pair
    if (&*fram_entries_iterator_ == fram_entry) {
      // Increment iterator to ensure that it still points to a valid address
      fram_entries_iterator_++;
    }

    // Remove key value pair from the FRAM map
    fram_entries_.erase(fram_entry->first);
  }
}
#endif
