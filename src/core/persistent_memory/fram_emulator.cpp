// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#include "core/persistent_memory/fram_emulator.hpp"
#include "core/persistent_memory/persistent_memory.hpp"

std::unique_ptr<std::vector<std::pair<uint32_t, uint32_t>>> FramEmulator::GetContent() {
  std::unique_ptr<std::vector<std::pair<uint32_t, uint32_t>>> ptr =
    std::make_unique<std::vector<std::pair<uint32_t, uint32_t>>>();

  ptr->push_back({PersistentMemory::kPcbType0, 0x4E55434C});  // PCB type [0] = NUCL
  ptr->push_back({PersistentMemory::kPcbType1, 0x454F2D48});  // PCB type [1] = EO-H
  ptr->push_back({PersistentMemory::kPcbType2, 0x3734335A});  // PCB type [2] = 743Z
  ptr->push_back({PersistentMemory::kPcbType3, 0x49000000});  // PCB type [3] = I
  ptr->push_back({PersistentMemory::kPcbVersion, 0});  // PCB version = 0.0.0
  ptr->push_back({PersistentMemory::kPcbId, 0});  // Serial number = 0
  ptr->push_back({PersistentMemory::kBoardName, 0x4E550000});  // Board name = NU
  ptr->push_back({PersistentMemory::kIpAddress, 0xC0A8022A});  // IP address = 192.168.2.42
  ptr->push_back({PersistentMemory::kNetmask, 0xFFFFFF00});  // Netmask = 255.255.255.0
  ptr->push_back({PersistentMemory::kGatewayAddress, 0xC0A80201});  // Gateway = 192.168.2.1
  ptr->push_back({PersistentMemory::kUseMotorPlugin, false});  // Activate Motor plugin
  ptr->push_back({PersistentMemory::kUseImuPlugin, false});  // Activate IMU plugin
  ptr->push_back({PersistentMemory::kUseGnssPlugin, false});  // Activate GNSS plugin
  ptr->push_back({PersistentMemory::kUseRcReceiverPlugin, false});  // Activate RC plugin
  ptr->push_back({PersistentMemory::kUseInePlugin, false});  // Activate INE plugin
  return ptr;
}
