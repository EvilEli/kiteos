// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#include "name_provider.hpp"
#include "core/persistent_memory/persistent_memory.hpp"


/**************************************************************************************************
 *     Private static member variables                                                            *
 **************************************************************************************************/
std::string *NameProvider::board_identifier_;
uint8_t NameProvider::board_identifier_binary_[23] = {0};
std::string *NameProvider::kite_name_;
std::string *NameProvider::board_name_;
std::string *NameProvider::full_board_namespace_;

/**************************************************************************************************
 *     Public methods                                                                             *
 **************************************************************************************************/

void NameProvider::Init() {
  NameProvider::board_identifier_ = new std::string();
  NameProvider::kite_name_ = new std::string();
  NameProvider::board_name_ = new std::string();
  NameProvider::full_board_namespace_ = new std::string();

  std::string pcb_type;
  PersistentMemory::GetStringValue(PersistentMemory::kPcbType0, pcb_type, 16);
  uint8_t pcb_version[3] = {0};
  bool pcb_version_available = PersistentMemory::GetValues(PersistentMemory::kPcbVersion,
                                                           pcb_version, 3);
  uint32_t pcb_id = 0;
  bool pcb_id_available = PersistentMemory::GetValue(PersistentMemory::kPcbId, &pcb_id);

  if (pcb_type.length() && pcb_version_available && pcb_id_available) {
    for (uint32_t i = 0; i < pcb_type.length(); i++) {
      board_identifier_binary_[i] = pcb_type[i];
    }
    for (uint8_t i = 0; i < 3; i++) {
      board_identifier_binary_[16 + i] = pcb_version[i];
    }
    for (uint8_t i = 0; i < 4; i++) {
      board_identifier_binary_[19 + i] = pcb_id >> (24 - (8 * i));
    }

    *board_identifier_ = pcb_type + "-"
        + std::to_string(pcb_version[0]) + "."
        + std::to_string(pcb_version[1]) + "."
        + std::to_string(pcb_version[2])
        + "#" + std::to_string(pcb_id);
  } else {
    *board_identifier_ = "Unknown_PCB";
  }
#if defined(BOARD_BOOTLOADER)
  // Kite prefix of bootloader is empty for now
  *kite_name_ = "";
  // Board prefix of bootloader is "BL"
  *full_board_namespace_ = "BL";
#else
  std::string kite_name;
  PersistentMemory::GetStringValue(PersistentMemory::kKiteName, kite_name, 4);
  if (kite_name.length() != 0) {
    *kite_name_ = kite_name;
    *full_board_namespace_ = kite_name + "/";
  }
  std::string board_name;
  PersistentMemory::GetStringValue(PersistentMemory::kBoardName, board_name, 4);
  if (board_name.length() != 0) {
    *board_name_ = board_name;
    *full_board_namespace_ += board_name;
  }
#endif
}

const std::string& NameProvider::GetBoardIdentifier() {
  return *board_identifier_;
}

const uint8_t* NameProvider::GetBoardIdentifierBinary() {
  return board_identifier_binary_;
}

const std::string& NameProvider::GetKiteName() {
  return *kite_name_;
}

const std::string& NameProvider::GetBoardName() {
  return *board_name_;
}

const std::string& NameProvider::GetFullBoardNamespace() {
  return *full_board_namespace_;
}
