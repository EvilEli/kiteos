// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.


#ifndef SRC_CORE_SYSTEM_TIME_PTP_MESSAGE_HPP_
#define SRC_CORE_SYSTEM_TIME_PTP_MESSAGE_HPP_


#include <cstdint>

#include "lwip/pbuf.h"

#include "message_data_structures.hpp"


namespace ptp {
//! Implements the Ptp Message class
class Message {
 public:
  /*!
   * \brief   Constructor of the PtpMessage class.
   *
   * \details Creates a PtpMessage object and returns the instance. When using this constructor, a
   *          message is created based on the given parameters. The message buffer is allocated but
   *          not filled with values.
   *
   * \param   message_type         PTP message type of the message to create.
   * \param   sequence_id          Sequence ID of the message to create.
   * \param   logMessageInterval  Log message interval field of the message ot create.
   *
   * \return  Returns constructed PtpMessage-instance.
   */
  Message(message::Type message_type, uint16_t sequence_id, uint64_t clock_id);

  /*!
   * \brief   Constructor of the PtpMessage class.
   *
   * \details Creates a PtpMessage object and returns the instance. When using this constructor, the
   *          message buffer is set to the given message buffer. This message buffer is then parsed
   *          and the message header and message body member variables are filled with the parsed
   *          information.
   *
   * \param   p   Pointer to the message buffer.
   *
   * \return  Returns constructed PtpMessage-instance.
   */
  explicit Message(struct pbuf *p);

  /*!
   * \brief   Constructor of the PtpMessage class.
   */
  ~Message();

  /*!
   * \brief   Fills the message buffer and returns it.
   *
   * \details Fills the message buffer according to the message header and message body member
   *          variables and returns it.
   *
   * \return  Returns a buffer containing the encoded message.
   */
  struct pbuf *GetMessageBuffer();

  message::Header message_header;  //!< PTP message header structure
  //! PTP message body union containing structures for all message types.
  message::MessageBody message_body;

 private:
  struct pbuf *message_buffer_ = nullptr;  //!< Buffer where the encoded message is stored.

  void SetMessageLength();  //!< Sets the message length according to the message type.
  void SetControlField();   //!< Sets the control filed according to the message type.
};
}  // namespace ptp

#endif  // SRC_CORE_SYSTEM_TIME_PTP_MESSAGE_HPP_
