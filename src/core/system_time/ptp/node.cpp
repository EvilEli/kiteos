// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.

#include "node.hpp"
#include "lwip/udp.h"
#include "lwip/igmp.h"
#include "message.hpp"
#include "hal/ethernet.hpp"
#include "core/persistent_memory/persistent_memory.hpp"


/**************************************************************************************************
 *     Defines                                                                                    *
 **************************************************************************************************/

// UDP ports for PTP event messages and general PTP messages
#define PTP_EVENT_MSG_PORT 319
#define PTP_GENERAL_MSG_PORT 320

// PTP multicast IP address
#define PTP_DEST_IP_ADDR_1 224
#define PTP_DEST_IP_ADDR_2 0
#define PTP_DEST_IP_ADDR_3 1
#define PTP_DEST_IP_ADDR_4 129

// Interval for generating announce messages in milliseconds
#define PTP_ANNOUNCE_MESSAGE_GENERATION_INTERVAL 2000.0

// Interval for generating sync messages in milliseconds
#define PTP_SYNC_MESSAGE_GENERATION_INTERVAL 1000.0


/**************************************************************************************************
 *     Private static member variables                                                            *
 **************************************************************************************************/

namespace ptp {
struct udp_pcb *Node::event_pcb_;
struct udp_pcb *Node::general_pcb_;
ip4_addr_t Node::group_ip_addr_;

bool Node::master_;
Node::State Node::state_ = kStateIdle;
uint16_t Node::sync_sequence_number_ = 0;
uint16_t Node::announce_sequence_number_ = 0;
SoftwareTimer Node::sync_timer_;
SoftwareTimer Node::announce_timer_;
SoftwareTimer Node::timeout_timer_;
Node::SynchronizationDataSet Node::synchronization_data_set_;
Node::DelayRespQueueElement Node::current_delay_req_;
RingBuffer<Node::DelayRespQueueElement, PTP_DELAY_RESP_QUEUE_SIZE> Node::delay_resp_queue_;
SoftwareTimer Node::synchronized_timeout_timer_;
int64_t Node::last_time_offset_ = 0xFFFFFFFFFFFFFFFF;

/**************************************************************************************************
 *     Public functions                                                                           *
 **************************************************************************************************/

bool Node::Init(bool master) {
  // Create UDP sockets
  if ((event_pcb_ = udp_new()) == nullptr) {
    return false;
  }
  if ((general_pcb_ = udp_new()) == nullptr) {
    return false;
  }

  // Set multicast IP address to send to
  IP4_ADDR(&group_ip_addr_,
           PTP_DEST_IP_ADDR_1, PTP_DEST_IP_ADDR_2, PTP_DEST_IP_ADDR_3, PTP_DEST_IP_ADDR_4);
  udp_set_multicast_netif_addr(event_pcb_, &group_ip_addr_);
  udp_set_multicast_netif_addr(general_pcb_, &group_ip_addr_);

  // Bind sockets to UDP ports
  if (udp_bind(event_pcb_, IP_ADDR_ANY, PTP_EVENT_MSG_PORT) != ERR_OK) {
    return false;
  }
  if (udp_bind(general_pcb_, IP_ADDR_ANY, PTP_GENERAL_MSG_PORT) != ERR_OK) {
    return false;
  }

  // Subscribe to multicast ip and register RX callbacks
  ASSERT_RESPONSE(igmp_joingroup(IP_ADDR_ANY, reinterpret_cast<ip_addr_t *>(&group_ip_addr_)), 0);
  udp_recv(event_pcb_, EventMessageReceivedCallback, nullptr);
  udp_recv(general_pcb_, GeneralMessageReceivedCallback, nullptr);

  // Register timestamp callbacks
  hal::Ethernet::RegisterRxTimestampCallback(RxTimestampCallback);
  hal::Ethernet::RegisterTxTimestampCallback(TxTimestampCallback);

  // Configure port for ptp packet filtering in ethernet hal
  hal::Ethernet::SetPtpEventMessagePort(PTP_EVENT_MSG_PORT);

  // store the master flag in a member variable
  master_ = master;

  return true;
}

void Node::Run() {
  switch (state_) {
    // Component is between two synchronization cycles or master is waiting for delay req messages
    case kStateIdle:
      if (master_) {
        if (sync_timer_.Elapsed()) {
          sync_timer_.SetTimeout(PTP_SYNC_MESSAGE_GENERATION_INTERVAL);
          SendSyncMessage();
          break;
        }
        if (delay_resp_queue_.ElementsAvailable()) {
          SendDelayRespMessage();
          break;
        }
        if (announce_timer_.Elapsed()) {
          announce_timer_.SetTimeout(PTP_ANNOUNCE_MESSAGE_GENERATION_INTERVAL);
          SendAnnounceMessage();
          break;
        }
      }
      break;

      // Sending sync message in progress, waiting for TX timestamp to be captured
    case kStateSendingSync:
      ASSERT(master_);  // This state should oly be reachable in master mode
      CheckTimeout();
      break;

      // Sync message was sent and TX timestamp was captured
    case kStateSendingSyncFinished:
      SendFollowUpMessage();
      ASSERT(master_);  // This state should oly be reachable in master mode
      break;

      // A sync message RX timestamp was captured, but the sync message was not processed yet
    case kStateSyncTimestampReceived:
      ASSERT(!master_);  // This state should oly be reachable in slave mode
      CheckTimeout();
      break;

      // A sync message was received, but the corresponding follow up message was not received yet
    case kStateSyncReceived:
      ASSERT(!master_);  // This state should oly be reachable in slave mode
      CheckTimeout();
      break;

      // A follow up message was received
    case kStateFollowUpReceived:
      ASSERT(!master_);  // This state should oly be reachable in slave mode
      SendDelayReqMessage();
      break;

      // Sending delay req message in progress, waiting for TX timestamp to be captured
    case kStateSendingDelayReq:
      ASSERT(!master_);  // This state should oly be reachable in slave mode
      CheckTimeout();
      break;

      // A delay req message was sent, waiting for delay resp message
    case kStateAwaitingDelayResp:
      ASSERT(!master_);  // This state should oly be reachable in slave mode
      CheckTimeout();
      break;

      // A delay resp message was received
    case kStateDelayRespReceived:
      ASSERT(!master_);  // This state should oly be reachable in slave mode
      UpdateSystemTime();
      state_ = kStateIdle;
      break;
  }
}

bool Node::IsSynchronized() {
  return !synchronized_timeout_timer_.Elapsed();
}

int64_t Node::GetLastTimeOffset() {
  return last_time_offset_;
}

/**************************************************************************************************
 *     Private functions                                                                          *
 **************************************************************************************************/

void Node::SendSyncMessage() {
  // Ensure that this message is only called in master mode
  ASSERT(master_);

  // Create default sync message
  Message sync_message = Message(message::kTypeSync, sync_sequence_number_++, PTP_CLOCK_ID);

  // Set sync message timestamp
  uint32_t current_seconds, current_nanoseconds;
  hal::Ethernet::Ieee1588GetSystemTime(&current_seconds, &current_nanoseconds);
  sync_message.message_body.sync_message_body.origin_timestamp.seconds_field =
      (uint64_t) current_seconds;
  sync_message.message_body.sync_message_body.origin_timestamp.nanoseconds_field =
      current_nanoseconds;

  // Set transmit timeout
  timeout_timer_.SetTimeout(PTP_SYNC_MESSAGE_GENERATION_INTERVAL / 10.0);

  // Update PTP state and send message
  state_ = kStateSendingSync;
  udp_sendto(event_pcb_, sync_message.GetMessageBuffer(), &group_ip_addr_, PTP_EVENT_MSG_PORT);
}

void Node::SendFollowUpMessage() {
  // Ensure that this message is only called in master mode
  ASSERT(master_);

  // Create default follow up message
  Message follow_up_message = Message(message::kTypeFollowUp, synchronization_data_set_.sequence_id,
                                      PTP_CLOCK_ID);

  // Set follow up message timestamp
  follow_up_message.message_body.sync_message_body.origin_timestamp =
      synchronization_data_set_.sync_transmit_timestamp;

  // Update PTP state and send follow up message
  state_ = kStateIdle;
  udp_sendto(general_pcb_, follow_up_message.GetMessageBuffer(), &group_ip_addr_,
             PTP_GENERAL_MSG_PORT);
}

void Node::SendDelayReqMessage() {
  // Ensure that this message is only called in slave mode
  ASSERT(!master_);

  // Create default delay req message
  Message delay_req_message = Message(message::kTypeDelayReq, synchronization_data_set_.sequence_id,
                                      PTP_CLOCK_ID);

  // Set delay req message timestamp
  uint32_t current_seconds, current_nanoseconds;
  hal::Ethernet::Ieee1588GetSystemTime(&current_seconds, &current_nanoseconds);
  delay_req_message.message_body.delay_req_message_body.origin_timestamp.seconds_field =
      (uint64_t) current_seconds;
  delay_req_message.message_body.delay_req_message_body.origin_timestamp.nanoseconds_field =
      current_nanoseconds;

  // Set transmit timeout
  timeout_timer_.SetTimeout(PTP_SYNC_MESSAGE_GENERATION_INTERVAL / 10.0);

  // Update PTP state and send message
  state_ = kStateSendingDelayReq;
  udp_sendto(event_pcb_, delay_req_message.GetMessageBuffer(), &group_ip_addr_, PTP_EVENT_MSG_PORT);
}

void Node::SendDelayRespMessage() {
  // Ensure that this message is only called in master mode
  ASSERT(master_);

  // Get first element from delay resp queue
  DelayRespQueueElement delay_resp_data;
  if (!delay_resp_queue_.Read(&delay_resp_data, 1)) {
    return;  // Queue empty
  }

  // Create default delay resp message
  Message delay_resp_message = Message(message::kTypeDelayResp, delay_resp_data.sequence_id,
                                       PTP_CLOCK_ID);

  // Set up delay resp message fields
  delay_resp_message.message_body.delay_resp_message_body.receive_timestamp =
      delay_resp_data.delay_req_receive_timestamp;
  delay_resp_message.message_body.delay_resp_message_body.requesting_port_identity =
      delay_resp_data.requesting_port_identity;
  delay_resp_message.message_header.correction_field = delay_resp_data.delay_req_correction;

  // Update PTP state and send message
  state_ = kStateIdle;
  udp_sendto(general_pcb_, delay_resp_message.GetMessageBuffer(), &group_ip_addr_,
             PTP_GENERAL_MSG_PORT);
}

void Node::SendAnnounceMessage() {
  // Ensure that this message is only called in master mode
  ASSERT(master_);

  // Create default announce resp message
  Message announce_message = Message(message::kTypeAnnounce, announce_sequence_number_++,
                                     PTP_CLOCK_ID);

  // Set announce message timestamp
  uint32_t current_seconds, current_nanoseconds;
  hal::Ethernet::Ieee1588GetSystemTime(&current_seconds, &current_nanoseconds);
  announce_message.message_body.announce_message_body.origin_timestamp.seconds_field =
      (uint64_t) current_seconds;
  announce_message.message_body.announce_message_body.origin_timestamp.nanoseconds_field =
      current_nanoseconds;

  // Set other announce message fields
  // TODO(jan): replace current_utc_offset_ by real value
  announce_message.message_body.announce_message_body.current_utc_offset = 0;
  // Highest priority
  announce_message.message_body.announce_message_body.grandmaster_priority_1 = 0;
  // Default clock class
  // TODO(jan): Assign meaningful value
  announce_message.message_body.announce_message_body.grandmaster_clock_quality.clock_class = 248;
  announce_message.message_body.announce_message_body.grandmaster_clock_quality.clock_accuracy =
      message::kClockAccuracyUnknown;  // TODO(jan): replace clock_accuracy_ by real value
  // TODO(jan): replace offset_scaled_log_variance_ by real value
  announce_message.message_body.announce_message_body.grandmaster_clock_quality
      .offset_scaled_log_variance = 0;
  // Highest priority
  announce_message.message_body.announce_message_body.grandmaster_priority_2 = 0;
  announce_message.message_body.announce_message_body.grandmaster_identity =
      announce_message.message_header.source_port_identity.clock_identity;
  // TODO(jan): replace steps_removed_ by real value
  announce_message.message_body.announce_message_body.steps_removed = 0;
  announce_message.message_body.announce_message_body.time_source = message::kTimeSourceGps;

  // Send message
  udp_sendto(general_pcb_, announce_message.GetMessageBuffer(), &group_ip_addr_,
             PTP_GENERAL_MSG_PORT);
}

void Node::CheckTimeout() {
  if (timeout_timer_.Elapsed()) {
    // Go to PTP idle state to abort the synchronization cycle
    state_ = kStateIdle;
  }
}

bool Node::UpdateSystemTime() {
  // Ensure that this message is only called in slave mode
  ASSERT(!master_);

  // Convert captured timestamps to single nanoseconds values
  int64_t t1 = synchronization_data_set_.sync_transmit_timestamp.seconds_field * 1000000000 +
      synchronization_data_set_.sync_transmit_timestamp.nanoseconds_field;
  int64_t t2 = synchronization_data_set_.sync_receive_timestamp.seconds_field * 1000000000 +
      synchronization_data_set_.sync_receive_timestamp.nanoseconds_field;
  int64_t t3 = synchronization_data_set_.delay_req_transmit_timestamp.seconds_field *
      1000000000 + synchronization_data_set_.delay_req_transmit_timestamp.nanoseconds_field;
  int64_t t4 = synchronization_data_set_.delay_req_receive_timestamp.seconds_field *
      1000000000 + synchronization_data_set_.delay_req_receive_timestamp.nanoseconds_field;

  // Calculate time offset to master
  int64_t sync_transmission_time = t2 - t1 - synchronization_data_set_.sync_correction;
  int64_t delay_req_transmission_time = t4 - t3 - synchronization_data_set_.delay_req_correction;
  int64_t average_transmission_time = (sync_transmission_time + delay_req_transmission_time) / 2;
  int64_t t2_master = t1 + average_transmission_time + synchronization_data_set_.sync_correction;
  int64_t time_offset = t2_master - t2;
  last_time_offset_ = time_offset;

  // Correct system time depending on current and previous system time offset
  if (abs(time_offset) > (PTP_SYNC_MESSAGE_GENERATION_INTERVAL * 1000000) ||
      abs(synchronization_data_set_.previous_master_clock_time -
          synchronization_data_set_.previous_slave_clock_time) >
          (PTP_SYNC_MESSAGE_GENERATION_INTERVAL * 1000000)) {
    // Correct system time using coarse correction method
    bool subtract = false;
    if (time_offset < 0) {
      subtract = true;
      time_offset *= -1;
    }
    uint32_t seconds = time_offset / 1000000000;
    uint32_t nanoseconds = time_offset % 1000000000;
    if (!hal::Ethernet::Ieee1588CoarseSystemTimeCorrection(seconds, nanoseconds, subtract)) {
      // Timestamp correction failed
      return false;
    }
  } else {
    // Correct system time using the fine timestamp correction method
    int64_t master_clock_count_n = t2_master - synchronization_data_set_.previous_master_clock_time;
    int64_t slave_clock_count_n = t2 - synchronization_data_set_.previous_slave_clock_time;
    int64_t clock_diff_count = t2_master - t2;
    double freq_scale_factor_n = static_cast<double>(master_clock_count_n + clock_diff_count) /
        static_cast<double>(slave_clock_count_n);
    if (freq_scale_factor_n <= 0.0) {
      return false;
    }
    if (!hal::Ethernet::Ieee1588RescaleAddend(freq_scale_factor_n)) {
      // Timestamp correction failed
      return false;
    }
  }

  // Store current clock times for next update cycle
  synchronization_data_set_.previous_master_clock_time = t2_master;
  synchronization_data_set_.previous_slave_clock_time = t2;

  // Reset synchronized timeout timer
  synchronized_timeout_timer_.SetTimeout(PTP_SYNC_MESSAGE_GENERATION_INTERVAL * 2.5);

  // Timestamp correction successful
  return true;
}

/**************************************************************************************************
 *     Callback functions                                                                         *
 **************************************************************************************************/

void Node::EventMessageReceivedCallback(
    void *arg, struct udp_pcb *pcb, struct pbuf *p, const ip_addr_t *addr, u16_t port) {
  // Create PTP message object from message buffer
  Message received_message = Message(p);

  switch (received_message.message_header.message_type) {
    case message::kTypeSync:
      if (!master_) {
        // Check if the sequence ID and source port identity of the received message matches the
        // current synchronization cycle and master
        if (state_ == kStateSyncTimestampReceived &&
            received_message.message_header.sequence_id == synchronization_data_set_.sequence_id &&
            received_message.message_header.source_port_identity ==
                synchronization_data_set_.master_port_identity) {
          // Store correction field content in sync correction
          synchronization_data_set_.sync_correction =
              received_message.message_header.correction_field / 0x10000u;

          // Set timeout for follow up message reception and update state
          timeout_timer_.SetTimeout(PTP_SYNC_MESSAGE_GENERATION_INTERVAL / 5.0);
          state_ = kStateSyncReceived;
        }
      }
      break;

    case message::kTypeDelayReq:
      if (master_) {
        // Check if the sequence ID and source port identity of the received message matches the
        // timestamp captured before
        if (state_ == kStateIdle &&
            received_message.message_header.sequence_id == current_delay_req_.sequence_id &&
            received_message.message_header.source_port_identity ==
                current_delay_req_.requesting_port_identity) {
          // Store message RX timestamp sequence ID port identity and correction field as new delay
          // resp queue element
          DelayRespQueueElement new_delay_resp;
          new_delay_resp.sequence_id = current_delay_req_.sequence_id;
          new_delay_resp.requesting_port_identity = current_delay_req_.requesting_port_identity;
          new_delay_resp.delay_req_receive_timestamp =
              current_delay_req_.delay_req_receive_timestamp;
          new_delay_resp.delay_req_correction = received_message.message_header.correction_field;
          delay_resp_queue_.WriteElement(new_delay_resp);
        }
      }
      break;

    case message::kTypePdelayReq:
      break;

    case message::kTypePdelayResp:
      break;

    default:
      // This case should actually not happen and means that someone in the network sends invalid
      // PTP packages
      break;
  }
}

void Node::GeneralMessageReceivedCallback(
    void *arg, struct udp_pcb *pcb, struct pbuf *p, const ip_addr_t *addr, u16_t port) {
  // Create PTP message object from message buffer
  Message received_message = Message(p);

  switch (received_message.message_header.message_type) {
    case message::kTypeAnnounce:
      break;

    case message::kTypeFollowUp:
      if (!master_) {
        // Check if the sequence ID of the received message matches the current synchronization
        // cycle and master
        if (state_ == kStateSyncReceived &&
            received_message.message_header.sequence_id == synchronization_data_set_.sequence_id &&
            received_message.message_header.source_port_identity ==
                synchronization_data_set_.master_port_identity) {
          // Store received sync message transmit timestamp
          synchronization_data_set_.sync_transmit_timestamp =
              received_message.message_body.follow_up_message_body.precise_origin_timestamp;

          // Add correction field content to sync correction
          synchronization_data_set_.sync_correction +=
              received_message.message_header.correction_field / 0x10000u;

          // Update state to notify the main thread that a follow up message was received
          state_ = kStateFollowUpReceived;
        }
      }
      break;

    case message::kTypeDelayResp:
      if (!master_) {
        // Check if the sequence ID of the received message matches the current synchronization
        // cycle and master. Also check if the delay response is dedicated for us or another slave.
        if (state_ == kStateAwaitingDelayResp &&
            received_message.message_header.sequence_id == synchronization_data_set_.sequence_id &&
            received_message.message_header.source_port_identity ==
                synchronization_data_set_.master_port_identity &&
            (received_message.message_body.delay_resp_message_body.requesting_port_identity
                .clock_identity & 0x0000FFFFFFFFFFFFu) == PTP_CLOCK_ID) {
          // Store received delay req message receive timestamp
          synchronization_data_set_.delay_req_receive_timestamp =
              received_message.message_body.delay_resp_message_body.receive_timestamp;

          // Store correction field content in delay req correction
          synchronization_data_set_.delay_req_correction =
              received_message.message_header.correction_field / 0x10000u;

          // Update state to notify the main thread that a delay resp message was received
          state_ = kStateDelayRespReceived;
        }
      }
      break;

    case message::kTypePdelayRespFollowUp:
      break;

    case message::kTypeManagement:
      break;

    case message::kTypeSignaling:
      break;

    default:
      // This case should actually not happen and means that someone in the network sends invalid
      // PTP packages
      break;
  }
}

void Node::TxTimestampCallback(uint32_t timestamp_seconds, uint32_t timestamp_nanoseconds,
                               uint8_t message_type, uint16_t sequence_id) {
  if (master_) {
    // If in sending sync state, the sync message Tx timestamp is awaited,
    // double check if the captured timestamp is really a sync message Tx timestamp
    if (state_ == kStateSendingSync && message_type == message::kTypeSync) {
      // Store captured sync message Tx timestamp and sequence ID
      synchronization_data_set_.sequence_id = sequence_id;
      synchronization_data_set_.sync_transmit_timestamp.seconds_field =
          (uint64_t) timestamp_seconds;
      synchronization_data_set_.sync_transmit_timestamp.nanoseconds_field = timestamp_nanoseconds;

      // Update PTP state to notify the main thread that a timestamp was captured
      state_ = kStateSendingSyncFinished;
    }
  } else {  // slave
    // If in sending delay req state, the delay req message Tx timestamp is awaited. Double check
    // if the captured timestamp is really a delay req message Tx timestamp. Also double check if
    // the sequence ID matches the sequence ID of the current synchronization data set.
    if (state_ == kStateSendingDelayReq && message_type == message::kTypeDelayReq &&
        sequence_id == synchronization_data_set_.sequence_id) {
      // Store captured delay req message TX timestamp
      synchronization_data_set_.delay_req_transmit_timestamp.seconds_field =
          (uint64_t) timestamp_seconds;
      synchronization_data_set_.delay_req_transmit_timestamp.nanoseconds_field =
          timestamp_nanoseconds;

      // Set timeout for delay response message and update state
      timeout_timer_.SetTimeout(PTP_SYNC_MESSAGE_GENERATION_INTERVAL / 2.0);
      state_ = kStateAwaitingDelayResp;
    }
  }
}

void Node::RxTimestampCallback(
    uint32_t timestamp_seconds, uint32_t timestamp_nanoseconds, uint8_t message_type,
    uint16_t sequence_id, uint64_t clock_identity, uint16_t port_number) {
  switch (message_type) {
    case message::kTypeSync:
      if (!master_) {
        if (state_ == kStateIdle) {
          // Store captured sync message RX timestamp and sequence ID and port identity
          synchronization_data_set_.sequence_id = sequence_id;
          synchronization_data_set_.sync_receive_timestamp.seconds_field =
              (uint64_t) timestamp_seconds;
          synchronization_data_set_.sync_receive_timestamp.nanoseconds_field =
              timestamp_nanoseconds;
          synchronization_data_set_.master_port_identity.clock_identity = clock_identity;
          synchronization_data_set_.master_port_identity.port_number = port_number;

          // Set timeout for sync message reception and update state
          timeout_timer_.SetTimeout(PTP_SYNC_MESSAGE_GENERATION_INTERVAL / 10.0);
          state_ = kStateSyncTimestampReceived;
        }
      }
      break;

    case message::kTypeDelayReq:
      if (master_) {
        if (state_ == kStateIdle) {
          // Store captured delay req message RX timestamp sequence ID and port identity in current
          // delay resp queue element
          current_delay_req_.sequence_id = sequence_id;
          current_delay_req_.requesting_port_identity.clock_identity = clock_identity;
          current_delay_req_.requesting_port_identity.port_number = port_number;
          current_delay_req_.delay_req_receive_timestamp.seconds_field = timestamp_seconds;
          current_delay_req_.delay_req_receive_timestamp.nanoseconds_field = timestamp_nanoseconds;
        }
      }
      break;

    case message::kTypePdelayReq:
      break;

    case message::kTypePdelayResp:
      break;

    default:
      // This case should actually not happen and means that someone in the network sends invalid
      // PTP packages
      break;
  }
}
}  // namespace ptp
