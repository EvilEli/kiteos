// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.

#include "message_data_structures.hpp"


/**************************************************************************************************
 *     PTP message data field structs                                                             *
 **************************************************************************************************/

namespace ptp::message {
void FlagField::ToBuffer(uint8_t *buffer) {
  buffer[0] = 0;
  buffer[0] |= alternate_master_flag ? 1u : 0;
  buffer[0] |= two_step_flag ? (1u << 1u) : 0;
  buffer[0] |= unicast_flag ? (1u << 2u) : 0;
  buffer[0] |= ptp_profile_specific_1 ? (1u << 5u) : 0;
  buffer[0] |= ptp_profile_specific_2 ? (1u << 6u) : 0;
  buffer[1] = 0;
  buffer[1] |= leap_61 ? 1u : 0;
  buffer[1] |= leap_59 ? (1u << 1u) : 0;
  buffer[1] |= current_utc_offset_valid ? (1u << 2u) : 0;
  buffer[1] |= ptp_timescale ? (1u << 3u) : 0;
  buffer[1] |= time_traceable ? (1u << 4u) : 0;
  buffer[1] |= frequency_traceable ? (1u << 5u) : 0;
}

void FlagField::FromBuffer(const uint8_t *buffer) {
  alternate_master_flag = buffer[0] | 1u;
  two_step_flag = buffer[0] | (1u << 1u);
  unicast_flag = buffer[0] | (1u << 2u);
  ptp_profile_specific_1 = buffer[0] | (1u << 5u);
  ptp_profile_specific_2 = buffer[0] | (1u << 6u);
  leap_61 = buffer[1] | 1u;
  leap_59 = buffer[1] | (1u << 1u);
  current_utc_offset_valid = buffer[1] | (1u << 2u);
  ptp_timescale = buffer[1] | (1u << 3u);
  time_traceable = buffer[1] | (1u << 4u);
  frequency_traceable = buffer[1] | (1u << 5u);
}

void PortIdentity::ToBuffer(uint8_t *buffer) {
  buffer[0] = (0xFF00000000000000ul & clock_identity) >> 56u;
  buffer[1] = (0x00FF000000000000ul & clock_identity) >> 48u;
  buffer[2] = (0x0000FF0000000000ul & clock_identity) >> 40u;
  buffer[3] = (0x000000FF00000000ul & clock_identity) >> 32u;
  buffer[4] = (0x00000000FF000000ul & clock_identity) >> 24u;
  buffer[5] = (0x0000000000FF0000ul & clock_identity) >> 16u;
  buffer[6] = (0x000000000000FF00ul & clock_identity) >> 8u;
  buffer[7] = 0x00000000000000FFul & clock_identity;
  buffer[8] = (0xFF00u & port_number) >> 8u;
  buffer[9] = 0x00FFu & port_number;
}

void PortIdentity::FromBuffer(const uint8_t *buffer) {
  clock_identity = (((uint64_t) buffer[0]) << 56u) | (((uint64_t) buffer[1]) << 48u) |
      (((uint64_t) buffer[2]) << 40u) | (((uint64_t) buffer[3]) << 32u) |
      (((uint64_t) buffer[4]) << 24u) | (((uint64_t) buffer[5]) << 16u) |
      (((uint64_t) buffer[6]) << 8u) | ((uint64_t) buffer[7]);
  port_number = (((uint64_t) buffer[8]) << 8u) | ((uint64_t) buffer[9]);
}

bool PortIdentity::operator==(const PortIdentity &other) {
  return (this->clock_identity == other.clock_identity) && (this->port_number == other.port_number);
}

void Timestamp::ToBuffer(uint8_t *buffer) {
  buffer[0] = (0xFF0000000000ul & seconds_field) >> 40u;
  buffer[1] = (0x00FF00000000ul & seconds_field) >> 32u;
  buffer[2] = (0x0000FF000000ul & seconds_field) >> 24u;
  buffer[3] = (0x000000FF0000ul & seconds_field) >> 16u;
  buffer[4] = (0x00000000FF00ul & seconds_field) >> 8u;
  buffer[5] = 0x0000000000FFul & seconds_field;
  buffer[6] = (0xFF000000u & nanoseconds_field) >> 24u;
  buffer[7] = (0x00FF0000u & nanoseconds_field) >> 16u;
  buffer[8] = (0x0000FF00u & nanoseconds_field) >> 8u;
  buffer[9] = 0x000000FFu & nanoseconds_field;
}

void Timestamp::FromBuffer(const uint8_t *buffer) {
  seconds_field = (((uint64_t) buffer[0]) << 40u) | (((uint64_t) buffer[1]) << 32u) |
      (((uint64_t) buffer[2]) << 24u) | (((uint64_t) buffer[3]) << 16u) |
      (((uint64_t) buffer[4]) << 8u) | ((uint64_t) buffer[5]);
  nanoseconds_field = (((uint32_t) buffer[6]) << 24u) | (((uint32_t) buffer[7]) << 16u) |
      (((uint32_t) buffer[8]) << 8u) | ((uint32_t) buffer[9]);
}

void ClockQuality::ToBuffer(uint8_t *buffer) {
  buffer[0] = clock_class;
  buffer[1] = clock_accuracy;
  buffer[2] = (0xFF00u & offset_scaled_log_variance) >> 8u;
  buffer[3] = 0x00FFu & offset_scaled_log_variance;
}

void ClockQuality::FromBuffer(const uint8_t *buffer) {
  clock_class = buffer[0];
  clock_accuracy = (ClockAccuracy) buffer[1];
  offset_scaled_log_variance = (((uint16_t) buffer[2]) << 8u) | ((uint16_t) buffer[3]);
}

/**************************************************************************************************
 *     PTP message header struct                                                                  *
 **************************************************************************************************/

void Header::ToBuffer(uint8_t *buffer) {
  buffer[0] = (transport_specific << 4u) | (0x0Fu & (message_type));
  buffer[1] = 0x0Fu & ptp_version;
  buffer[2] = (0xFF00u & message_length) >> 8u;
  buffer[3] = 0x00FFu & message_length;
  buffer[4] = domain_number;
  buffer[5] = 0;  // reserved
  flag_field.ToBuffer(&(buffer[6]));
  buffer[8] = (0xFF00000000000000ul & static_cast<uint64_t >(correction_field)) >> 56u;
  buffer[9] = (0x00FF000000000000ul & static_cast<uint64_t >(correction_field)) >> 48u;
  buffer[10] = (0x0000FF0000000000ul & static_cast<uint64_t >(correction_field)) >> 40u;
  buffer[11] = (0x000000FF00000000ul & static_cast<uint64_t >(correction_field)) >> 32u;
  buffer[12] = (0x00000000FF000000ul & static_cast<uint64_t >(correction_field)) >> 24u;
  buffer[13] = (0x0000000000FF0000ul & static_cast<uint64_t >(correction_field)) >> 16u;
  buffer[14] = (0x000000000000FF00ul & static_cast<uint64_t >(correction_field)) >> 8u;
  buffer[15] = 0x00000000000000FFul & static_cast<uint64_t >(correction_field);
  buffer[16] = 0;  // reserved
  buffer[17] = 0;  // reserved
  buffer[18] = 0;  // reserved
  buffer[19] = 0;  // reserved
  buffer[20] = (0xFF00000000000000ul & source_port_identity.clock_identity) >> 56u;
  buffer[21] = (0x00FF000000000000ul & source_port_identity.clock_identity) >> 48u;
  buffer[22] = (0x0000FF0000000000ul & source_port_identity.clock_identity) >> 40u;
  buffer[23] = (0x000000FF00000000ul & source_port_identity.clock_identity) >> 32u;
  buffer[24] = (0x00000000FF000000ul & source_port_identity.clock_identity) >> 24u;
  buffer[25] = (0x0000000000FF0000ul & source_port_identity.clock_identity) >> 16u;
  buffer[26] = (0x000000000000FF00ul & source_port_identity.clock_identity) >> 8u;
  buffer[27] = 0x00000000000000FFul & source_port_identity.clock_identity;
  buffer[28] = (0xFF00u & source_port_identity.port_number) >> 8u;
  buffer[29] = 0x00FFu & source_port_identity.port_number;
  buffer[30] = (0xFF00u & sequence_id) >> 8u;
  buffer[31] = 0x00FFu & sequence_id;
  buffer[32] = control_field;
  buffer[33] = log_message_interval;
}

void Header::FromBuffer(uint8_t *buffer) {
  transport_specific = (buffer[0] & 0xF0u) >> 4u;
  message_type = (Type)(buffer[0] & 0x0Fu);
  ptp_version = buffer[1] & 0x0Fu;
  message_length = (((uint16_t) buffer[2]) << 8u) | ((uint16_t) buffer[3]);
  domain_number = buffer[4];
  flag_field.FromBuffer(&(buffer[6]));
  correction_field = (((int64_t) buffer[8]) << 56u) | (((int64_t) buffer[9]) << 48u) |
      (((int64_t) buffer[10]) << 40u) | (((int64_t) buffer[11]) << 32u) |
      (((int64_t) buffer[12]) << 24u) | (((int64_t) buffer[13]) << 16u) |
      (((int64_t) buffer[14]) << 8u) | ((int64_t) buffer[15]);
  source_port_identity.clock_identity =
      (((uint64_t) buffer[20]) << 56u) | (((uint64_t) buffer[21]) << 48u) |
          (((uint64_t) buffer[22]) << 40u) | (((uint64_t) buffer[23]) << 32u) |
          (((uint64_t) buffer[24]) << 24u) | (((uint64_t) buffer[25]) << 16u) |
          (((uint64_t) buffer[26]) << 8u) | ((uint64_t) buffer[27]);
  source_port_identity.port_number = (((int64_t) buffer[28]) << 8u) | ((int64_t) buffer[29]);
  sequence_id = (((uint16_t) buffer[30]) << 8u) | ((uint16_t) buffer[31]);
  control_field = (ControlField) buffer[32];
  log_message_interval = buffer[33];
}

/**************************************************************************************************
 *     PTP message body structs                                                                   *
 **************************************************************************************************/

void SyncMessageBody::ToBuffer(uint8_t *buffer) {
  origin_timestamp.ToBuffer(buffer);
}

void SyncMessageBody::FromBuffer(uint8_t *buffer) {
  origin_timestamp.FromBuffer(buffer);
}

void DelayReqMessageBody::ToBuffer(uint8_t *buffer) {
  origin_timestamp.ToBuffer(buffer);
}

void DelayReqMessageBody::FromBuffer(uint8_t *buffer) {
  origin_timestamp.FromBuffer(buffer);
}

void PdelayReqMessageBody::ToBuffer(uint8_t *buffer) {
  origin_timestamp.ToBuffer(buffer);
  for (int i = 0; i < 10; i++) {
    buffer[i + 10] = 0;  // reserved
  }
}

void PdelayReqMessageBody::FromBuffer(uint8_t *buffer) {
  origin_timestamp.FromBuffer(buffer);
}

void PdelayRespMessageBody::ToBuffer(uint8_t *buffer) {
  request_receipt_timestamp.ToBuffer(buffer);
  requesting_port_identity.ToBuffer(&(buffer[10]));
}

void PdelayRespMessageBody::FromBuffer(uint8_t *buffer) {
  request_receipt_timestamp.FromBuffer(buffer);
  requesting_port_identity.FromBuffer(&(buffer[10]));
}

void FollowUpMessageBody::ToBuffer(uint8_t *buffer) {
  precise_origin_timestamp.ToBuffer(buffer);
}

void FollowUpMessageBody::FromBuffer(uint8_t *buffer) {
  precise_origin_timestamp.FromBuffer(buffer);
}

void DelayRespMessageBody::ToBuffer(uint8_t *buffer) {
  receive_timestamp.ToBuffer(buffer);
  requesting_port_identity.ToBuffer(&(buffer[10]));
}

void DelayRespMessageBody::FromBuffer(uint8_t *buffer) {
  receive_timestamp.FromBuffer(buffer);
  requesting_port_identity.FromBuffer(&(buffer[10]));
}

void PdelayRespFollowUpMessageBody::ToBuffer(uint8_t *buffer) {
  response_origin_timestamp.ToBuffer(buffer);
  requesting_port_identity.ToBuffer(&(buffer[10]));
}

void PdelayRespFollowUpMessageBody::FromBuffer(uint8_t *buffer) {
  response_origin_timestamp.FromBuffer(buffer);
  requesting_port_identity.FromBuffer(&(buffer[10]));
}

void AnnounceMessageBody::ToBuffer(uint8_t *buffer) {
  origin_timestamp.ToBuffer(&(buffer[0]));
  buffer[10] = (0xFF00u & current_utc_offset) >> 8u;
  buffer[11] = 0x00FFu & current_utc_offset;
  buffer[12] = 0;  // reserved
  buffer[13] = grandmaster_priority_1;
  grandmaster_clock_quality.ToBuffer(&(buffer[14]));
  buffer[18] = grandmaster_priority_2;
  buffer[19] = (0xFF00000000000000ul & grandmaster_identity) >> 56u;
  buffer[20] = (0x00FF000000000000ul & grandmaster_identity) >> 48u;
  buffer[21] = (0x0000FF0000000000ul & grandmaster_identity) >> 40u;
  buffer[22] = (0x000000FF00000000ul & grandmaster_identity) >> 32u;
  buffer[23] = (0x00000000FF000000ul & grandmaster_identity) >> 24u;
  buffer[24] = (0x0000000000FF0000ul & grandmaster_identity) >> 16u;
  buffer[25] = (0x000000000000FF00ul & grandmaster_identity) >> 8u;
  buffer[26] = 0x00000000000000FFul & grandmaster_identity;
  buffer[27] = (0xFF00u & steps_removed) >> 8u;
  buffer[28] = 0x00FFu & steps_removed;
  buffer[29] = time_source;
}

void AnnounceMessageBody::FromBuffer(uint8_t *buffer) {
  origin_timestamp.FromBuffer(&(buffer[0]));
  current_utc_offset = (((int16_t) buffer[10]) << 8u) | ((int16_t) buffer[11]);
  grandmaster_priority_1 = buffer[13];
  grandmaster_clock_quality.FromBuffer(&(buffer[14]));
  grandmaster_priority_2 = buffer[18];
  grandmaster_identity = (((uint64_t) buffer[19]) << 56u) | (((uint64_t) buffer[20]) << 48u) |
      (((uint64_t) buffer[21]) << 40u) | (((uint64_t) buffer[22]) << 32u) |
      (((uint64_t) buffer[23]) << 24u) | (((uint64_t) buffer[24]) << 16u) |
      (((uint64_t) buffer[25]) << 8u) | ((uint64_t) buffer[26]);
  steps_removed = (((uint16_t) buffer[27]) << 8u) | ((uint16_t) buffer[28]);
  time_source = (TimeSource) buffer[29];
}

void SignalingMessageBody::ToBuffer(uint8_t *buffer) {
  // TODO(jan): implement this
}

void SignalingMessageBody::FromBuffer(uint8_t *buffer) {
  // TODO(jan): implement this
}

void ManagementMessageBody::ToBuffer(uint8_t *buffer) {
  // TODO(jan): implement this
}

void ManagementMessageBody::FromBuffer(uint8_t *buffer) {
  // TODO(jan): implement this
}

/**************************************************************************************************
 *     PTP message body union                                                                     *
 **************************************************************************************************/

void MessageBody::ToBuffer(Type message_type, uint8_t *buffer) {
  switch (message_type) {
    case kTypeSync:
      sync_message_body.ToBuffer(buffer);
      break;

    case kTypeDelayReq:
      delay_req_message_body.ToBuffer(buffer);
      break;

    case kTypePdelayReq:
      pdelay_req_message_body.ToBuffer(buffer);
      break;

    case kTypePdelayResp:
      pdelay_resp_message_body.ToBuffer(buffer);
      break;

    case kTypeFollowUp:
      follow_up_message_body.ToBuffer(buffer);
      break;

    case kTypeDelayResp:
      delay_resp_message_body.ToBuffer(buffer);
      break;

    case kTypePdelayRespFollowUp:
      pdelay_resp_follow_up_message_body.ToBuffer(buffer);
      break;

    case kTypeAnnounce:
      announce_message_body.ToBuffer(buffer);
      break;

    case kTypeSignaling:
      signaling_message_body.ToBuffer(buffer);
      break;

    case kTypeManagement:
      management_message_body.ToBuffer(buffer);
      break;
  }
}

void MessageBody::FromBuffer(Type message_type, uint8_t *buffer) {
  switch (message_type) {
    case kTypeSync:
      sync_message_body.FromBuffer(buffer);
      break;

    case kTypeDelayReq:
      delay_req_message_body.FromBuffer(buffer);
      break;

    case kTypePdelayReq:
      pdelay_req_message_body.FromBuffer(buffer);
      break;

    case kTypePdelayResp:
      pdelay_resp_message_body.FromBuffer(buffer);
      break;

    case kTypeFollowUp:
      follow_up_message_body.FromBuffer(buffer);
      break;

    case kTypeDelayResp:
      delay_resp_message_body.FromBuffer(buffer);
      break;

    case kTypePdelayRespFollowUp:
      pdelay_resp_follow_up_message_body.FromBuffer(buffer);
      break;

    case kTypeAnnounce:
      announce_message_body.FromBuffer(buffer);
      break;

    case kTypeSignaling:
      signaling_message_body.FromBuffer(buffer);
      break;

    case kTypeManagement:
      management_message_body.FromBuffer(buffer);
      break;
  }
}
}  // namespace ptp::message
