// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.


#ifndef SRC_CORE_SYSTEM_TIME_PTP_MESSAGE_DATA_STRUCTURES_HPP_
#define SRC_CORE_SYSTEM_TIME_PTP_MESSAGE_DATA_STRUCTURES_HPP_


#include <cstdint>


/**************************************************************************************************
 *     Enums                                                                                      *
 **************************************************************************************************/

namespace ptp::message {
//! Enum defining the message type field of a PTP message header.
enum Type : uint8_t {
  kTypeSync = 0x0,
  kTypeDelayReq = 0x1,
  kTypePdelayReq = 0x2,
  kTypePdelayResp = 0x3,
  kTypeFollowUp = 0x8,
  kTypeDelayResp = 0x9,
  kTypePdelayRespFollowUp = 0xA,
  kTypeAnnounce = 0xB,
  kTypeSignaling = 0xC,
  kTypeManagement = 0xD
};

//! Enum defining the control field of a PTP message header.
enum ControlField : uint8_t {
  kControlFieldSync = 0x00,
  kControlFieldDelayReq = 0x01,
  kControlFieldFollowUp = 0x02,
  kControlFieldDelayResp = 0x03,
  kControlFieldManagement = 0x04,
  kControlFieldOther = 0x05
};

//! Enum defining the clock accuracy field of the clock quality field of a PTP message.
enum ClockAccuracy : uint8_t {
  kClockAccuracyWithin25Nanoseconds = 0x20,
  kClockAccuracyWithin100Nanoseconds = 0x21,
  kClockAccuracyWithin250Nanoseconds = 0x22,
  kClockAccuracyWithin1Microsecond = 0x23,
  kClockAccuracyWithin2Microseconds5 = 0x24,
  kClockAccuracyWithin10Microseconds = 0x25,
  kClockAccuracyWithin25Microseconds = 0x26,
  kClockAccuracyWithin100Microseconds = 0x27,
  kClockAccuracyWithin250Microseconds = 0x28,
  kClockAccuracyWithin1Millisecond = 0x29,
  kClockAccuracyWithin2Milliseconds5 = 0x2A,
  kClockAccuracyWithin10Milliseconds = 0x2B,
  kClockAccuracyWithin25Milliseconds = 0x2C,
  kClockAccuracyWithin100Milliseconds = 0x2D,
  kClockAccuracyWithin250Milliseconds = 0x2E,
  kClockAccuracyWithin1Second = 0x2F,
  kClockAccuracyWithin10Seconds = 0x30,
  kClockAccuracyOver10Seconds = 0x31,
  kClockAccuracyUnknown = 0xFE
};

//! Enum defining the time source field of a PTP message.
enum TimeSource : uint8_t {
  kTimeSourceAtomicClock = 0x10,
  kTimeSourceGps = 0x20,
  kTimeSourceTerrestrialRadio = 0x30,
  kTimeSourcePtp = 0x40,
  kTimeSourceNtp = 0x50,
  kTimeSourceHandSet = 0x60,
  kTimeSourceOther = 0x90,
  kTimeSourceInternalOscillator = 0xA0
};


/**************************************************************************************************
 *     PTP message data field structs                                                             *
 **************************************************************************************************/


//! Structure defining the flag field of a PTP message header.
struct FlagField {
  bool alternate_master_flag;  //!< FALSE if the port of the originator is in the MASTER state.
  //! For a one-step clock, the value of twoStepFlag shall be FALSE. For a two-step clock, the value
  //! of twoStepFlag shall be TRUE.
  bool two_step_flag;
  //! TRUE, if the transport layer protocol address to which this message was sent is a unicast
  //! address. FALSE, if the transport layer protocol address to which this message was sent is a
  //! multicast address.
  bool unicast_flag;
  bool ptp_profile_specific_1;  //!< As defined by an alternate PTP profile; otherwise FALSE.
  bool ptp_profile_specific_2;  //!< As defined by an alternate PTP profile; otherwise FALSE.
  bool leap_61;  //!< The value of timePropertiesDS.leap61.
  bool leap_59;  //!< The value of timePropertiesDS.leap59.
  bool current_utc_offset_valid;  //!< The value of timePropertiesDS.currentUtcOffsetValid.
  bool ptp_timescale;  //!< The value of timePropertiesDS.ptpTimescale.
  bool time_traceable;  //!< The value of timePropertiesDS.timeTraceable.
  bool frequency_traceable;  //!< The value of timePropertiesDS.frequencyTraceable.

  void ToBuffer(uint8_t *buffer);  //!< Write content to flat buffer
  void FromBuffer(const uint8_t *buffer);  //!< Read content from flat buffer
};

//! Structure defining the port identity field of a PTP message header.
struct PortIdentity {
  uint64_t clock_identity;  //!< Unique identifier of the clock.
  //! The value of the portNumber for a port on a PTP node supporting a single PTP port shall be 1.
  //! The values of the port numbers for the N ports on a PTP node supporting N PTP ports shall be
  //! 1, 2, ...N, respectively.
  uint16_t port_number;

  void ToBuffer(uint8_t *buffer);  //!< Write content to flat buffer.
  void FromBuffer(const uint8_t *buffer);  //!< Read content from flat buffer.
  //! Returns true if the content of the other port identity is equal to the content of this port
  //! identity.
  bool operator==(const PortIdentity &other);
};

//! Structure defining the timestamp field of a PTP message.
struct Timestamp {
  //! The integer portion of the timestamp in units of seconds. This is only 48 bit in the actual
  //! message.
  uint64_t seconds_field;
  //! The fractional portion of the timestamp in units of nanoseconds. This field is always less
  //! than 10^9.
  uint32_t nanoseconds_field;

  void ToBuffer(uint8_t *buffer);  //!< Write content to flat buffer.
  void FromBuffer(const uint8_t *buffer);  //!< Read content from flat buffer.
};

//! Structure defining the clock quality field of a PTP message header.
struct ClockQuality {
  uint8_t clock_class;  //!< An attribute defining a clock’s TAI traceability.
  ClockAccuracy clock_accuracy;  //!< An attribute defining the accuracy of a clock.
  uint16_t offset_scaled_log_variance;  //!< An attribute defining the stability of a clock.

  void ToBuffer(uint8_t *buffer);  //!< Write content to flat buffer.
  void FromBuffer(const uint8_t *buffer);  //!< Read content from flat buffer.
};


/**************************************************************************************************
 *     PTP message header struct                                                                  *
 **************************************************************************************************/

//! Structure defining the header of a PTP message.
struct Header {
  //! The transportSpecific field may be used by a lower layer transport protocol. It is only 4 bit
  //! in the actual message.
  uint8_t transport_specific;
  //! The message type shall indicate the type of the message. It is only 4 bit in the actual
  //! message.
  Type message_type;
  //! The value of the ptp version field shall be the value of the version number member of the data
  //! set of the originating node. It is only 4 bit in the actual message.
  uint8_t ptp_version;
  //! The value of the message length shall be the total number of octets that form the PTP message.
  //! The counted octets start with the first octet of the header and include and terminate with the
  //! last octet of any suffix or, if there are no suffix members with the last octet of the
  //! message.
  uint16_t message_length;
  uint8_t domain_number;  //!< Further information in IEC61588 chapter 13.3.2.5
  FlagField flag_field;  //!< Structure containing the flags of the message header
  //! The correctionField is the value of the correction measured in nanoseconds and multiplied by
  //! 2^16. For example, 2.5 ns is represented as 0x0000000000028000. A value of one in all bits,
  //! except the most significant, of the field shall indicate that the correction is too big to be
  //! represented.
  int64_t correction_field;
  ///! The value of the portDS.portIdentity member of the data set of the port that originated the
  /// message.
  PortIdentity source_port_identity;
  //! The sequence ID of a message shall be one greater than the sequence ID of the previous message
  //! of the same message type sent to the same message destination address by the transmitting
  //! port.
  uint16_t sequence_id;
  //! The value of control field depends on the message type defined in the message type field . The
  //! use of this field by the receiver is deprecated.
  ControlField control_field;
  //! The value of the log message interval field is determined by the type of the message.
  int8_t log_message_interval;

  void ToBuffer(uint8_t *buffer);  //!< Write content to flat buffer.
  void FromBuffer(uint8_t *buffer);  //!< Read content from flat buffer.
};


/**************************************************************************************************
 *     PTP message body structs                                                                   *
 **************************************************************************************************/

//! Structure defining the body of a PTP sync message.
struct SyncMessageBody {
  Timestamp origin_timestamp;  //!< Estimate of the sync message transmit timestamp.

  void ToBuffer(uint8_t *buffer);  //!< Write content to flat buffer.
  void FromBuffer(uint8_t *buffer);  //!< Read content from flat buffer.
};

//! Structure defining the body of a PTP delay req message.
struct DelayReqMessageBody {
  Timestamp origin_timestamp;  //!< Estimate of the delay req message transmit timestamp.

  void ToBuffer(uint8_t *buffer);  //!< Write content to flat buffer.
  void FromBuffer(uint8_t *buffer);  //!< Read content from flat buffer.
};

//! Structure defining the body of a PTP pdelay req message.
struct PdelayReqMessageBody {
  //! Estimate of the pdelay req message transmit timestamp.
  Timestamp origin_timestamp;

  void ToBuffer(uint8_t *buffer);  //!< Write content to flat buffer.
  void FromBuffer(uint8_t *buffer);  //!< Read content from flat buffer.
};

//! Structure defining the body of a PTP pdelay resp message.
struct PdelayRespMessageBody {
  Timestamp request_receipt_timestamp;  //!< Time when the pdelay req was received.
  //! Source port identity of the received pdelay req message.
  PortIdentity requesting_port_identity;

  void ToBuffer(uint8_t *buffer);  //!< Write content to flat buffer.
  void FromBuffer(uint8_t *buffer);  //!< Read content from flat buffer.
};

//! Structure defining the body of a PTP follow up message.
struct FollowUpMessageBody {
  Timestamp precise_origin_timestamp;  //!< Sync message transmit timestamp

  void ToBuffer(uint8_t *buffer);  //!< Write content to flat buffer.
  void FromBuffer(uint8_t *buffer);  //!< Read content from flat buffer.
};

//! Structure defining the body of a PTP delay resp message.
struct DelayRespMessageBody {
  Timestamp receive_timestamp;  //!< Receive timestamp of the delay req message.
  //! Source port identity of the delay req message.
  PortIdentity requesting_port_identity;

  void ToBuffer(uint8_t *buffer);  //!< Write content to flat buffer.
  void FromBuffer(uint8_t *buffer);  //!< Read content from flat buffer.
};

//! Structure defining the body of a PTP pdelay resp follow up message.
struct PdelayRespFollowUpMessageBody {
  Timestamp response_origin_timestamp;  //!< Pdelay resp Transmit timestamp.
  //! Source port identity of the pdelay req message.
  PortIdentity requesting_port_identity;

  void ToBuffer(uint8_t *buffer);  //!< Write content to flat buffer.
  void FromBuffer(uint8_t *buffer);  //!< Read content from flat buffer.
};

//! Structure defining the body of a PTP announce message.
struct AnnounceMessageBody {
  //! Announce message transmit timestamp.
  Timestamp origin_timestamp;
  //! The value of the timePropertiesDS.currentUtcOffset member of the data set.
  int16_t current_utc_offset;
  //! The value of the parentDS.grandmasterPriority1 member of the data set.
  uint8_t grandmaster_priority_1;
  //! The value of the parentDS.grandmasterClockQuality member of the data set.
  ClockQuality grandmaster_clock_quality;
  //! The value of the parentDS.grandmasterPriority2 member of the data set.
  uint8_t grandmaster_priority_2;
  //! The value of the parentDS.grandmasterIdentity member of the data set.
  uint64_t grandmaster_identity;
  //! The value of currentDS.stepsRemoved of the data set of the clock issuing this message.
  uint16_t steps_removed;
  //! The value of the timePropertiesDS.timeSource member of the data set.
  TimeSource time_source;

  void ToBuffer(uint8_t *buffer);  //!< Write content to flat buffer.
  void FromBuffer(uint8_t *buffer);  //!< Read content from flat buffer.
};

//! Structure defining the body of a PTP signaling message.
struct SignalingMessageBody {
  // TODO(jan): implement this

  void ToBuffer(uint8_t *buffer);  //!< Write content to flat buffer.
  void FromBuffer(uint8_t *buffer);  //!< Read content from flat buffer.
};

//! Structure defining the body of a PTP management message.
struct ManagementMessageBody {
  // TODO(jan): implement this

  void ToBuffer(uint8_t *buffer);  //!< Write content to flat buffer.
  void FromBuffer(uint8_t *buffer);  //!< Read content from flat buffer.
};


/**************************************************************************************************
 *     PTP message body union                                                                     *
 **************************************************************************************************/

//! Union defining the body of a PTP message. Depending on the message type, which is defined in the
//! header, the corresponding message body should be used by the application.
union MessageBody {
  //! Message body that is used if the message is a sync message
  SyncMessageBody sync_message_body;
  //! Message body that is used if the message is a delay req message
  DelayReqMessageBody delay_req_message_body;
  //! Message body that is used if the message is a pdelay req message
  PdelayReqMessageBody pdelay_req_message_body;
  //! Message body that is used if the message is a pdelay resp message
  PdelayRespMessageBody pdelay_resp_message_body;
  //! Message body that is used if the message is a follow up message
  FollowUpMessageBody follow_up_message_body;
  //! Message body that is used if the message is a delay resp message
  DelayRespMessageBody delay_resp_message_body;
  //! Message body that is used if the message is a pdelay resp message
  PdelayRespFollowUpMessageBody pdelay_resp_follow_up_message_body;
  //! Message body that is used if the message is a announce message
  AnnounceMessageBody announce_message_body;
  //! Message body that is used if the message is a signaling message
  SignalingMessageBody signaling_message_body;
  //! Message body that is used if the message is a management message
  ManagementMessageBody management_message_body;

  void ToBuffer(Type message_type, uint8_t *buffer);  //!< Write content to flat buffer.
  //! Read content from flat buffer.
  void FromBuffer(Type message_type, uint8_t *buffer);
};
}  // namespace ptp::message

#endif  // SRC_CORE_SYSTEM_TIME_PTP_MESSAGE_DATA_STRUCTURES_HPP_
