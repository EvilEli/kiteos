// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#ifndef SRC_CORE_SYSTEM_TIME_GNSS_SYNC_HPP_
#define SRC_CORE_SYSTEM_TIME_GNSS_SYNC_HPP_

#include "drivers/neo_m8t/multi_access_handler.hpp"
#include "core/software_timer.hpp"

//! Class implementing a synchronization algorithm to synchronize the system time with a GNSS module
//! Using the PPS_OUT pulses which are timestamped by the GPS.
class GnssSync {
 public:
  /*!
   * Constructor of the GNSS sync class.
   */
  static void Init();

  /*!
   * Destructor of the GNSS sync class.
   */
  static bool IsSynchronized();

  /*!
   * \brief   Gets the current accuracy of the synchronization.
   *
   * \return  Returns the current synchronization accuracy with respect to the GPS time in
   *          nanoseconds.
   */
  static uint64_t GetAccuracy();

 private:
  //! Callback called by the NEO M8T driver when a TIM-TM2 message was received.
  static void TimTm2Callback(ubx::TimTm2* message);

  //! Callback called when the timeout timer elapsed.
  static void TimeoutTimerCallback(void *context);

  //! Driver handler used to access the NEO-M8T device.
  static neo_m8t::MultiAccessHandler* neo_m8t_;
  //! Timer used to detect when no valid PPS timestamps were received within a certain timeout time.
  static SoftwareTimer timeout_timer_;

  static int64_t previous_gps_time_;     //!< GPS timestamp of the last PPS pulse.
  static int64_t previous_system_time_;  //!< Internal system time timestamp of the last PPS pulse.
  //! Current synchronization accuracy with respect to the GPS time in nanoseconds.
  static uint64_t accuracy_;
  //! Flag indicating if the internal system time is currently synchronized to the GPS time.
  static bool is_synchronized_;
  static uint64_t current_accuracy_;   //!< Accuracy estimate of the last PPS timestamp.
  static uint64_t previous_accuracy_;  //!< Accuracy estimate of the previous PPS timestamp.
  static uint32_t timeout_cnt_;  //!< Number of timeouts since the last PPS timestamp was received.
  static uint64_t timeout_accuracy_increment_;  //!< value to add to accuracy each timeout.
};

#endif  // SRC_CORE_SYSTEM_TIME_GNSS_SYNC_HPP_
