// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#include "core/state_node.hpp"
#include "core/system_time/system_time.hpp"
#include "core/name_provider.hpp"
#include "utils/debug_utils.h"

/**************************************************************************************************
 *     Private static member variables                                                            *
 **************************************************************************************************/
StateNode* StateNode::node_registry_[MAX_NODE_CNT] = {nullptr};
pulicast::Channel* StateNode::state_channel_;
pulicast::Channel* StateNode::command_channel_;
SoftwareTimer StateNode::update_timer_;

/**************************************************************************************************
 *     Public methods                                                                             *
 **************************************************************************************************/
StateNode::StateNode(std::string_view state, const std::string& origin, CommandCallback command_cb)
  : state_(state),
    origin_(origin),
    command_cb_(command_cb) {
  ASSERT_RESPONSE(Register(), true);
}

StateNode::~StateNode() {
  node_registry_[node_index_] = nullptr;
}

void StateNode::Init(pulicast::Namespace puli_namespace) {
  state_channel_ = &puli_namespace["/state"];
  command_channel_ = &puli_namespace["/command"];
  command_channel_->Subscribe<kitecom::commandmessage>(StateNode::MsgHandlerCommand);
  update_timer_.SetTimeoutPeriodic(STATE_NODE_UPDATE_RATE, StateNode::PublishAll, nullptr);
}

void StateNode::SetState(std::string_view state) {
  state_ = state;
  Publish();
}


/**************************************************************************************************
 *     Private methods                                                                            *
 **************************************************************************************************/
bool StateNode::Register() {
  bool registration_successful = false;
  for (int i = 0; i < MAX_NODE_CNT; i++) {
    if (node_registry_[i] == nullptr) {
      node_registry_[i] = this;
      node_index_ = i;
      registration_successful = true;
      break;
    }
  }
  return registration_successful;
}

void StateNode::Publish() {
  kitecom:: statemessage msg;
  msg.ts = SystemTime::GetTimestamp();
  msg.state = state_;
  msg.origin = origin_;
  msg.destination = "state_machine";
  *state_channel_ << msg;
}


void StateNode::MsgHandlerCommand(const kitecom::commandmessage &msg) {
  StateNode *node = FindNode(msg.destination);
  if (node) {
    node->command_cb_(msg.command);
  }
}

StateNode *StateNode::FindNode(std::string_view destination) {
  for (auto & i : node_registry_) {
    if (i != nullptr) {
      if (destination.compare(i->origin_) == 0) {
        return i;
      }
    }
  }
  return nullptr;
}


/**************************************************************************************************
 *     Callbacks                                                                                  *
 **************************************************************************************************/

void StateNode::PublishAll(void *arg) {
  for (auto & i : node_registry_) {
    if (i != nullptr) {
      i->Publish();
    }
  }
}
