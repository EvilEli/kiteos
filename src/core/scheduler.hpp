// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#ifndef SRC_CORE_SCHEDULER_HPP_
#define SRC_CORE_SCHEDULER_HPP_

#include "utils/thread_safe_buffer.hpp"
#include "utils/debug_utils.h"
#include "core/system_time/system_time.hpp"


// Size of the task buffer
#define SCHEDULER_TASK_BUFFER_SIZE (THREAD_SAFE_BUFFER_MAX_PROCESS_CNT * 128)
// Maximum number of processes that can access the task buffer simultaneously
#define SCHEDULER_TASK_BUFFER_PROCESS_CNT THREAD_SAFE_BUFFER_MAX_PROCESS_CNT
// Maximum number of tasks that is executed in one call of the run method
#define SCHEDULER_MAX_TASKS_PER_RUN 4


/*!
 *  \brief   Implements the scheduler that handles mainloop tasks.
 *
 *  \details This class can be used to enqueue tasks which have to run in the mainloop. Tasks may
 *           also be enqueued from an interrupt context.
 */
class Scheduler {
 public:
  template<typename CONTEXT_TYPE>
  using TaskFunction = void(*)(CONTEXT_TYPE*);  //!< Function type used for Task functions.

  //! Structure defining a task to be executed.
  template<typename CONTEXT_TYPE>
  struct Task {
    TaskFunction<CONTEXT_TYPE> task_function = nullptr;  //!< Function executing the task.
    void* function_argument = nullptr;     //!< Argument to pass to the task function.
  };

  /*!
   * \brief Lets the Scheduler do its work. Should be called as often as possible.
   */
  inline static void Run() {
    main_loop_counter_++;
    CheckCycleTime();

    Task<void> task;
    for (uint32_t i = 0; i < SCHEDULER_MAX_TASKS_PER_RUN; i++) {
      // Try to read an element from the task queue, return if no element was available
      if (!task_buffer_.ReadElement(&task)) {
        return;
      }

      // Execute task
      task.task_function(task.function_argument);
    }
  }

  /*!
   * \brief     Adds a task to the task buffer. Tasks enqueued to the task buffer will be executed
   *            eventually in a synchronous context.
   *
   * \attention Do not use this within static initializations.
   *
   * \param     task  Task to be enqueued.
   */
  template<typename CONTEXT_TYPE>
  inline static void EnqueueTask(Task<CONTEXT_TYPE> task) {
    ASSERT_RESPONSE(task_buffer_.WriteElement((Task<void>)(task)), true);
  }

  /*!
   * \brief  Adds a task to the task buffer. Tasks enqueued to the task buffer will be executed
   *         eventually in a synchronous context.
   *
   * \param  task_function      Function executing the task to be enqueued.
   * \param  function_argument  Argument to pass to the task function.
   */
  template<typename CONTEXT_TYPE>
  inline static void EnqueueTask(TaskFunction<CONTEXT_TYPE> task_function,
                          CONTEXT_TYPE* function_argument) {
    ASSERT_RESPONSE(task_buffer_.WriteElement(
        {.task_function = reinterpret_cast<TaskFunction<void>>(task_function),
         .function_argument = reinterpret_cast<void*>(function_argument)}), true);
  }

  /*!
     * \brief   Adds a task to the task buffer. Tasks enqueued to the task buffer will be executed
     *          eventually in a synchronous context.
     *
     * \details This function is necessary to be able to enqueue a task with a nullptr argument.
     *
     * \param   task_function      Function executing the task to be enqueued.
     * \param   function_argument  Argument to pass to the task function.
     */
  inline static void EnqueueTask(TaskFunction<void> task_function, void *function_argument) {
    ASSERT_RESPONSE(task_buffer_.WriteElement({.task_function = task_function,
                                               .function_argument = function_argument}), true);
  }

  /*!
   * \brief Gets the number of main loops passed and maximum cycle time sice this function was
   *        called the last time.
   *
   * \param main_loop_counter  Variable where the number of main loops passed, since this function
   *                           was called the last time, is stored.
   * \param max_cycle_time     Variable where the maximum cycle time passed, since this function was
   *                           called the last time, is stored.
   */
  inline static void GetPerformanceStats(uint32_t *main_loop_counter, uint32_t *max_cycle_time) {
    *main_loop_counter = main_loop_counter_;
    *max_cycle_time = max_cycle_time_;
    main_loop_counter_ = 0;
    max_cycle_time_ = 0;
  }

 private:
  //! Updates maxCycleTime.
  inline static void CheckCycleTime() {
    uint64_t current_time = SystemTime::GetTimestamp();
    if (last_timestamp_ > 0) {
      int32_t cycle_time = ((int32_t)(current_time - last_timestamp_)) / 1000;
      if (cycle_time > 0) {
        if (cycle_time > max_cycle_time_) {
          max_cycle_time_ = cycle_time;
        }
      }
    }
    last_timestamp_ = current_time;
  }

  //! Buffer containing the tasks to be executed
  inline static ThreadSafeBuffer<Task<void>, SCHEDULER_TASK_BUFFER_SIZE,
                                 SCHEDULER_TASK_BUFFER_PROCESS_CNT> task_buffer_;

  inline static uint32_t main_loop_counter_ = 0;  //!< Counter for main loops per second.
  inline static int64_t last_timestamp_ = 0;      //!< Last checked timestamp.
  inline static int32_t max_cycle_time_ = 0;      //!< Maximum cycle time in micro seconds.
};

#endif  // SRC_CORE_SCHEDULER_HPP_
