// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#ifndef SRC_CORE_ETHERNET_SERVICES_ETHERNET_SERVICES_HPP_
#define SRC_CORE_ETHERNET_SERVICES_ETHERNET_SERVICES_HPP_

#include "drivers/ksz8563r.hpp"
#include "drivers/ksz8567r.hpp"

/*!
 *  \brief   Implements the Ethernet services.
 *
 *  \details This class is used to setup the Ethernet peripheral, configure and process the LwIP
 *           Ethernet stack and the pulicast framework.
 */
class EthernetServices {
 public:
  /*!
   * \brief  Initializes the Ethernet services.
   *
   * \return True if it is initialized successfully. False otherwise.
   */
  static bool Init();

  /*!
   * \brief Lets the Ethernet services do their work. Should be called periodically.
   */
  static void Run();

 private:
  static Ksz8563R* ksz8563r_;       //!< instance of the KSZ8563R driver.
  static Ksz8567R* ksz8567r_east_;  //!< instance of the KSZ8567R driver for the east switch.
  static Ksz8567R* ksz8567r_west_;  //!< instance of the KSZ8567R driver for the west switch.
};

#endif  // SRC_CORE_ETHERNET_SERVICES_ETHERNET_SERVICES_HPP_
