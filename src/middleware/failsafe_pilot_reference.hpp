// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.
#ifndef SRC_MIDDLEWARE_FAILSAFE_PILOT_REFERENCE_HPP_
#define SRC_MIDDLEWARE_FAILSAFE_PILOT_REFERENCE_HPP_

#include "drivers/spektrum_receiver/definitions.hpp"
#include "drivers/spektrum_receiver/multi_access_handler.hpp"
#include "drivers/failsafe_switch.hpp"

/**
 * @brief Pilot Reference class with integrated safety switch
 */
class FailsafePilotReference {
 public:
  /*!
   * \brief Constructor of the failsafe pilot reference component.
   */
  FailsafePilotReference();

  /*!
   * \brief Destructor of the failsafe pilot reference component.
   */
  ~FailsafePilotReference() = default;

 private:
  /*!
   * \brief  Called when new channel data arrives from driver.
   *
   * \param  channel_data  Structure to be filled with the latest set points if new set points are
   *                         available.
   * \param  ctx Context for callback
   */
  static void ChannelDataCallback(spektrum_receiver::ChannelData channel_data, void* ctx);

  //! Spektrum receiver driver handler instance which handles the RC receiver communication.
  spektrum_receiver::MultiAccessHandler spektrum_receiver_handler_;
  FailsafeSwitch failsafe_switch_;  //!< Driver controlling the failsafe GPIO
};

#endif  // SRC_MIDDLEWARE_FAILSAFE_PILOT_REFERENCE_HPP_
