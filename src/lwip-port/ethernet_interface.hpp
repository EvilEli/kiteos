// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.

#ifndef SRC_LWIP_PORT_ETHERNET_INTERFACE_HPP_
#define SRC_LWIP_PORT_ETHERNET_INTERFACE_HPP_

#include "lwip/netif.h"


/*!
 *  \brief Implements the interface between LwIP and the Ethernet hardware.
 */
class EthernetInterface {
 public:
  /*!
  * \brief  Initializes the Ethernet interface.
  *
  * \param  ip    IP address as character array e.g. in the form "192.168.2.42".
  * \param  mask  Netmask as character array e.g. in the form "255.255.255.0".
  * \param  gate  Gateway address as character array e.g. in the form "192.168.2.1".
  *
  * \return True if it is initialized successfully. False otherwise.
  */
  static bool Init(const char *ip, const char *mask, const char *gate);

  /*!
  * \brief  Initializes the Ethernet interface.
  *
  * \param  ip    IP address in little endian byte order.
  * \param  mask  Netmask in little endian byte order.
  * \param  gate  Gateway address in little endian byte order.
  *
  * \return True if it is initialized successfully. False otherwise.
  */
  static bool Init(uint32_t ip, uint32_t mask, uint32_t gate);

  /*!
  * \brief  Initializes the Ethernet interface.
  *
  * \param ip    IP address as reference to lwip ip_addr_t type.
  * \param mask  Netmask as reference to lwip ip_addr_t type.
  * \param gate  Gateway address reference to lwip ip_addr_t type.
  *
  * \return True if it is initialized successfully. False otherwise.
  */
  static bool Init(ip_addr_t* ip, ip_addr_t* mask, ip_addr_t* gate);

  /*!
   * \brief Lets the Ethernet interface do its work. Should be called periodically.
   */
  static void Run();

 private:
  /*!
  * \brief  This function is called by LwIP for packet transmission.
  *
  * \param  network_interface  The lwip network interface structure.
  * \param  packet_buffer      The MAC packet to send (e.g. IP packet including MAC addresses and
   *                           type).
  *
  * \return ERR_OK if the packet could be sent an err_t value if the packet couldn't be sent.
  */
  static err_t LwipSendCallback(struct netif *network_interface, struct pbuf *packet_buffer);

  /*!
  * \brief  This function is called by LwIP to set up the LwIP network interface and Ethernet
   *        hardware.
  *
  * \param  network_interface  The lwip network interface structure.
   *
  * \return ERR_OK if the loopif is initialized, ERR_MEM if private data couldn't be allocated, any
   *        other err_t on other error.
  */

  static err_t LwipInitCallback(struct netif *network_interface);

  /*!
   * \brief     This function is called by LwIP to configure the hardware for MAC filtering.
   *
   * \attention In the current implementation, this function does nothing but enabling multicast.
   *
   * \param     network_interface  The lwip network interface structure.
   * \param     group_address      The multicast group adress that should be configured.
   * \param     mac_filter_action  The action that should be configured for the provided group
   *                               address.
   *
   * \return    ERR_OK on success, ERR_MEM if private data couldn't be allocated, any other err_t on
   *            other error.
   */
  static err_t LwipSetMacFilterCallback(struct netif *network_interface,
                                        const ip4_addr_t *group_address,
                                        enum netif_mac_filter_action mac_filter_action);

  static struct netif network_interface_;  //!< lwIP network interface structure.
};
#endif  // SRC_LWIP_PORT_ETHERNET_INTERFACE_HPP_
