cmake_minimum_required(VERSION 3.13)

#-------------------------------------------------------------------------------
# Custom commands for cpplint
add_custom_target(cpplint
    COMMENT "Running cpplint..."
    COMMAND cpplint --recursive --repository=${KITEOS_ROOT} --linelength=100 --extensions=cpp,hpp --filter=-legal/copyright,-build/c++11,-runtime/references,-readability/check,-runtime/threadsafe_fn
    ${APP_ROOT}/main.cpp
    ${TEST_APP_ROOT}/main.cpp
    ${SRC_ROOT}
    ${UNIT_TEST_ROOT}
    )

# Custom commands for cpplint
add_custom_target(clint
    COMMENT "Running clint..."
    COMMAND cpplint --recursive --repository=${KITEOS_ROOT} --linelength=100  --extensions=c,h --filter=-readability/casting,-legal/copyright,-build/c++11,-runtime/references
    ${SRC_ROOT}
    )

# Custom commands for lint
add_custom_target(lint
    COMMENT "Running linter..."
    DEPENDS clint cpplint
    )
