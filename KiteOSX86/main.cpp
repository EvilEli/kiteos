// Copyright (C) 2021 Kiteswarms Ltd
//
//  This file is part of KiteOS.
//
//  KiteOS is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  KiteOS is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with KiteOS. If not, see <https://www.gnu.org/licenses/>.

// Define macro for poisoned class.
#define FRAM_ACCESS FramAccess

#include <iostream>
#include <boost/program_options.hpp>
#include <chrono>
#include <asio/basic_waitable_timer.hpp>
#include "core/persistent_memory/fram_emulator.hpp"
#include "core/persistent_memory/fram_access.hpp"
#include "core/scheduler.hpp"
#include "core/kiteos-version.hpp"
#include "core/plugin_manager/plugin_manager.hpp"
#include "core/plugin_manager/external_plugin_manager.hpp"
#include "core/logger.hpp"
#include "core/state_node.hpp"
#include "core/board_monitor.hpp"
#include "core/name_provider.hpp"
#include "core/asio_service.hpp"
#include "core/puli_driver_interface.hpp"

#define DEFAULT_MAINLOOP_FREQ 50000

using namespace std::chrono_literals;
namespace po = boost::program_options;

void main(int argc, char** argv) {
  std::cout << "\033[1;36m"
            << "KiteOS (x86) version "
            << GIT_TAG
            << (GIT_IS_DIRTY ? "-dirty" : "")
            << "\033[0m"
            << std::endl;

  // Declare the supported options.
  po::options_description desc("Allowed options");
  desc.add_options()
    ("help,h", "Print help message")
    ("port", po::value<uint16_t>()->default_value(pulicast::DEFAULT_PORT), "UDP Port")
    ("ttl", po::value<uint16_t>()->default_value(0), "Multicast TTL")
    ("mainloop-frequency,f", po::value<uint32_t>()->default_value(DEFAULT_MAINLOOP_FREQ),
      "The number of mainloops executed per second")
    ("fram-json,j", po::value<std::string>(), "FRAM content .json");

  po::variables_map vm;
  po::store(po::parse_command_line(argc, argv, desc), vm);
  po::notify(vm);

  if (vm.count("help")) {
    std::cout << desc << std::endl;
    return;  // return when you only needed help info
  }

  // Print specified options
  uint16_t port = vm["port"].as<uint16_t>();
  std::cout << "\033[1;33m" << "Port: " << "\033[0m"
            << port << std::endl;
  uint8_t ttl = vm["ttl"].as<uint16_t>();
  std::cout << "\033[1;33m" << "TTL: "  << "\033[0m"
            << static_cast<uint16_t>(ttl) << std::endl;
  uint32_t mainloop_frequency = vm["mainloop-frequency"].as<uint32_t>();
  std::cout << "\033[1;33m" << "Mainloops per second: " << "\033[0m"
            << mainloop_frequency << std::endl;

  if (vm.count("fram-json")) {
    std::string fram_json_filename = *boost::unsafe_any_cast<std::string>(&vm["fram-json"].value());
    std::cout << "\033[1;33m" << "FRAM json file: " << "\033[0m"
              << fram_json_filename << std::endl;
    FramEmulator::Init(fram_json_filename);
  }

  hal::SystickTimer::Init();
  SoftwareTimer::Init();
  FRAM_ACCESS::Init();
  NameProvider::Init();

  PulicastEmbeddedNode node("KITEOS_X86", NameProvider::GetKiteName(),
                            port, ttl,
                            100ms, AsioService::GetService(),
                            std::nullopt);

  PuliDriverInterface::Init(&node);

  Logger::Init(node);
  StateNode::Init(node);
  auto board_monitor = std::make_unique<BoardMonitor>(node / NameProvider::GetBoardName());
  PluginManager::Init(node);

#ifdef EXTERNAL_PLUGINS
  ExternalPluginManager::Init(node);
#endif

  auto asio_timer = asio::basic_waitable_timer<std::chrono::system_clock>(
      *(AsioService::GetService()), std::chrono::system_clock::duration::zero());

  uint32_t period_us = 1000000/vm["mainloop-frequency"].as<uint32_t>();
  std::function<void(const asio::error_code &)> asio_lambda;
  asio_lambda = [&](const asio::error_code &){
    Scheduler::Run();
    FRAM_ACCESS::Run();
    asio_timer.expires_at(asio_timer.expires_at()+ std::chrono::microseconds(period_us));
    asio_timer.async_wait(asio_lambda);
  };
  asio_timer.async_wait(asio_lambda);
  AsioService::GetService()->run();
}
