#include "hal/systick_timer.hpp"
#include "core/software_timer.hpp"
#include "gtest/gtest.h"


class CoreSoftwareTimerTest : public testing::Test {
 protected:
  void SetUp() override {
    // set stuff up here
    SoftwareTimer::Init();
  }
};

TEST_F(CoreSoftwareTimerTest, SoftwareTimerElapsed) {
  SoftwareTimer test_timer;
  test_timer.SetTimeout(3);
  EXPECT_FALSE(test_timer.Elapsed());
  // First tick
  SysTick_Handler();
  EXPECT_FALSE(test_timer.Elapsed());
  // Second tick
  SysTick_Handler();
  EXPECT_FALSE(test_timer.Elapsed());
  // Third tick
  SysTick_Handler();
  EXPECT_TRUE(test_timer.Elapsed());
}
