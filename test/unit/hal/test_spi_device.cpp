#include "hal/spi/device.hpp"
#include "gtest/gtest.h"

#define EXPECT_BITS_EQ(expected, actual, mask) EXPECT_EQ(expected & mask, actual & mask)

class HalSpiDeviceTest: public testing::Test {
 protected:
  void SetUp() override {
    // set stuff up here
    // Initialize SPI peripheral with zeros
    *SPI1 = {0};
  }
};

void SpiDeviceCallback(void* cb_arg) {
}

TEST_F(HalSpiDeviceTest, Transceive) {
  hal::spi::Device* test_device = hal::spi::Device::GetDevice(hal::spi::kPort1);

  uint8_t tx_buffer[3];
  uint8_t rx_buffer[3];
  void* ptr1 = reinterpret_cast<void*>(0x11111111);

  hal::spi::Device::Job job = {
    {
      hal::spi::kPhaseFirstEdge,
      hal::spi::kPolarityHigh,
      hal::spi::kBaud1MHz,
      hal::Gpio::kPortA,
      hal::Gpio::kPin11,
    },
      hal::Gpio(hal::Gpio::kPortA, hal::Gpio::kPin11, hal::Gpio::kModeOutput),
      tx_buffer, rx_buffer, 3,
      SpiDeviceCallback, ptr1,
      false
  };

  EXPECT_BITS_EQ(~SPI_CR1_CSTART, SPI1->CR1, SPI_CR1_CSTART);
  test_device->Transceive(&job);
  EXPECT_BITS_EQ(SPI_CR1_CSTART, SPI1->CR1, SPI_CR1_CSTART) << "Transfer start bit";
  hal::spi::Device::FreeRegistry();
}
