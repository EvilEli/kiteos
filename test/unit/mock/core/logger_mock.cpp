#include <functional>
#include "logger_mock.hpp"
#include "utils/debug_utils.h"
#include "gmock/gmock.h"

static std::function<void(const char *, Logger::LogLevel)> _report;

LoggerMock::LoggerMock() {
  _report = [this](const char *message, Logger::LogLevel level){
              Report(std::string(message), level);
            };
}

LoggerMock::~LoggerMock() {
  _report = {};
}

void Logger::Report(const char *message, Logger::LogLevel level) {
  ASSERT(_report);
  _report(message, level);
}
