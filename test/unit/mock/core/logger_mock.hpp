#ifndef TEST_UNIT_MOCK_CORE_LOGGER_MOCK_HPP_
#define TEST_UNIT_MOCK_CORE_LOGGER_MOCK_HPP_

#include <string>
#include "core/logger.hpp"
#include "gmock/gmock.h"

struct LoggerMock {
  LoggerMock();
  ~LoggerMock();
  MOCK_METHOD2(Report, void(std::string, Logger::LogLevel));
};
#endif  // TEST_UNIT_MOCK_CORE_LOGGER_MOCK_HPP_
