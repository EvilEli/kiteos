#ifndef TEST_UNIT_MOCK_CORE_STATE_NODE_HPP_
#define TEST_UNIT_MOCK_CORE_STATE_NODE_HPP_

#include <string>
#include "gmock/gmock.h"

/*!
 *  \brief     Implements the mocked StateNode class.
 *
 *  \author    Elias Rosch
 *  \date      2020
 *
 */
class StateNode {
 public:
  MOCK_METHOD1(SetState, void(std::string_view state));
};

#endif  // TEST_UNIT_MOCK_CORE_STATE_NODE_HPP_
