#include "utils/thread_safe_buffer.hpp"
#include "gtest/gtest.h"

/**************************************************************************************************
 *     GetChannelName                                                                             *
 **************************************************************************************************/
TEST(ThreadSafeBufferTest, WriteElementFull) {
  ThreadSafeBuffer<uint8_t, 3, 1> buffer;
  EXPECT_TRUE(buffer.WriteElement(0xAA));
  EXPECT_TRUE(buffer.WriteElement(0xBB));
  EXPECT_TRUE(buffer.WriteElement(0xCC));
  EXPECT_FALSE(buffer.WriteElement(0xDD));
}

TEST(ThreadSafeBufferTest, ReadEmpty) {
  ThreadSafeBuffer<uint8_t, 3, 1> buffer;
  uint8_t rdata;
  EXPECT_FALSE(buffer.ReadElement(&rdata));
}

TEST(ThreadSafeBufferTest, WriteElementReadElement) {
  uint8_t wdata[] = {
    0xAA,
    0xBB,
    0xCC,
    0xDD,
    0xEE,
  };
  ThreadSafeBuffer<uint8_t, 3, 1> buffer;
  uint8_t rdata;
  for (auto i : wdata) {
    EXPECT_TRUE(buffer.WriteElement(i));
    EXPECT_TRUE(buffer.ReadElement(&rdata));
    EXPECT_EQ(i, rdata);
  }
}
