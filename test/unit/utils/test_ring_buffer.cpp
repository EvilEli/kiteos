#include <cstring>
#include "utils/ring_buffer.hpp"
#include "gtest/gtest.h"
/**************************************************************************************************
 *     GetChannelName                                                                             *
 **************************************************************************************************/
TEST(RingBufferTest, WriteAtOnceReadAtOnce) {
  uint8_t wdata[] = {
    0xAA,
    0xBB,
    0xCC,
    0xDD,
    0xEE,
  };
  RingBuffer<uint8_t, 5> ring_buffer;
  EXPECT_EQ(5U, ring_buffer.Write(wdata, 5));

  EXPECT_TRUE(ring_buffer.ElementsAvailable());
  uint8_t rdata[5];
  EXPECT_EQ(5U, ring_buffer.Read(rdata, 5));
  EXPECT_TRUE(0 == std::memcmp(wdata, rdata, 5));
}

TEST(RingBufferTest, WriteFull) {
  uint8_t wdata[] = {
    0xAA,
    0xBB,
    0xCC,
    0xDD,
    0xEE,
  };
  RingBuffer<uint8_t, 3> ring_buffer;
  EXPECT_EQ(3U, ring_buffer.Write(wdata, 5));
}

TEST(RingBufferTest, WriteElementFull) {
  RingBuffer<uint8_t, 3> ring_buffer;
  EXPECT_TRUE(ring_buffer.WriteElement(0xAA));
  EXPECT_TRUE(ring_buffer.WriteElement(0xBB));
  EXPECT_TRUE(ring_buffer.WriteElement(0xCC));
  EXPECT_FALSE(ring_buffer.WriteElement(0xDD));
}

TEST(RingBufferTest, ReadEmpty) {
  RingBuffer<uint8_t, 3> ring_buffer;
  uint8_t rdata[5];
  EXPECT_EQ(0U, ring_buffer.Read(rdata, 5));
}

TEST(RingBufferTest, WriteElementReadElement) {
  uint8_t wdata[] = {
    0xAA,
    0xBB,
    0xCC,
    0xDD,
    0xEE,
  };
  RingBuffer<uint8_t, 3> ring_buffer;

  for (auto i : wdata) {
    EXPECT_TRUE(ring_buffer.WriteElement(i));
    uint8_t read_elem;
    EXPECT_TRUE(ring_buffer.ReadElement(&read_elem));
    EXPECT_EQ(i, read_elem);
  }
}
