# Mock Files for Unit Tests of STM32H7 Firmware on X86 Architectures

The following approach is taken from the book `Test Driven Development for Embedded C`

Currently the files needed to mock the stm32h7 architecture for X86 consist of:
```
cmsis_version.h     (Derived from cmsis_core)
cmsis_gcc.h         (Derived from cmsis_core)
core_cm7.h          (Derived from cmsis_core)
stm32h7xx.h         (Derived from cmsis_device_h7)
system_stm32h7xx.h  (Derived from cmsis_device_h7)
stm32h743xx.h       (Derived from cmsis_device_h7)
```
## cmsis_version.h
Is an exact copy from `cmsis_core/Include/cmsis_version.h`.
Needed to make `core_cm7.h` happy.

## cmsis_gcc.h
Here some functions need to be adjusted since several assembler instructions do not exist on X86 architecture.
Currently simply commenting the assembler calls seems to work.
Please adjust the follwing functions:
```
__STATIC_FORCEINLINE void __ISB(void) - isb
__STATIC_FORCEINLINE void __DSB(void) - ds
__STATIC_FORCEINLINE void __disable_irq(void) - cpsid
__STATIC_FORCEINLINE void __set_MSP(uint32_t topOfMainStack) - msr
```

## core_cm7.h

1. First we change the cmsis_compiler.h file with the mocked one.
Instead of including the generic `cmsis_compiler.h` we replace it by the mocked one `cmsis_gcc.h`.
2. Next we remove/comment the line `#include "mpu_armv7"`.

## stm32h7xx.h
Is an exact copy from `cmsis_device_h7/Include/stm32h7xx.h`

## system_stm32h7xx.h
Is an exact copy from `cmsis_device_h7/Include/system_stm32h7xx.h`

## stm32h743xx.h
This file is where most part of the magic happens.

1. First add the following definition at the beginning (after name mangling directive).
```
#ifndef EXTERN
#define EXTERN extern
#endif
```

2. Next the perpheral declarations that are tied to specific memory addresses need to be "released" so that the compiler itself decides where to place them in memory.
Search for `Peripheral_declaration` and then replace the peripheral base addresses by
variable adresses. Take this as example:

Replace this
```
#define TIM2                ((TIM_TypeDef *) TIM2_BASE)
```
by this
```
EXTERN TIM_TypeDef gTIM2;
#define TIM2                ((TIM_TypeDef *) &gTIM2)
```

The `EXTERN` keyword is used to prevent multiple definitions that would arise when the header is included multiple times. In order to create the variable once, there needs to be one includer (usually the main test file) which defines the `EXTERN` to be empty.
Example:
```
#define EXTERN
```
