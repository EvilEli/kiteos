# KiteOS
KiteOS is the designated firmware implemented on dedicated STM32H7 microprocessors
and is used for physical interaction with any sensor or actuator.
The core functionality consists of a __hardware abstraction layer__ and a set of __core services__
that are built on top to provide basic operations, e. g. communication, timing, and persistent memory manipulaton.
Additionally KiteOS ships with a set of __device drivers__ that can be used to interface sensors, actuators
and other devices via software. See the [Changelog](CHANGELOG.md) for information on the history of changes.

## Communicating with the Embedded Hardware
### Pulicast
KiteOS uses [pulicast](https://gitlab.com/kiteswarms/pulicast-cpp) as communication framework.

In order to communicate via pulicast from python see [pulicast-python](https://gitlab.com/kiteswarms/pulicast-python) for more information.
KiteOS is communicating on the default port `8765`.
Make sure to set `ttl = 1` or higher when you try to communicate with the hardware.

### KITECOM
The message bindings that are used for KiteOS should be installed on the host system. With these
installed you can easily decode and encode messages in your favorite language (e.g. Python or C++).
Also the usage of debugging tools is enabled. See
[KITECOM](https://gitlab.com/kiteswarms/kitecom) for instructions on how to install the
message bindings.

## How to get started

### Configuration
KiteOS supports different modules (we call it plugins) that can be enabled or disabled individually.
Additional parameters that can be modified are the `kite_name` or `board_name`. These parameters
change the naming of the published an subscribed channels. The pulicast node name is determined
by the parameters `pcb_type`, `pcb_version`, `pcb_id`.

Read the [Persistent Memory Interface Documentation](https://kiteswarms.gitlab.io/persistent-memory-interface) in order to get more information.

If you want to configure KiteOS using `.json` files you need to install [Persistent Memory Interface](https://gitlab.com/kiteswarms/persistent-memory-interface).

### Using ARM binaries
A board always contains two programs: the bootloader and the actual firmware.
The bootloader is started on power up and loads the firmware.
It is also used to update the firmware during normal operation.
To successfully load a firmware via ethernet, it is required to have a working bootloader flashed.

You can get the latest released binaries from the [releases section](https://gitlab.com/kiteswarms/kiteos/-/releases).
Alternatively build the binaries from source.

Use the [KiteOS-flashtool](https://gitlab.com/kiteswarms/kiteos-flashtool) to update the firmware on a board via ethernet.

If you have a new board or a freshly flashed firmware not working without obvious reasons,
this may be because there is no working bootloader flashed to the board.
In that case, you should flash bootloader and firmware to the board via [JTAG](#Debugging).

## Building from source
### Build the firmware
You need setuptools and zcm to execute the cmake scripts.
Setuptools is installed by pip and zcm can be installed from a [.deb package](https://github.com/ZeroCM/zcm/releases).
```
pip3 install setuptools-scm
```
#### ARM Embedded Toolchain
Since the firmware needs to be cross-compiled for a different architecture, a dedicated build toolchain must be used.
Luckily the ARM developers provide a [prebuilt toolchain](https://developer.arm.com/tools-and-software/open-source-software/developer-tools/gnu-toolchain/gnu-rm) for our needs.
Just grab the latest one from the [Download section](https://developer.arm.com/tools-and-software/open-source-software/developer-tools/gnu-toolchain/gnu-rm/downloads) and extract it to a directory of your choice.
In order to inform the KiteOS build system about the location of the toolchain it is recommended to define it in an environment variable.
Either in your local terminal:
```
export GNU_ARM_EMBEDDED_TOOLCHAIN_PATH=<path-to-toolchain-dir>
```
Or system-wide by adding the following line to `/etc/environment`:
```
GNU_ARM_EMBEDDED_TOOLCHAIN_PATH=<path-to-toolchain-dir>
```

Clone the repo and submodules (make sure you have added your SSH keys to GitLab and you have access rights to the Kiteswarms group):
```
git clone --recurse-submodules git@gitlab.com:kiteswarms/kiteos.git
```

#### Compile
Create build folder:
```
cd KiteOSMain
mkdir build
cd build
```

To build the firmware:
```
cmake ..
make -j <num-of-jobs>
```

The binaries will be compiled to the respective location `KiteOSMain/build/KiteOS/<target_name>.bin`.

### Build the x86 binary

KiteOS x86 requires rapidjson which can be installed from APT
```
sudo apt update
sudo apt install rapidjson-dev
```

Create build folder:
```
cd KiteOSX86
mkdir build
cd build
```

To build the firmware:
```
cmake ..
make -j <num-of-jobs>
```

The binaries will be compiled to the respective location `KiteOSMain/build/<target_name>.bin`.

To generate a installable `.deb` package :
```
cpack
```

### Build the unit tests
In order to be able to build the KiteOS unit tests, a few packages are needed.
On Debian or Ubuntu type:
```
sudo apt update
sudo apt install g++-multilib lcov
```

Create build folder:
```
cd KiteOSTest
mkdir build
cd build
```

To build the test executables:
```
cmake ..
make -j <num-of-jobs>
```
The binaries will be compiled to the current directory and will be executed automatically after the build process.

You can find a coverage report of the unit tests here:
[![test coverage](https://gitlab.com/kiteswarms/kiteos/badges/develop/coverage.svg?job=Unit+Tests)](https://kiteswarms.gitlab.io/kiteos/test_coverage/index.html)

## Debugging firmware
In order to develop and debug KiteOS code we recommend to use the [VS Code](https://code.visualstudio.com) (where we provide configuration files).
However feel free to grab any other IDE that supports openOCD.
In case you chose to use VS Code, there is an extension which makes debugging live a lot more comfortable.
In VS Code we recommend to install the [Cortex-Debug](https://marketplace.visualstudio.com/items?itemName=marus25.cortex-debug)
extension as the configuration files in this repo are set up to work with it.

### `udev` rules
In order to be able to flash the firmware with openOCD, you need `udev` access rights for the ST-LINK debugger. To get these, you need to add a `udev` rule:
- create a new file for the STM nucleo devboards with the format `<prio>-<description>.rules` in `/etc/udev/rules.d/` (e.g. `/etc/udev/rules.d/49-stlinkv2-1.rules`)
- This file should contain the following:
```
# stm32 nucleo boards, with onboard st/linkv2-1
# ie, STM32F0, STM32F4.
# STM32VL has st/linkv1, which is quite different

SUBSYSTEMS=="usb", ATTRS{idVendor}=="0483", ATTRS{idProduct}=="374a", \
    MODE:="0666", \
    SYMLINK+="stlinkv2-1_%n"

SUBSYSTEMS=="usb", ATTRS{idVendor}=="0483", ATTRS{idProduct}=="374b", \
    MODE:="0666", \
    SYMLINK+="stlinkv2-1_%n"

SUBSYSTEMS=="usb", ATTRS{idVendor}=="0483", ATTRS{idProduct}=="3752", \
    MODE:="0666", \
    SYMLINK+="stlinkv2-1_%n"

SUBSYSTEMS=="usb", ATTRS{idVendor}=="0483", ATTRS{idProduct}=="3748", \
    MODE:="0666", \
    SYMLINK+="stlinkv2-1_%n"
```
- If you share your linux system with other users, or just don't like the idea of write permission for everybody, you can replace `MODE:="0666"` with `OWNER:="yourusername"` to create a device owned by you, or with `GROUP:="somegroupname"` a device owned by some group, and manage access using standard unix groups.

### openOCD
Furthermore make sure you have the latest version of [openOCD](http://openocd.org/) to ensure it ships with the configuration files needed for the `STM32H7x`.
You can build it from source, which needs some packages. In the configure step, the stlink debugger used to debug the ethernet generation, and the old debugger for the mfw hardware are chosen:
```
sudo apt install libtool pkg-config libusb-1.0-0-dev

git clone https://git.code.sf.net/p/openocd/code openocd-code
cd openocd-code/
./bootstrap
./configure --enable-stlink --enable-ti-icdi
make
sudo make install
```

You can check if openOCD works by flashing the binary to the controller from the command line:
```
openocd -f <path-to-KiteOS-root>/openocd/stm32h7x.cfg -c "program <path-to-KiteOS-bin> verify;reset;exit"
```

Flashing a bad binary can lead to a bricked controller.
In such cases it needs to be mass erased while holding the reset signal.
Add the following line to `<path-to-KiteOS-root>/openocd/stm32h7x.cfg`:
```
reset_config connect_assert_srst
```
To execute the mass erase command:
```
openocd -f <path-to-KiteOS-root>/openocd/stm32h7x.cfg -c "init;reset halt;stm32h7x unlock 0;stm32h7x mass_erase 0;reset;exit"
```

## Documentation
The documentation can be found [here](https://kiteswarms.gitlab.io/kiteos/).
If you want to build it yourself just execute the following in the root of KiteOS:
```
pip3 install -r docs-requirements.txt
```

### Doxygen
The API documentation of the repository is managed and maintained using [Doxygen](http://www.doxygen.nl).
If you want to build it yourself you need the following packages:
```
sudo apt install doxygen lcov
pip3 install coverxygen
```

You can then build the doxygen output and the doxygen coverage from the build directory:
```
make doxygen
```
The generated doxygen output:
```
docs/build/html/doxy_output/index.html
```
The doxygen coverage report:
```
docs/build/html/doxy_output/coverage/index.html
```

You can find the doxygen output online [here](https://kiteswarms.gitlab.io/kiteos/doxy_output/html/index.html)

You can find a coverage report of the documentation online:
[![documentation coverage](https://gitlab.com/kiteswarms/kiteos/badges/master/coverage.svg?job=Documentation)](https://kiteswarms.gitlab.io/kiteos/doxy_output/coverage/index.html)
