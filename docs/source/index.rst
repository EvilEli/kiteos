.. KiteOS documentation master file, created by
   sphinx-quickstart on Tue Apr 14 17:35:58 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to KiteOS's documentation!
==================================

The **module architecture** and all related topics are explained in the following:

.. toctree::
  :maxdepth: 2
  :caption: Contents:

  kiteos_main/kiteos_main.rst
  iap_communication_protocol/iap_communication_protocol.rst
  persistent_memory/persistent_memory.rst
  x86
  API <doxy_output/html/index.html#http://>
  Doxygen Coverage <doxy_output/coverage/index.html#http://>
  Test Coverage <test_coverage/index.html#http://>


KiteOS API
==========

The KiteOS API documentation can be found here:

`Doxygen API docs <doxy_output/html/index.html>`_


Doxygen Coverage Report
=======================

The latest coverage report can be found here:

`Doxygen Coverage report <doxy_output/coverage/index.html>`_

Test Coverage Report
====================

The latest coverage report can be found here:

`Test Coverage report <test_coverage/index.html>`_



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
