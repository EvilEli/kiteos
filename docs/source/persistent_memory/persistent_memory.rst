.. _persistent_memory_communication_protocol:

****************************************
Persistent Memory Communication Protocol
****************************************

Persistent memory messages are sent on the "mem" pulicast channel as timestamped vector byte messages.

A persistent memory manager message consists of a source ID of the message sender, a destination ID
of the message receiver, a message type and message data.

.. texfigure:: texfigures/message_definition.tex
    :align: center

:Source ID:      ID of the message sender.
:Destination ID: ID of the message receiver.
:Message type:   Type of the message, defining how the message data is parsed.
:Message data:   Message data depending on the message type.

The ID of an embedded hardware component consists of PCB type, PCB version and PCB ID. For boards
which show up as "Unknown_PCB", the PCB ID is 0.

.. texfigure:: texfigures/embedded_hardware_id.tex
    :align: center

:PCB type:    PCB type stored in the persistent memory of the board to access.
:PCB version: PCB version stored in the persistent memory of the board to access.
:PCB ID:      PCB ID stored in the persistent memory of the board to access.

For high level software components, the pulicast session ID, padded with leading zeros is used as
ID.

.. texfigure:: texfigures/high_level_software_id.tex
    :align: center

:Pulicast session ID: Session ID of the pulicast node used by the software component.

The message type field is a single byte defining the message type. The supported message types are
listed in the following table.

+-------+-------------------+
| Value | Message type      |
+=======+===================+
|   0   | Get size command  |
+-------+-------------------+
|   1   | Get size response |
+-------+-------------------+
|   2   | Read command      |
+-------+-------------------+
|   3   | Read response     |
+-------+-------------------+
|   4   | Read all command  |
+-------+-------------------+
|   5   | Read all response |
+-------+-------------------+
|   6   | Write command     |
+-------+-------------------+
|   7   | Write response    |
+-------+-------------------+
|   8   | Erase command     |
+-------+-------------------+
|   9   | Erase command     |
+-------+-------------------+
|  255  | Error             |
+-------+-------------------+

Get size command message
########################

Tells the persistent memory manager to return the size of the persistent memory. The persistent
memory manager answers with an get size response message.

.. texfigure:: texfigures/get_size_command_message_definition.tex
    :align: center

Get size response message
#########################

Response sent by the persistent memory manager after a get size command was received.

.. texfigure:: texfigures/get_size_response_message_definition.tex
    :align: center

:Size:           Maximum number of Values that can be stored in the persistent memory.
:Occupied space: Number of values currently stored in the persistent memory.

Read command message
####################

Tells the persistent memory manager to read a value from the persistent memory. After successful
execution, the persistent memory manager answers with a read response message.

.. texfigure:: texfigures/read_command_message_definition.tex
    :align: center

:Key: Key corresponding to the value to read from the persistent memory.

Read response message
#####################

Response sent by the persistent memory manager after successfully executing a read command.

.. texfigure:: texfigures/read_response_message_definition.tex
    :align: center

:Key:   Key corresponding to the value read from the persistent memory.
:Value: Value read from the persistent memory.

Read all command message
########################

Tells the persistent memory manager to read all values present in the persistent memory. After
successful execution, the persistent memory manager answers with one or multiple read all response
messages, depending on how much values were read from the persistent memory.

.. texfigure:: texfigures/read_all_command_message_definition.tex
    :align: center

Read all response message
#########################

Response sent by the persistent memory manager after successfully executing a read all command. A
message can contain up to 10 key value pairs. If a lot of persistent memory values were read, the
persistent memory manager may answer with multiple read all response messages. For example, if 42
key value pairs were read from the persistent memory, 5 read all response messages are sent. The
first 4 messages then contain 10 and the fifth message contains 2 key value pairs.

.. texfigure:: texfigures/read_all_response_message_definition.tex
    :align: center

:Number of values: Total number of values read from the persistent memory.
:Start index:      Index of the first key value pair in this message.
:Key value pairs:  A list of up to ten key value pairs read from the persistent memory. This field
                   is further explained below.

.. texfigure:: texfigures/key_value_pairs_field_definition.tex
    :align: center

:Key 1:   Key corresponding to the first value contained in this message.
:Value 1: First value contained in this message
:Key 2:   Key corresponding to the second value contained in this message.
:Value 2: Second value contained in this message.

Write command message
#####################

Tells the persistent memory manager to write a value to the persistent memory. After completing the
write operation, the persistent memory manager answers with a write response message.

.. texfigure:: texfigures/write_command_message_definition.tex
    :align: center

:Key:             Key corresponding to the value to write to the persistent memory.
:Value:           Value to write to the persistent memory.
:Ignore fuse bit: If this value is not zero, the write command also overwrites values protected by
                  a fuse bit. Overwriting fused values can cause a lot of damage. This bit should
                  always be zero, except you exactly know what you are doing.

Write response message
######################

Response sent by the persistent memory manager after executing a write command.

.. texfigure:: texfigures/write_response_message_definition.tex
    :align: center

:Key:   Key corresponding to the value written to the persistent memory.
:Value: Value written to the persistent memory.
:Error: If this value is not zero, an error occurred. This is the case if a value protected by a
        fuse bit should be overwritten by a write command which has the ignore fuse bit flag not
        set.

Erase command message
#####################

Tells the persistent memory manager to erase a key value pair from the persistent memory. After
completing the erase operation, the persistent memory manager answers with an erase response
message.

.. texfigure:: texfigures/erase_command_message_definition.tex
    :align: center

:Key:             Key of the key value pair to erase from the persistent memory.
:Ignore fuse bit: If this value is not zero, the erase command also erases values protected by a
                  fuse bit. Erasing fused values can cause a lot of damage. This bit should always
                  be zero, except you exactly know what you are doing.

Erase response message
######################

Response sent by the persistent memory manager after executing an erase command.

.. texfigure:: texfigures/erase_response_message_definition.tex
    :align: center

:Key:   Key of the key value pair erased from the persistent memory.
:Error: If this value is not zero, an error occurred. This is the case if a value protected by a
        fuse bit should be erased by an erase command which has the ignore fuse bit flag not set.

Error message
#############

An error message is sent by the persistent memory manager instead of a regular response message, if
it is not able to decode a received command message.

.. texfigure:: texfigures/error_message_definition.tex
    :align: center

:Error type: Error number indicating what went wrong. This may be a value from the following table.

+-------+------------------------+
| Value | Error type             |
+=======+========================+
|   0   | Unknown command        |
+-------+------------------------+
|   1   | Invalid command length |
+-------+------------------------+
