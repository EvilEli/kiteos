.. _iap_communication_protocol:

************************************************
In Application Programmer Communication Protocol
************************************************

In application programmer (IAP) messages are sent on the "iap" pulicast channel as timestamped vector byte messages.
  
An IAP message consists of a source ID of the message sender, a
destination ID of the message receiver, a message type and message data.

.. texfigure:: texfigures/message_definition.tex
    :align: center

:Source ID:      ID of the message sender.
:Destination ID: ID of the message receiver.
:Message type:   Type of the message, defining how the message data is parsed.
:Message data:   Message data depending on the message type.

The ID of an embedded hardware component consists of PCB type, PCB version and PCB ID. For boards
which show up as "Unknown_PCB", the PCB ID is 0. The ID only consisting of 0xFF is the broadcast ID.
This ID may be used as destination ID of messages with no specific receiver.

.. texfigure:: texfigures/embedded_hardware_id.tex
    :align: center

:PCB type:    PCB type stored in the persistent memory of the board to access.
:PCB version: PCB version stored in the persistent memory of the board to access.
:PCB ID:      PCB ID stored in the persistent memory of the board to access.

For high level software components, the pulicast session ID, padded with leading zeros is used as
ID.

.. texfigure:: texfigures/high_level_software_id.tex
    :align: center

:Pulicast session ID: Session ID of the pulicast node used by the software component.

The message type field is a single byte defining the message type. The supported message types are
listed in the following Table.

+-------+------------------------------+
| Value | Message type                 |
+=======+==============================+
|   0   | State message                |
+-------+------------------------------+
|   1   | Erase command                |
+-------+------------------------------+
|   2   | Erase response               |
+-------+------------------------------+
|   3   | Write command                |
+-------+------------------------------+
|   4   | Write response               |
+-------+------------------------------+
|   5   | Read command                 |
+-------+------------------------------+
|   6   | Read response                |
+-------+------------------------------+
|   7   | Jump command                 |
+-------+------------------------------+
|   8   | Jump response                |
+-------+------------------------------+
|   9   | Set firmware broken command  |
+-------+------------------------------+
|  10   | Set firmware broken response |
+-------+------------------------------+
|  100  | Reset command                |
+-------+------------------------------+
|  101  | Reset response               |
+-------+------------------------------+
|  255  | Error                        |
+-------+------------------------------+

State message
#############

The state message is periodically sent by the IAP plugin and informs the flash tool about the
current IAP state. The destination ID of this message is the broadcast ID.

.. texfigure:: texfigures/state_message_definition.tex
    :align: center

:Memory area: Memory area from which the current program is executed. May be 0 for bootloader memory
              or 1 for Firmware memory.

Erase command message
#####################

Tells the IAP to erase a memory area. The bootloader can only be erased by the firmware, while the
firmware can only be erased by the bootloader. After an erase command was successfully executed, the
IAP answers with an erase response message.

.. texfigure:: texfigures/erase_command_message_definition.tex
    :align: center

:Memory area: Memory area to erase. May be 0 for bootloader memory or 1 for firmware memory.

Erase response message
######################

Response sent by the IAP after a memory area was erased.

.. texfigure:: texfigures/erase_response_message_definition.tex
    :align: center

:Memory area: Memory area which was erased. May be 0 for bootloader memory or 1 for firmware memory.

Write command message
#####################

Tells the IAP to write data to the flash memory. Each memory area is separated in blocks of 32
bytes. This command writes a single data block. The bootloader can only write to the firmware
memory, while the firmware can only write to the bootloader memory. The IAP answers with a write
response after successful execution.

.. texfigure:: texfigures/write_command_message_definition.tex
    :align: center

:Memory area: Memory area to write to. May be 0 for bootloader memory or 1 for firmware memory.
:Block index: Index of the data block to write, starting with 0 for the data block with the lowest
              memory address.
:Data:        Data block to write.

Write response message
######################

Response sent by the IAP after a successful execution of a write command.

.. texfigure:: texfigures/write_response_message_definition.tex
    :align: center

:Memory area: Memory area which was written to. May be 0 for bootloader memory or 1 for firmware
              memory.
:Block index: Index of the written data block, starting with 0 for the data block with the lowest
              memory address.

Read command message
####################

Tells the IAP to read data from the flash memory. Each memory area is separated in blocks of 32
bytes. This command reads a single data block. The IAP answers with a read response message after
successful execution.

.. texfigure:: texfigures/read_command_message_definition.tex
    :align: center

:Memory area: Memory area to read from. May be 0 for bootloader memory or 1 for firmware memory.
:Block index: Index of data block to read, starting with 0 for the data block with the lowest memory
              address.

Read response message
#####################

Response sent by the IAP after successfully executing a read command.

.. texfigure:: texfigures/read_response_message_definition.tex
    :align: center

:Memory area: Memory area from which was read. May be 0 for bootloader memory or 1 for firmware
              memory.
:Block index: Index of the read data block, starting with 0 for the data block with the lowest
              memory address.
:Data:        Read data block.

Jump command message
####################

Tells the IAP to jump to a certain memory area. The bootloader can only jump to the firmware, while
the firmware can only jump to the bootloader. After reception, the IAP answers with a jump response.
Wait for the state message to check if the jump was successfully executed.

.. texfigure:: texfigures/jump_command_message_definition.tex
    :align: center

:Memory area: Memory area to jump to. May be 0 for bootloader memory or 1 for Firmware memory.

Jump response message
#####################

Response sent by the IAP after receiving a jump command. Wait for the state message to check if the
jump was successfully executed.

.. texfigure:: texfigures/jump_response_message_definition.tex
    :align: center

:Memory area: Memory area to jump to. May be 0 for bootloader memory or 1 for Firmware memory.

Set firmware broken command message
###################################

Tells the IAP to set a flag marking the current firmware as broken or working, depending on the
broken field.

.. texfigure:: texfigures/set_firmware_broken_command_message_definition.tex
    :align: center

:Broken: If != 0, the firmware is marked as broken. If 0 the firmware is marked as working.

Set firmware broken response message
####################################

Response sent by the IAP after a successful execution of a set firmware broken command.

.. texfigure:: texfigures/set_firmware_broken_response_message_definition.tex
    :align: center

:Broken: If != 0, the firmware was marked as broken. If 0 the firmware was marked as working.

Reset command message
#####################

Tells the IAP to reset the microcontroller.

.. texfigure:: texfigures/reset_command_message_definition.tex
    :align: center

Reset response message
######################
Response sent by the IAP after receiving a reset command.

.. texfigure:: texfigures/reset_response_message_definition.tex
    :align: center

Error message
#############

An error message is sent by the IAP instead of a regular response message, if it is not able to
decode a received command message.

.. texfigure:: texfigures/error_message_definition.tex
    :align: center

:Error type: Error number indicating what went wrong. This may be a value from the following table.

+-------+------------------------+
| Value | Error type             |
+=======+========================+
|   0   | Unknown command        |
+-------+------------------------+
|   1   | Invalid command length |
+-------+------------------------+